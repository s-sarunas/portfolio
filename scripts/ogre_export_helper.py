import math

from mathutils import Vector
from bpy_extras.io_utils import axis_conversion

round = "{:.6f}" # all numbers are rounded to 6 d.p. 

# Blender right-handed system: y forward, z up, x right
# Ogre3D right-handed system: -z forward, y up, x right
# x = x, y = z, z = -y
ogre_forward_vec = Vector((0.0, 0.0, -1.0)) # -z
conv_matrix = axis_conversion(
    from_forward='Y', 
    from_up='Z', 
    to_forward='-Z', 
    to_up ='Y').to_4x4()

# performs element-wise multiplication of two Vectors
# @note Vector * Vector does the dot product
def mulVec(vec1, vec2):
    return Vector(x * y for x, y in zip(vec1, vec2))

# recursively counts the number of children in an object list
def countChildren(obj_list, counter=[0]): 
    counter[0] += len(obj_list)
     
    for obj in obj_list:
        if obj.children:
            countChildren(obj.children, counter)
            
    return counter[0]

# formats an iterable as an inline string
def formatInline(iterable):
    result = ""
    for element in iterable:
        result += round.format(element)
        
        if element is not iterable[-1]:
            result += ' '
    
    return result

# gets min/max coordinates of the object's AABB
# use getMinDimension and getMaxDimension to get the respective dimensions
# @note Blender has only OOBBs, no AABBs
def getDimension(ob, comp):
    # create Ogre3D-conforming world space vectors of the bounding box from 
    # float3s
    ob_ogre_matrix_world = conv_matrix * ob.matrix_world
    vertices = [ob_ogre_matrix_world * Vector(f3) for f3 in ob.bound_box]
    
    # apply the given function to each vertex to find the extreme
    extreme = vertices[0]
    for vert in vertices[1:]:
        for index, scalar in enumerate(vert):
            if comp(scalar, extreme[index]):
                extreme[index] = scalar
            
    return extreme
           
def getMinDimension(ob):
    return getDimension(ob, lambda a,b : a < b)
            
def getMaxDimension(ob):
    return getDimension(ob, lambda a,b : a > b)

class ExportError(Exception):
    def __init__(self, message):
        self.message = message

class ExportFile:
    def __init__(self, filepath):
        self.tags = [] # a stack of tags
        self.file = open(filepath, "w")
        
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if self.file.closed is False:
            self.file.close()
        
    def __del__(self):
        if self.file.closed is False:
            self.file.close()
        
    def indent(self):
        for tag in self.tags:
            self.file.write('\t')

    def openTag(self, name):
        if self.tags:
            self.file.write('>\n')
        
        self.indent()
        self.file.write('<' + name) 
        
        self.tags.append(name)
        
    def closeTag(self, inline=False):
        tag_name = self.tags[-1]
        self.tags.pop()
        
        if inline:
            self.file.write('/')
        else:
            self.file.write('>\n')
            
            self.indent()
            self.file.write('</' + tag_name + '')
            
            if len(self.tags) == 0:
                self.file.write('>')
        
    def addAttribute(self, name, value):
        if isinstance(value, float):
            value = round.format(value)
            
        self.file.write(' ' + name + '="' + str(value) + '"')
 
    # @writes common iterables to the output file
    def writeIterable(self, iterable, names):
        for element, name in zip(iterable, names):
            self.addAttribute(name, element)
                
    def __writeVector(self, vector):
        self.writeIterable(vector, ("x", "y", "z"))
            
    def __writeQuaternion(self, quaternion):
        self.writeIterable(quaternion, ("w", "x", "y", "z"))

    def writeColourValue(self, colour):
        self.writeIterable(colour, ("r", "g", "b", "a"))
        
    # @writes Ogre3D-conforming properties of an object to the output file
    def writeVector(self, vector):
        self.__writeVector(conv_matrix * vector)
        
    def writePosition(self, ob):
        self.openTag("position")
        self.__writeVector((conv_matrix * ob.matrix_world).translation)
        self.closeTag(inline=True)
                   
    def writeOrientation(self, ob):
        # @note this does not work on quaternions
        #w_quat = (conv_matrix * ob.matrix_world).to_quaternion()
        
        w_quat = ob.matrix_world.to_quaternion()
        w_quat.z, w_quat.y = w_quat.y, w_quat.z         # swap z with y
        w_quat.z = -w_quat.z                            # negate z
        
        self.openTag("quaternion")
        self.__writeQuaternion(w_quat)
        self.closeTag(inline=True)
               
    def writeScale(self, ob):
        # @note with scale, the z coordinate is not negated
        w_scale = ob.matrix_world.to_scale()    
        w_scale.z, w_scale.y = w_scale.y, w_scale.z     # swap z with y
        
        self.openTag("scale")
        self.__writeVector(w_scale)
        self.closeTag(inline=True)
                   
    def writeDirection(self, ob):
        # direction = rotation quaternion * forward vector (-z in Ogre3D)   
        direction = conv_matrix * (ob.matrix_world.to_quaternion() * ogre_forward_vec)
        
        self.openTag("direction")
        self.__writeVector(direction)
        self.closeTag(inline=True)
        
    def writeColour(self, colour):
        self.openTag("colour")
        self.writeColourValue(colour)
        self.closeTag(inline=True)
        
    # writes position, quaternion or direction and scale data of an object to 
    # the output file 
    def writeTransforms(self, ob, scale=True, pos=True, quat=True, dir=True):
        if pos:
            # <position>
            self.writePosition(ob)

        if quat:  
            # <quaternion>  
            self.writeOrientation(ob)
        elif dir:
            # <direction>
            self.writeDirection(ob)
            
        if scale:  
            # <scale>
            self.writeScale(ob)
