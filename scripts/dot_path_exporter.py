import bpy
import os
import sys
import importlib

from bpy.types import Operator
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty

sys.path.append(os.environ['3DGAME_ROOT'])
import ogre_export_helper

# reload the changes made to the file
importlib.reload(ogre_export_helper)

# add to the global namespace
from ogre_export_helper import *

vertices_left = []
    
# @note assuming only 2 vertices have connections to the vertex_index vertex
def findConnected(vertex_index, edge_list):
    for e in edge_list: # for each edge
        if e.vertices[0] == vertex_index and e.vertices[1] in vertices_left:
            vertices_left.remove(e.vertices[1])
            return e.vertices[1]
        elif e.vertices[1] == vertex_index and e.vertices[0] in vertices_left:
            vertices_left.remove(e.vertices[0])
            return e.vertices[0]
    
    raise ExportError('Vertex with index "' + vertex_index + 
        '" has no connected vertices - the path is discontinuous')
    
def writeVertex(file, vertex):
    file.openTag("vertex")
    file.writeVector(vertex.co)
    file.closeTag(inline=True)
    
    print('Vertex {} was written to the path'.format(vertex.index))
             
def export(context, self, file): 
    active_object = bpy.context.selected_objects[0]
    active_data = active_object.data
    vertex_count = len(active_data.vertices)
   
    for i in range(1, vertex_count):
        vertices_left.append(i)
    
    print('Path vertex count is {}:'.format(vertex_count))
    
    # <vertices>
    file.openTag("vertices")
    file.addAttribute("count", vertex_count)
    
    # write the first vertex
    writeVertex(file, active_data.vertices[0])

    vertex_index = 0
    for i in range(0, vertex_count - 1):
        # find the index of the connected vertex
        vertex_index = findConnected(vertex_index, active_data.edges)
        
        # write that vertex to the file
        writeVertex(file, active_data.vertices[vertex_index]) 

    # </vertices>
    file.closeTag()
    
    if len(vertices_left) == 0:
        print('All vertices have been successfully exported!')
    else:
        print('WARNING: Vertices not exported:')
        for vertex in vertices_left:
            print(vertex)
        
# Export helper contains variable filename, invoke() method
# that calls the file selector and the check() method that checks for overwriting
class FileSelector(Operator, ExportHelper):
    # how the operator is called, i.e. bpy.ops.export.get_filename
    bl_idname = "export.get_filename"  
    # operator's button label
    bl_label = "Export Path" 
    
    # ExportHelper class uses these
    filename_ext = ".path"

    # sets the filter to show only files with a certain extension
    filter_glob = StringProperty(
        default="*.path",
        options={'HIDDEN'})
    
    # call the export function, execute is overridden from the Operator class
    def execute(self, context):
        # clears the Blender's console
        os.system('cls')
        
        selected_objects = bpy.context.selected_objects
        if len(selected_objects) != 1:
            print('ERROR: None or too many objects are selected, terminating...')
        elif selected_objects[0].type != 'MESH':
            print ('ERROR: Selected object is not a mesh, terminating...')
        else:          
            with ExportFile(self.filepath) as file:
                # <path>
                file.openTag("path")
                
                export(context, self, file)
                
                # </path>
                file.closeTag()
       
        return {'FINISHED'}
    
# "__main__" means that this source code is called as the main program
# @note if this source code is being imported by another module, there will be 
# the name of the module instead of "main"
if __name__ == "__main__":
    bpy.utils.register_class(FileSelector)
    bpy.ops.export.get_filename('INVOKE_DEFAULT')