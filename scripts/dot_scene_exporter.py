import bpy
import os
import time
import math
import sys
import importlib

from bpy.types import Operator
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty
from enum import Enum
from idprop.types import IDPropertyArray

sys.path.append(os.environ['3DGAME_ROOT'])
import ogre_export_helper

# reload the changes made to the file
importlib.reload(ogre_export_helper)

# add to the global namespace
from ogre_export_helper import *

objects = bpy.context.selected_objects
default_custom_properties = {
    "type": ("static", str), 
    "delayed": ("false", str), 
    "export_transforms": ("true", str),
    "export_entity": ("true", str),
    "export": ("true", str)
}

reserved_custom_properties = ["_RNA_UI"]
for key, value in default_custom_properties.items():
    reserved_custom_properties.append(key)

# console information output
# @note auto() is not available in python 3.5.1 currently in use, only in 3.6
class Type(Enum):
    ACTOR = 0
    LIGHT = 1
    PARTICLE = 2
    CAMERA = 3
    
def exported(type, obj): 
    if type is Type.ACTOR:
        print("{: <25} {: <10} {: <10}".format('  ' + obj.name, obj.type, obj["type"]))
    else:
        print("{: <25} {: <10}".format('  ' + obj.name, obj.type))

# creates actor-wide and type-specific actor properties
def createCustomProperties(actors):
    for actor in actors:
        # add the actor-wide properties
        for n, p in default_custom_properties.items():
            if n not in actor.keys():
                actor[n] = p[0] # property does not exist
            else:
                # the property already exists 
                # check if the property is of correct type
                if not isinstance(actor[n], p[1]):
                    # if an int or a float is given when a 
                    # string is expected, map the values
                    if (isinstance(actor[n], int) or 
                        isinstance(actor[n], float) and 
                        p[1] == str
                        ):
                        if actor[n] <= 0:
                            actor[n] = "false"
                        elif actor[n] > 0:
                            actor[n] = "true"
                    else:
                        actor[n] = p[0]
                    
        # add type-specific properties
        actor_type = getCustomProperty(actor, "type")
        
        if actor_type == "border":
            if "border_type" not in actor.keys():
                actor["border_type"] = "normal"
        elif actor_type == "region":
            actor["min"] = getMinDimension(actor)
            actor["max"] = getMaxDimension(actor)
        elif actor_type == "butterfly":
            if "path_filename" not in actor.keys():
                actor["path_filename"] = "file.path"
            if "animation_name" not in actor.keys():
                actor["animation_name"] = "my_animation"
        elif actor_type == "terrain":
            if "polysoup_mesh" not in actor.keys():
                actor["polysoup_filename"] = "polysoup_" + actor.name + ".mesh"
            if actor["export_transforms"] == "true":
                print('WARNING: "terrain" actors cannot have transformations '
                    'for the polysoup to work')
                actor["export_transforms"] = "false"
                
        if actor.children:
            createCustomProperties(actor.children)
            
def createCustomSceneProperties(camera):
    if camera:
        if "skybox_name" not in camera.keys():
            camera["skybox_name"] = "unset"

# @returns the custom property from the object, raises and exception otherwise    
def getCustomProperty(obj, name):
    try:
        return obj[name]
    except KeyError as err:    
        raise ExportError('Object "' + obj.name + 
            '" does not have a custom parameter ' + name) from err

# write scene-related settings, some of which are camera's custom properties    
def writeSettings(file, camera):
    # <settings>
    file.openTag("settings")
    
    if not camera:
        print("WARNING: Some scene settings cannot be exported without an active "
            "camera selected")
    else:       
        # <skybox name="">
        file.openTag("skybox")
        file.addAttribute("name", getCustomProperty(camera, "skybox_name"))
        file.closeTag(inline=True)
        
    # <ambient>
    world = bpy.context.scene.world
    if world is None:
        world = bpy.data.worlds.new("World")
        print("WARNING: A world was not found in current context and "
            "was created for you in Properties -> World")
            
    file.openTag("ambient")
    file.writeColourValue(world.ambient_color)
    file.closeTag(inline=True)
    
    file.closeTag()

# scene write functions
def writeActors(file, actors):
    if not actors:
        print('WARNING: No actors were selected for exporting')
    else: 
        # <actors>
        file.openTag("actors")

        file.addAttribute("count", countChildren(actors))
        
        for actor in actors:
            writeActor(file, actor)
                  
        # </actors>
        file.closeTag()  
        
def writeActor(file, actor, isChild=False): 
    actor_type = getCustomProperty(actor, "type")
    
    # <actor name="", type="">
    file.openTag("actor")
    file.addAttribute("name", actor.name)
    file.addAttribute("type", actor_type)
    
    # children cannot be delayed
    if isChild == False:
        file.addAttribute("delayed", getCustomProperty(actor, "delayed"))
    
    if getCustomProperty(actor, "export_transforms") == "true":
        # <position> <quaternion> <scale>
        file.writeTransforms(actor, dir=False)
        
    if getCustomProperty(actor, "export_entity") == "true":
        # <entity mesh=""> 
        file.openTag("entity")
        file.addAttribute("mesh", actor.data.name + '.mesh')  
        file.closeTag(inline=True)             
    
    # export params
    # <params>
    file.openTag("params")
    
    for key in actor.keys():
        if key not in reserved_custom_properties:
            param_value = str(actor[key])

            # find the param type
            if param_value == "true" or param_value == "false":
                param_type = "bool"
            else:
                if isinstance(actor[key], str):
                    param_type = "string"
                elif isinstance(actor[key], float):
                    param_type = "float"
                elif isinstance(actor[key], int):
                    param_type = "int"
                elif isinstance(actor[key], IDPropertyArray):
                    param_type = "vector3"
                    param_value = formatInline(actor[key])
                else:
                    raise ExportError('A custom parameter with type "' + 
                        str(actor_type) + '" cannot be exported, for actor "' + 
                        actor.name + '" parameter name "' + key + '"')
                        
            file.openTag("param")
            file.addAttribute(key, param_value)
            file.addAttribute("type", param_type)
            file.closeTag(inline=True)
        
    # </params>
    file.closeTag()
    
    # export children
    if actor.children:
        # <children>
        file.openTag("children")
        file.addAttribute("count", len(actor.children))
        
        for child in actor.children:
            writeActor(file, child, isChild=True)
       
        # </children>
        file.closeTag()
        
    # </actor>
    file.closeTag()
        
    exported(Type.ACTOR, actor) 
    
# @todo do exporting for a spotlight
# @todo lights lack information about shadows and specular lighting                     
def writeLights(file, lights):
    if not lights:
        print('WARNING: No lights were selected for exporting')
    else:
        # <lights>
        file.openTag("lights")
    
        for light in lights:
            writeLight(file, light)
                
        # </lights>
        file.closeTag()
    
def writeLight(file, light):
    # everything here is done with the object instead of the lamp
    # access lamp information using obj.data (which is lamp.data)
    
    # <light name="" type="" power="">
    if light.data.type == 'SUN':
        light_type = 'DIRECTIONAL'
    else:
        light_type = light.data.type;
        
    file.openTag("light")
    file.addAttribute("name", light.name)
    file.addAttribute("type", light_type)
    file.addAttribute("power", light.data.energy)
            
    # <colour r="" g="" b="">
    file.writeColour(light.data.color)
        
    if light_type == 'POINT':
        # <position>
        file.writeTransforms(light, scale=False, quat=False, dir=False)
        
        # <attenuation>
        file.openTag("attenuation")
        file.addAttribute("range", light.data.distance)
        file.addAttribute("constant", "1.0")
        file.addAttribute("linear", light.data.linear_attenuation)
        file.addAttribute("quadratic", light.data.quadratic_attenuation)
        file.closeTag(inline=True)
    else:
        if light.data.type == 'SUN':
            # <position> <direction>
            file.writeTransforms(light, scale=False, quat=False, dir=True)
        else:
            print('WARNING: Lamp with type: ' + light.data.type + 
                ' was found, that cannot be exported yet')
    
    # </light>
    file.closeTag()
    
    exported(Type.LIGHT, light)

def writeParticles(file, particles):
    if not particles:
        print('WARNING: No particles were selected for exporting')
    else:
        # <particles>
        file.openTag("particles")
        
        for particle in particles:
            for system in particle.particle_systems:
                writeParticle(file, system)
            
                exported(Type.PARTICLE, particle)
                
        # </particles>
        file.closeTag()
        
def writeParticle(file, particle_system):
    # <particle name="" templateName="">
    file.openTag("particle")
    file.addAttribute("name", particle_system.name)
    file.addAttribute("templateName", particle_system.name)
    file.closeTag(inline=True)
    
def writeCamera(file, camera):
    if not camera:
        print('WARNING: No camera was selected for exporting')
    else:
        # <camera>
        file.openTag("camera")

        # <position> <direction>
        file.writeTransforms(camera, scale=False, quat=False, dir=True)

        # </camera>
        file.closeTag()
        
        exported(Type.CAMERA, camera)
                            
def export(context, self, file):   
    # forward declarations to avoid scope issues
    lights = []
    particles = []
    camera = None 
    regions = []
    actors = []
    for obj in objects:
        # if not in an object group (not static geometry)  
        if not obj.users_group: 
            # add all object types to appropriate lists for further processing
            if obj.type == "LAMP":
                lights.append(obj)
            elif obj.type == "CAMERA": 
                # only a single camera is supported
                camera = obj
            elif obj.type == "MESH":
                if obj.parent == None or obj.parent.type == "ARMATURE":
                    if getCustomProperty(obj, "export") == "true":
                        actors.append(obj)
                        
                        # particle systems have to be attached 
                        # to other objects as a base for emission
                        if obj.particle_systems:                     
                            particles.append(obj)  
            elif obj.type is not "ARMATURE":
                print('WARNING: Object with type: ' + obj.type + 
                    ' cannot be exported yet.') 
             
    # print out the exported scene objects into the console via exported()                  
    print('Exported objects: ')   
           
    # <settings>     
    createCustomSceneProperties(camera)           
    writeSettings(file, camera)

    # <actors>
    createCustomProperties(actors)   
    writeActors(file, actors) 
    
    # <lights> 
    writeLights(file, lights)
    
    # <particles> 
    writeParticles(file, particles)
    
    # <camera> 
    writeCamera(file, camera)

def export_static(context, self, file):   
    # print out the individual batches into the console
    print('\nStatic geometry batches: ')
    
    # <staticgeometry>
    file.openTag("staticgeometry")

    scene = bpy.context.scene
    for group in bpy.data.groups:
        if len(group.objects) > 0 and group.objects[0].is_visible(scene):           
            # <batch>
            file.openTag("batch")
            file.addAttribute("mesh", group.name + '.mesh')
            
            for j in range(0, len(group.objects)):
                object = group.objects[j]
                
                print('  ', object.name, '\t Batch:', group.name)

                if object.type == "MESH":
                    # <entity>
                    file.openTag("entity")
                    
                    # <position> <quaternion> <scale>
                    file.writeTransforms(object, dir=False)
                    
                    # </entity>
                    file.closeTag()
                else:
                    print('WARNING: One of the batched objects is not of type ' 
                          '"MESH" and was skipped');
            
            # </batch>
            file.closeTag()
    
    # </staticgeometry>
    file.closeTag()       

# Export helper contains variable filename, invoke() method
# that calls the file selector and the check() method that checks for overwriting
class FileSelector(Operator, ExportHelper):
    # how the operator is called, i.e. bpy.ops.export.get_filename
    bl_idname = "export.get_filename"  
    # operator's button label
    bl_label = "Export"
    
    # ExportHelper class uses these
    filename_ext = ".scene"

    # sets the filter to show only files with a certain extension
    filter_glob = StringProperty(
        default="*.scene",
        options={'HIDDEN'})
            
    static_geometry = bpy.props.BoolProperty(
        name="Static Geometry",
        default=True,
        description="Export as batches")
    
    # call the export function, execute is overridden from the Operator class
    def execute(self, context):
        # clears the console
        os.system('cls')

        if objects:
            # sets the the start time of the execution
            time_start = time.time()
            
            with ExportFile(self.filepath) as file:
                # <scene>
                file.openTag("scene")
                
                # export the <scene>
                export(context, self, file)
            
                # export the <staticgeometry>
                if self.static_geometry:
                    export_static(context, self, file)
                    
                # </scene>
                file.closeTag()
                
            print('\nWriting finshed in: %.4f sec' % (time.time() - time_start))
        else:
            print('ERROR: No objects are selected, terminating...')
            
        return {'FINISHED'}
    
# "__main__" means that this source code is called as the main program
# @note if this source code is being imported by another module, this will be the name
# of the module instead of "main"
if __name__ == "__main__":
    bpy.utils.register_class(FileSelector)
    bpy.ops.export.get_filename('INVOKE_DEFAULT')
    