// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

float4x4 worldViewProj;
float4x4 worldMatrix;

struct VS_INPUT // a2v - application to vertex shader
{ 
	float4 position : POSITION;
	float2 texCoord : TEXCOORD0;
	float3 normal : NORMAL;
};

struct VS_OUTPUT // v2p - vertex shader to pixel shader
{ 
	float4 position : POSITION;
	float2 texCoord : TEXCOORD0;
	float3 normal : TEXCOORD1;
	float3 worldPos : TEXCOORD2;
};
 
VS_OUTPUT main(VS_INPUT input)
{
    VS_OUTPUT output;

	// transform position from object to projection space
    output.position = mul(worldViewProj, input.position); 

	// copy the texture coordinate through
	output.texCoord = input.texCoord;

	// transform normal from object to world space
	output.normal = normalize(mul((float3x3)worldMatrix, input.normal));

	// calculate the world position of the pixel (D3D automatically 
	// interpolates across the triangle from vertex position to pixel 
	// position when it is in pixel shader)
	output.worldPos = mul(worldMatrix, input.position).xyz;

    return output;
}