// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

uniform sampler2D rt0 : register(s0);
 
// the float4 that is returned is of type COLOR0 (same can be done with 
// PS_INPUT and PS_OUTPUT data structures)
float4 main(float2 texCoord : TEXCOORD0) : COLOR0 
{
    float4 color = tex2D(rt0, texCoord);
 
    float value = (color.r + color.g + color.b) / 3; 
    color.r = value;
    color.g = value;
    color.b = value;
 
    return color;
}