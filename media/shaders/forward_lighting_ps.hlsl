// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "forward_lighting.hlsli"

uniform float3 cameraPosition;
uniform float4 diffuseColor;
uniform float specularExponent;
uniform float3 specularColor;
uniform float4 surfaceAmbient;
uniform float4 surfaceEmissive;
uniform float3 sceneAmbient;
// directional lights - direction in the form (-x, -y, -z)
// point lights - position in the form (x, y, z)
uniform float4 lightPosition[LIGHTS_MAX]; 
uniform float3 lightDiffuse[LIGHTS_MAX];
uniform float lightPower[LIGHTS_MAX];
// x = range
// y = constant attenuation
// z = linear attenuation
// w = quadratic attenuation
uniform float4 lightAttenuation[LIGHTS_MAX]; 
uniform float4 lightTypes;

#if NUM_TEXTURES != 0
uniform Texture2D textures[NUM_TEXTURES];
#endif

SamplerState Sampler2D
{
	Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

struct VS_OUTPUT // v2p - vertex shader to pixel shader
{ 
	float2 position : VPOS;
	float2 texCoord : TEXCOORD0;
	float3 normal : TEXCOORD1;
	float3 worldPos : TEXCOORD2;
};

// the float4 that is returned is of type COLOR0 (same can be done with PS_INPUT 
// and PS_OUTPUT data structures)
float4 main(VS_OUTPUT input) : SV_TARGET
{
	// calculate the specular intensity
	float specularIntensity = 
		(specularColor.r + specularColor.g + specularColor.b) / 3;

	float4 pixelBase;
#if NUM_TEXTURES == 0
	// use the plain diffuse color as the base
	pixelBase = diffuseColor; 
#elif NUM_TEXTURES == 1
	// sample the base colour from the texture
	pixelBase = textures[0].Sample(Sampler2D, input.texCoord); 
	pixelBase *= diffuseColor;
#elif NUM_TEXTURES == 2
	// alpha blend two textures
	pixelBase = alphaBlend(textures[0], textures[1], Sampler2D, input.texCoord);
	pixelBase *= diffuseColor;
#endif

	// calculate the emissive light
	float3 finalColor = pixelBase.rgb * surfaceEmissive.rgb;

	// calculate the hemispheric ambient light
	finalColor += calculateAmbient(input.normal, surfaceAmbient.rgb, 
		sceneAmbient, pixelBase);

	// Ogre3D does not support custom int/uint parameters for separate renderables
	float unpackedLightTypes[LIGHTS_MAX] = 
		{lightTypes.x, lightTypes.y, lightTypes.z, lightTypes.w};

	for (uint i = 0; i < LIGHTS_MAX; ++i)
	{
		if (unpackedLightTypes[i] == 0)
		{
			finalColor += calculatePointLight(
				lightPosition[i], lightAttenuation[i], cameraPosition, 
				lightDiffuse[i], lightPower[i], input.worldPos, specularExponent, 
				input.normal, specularIntensity, pixelBase);
		}
		else if (unpackedLightTypes[i] == 1.0)
		{
			finalColor += calculateDirectionalLight(lightPosition[i], cameraPosition, 
				lightDiffuse[i], lightPower[i], input.worldPos, specularExponent, 
				input.normal, specularIntensity, pixelBase);
		}
		else if (unpackedLightTypes[i] == -1)
			break;
	}

	// convert the color to linear space (from gamma space) by squaring it 
	// (should be raised to the power of 2.2 to be precise)
	//float4 linearColor = float4(finalColor.rgb * finalColor.rgb, pixelBase.a);

	// return the compound pixel colour
	return float4(finalColor.rgb, pixelBase.a);
}