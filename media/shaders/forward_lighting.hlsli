// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

float4 alphaBlend(
	Texture2D tex1, 
	Texture2D tex2, 
	SamplerState samplerType, 
	float2 texCoords
	)
{
	float4 color0 = tex1.Sample(samplerType, texCoords); 
	float4 color1 = tex2.Sample(samplerType, texCoords);

	// alpha blending
	return color0 * (1.0 - color1.a) + color1;
}

float3 calculateAmbient(
	float3 pixelNormal, 
	float3 upColor,			// object's surface ambient colour
	float3 downColor,		// scene's ambient colour
	float4 baseColor
	)
{
	// HEMISPHERIC AMBIENT LIGHT

	// convert from [-1, 1] to [0, 1]
	float up = pixelNormal.y * 0.5 + 0.5;

	float3 ambientRange = (upColor - downColor);

	// return the ambient value
	return (downColor + up * ambientRange) * baseColor.rgb;
}

float3 phongBlinn(
	float3 toLight, 
	float3 cameraPosition,
	float3 lightDiffuse,
	float lightPower,
	float3 pixelWorldPosition, 
	float specularExponent, 
	float3 pixelNormal, 
	float specularIntensity
	)
{
	// PHONG DIFFUSE
	float NdotL = saturate(dot(toLight, pixelNormal));
	float3 color = lightDiffuse.rgb * lightPower * NdotL;

	// BLINN SPECULAR
	float3 toEye = normalize(cameraPosition.xyz - pixelWorldPosition);
	float3 halfWay = normalize(toEye + toLight);
	float NdotH = saturate(dot(halfWay, pixelNormal));

	color += lightDiffuse.rgb * lightPower * pow(NdotH, specularExponent) * 
		specularIntensity;

	return color;
}

float3 calculatePointLight(
	float4 lightPosition,
	float4 lightAttenuation,
	float3 cameraPosition,
	float3 lightDiffuse,
	float lightPower,
	float3 pixelWorldPosition, 
	float specularExponent, 
	float3 pixelNormal, 
	float specularIntensity, 
	float4 color
	)
{
	float3 toLight = lightPosition.xyz - pixelWorldPosition;
	float distToLight = length(toLight);
	toLight /= distToLight; // normalize

	// SQUARED ATTENUATION
	// lightAttenuation.range should be stored as a reciprocal and then 
	// multiplied instead of dividing (multiplication is faster)
	//float distToLightNorm = 1.0 - saturate(distToLight /* * */ / lightAttenuation.x);	
	//float atten = distToLightNorm * distToLightNorm;

	// LINEAR/QUADRATIC WEIGHTED ATTENUATION
	// intensity = light power * (1 / (Qr^2 + Lr + C))
	float r = distToLight;
	float D = lightAttenuation.x;
	float C = lightAttenuation.y;
	float L = lightAttenuation.z;
	float Q = lightAttenuation.w;

	float atten = lightPower * (D / (D + L * r)) * (D*D / (D*D + Q * r*r));

	// RESTRICTING TO RANGE
	float restricted_atten = 0;
	if (r < D)
		restricted_atten = atten * (D - r) / D;
	
	float3 finalColor = phongBlinn(toLight, cameraPosition, lightDiffuse, 
		lightPower, pixelWorldPosition, specularExponent, pixelNormal, 
		specularIntensity);

	// NOTE: does not take pixel's current colour into consideration (replaces 
	// the surface colour)
	return finalColor /** color.rgb*/ * restricted_atten;
}

float3 calculateDirectionalLight(
	float4 toLight, 
	float3 cameraPosition,
	float3 lightDiffuse,
	float lightPower,
	float3 pixelWorldPosition, 
	float specularExponent, 
	float3 pixelNormal, 
	float specularIntensity,
	float4 color
	)
{
	float3 finalColor = phongBlinn(toLight.xyz, cameraPosition, lightDiffuse, 
		lightPower, pixelWorldPosition, specularExponent, pixelNormal, 
		specularIntensity);

	return finalColor * color.rgb;
}