// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

float4x4 worldViewProj;

struct VS_OUTPUT
{
   float4 position : POSITION0;
   float2 texCoord : TEXCOORD0;
};

struct VS_INPUT
{
	float4 position : POSITION0;
	float2 texCoord : TEXCOORD0;
};
 
VS_OUTPUT main(VS_INPUT input)
{
    VS_OUTPUT output;

    output.position = mul(worldViewProj, input.position);
	output.texCoord = input.texCoord;

    return output;
}