// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "AppState.h"

AppState::AppState(Listener* listener, const Ogre::String& menuName, 
	const Ogre::String& overlayName)
  : mListener(listener),
	mSimpleMenu(nullptr),
	mOverlay(nullptr)
{
	if (!overlayName.empty())
	{
		mOverlay = Ogre::OverlayManager::getSingleton().getByName(overlayName);
		mOverlay->show();
	}

	if (!menuName.empty())
	{
		mSimpleMenu = std::make_unique<SimpleMenu>(menuName);
		mSimpleMenu->setListener(this);
	}
}

AppState::~AppState()
{
}

void AppState::show()
{
	if (mOverlay)
		mOverlay->show();

	if (mSimpleMenu)
		mSimpleMenu->show();
}

void AppState::hide()
{
	if (mOverlay)
		mOverlay->hide();

	if (mSimpleMenu)
		mSimpleMenu->hide();
}

void AppState::start()
{
	show();

	mResult.reset();

	SDL_SetRelativeMouseMode(SDL_FALSE);
}

void AppState::end(const AppResultType::value_type result) 
{ 	
	hide();

	mResult = result;

	SDL_SetRelativeMouseMode(SDL_TRUE);
}

void AppState::notifyListener(const MenuEvent menuEvent, 
	const StringInterface* params)
{
	assert(mListener && 
		"listener is no longer valid");

	mListener->handleMenuEvents(menuEvent, params);
}

void AppState::notifyListener(Config::Property&& prop)
{
	assert(mListener && 
		"listener is no longer valid");

	mListener->handleMenuEvents(std::move(prop));
}

// InputHandler.h
bool AppState::mouseMoved(const SDL_MouseMotionEvent& arg)   
{
	if (mSimpleMenu)
		mSimpleMenu->mouseMoved(arg);

	return true;
}

// InputHandler.h
bool AppState::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	if (mSimpleMenu)
		mSimpleMenu->mousePressed(arg, id);

	return true;
}

// InputHandler.h
bool AppState::mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	if (mSimpleMenu)
		mSimpleMenu->mouseReleased(arg, id);

	return true;
}

// InputHandler.h
bool AppState::keyPressed(const SDL_KeyboardEvent& arg)
{
	if (mSimpleMenu)
		mSimpleMenu->keyPressed(arg);

	return true;
}

// InputHandler.h
bool AppState::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
{
	if (mSimpleMenu)
		mSimpleMenu->joyButtonPressed(evt, button);

	return true;
}

// InputHandler.h
bool AppState::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	if (mSimpleMenu)
		mSimpleMenu->joyButtonReleased(evt, button);

	return true;
}

// InputHandler.h
bool AppState::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
{
	if (mSimpleMenu)
		mSimpleMenu->joyPovMoved(arg, index);

	return true;
}