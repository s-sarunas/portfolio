// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "CollisionManager.h"

#include "InAppDebug.h"
#include "Collidable.h"

CollisionManager* CollisionManager::msSingleton = nullptr;

CollisionManager::CollisionManager()
  : mDebugOutput(false)
{
	msSingleton = this;
}

CollisionManager::~CollisionManager()
{
	msSingleton = nullptr;
}

CollisionManager* CollisionManager::getSingletonPtr() NOEXCEPT
{
	return msSingleton;
}

CollisionManager& CollisionManager::getSingleton()
{
	assert(msSingleton);
	return (*msSingleton);
}

void CollisionManager::notifyCollisionOccured(Collidable* one, Collidable* two)
{
	for (const auto& listener : mListeners)
		listener->collisionOccured(one, two);
}

void CollisionManager::registerCollisions(Collidable* one, Collidable* two)
{
	if (one != two && // not the same collidable
		!one->isCollisionMasked(two->getTypeFlag()) && // not masked
		one->getParent() != two) // not parent
	{
		if (one->getBoundingBox().intersects(two->getBoundingBox())) 
		{ // if intersects
			one->addCollision(two); // register as a collision
		}
	}
}

void CollisionManager::processCollision(Collidable* one, Collidable* two)
{
	if (mDebugOutput)
	{
		DEBUG_WRITELN("A collision occured between: " + one->getName() + 
			" and " + two->getName());
	}

	// notify both collidables of the collision
	one->handleCollision(two, true); 
	two->handleCollision(one, false); 

	if (one->getVisibleToListeners())
	{ // notify the listeners
		notifyCollisionOccured(one, two); 
		notifyCollisionOccured(two, one); 
	}
}

void CollisionManager::handleCollisions()
{
	updatePendingCollidables();

	// for each collidable held by the collision manager
	for (const auto& collidable : mCollidables)
	{
		if (collidable->getActorGroup() == ACTOR_DYNAMIC_GROUP_NAME && 
			collidable->_isUpdatePending()) 
		{ // only if dynamic and needs update
			handleOneCollision(collidable);

			collidable->_updated();
		}
	}
}

void CollisionManager::handleOneCollision(Collidable* one)
{
	// clear and register all collisions with the other collidables
	one->clearCollisions();

	// generate a list of collisions between this actor and all other collidables
	std::for_each(mCollidables.begin(), mCollidables.end(), 
		std::bind(&CollisionManager::registerCollisions, this, one, std::placeholders::_1));

	// process all the collisions
	if (one->hasCollisions())
	{ // at least one collision is present
		for (auto& pair : one->getCollisions())
		{
			if (pair.second == true)
				processCollision(one, pair.first);
			
		}
	}
}

void CollisionManager::registerCollidable(Collidable* collidable)
{
	// defer removal not to invalidate iterators
	mPendingCollidables.emplace_back(collidable, true);

	DEBUG_OUTPUT(LVL_GREEN, "Collidable with name \"" + collidable->getName() + 
		"\" marked for registration for collision detection");
}

void CollisionManager::removeCollidable(Collidable* collidable)
{
	// defer removal not to invalidate iterators
	mPendingCollidables.emplace_back(collidable, false);

	DEBUG_OUTPUT(LVL_GREEN, "Collidable with name \"" + collidable->getName() +
		"\" marked for removal from collision detection");
}

void CollisionManager::updatePendingCollidables()
{
	if (!mPendingCollidables.empty())
	{
		for (const auto& pair : mPendingCollidables)
		{
			if (pair.second)
				mCollidables.push_back(static_cast<Collidable*>(pair.first));
			else
				mCollidables.erase(
					std::remove(mCollidables.begin(), mCollidables.end(), pair.first), 
					mCollidables.end());
		}

		mPendingCollidables.clear();
	}
}
