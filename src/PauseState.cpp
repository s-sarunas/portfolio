// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "PauseState.h"

#include "SimpleMenu.h"
#include "Button.h"

// SimpleMenu::Listener
void PauseState::buttonHit(const Button* button) 
{
	const Ogre::String& name = button->getName();
	if (name == "resume")
		notifyListener(MENU_EVENT_RETURN_TO_GAME);
	else if (name == "menu")
		notifyListener(MENU_EVENT_RETURN_TO_MENU);
	else if (name == "quit")
		notifyListener(MENU_EVENT_QUIT);

	end(button->getIndex());
}

PauseState::PauseState(AppState::Listener* listener)
  : AppState(listener, "PauseMenu")
{
	getSimpleMenu()->createButtons(
		{"resume", "settings", "menu", "quit"},
		{"Resume", "Settings", "Menu", "Desktop"});
}

PauseState::~PauseState()
{
}