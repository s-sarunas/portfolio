// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "MenuState.h"

#include "SimpleMenu.h"
#include "Button.h"

// SimpleMenu::Listener
void MenuState::buttonHit(const Button* button) 
{
	Widget::size_type index = button->getIndex();
	if (index < 2)
	{
		notifyListener(Config::make_property("game_mode", 
			index, PARAMETER_TYPE_UNSIGNED_INT));
	}
	else
	{
		if (button->getName() == "quit")
			notifyListener(MENU_EVENT_QUIT);
	}

	end(index);
}

MenuState::MenuState(AppState::Listener* listener)
  : AppState(listener, "MainMenu")
{
	getSimpleMenu()->createButtons(
		{"freeplay", "challenge", "settings", "quit"}, 
		{"Freeplay", "Challenge", "Settings", "Quit"});
}

MenuState::~MenuState()
{
}