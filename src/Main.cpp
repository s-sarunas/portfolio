// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include <string>

#define NOMINMAX
#include "windows.h"
#include "psapi.h"

#include "rapidxml.hpp"

#include "GamePrerequisites.h"
#include "Core.h"
#include "Config.h"

Ogre::SceneManager* gsSceneManager = nullptr;
Ogre::Camera* gsCamera = nullptr;
Config gsConfig;
GameModes gsGameMode;

NODISCARD Ogre::SceneManager* const getSceneManager() 
{ 
	assert(gsSceneManager); 
	return gsSceneManager;
}
NODISCARD Ogre::Camera* const getCamera() 
{ 
	assert(gsCamera); 
	return gsCamera;
}
NODISCARD Config& getConfig() NOEXCEPT 
{ 
	return gsConfig; 
}
NODISCARD const GameModes getGameMode() NOEXCEPT 
{ 
	return gsGameMode; 
}

void setSceneManager(Ogre::SceneManager* manager) NOEXCEPT 
{
	gsSceneManager = manager;
}
void setCamera(Ogre::Camera* camera) NOEXCEPT 
{ 
	gsCamera = camera; 
}
void setGameMode(const GameModes mode) NOEXCEPT 
{ 
	gsGameMode = mode; 
}

/*
	
	Compiler used: MSVC 2013 18.00.40629 for x86

	Dependencies:
		Graphics engine: Ogre3D v1.9
		Input/window library: SDL2 v2.0.8
		Boost C++ libraries: v1.61.0

////////////////////////////////////////////////////////////////////////////////
// PROFILING
////////////////////////////////////////////////////////////////////////////////
* Profiling of DebugConsole (debug build):
	Output of a 100 const char* raw strings:
		passing to the DebugConsole via DEBUG_WRITE, the main thread takes:
			320 microseconds (avg of 3, 3.2us per call)
		output via cout on the main thread takes:
			5756 microseconds (avg of 3, 57.56us per call)

* Profiling of DebugConsole using a lock-free concurrent linked list using CAS - 
  boost's single-consumer/single-producer (boost::lockfree::spsc_queue):
	 Reduces performance by 25%

////////////////////////////////////////////////////////////////////////////////
// KNOWN BUGS
////////////////////////////////////////////////////////////////////////////////
Major source of bugs:
	A lack of copy semantics for Ogre interfaces: the workaround of copying using 
	the public interface creates discrepancies between copied objects (thus only 
	Actor, DynamicActor and Ball have them).
	
	A lack of collision detection middleware: 
		The exact point of collision cannot be found.

		At high speeds an object (especially the ball) suffers from	tunneling 
		and can fly through another object without collision etc. 

		The collidables are not spacially partitioned and not pre-tested with 
		cheaper tests, since AABB is used throughout.

		Only AABB and Sphere are supported by Ogre.

Fullscreen option doesn't work since the switch to SDL2 for window management and
input (a63a248), fullscreen is always set to false in Core::initializeSDL().
Results in lower frame rates, since OS is being updated in the background.

When stepping through frames in debug mode, sometimes renderOneFrame in the main
game loop takes a long time creating a large update delta.

////////////////////////////////////////////////////////////////////////////////
// CLARIFICATIONS AND TECHNICAL IMPROVEMENTS
////////////////////////////////////////////////////////////////////////////////
Allocators for actor collection maps and lists would be beneficial to allocate 
big chunks of static memory (and append dynamic memory if needed) based on the 
number of actors in the .scene file, especially in bigger scenes.

Using 2 byte integers (uint16_t) rarely saves space because the extra 2 bytes are
usually taken up by padding instead, unless another variable takes up the
remaining bytes to fill in the word.

There is no support for spotlights, no resource management, spatial
partitioning, LOD, shadows etc.

Some features like phong lighting and alpha blending are available in Ogre and 
were only reproduced for learning purposes.

Move semantics:
	Defined even if implicitly generated versions would suffice (implicit 
	generation by compiler is not supported on MSVC 2013).

	There is no need to set the rhs pointers to nullptr as they're not owning.

Math.h:
	Polynomial definitions should use expression templates (such as boost::proto) 
	to provide compile-time evaluation of mathematical functions. Especially 
	useful in finding the asymptotic speed of the ball.

	Spline interpolation used for the butterfly's movement is not meant to be used 
	in 3D space, NURBS or Bezier curves would've been more appropriate.

	In rayTriangleIntersection, edge1 and edge2 could be precalculated and reused.

Actor.h:
	Ogre::Entity in Actor class could be replaced by Ogre::MovableObject and then 
	used for lights, camera, particles etc (since everything needs a scene node).

	Calling virtual functions createActor and createUnmanaged for each Actor is 
	slow compared to a non-virtual alternative.

	Could've used std::shared_ptr deleter to manage actors, however there are 
	some memory concerns surrounding deleters for std::shared_ptr.

Collision detection:
	If an object A collided with an object B, there should be no need to check for 
	collisions B vs A.

Exception handling:
	Non-trivial virtual functions are assumed to throw and cout output operations
	are assumed not to throw, since cout.exceptions() is not used.

	Try-catch blocks in constructors are unnecessary, only used for educational
	purposes

DebugConsole.h:
	Should use a stream for output: to allow stream flags/manipulators or 
	define its own and have less bloating of the general template operator<< in 
	DebugConsole in larger applications.

	Object cout should not be used, everything should go through the DebugConsole, 
	be noexcept and error-tested.

	Does not support two lines of input and should be done as part of the GUI 
	to avoid using platform-specific console manipulation and hackery.

DebugTable.h:
	Does not support pointer to char, since char* is a null-terminated raw string, 
	therefore no function overload exists for both types.

There should be no need for pre-compiled headers in the final version of the 
application.

////////////////////////////////////////////////////////////////////////////////
// C++17 POSSIBLE NOTED CHANGES
////////////////////////////////////////////////////////////////////////////////
All static variables should be defined in headers, so that their definitions 
could be ordered by #includes, since linker processes translation units in no 
particular order (only a problem in constructors/destructors, especially with 
libraries). C++17 variable inlining can be used to avoid this so that the point 
of instantiation is known.

estd::is_any could use fold expressions or std::disjunction for a sequence of 
type traits.

Two std::maps could be properly merged with nodes in StringInterface.

std::invoke could be used in DebugConsole to avoid explicit binding.

noexcept with std::is_nothrow_invocable could be used in AbilitiesManager, Actor,
Scene and GamePrerequisites as part of the NOEXCEPT_COND macro.

estd::passable_by_value should use std::is_invocable instead of std::is_function.

Could use:
	std::valarray instead of boost::multi_array for matrices.
	std::variant instead of boost::variant.
	std::string_view::remove_prefix, remove_suffix, starts_with, ends_with
	instead of estd::get_prefix, get_suffix, has_prefix, has_suffix

In many places:
	constexpr for compile-time evaluation.
	[first, second] syntax in range-for with std::pair.

estd::clamp and estd::insert_or_assign could be replaced by the STL versions of 
the functions.

NODISCARD macro could be [[nodiscard]] instead of being empty in GamePrerequisites.

Additional braces could be removed in DebugConsole.

////////////////////////////////////////////////////////////////////////////////
// OTHER NOTED IMPROVEMENTS
////////////////////////////////////////////////////////////////////////////////
GUI is only a placeholder, should use a middleware that generates it fully from
a file and not requires any changes to the program code. All captions should be 
replaced using regular expressions based on the locale the user uses.

Challenge borders should have several shape animations to concave/convex in the 
up axis and shrink/extend to match the outline of the ground and the width of 
the playable area or have a separate mesh for each player.

The paddle could move in both, x and y directions in a predefined region if 
the collision detection was proper.

There should be separate regions for each of the border, instead of using the 
border geometry's min and max positions.

Grounded mixin should interpolate between two face normals when rotating an 
actor to match the slope of the ground.

Binary search is fast enough that DebugConsole could've easily used a single 
operand.

Strings could be joined in python scripts before writing to improve performance.

Text in the scoreboards should be scaled to fit.

Square textures of powers of 2 should be used everywhere.
*/

#define EXCEPTION_MESSAGE_BOX(message) MessageBox(NULL, message, \
	"An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);

struct memory_punct: std::numpunct<char>
{
	char_type do_thousands_sep() const { return ','; };
	char_type do_decimal_point() const { return '.'; };
	string_type do_grouping() const { return "\03"; };
	string_type do_truename() const { return "true"; };
	string_type do_falsename() const { return "false"; };
};

std::string formatMemory(const size_t bytes)
{
	// working code to add commas as thousands separators
	/*std::string s = std::to_string(bytes / 1024);

	for (unsigned int i = 3; i < s.length(); i = i + 3)
	{
		if (s.length() > i)
		{
			s.insert(s.length() - i, ",");
			++i;
		}
	}*/

	/*	
		A .png of ASCII characters is used, therefore this cannot work with any 
		implementation defined locale, since there might be no graphical
		representation for the punctuation characters used (e.g. there is no 
		direct graphical representation for no-break space - &nbsp - used as 
		thousands_sep() in the French locale)
	*/
	// using locales with overidden numpunct<char> facet
	// copy the C locale and use memory_punct facet with it
	std::locale loc(std::locale::classic(), new memory_punct()); 
	// use output string stream to convert from size_t to string
	std::ostringstream oss; 
	oss.imbue(loc);
	oss << (bytes / 1024);

	return oss.str() + " KB";
}

std::string getMemoryUsage() 
{
#if defined(_WIN32)
	PROCESS_MEMORY_COUNTERS info;

	GetProcessMemoryInfo(GetCurrentProcess(), &info, sizeof(info));

	return formatMemory(info.WorkingSetSize);
#else
	return Ogre::StringUtil::BLANK;
#endif
}

std::vector<std::string> parseCmdLine(LPSTR strCmdLine)
{
	return estd::split(strCmdLine, " ");
}

INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
try {
	Core application;
	application._go(parseCmdLine(strCmdLine));
}
catch (const Ogre::Exception& e) {
	EXCEPTION_MESSAGE_BOX(e.getFullDescription().c_str());
}
catch (const rapidxml::parse_error& e) {
	EXCEPTION_MESSAGE_BOX((std::string("Uncaught RapidXML exception: ") + 
		e.what()).c_str());
}
catch (const std::exception& e) {
	EXCEPTION_MESSAGE_BOX((std::string("Uncaught STL exception: ") + 
		e.what()).c_str());
}
catch (...) {
	EXCEPTION_MESSAGE_BOX("Uncaught exception!");
}