// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "SettingsState.h"

#include "SimpleMenu.h"
#include "Button.h"
#include "Dropdown.h"
#include "AI.h"

// SimpleMenu::Listener
void SettingsState::itemSelected(const Dropdown* dropdown)
{
	const Ogre::String& name = dropdown->getName();
	if (name == "paddle_character")
	{
		Ogre::String value;
		switch (dropdown->getValue())
		{
		case 0: 
			value = "fox.mesh";
			break;
		default:
			break;
		}

		notifyListener(Config::make_property(name, 
			value, PARAMETER_TYPE_STRING));
	}
	else if (name == "ai_difficulty")
	{
		notifyListener(Config::make_property(name, 
			dropdown->getValue(), PARAMETER_TYPE_UNSIGNED_INT));
	}
}

// SimpleMenu::Listener
void SettingsState::buttonHit(const Button* button) 
{
	end(NULL);
}

SettingsState::SettingsState(AppState::Listener* listener)
  : AppState(listener, "SettingsMenu")
{
	SimpleMenu* menu = getSimpleMenu();
	menu->createDropdown("paddle_character", "Character", 
		{"Fox"}, NULL);
	menu->createDropdown("ai_difficulty", "Difficulty", 
		{"Easy", "Medium", "Hard"}, AI::DIFFICULTY_MEDIUM);

	menu->createButton("back", "Back");
}

SettingsState::~SettingsState()
{
}