// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "DebugTable.h"

#include "InAppDebug.h"
#include "Scene.h"

Ogre::String DebugTable::msDefaultValue = "ERROR";

class to_string_visitor: public boost::static_visitor<Ogre::String>
{
public:
	Ogre::String operator()(const Ogre::String& arg) const NOEXCEPT { return arg; } // copies
	Ogre::String operator()(const Ogre::String* arg) const { return *arg; } // copies

	Ogre::String operator()(const bool* arg) const
	{
		return Ogre::StringConverter::toString(*arg) + 
			'(' + Ogre::StringConverter::toString(static_cast<int>(*arg)) + ')';
	}

	// creates a string and moves
	template <typename Ty>
	Ogre::String operator()(const Ty* arg) const 
	{ 
		return Ogre::StringConverter::toString(*arg); 
	} 

	// moves from the function and moves again here
	Ogre::String operator()(const std::function<Ogre::String()>& arg) const
	{
		return arg(); 
	}
};

DebugTable::DebugTable(const Ogre::RenderWindow* window)
  : mOverlayManager(Ogre::OverlayManager::getSingletonPtr()),
	mOverlay(mOverlayManager->getByName(DEBUG_TABLE_OVERLAY_NAME)),
	mContainer(mOverlay->getChild(DEBUG_TABLE_OVERLAY_CONTAINER_NAME)),
	mCaptionWidth(NULL),
	//mTableWidth(mContainer->getWidth()),
	mNumPersistentRows(NULL)
{
	// populate the table with the persistent table rows that do no depend on 
	// the scene - accepts 3 types of arguments: Ogre::String, std::function 
	// returning Ogre::String or a pointer to a type convertible 
	// by Ogre::StringConverter::toString()
	addRow("FPS", 
		[window]() { 
			return Ogre::StringConverter::toString(window->getLastFPS()); 
		}
	);
	addRow("batch_count", 
		[window]() { 
			return Ogre::StringConverter::toString(window->getBatchCount()); 
		}
	);
	addRow("triangle_count", 
		[window]() { 
			return Ogre::StringConverter::toString(window->getTriangleCount()); 
		}
	);
	addRow("memory_total", &getMemoryUsage);
	addRow("asset_memory", 
		[]() { 
			size_t assetMemoryUsage = 0;

			auto resManagerIt = Ogre::ResourceGroupManager::getSingleton().
				getResourceManagerIterator();

			for (auto it = resManagerIt.begin(); it != resManagerIt.end(); ++it)
			{
				assetMemoryUsage += it->second->getMemoryUsage();
			}

			return formatMemory(assetMemoryUsage);
		}
	);
	addRow("num_nodes", 
		[]() {
			InAppDebug* inAppDebug = InAppDebug::getSingletonPtr();

			return Ogre::StringConverter::toString(
				inAppDebug->calcNumNodes(getSceneManager()->getRootSceneNode())) +
				'(' + Ogre::StringConverter::toString(
				inAppDebug->calcNumNodes(inAppDebug->getRootSceneNode())) + ')';
		}
	);
	addRow("num_vs|gp|ps", 
		[]() {
			InAppDebug* inAppDebug = InAppDebug::getSingletonPtr();

			Ogre::GpuProgramManager* gpuProgramManager = 
				Ogre::GpuProgramManager::getSingletonPtr();

			return Ogre::StringConverter::toString(
				inAppDebug->getNumResourcesWithStr(gpuProgramManager, "_vs")) 
				+ "|" +
				Ogre::StringConverter::toString(
				inAppDebug->getNumResourcesWithStr(gpuProgramManager, "_gp"))
				+ "|" +
				Ogre::StringConverter::toString(
				inAppDebug->getNumResourcesWithStr(gpuProgramManager, "_ps"));
		}
	);
	addRow("hardware_skinned", "False");
	addRow("frame_time");
	addRow("delta_time");

	mNumPersistentRows = mRows.size();
}

DebugTable::~DebugTable()
{
	try {
		if (mOverlay)
			mOverlayManager->destroy(mOverlay);
	}
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy debug table");
	}
}

void DebugTable::update()
{
	if (mOverlay->isVisible())
	{
		for (const auto& tuple : mRows)
		{
			if (!variantTypeEquals<Ogre::String>(tuple))
			{ // only automatically update non-strings
				std::get<1>(tuple)->setCaption(
					boost::apply_visitor(to_string_visitor(), std::get<2>(tuple)));
			}
		}

		// increase the width of the table if needed
		//updateTableWidth();
	}
}

bool DebugTable::toggle()
{
	bool visible = mOverlay->isVisible();
	if (visible)
		mOverlay->hide();
	else
		mOverlay->show();

	return !visible;
}

void DebugTable::updateCaptionWidth(const Ogre::String::size_type newLength)
{
	if (newLength > mCaptionWidth)
	{
		mCaptionWidth = newLength;

		// update the entire column
		for (const auto& tuple : mRows)
		{
			std::get<1>(tuple)->setLeft(
				static_cast<Ogre::Real>(
					mCaptionWidth * DEBUG_TABLE_CHARACTER_WIDTH));
		}
	}
}

Ogre::OverlayElement* const DebugTable::createOverlayElement(
	const Ogre::String& templateName, const Ogre::String& overlayName, 
	const Ogre::String& caption, Ogre::Real left, Ogre::Real top)
{
	Ogre::OverlayElement* overlayElement = 
		mOverlayManager->createOverlayElementFromTemplate(
			templateName,						// template name
			"TextArea",							// type name 
			templateName + '/' + overlayName);	// instance name

	overlayElement->setTop(top);
	overlayElement->setLeft(left);
	overlayElement->setCaption(caption);

	mContainer->addChild(overlayElement);

	return overlayElement;
}

void DebugTable::addRow(const Ogre::String& caption, char* value)
{
	addRow(caption, Ogre::String(value));
}

void DebugTable::addRow(const Ogre::String& caption, const char* value)
{
	addRow(caption, Ogre::String(value));
}

void DebugTable::addRow(const Ogre::String& caption, const VariantType& value)
{
	// check for duplicates
	for (auto& tuple : mRows)
	{
		if (std::get<0>(tuple)->getCaption() == caption)
		{
			DEBUG_OUTPUT(LVL_YELLOW, "A row with caption \"" + caption + 
				"\" already exists in the debug table", true);
			return;
		}
	};

	// create a new caption and value overlay element
	const Ogre::Real top = 
		static_cast<Ogre::Real>(
			DEBUG_TABLE_GAP_HORIZONTAL_WIDTH + 
			(mRows.size() * DEBUG_TABLE_CHARACTER_HEIGHT));

	Ogre::OverlayElement* captionElement = 
		createOverlayElement(
			DEBUG_TABLE_CAPTION_TEMPLATE_NAME,	// template name
			caption,							// overlay name
			caption,							// caption
			DEBUG_TABLE_GAP_VERTICAL_WIDTH,		// left
			top);								// top
		 

	Ogre::OverlayElement* valueElement = 
		createOverlayElement(
			DEBUG_TABLE_VALUE_TEMPLATE_NAME,					
			caption,											
			boost::apply_visitor(to_string_visitor(), value),	
			static_cast<Ogre::Real>(mCaptionWidth * DEBUG_TABLE_CHARACTER_WIDTH), 
			top);

	// add the table row to the managing container
	mRows.push_back(std::make_tuple(captionElement, valueElement, value));

	// update the width of the caption column if needed
	updateCaptionWidth(caption.length());
}

void DebugTable::setValue(const Ogre::String& caption, char* value)
{
	setValue(caption, Ogre::String(value));
}

void DebugTable::setValue(const Ogre::String& caption, const char* value)
{	
	setValue(caption, Ogre::String(value));
}

void DebugTable::setValue(const Ogre::String& caption, const Ogre::String& value)
{
	for (auto& tuple : mRows)
	{
		if (std::get<0>(tuple)->getCaption() == caption)
		{
			if (variantTypeEquals<Ogre::String>(tuple))
			{ // only strings can be manually changed in the debug table
				// copies over to keep the container up-to-date
				std::get<2>(tuple) = value; 
				std::get<1>(tuple)->setCaption(value);
			}
			else
			{
				DEBUG_OUTPUT(LVL_YELLOW, "Debug table row with name \"" + 
					caption + "\" is not a string - only strings "
					"can be reassigned", true);
			}

			return;
		}
	};

	addRow(caption, value);
}

// unused, there is no longer a backdrop that requires the table to be of 
// correct width
/*void DebugTable::updateTableWidth()
{
	// find max value length in characters
	Ogre::ushort maxValueLength = 0;
	for (const auto& element : mValues)
	{
		if (element.first->getCaption().length() > maxValueLength)
			maxValueLength = element.first->getCaption().length();
	}

	// calculate the new table width based on maximum caption and value lenghts
	Ogre::ushort newTableWidth = 
		((maxValueLength + mCaptionWidth) * DEBUG_TABLE_CHARACTER_WIDTH) + 
		DEBUG_TABLE_GAP_VERTICAL_WIDTH * 2;

	// change the table width if it's larger than the current table width
	if (newTableWidth > mTableWidth)
	{
		mContainer->setWidth(newTableWidth);

		DEBUG_OUTPUT(LVL_GREEN, "Debug table width was changed from \"" +
			Ogre::StringConverter::toString(mTableWidth) + "\" to \"" + 
			Ogre::StringConverter::toString(newTableWidth) + '\"');

		mTableWidth = newTableWidth;
	}
}*/

void DebugTable::clearNonPersistentRows()
{
	TableType::const_iterator start = mRows.begin();
	std::advance(start, mNumPersistentRows);

	for (TableType::const_iterator it = start; it != mRows.end(); ++it)
	{
		estd::tuple_do_for_type<Ogre::OverlayElement*>(*it, 
			[this](Ogre::OverlayElement* element) {
				mOverlayManager->destroyOverlayElement(element); 
			}
		);
	}

	mRows.erase(start, mRows.end());
}

template <typename Ty>
bool DebugTable::variantTypeEquals(const TableRow& tuple) NOEXCEPT
{ 
	const auto size = std::tuple_size<TableRow>::value;
	return std::get<size - 1>(tuple).type().hash_code() == typeid(Ty).hash_code();
}