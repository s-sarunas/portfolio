// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Region.h"

#include "InAppDebug.h"

Region::Region(const ActorClass& desc, const Ogre::Vector3& min, 
	const Ogre::Vector3& max)
  : Actor(desc),
	StaticActor(desc),
	Collidable(desc.getTypeFlag()),
	mAxisAlignedBox(min, max)
{
	getCollisionMask().set(ACTOR_BALL_TYPE_FLAG);
}

Region::~Region()
{
}

// Debuggable.h
void Region::initDebuggable()
{
	InAppDebug::getSingleton().addDebugObject(mAxisAlignedBox.getCenter(), true, 
		REGION_DEBUG_MESH_NAME, Ogre::Quaternion::IDENTITY, mAxisAlignedBox.getSize());
}

void Region::handleCollision(Collidable* collidable, bool isColliding)
{
}

std::shared_ptr<Actor> RegionFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Region>(desc, 
		desc.getParam<Ogre::Vector3>("min"),
		desc.getParam<Ogre::Vector3>("max"));
}