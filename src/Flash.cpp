// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Flash.h"

#include "InAppDebug.h"

Flash::Flash(const ActorClass& desc)
  : Actor(desc),
	StaticActor(desc)
{
}

Flash::~Flash()
{
}

Flash::Flash(Flash&& rhs)
  : Actor(std::move(rhs)),
    StaticActor(std::move(rhs))
{
}

Flash& Flash::operator=(Flash&& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		Actor::operator=(std::move(rhs));
		StaticActor::operator=(std::move(rhs));

		// free existing resources of the source
		// ...

		// copy data from the source object
		// ....

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		// ...
	}

	return *this;
}

void Flash::show()
{
	getNode()->roll(Ogre::Degree(
		RNG(FLASH_LIGHT_ROLL_SPEED_MIN, FLASH_LIGHT_ROLL_SPEED_MAX)));

	Actor::show();
}

std::shared_ptr<Actor> FlashFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Flash>(desc);
}