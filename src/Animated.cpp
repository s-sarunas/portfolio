// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Animated.h"

Animated<1>::Animated(Ogre::Entity* entity, const Ogre::String& animName, 
	bool loop, bool inverse)
  : mAnimationState(
		AnimationStateWrapper(entity, ANIMATION_COMPLETE, animName, loop, inverse)) 
{
}

Animated<1>::~Animated() 
{
}

Animated<1>::Animated(Animated&& rhs) NOEXCEPT
  : mAnimationState(std::move(rhs.mAnimationState))
{
}

Animated<1>& Animated<1>::operator=(Animated&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		mAnimationState = std::move(rhs.mAnimationState);
	}

	return *this;
}

void Animated<1>::update(const Ogre::Real deltaSeconds)
{
	mAnimationState.update(deltaSeconds);
}

////////////////////////////////////////////////////////////////////////////////

Animated<2>::Animated(Ogre::Entity* entity, const Ogre::String& topAnimName, 
	const Ogre::String& botAnimName, bool loop, bool inverse) 
  :	mTopAnimationState(AnimationStateWrapper(entity, ANIMATION_TOP, 
		topAnimName, loop, inverse)),
	mBotAnimationState(AnimationStateWrapper(entity, ANIMATION_BOTTOM,
		botAnimName, loop, inverse))
{
}

Animated<2>::~Animated()
{
}

Animated<2>::Animated(Animated&& rhs) NOEXCEPT
  : mTopAnimationState(std::move(rhs.mTopAnimationState)),
	mBotAnimationState(std::move(rhs.mBotAnimationState))
{
}

Animated<2>& Animated<2>::operator=(Animated&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		mTopAnimationState = std::move(rhs.mTopAnimationState);
		mBotAnimationState = std::move(rhs.mBotAnimationState);
	}

	return *this;
};

void Animated<2>::update(const Ogre::Real deltaSeconds)
{
	mTopAnimationState.update(deltaSeconds);
	mBotAnimationState.update(deltaSeconds);
}