// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "DotSceneLoader.h"

#include "InAppDebug.h"
#include "Actor.h"

DotSceneLoader::DotSceneLoader(ActorCreator* creator, const Ogre::String& 
	sceneFilename, Ogre::SceneNode* rootSceneNode, const Ogre::String& groupName,
	bool useInAppDebug) 
try
  : mXMLRoot(nullptr),
	mRootSceneNode(rootSceneNode),
	mCreator(creator),
    mCreateDebugEntities(useInAppDebug),
	mBoundingBoxesVisible(false),
	mStaticGeometry(nullptr)
{
	DEBUG_OUTPUT(LVL_GREEN, "Scene with name \"" + sceneFilename + 
		"\" is being parsed");

	mFilename = sceneFilename;

	parseDotScene(sceneFilename, groupName);

	DEBUG_OUTPUT(LVL_GREEN, "Finished parsing scene \"" + sceneFilename + 
		'\"');
}
catch (...)
{
	DEBUG_OUTPUT(LVL_RED, "Cannot load a scene with name \"" + sceneFilename + 
		'\"');
	unloadScene();
}
 
DotSceneLoader::~DotSceneLoader()
{
	try {
		unloadScene();
	} 
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy dotsceneloader");
	}
}

bool DotSceneLoader::toggleStaticGeometryBoundingBoxes()
{
	mBoundingBoxesVisible = !mBoundingBoxesVisible;

	Ogre::StaticGeometry::RegionIterator regionIt = mStaticGeometry->getRegionIterator();
	unsigned int counter = 0;
	for (auto it = regionIt.begin(); it != regionIt.end(); ++it)
	{
		it->second->getParentSceneNode()->showBoundingBox(mBoundingBoxesVisible);
		++counter;
	}

	DEBUG_WRITE("Number of static geometry regions: " << counter << '\n');

	return mBoundingBoxesVisible;
}

void DotSceneLoader::updateStaticGeometryLightList()
{
	Ogre::StaticGeometry::RegionIterator it = mStaticGeometry->getRegionIterator();
	while (it.hasMoreElements())
	{
		Ogre::StaticGeometry::Region* region = it.getNext();

		Ogre::StaticGeometry::Region::LODIterator lod = region->getLODIterator();
		while (lod.hasMoreElements())
		{
			auto material = lod.getNext()->getMaterialIterator();
			while (material.hasMoreElements())
			{
				auto geometry = material.getNext()->getGeometryIterator();
				while (geometry.hasMoreElements())
				{
					Actor::_getMovableObjectListener().
						_addLightTypes(geometry.getNext());
				}
			}
		}
	}
}

void DotSceneLoader::delayActor(ActorClass&& desc)
{
	mDelayedActorDescriptions[desc.getType()].push_back(std::move(desc));
}

std::vector<Actor*> DotSceneLoader::loadDelayedActors(const Ogre::String& oldType, 
	const Ogre::String& newType, const Ogre::String& meshName, 
	StringInterface* additionalParams)
{
	const DelayedActorDescriptionMap::iterator it = 
		mDelayedActorDescriptions.find(oldType);

	std::vector<Actor*> actors;
	if (it != mDelayedActorDescriptions.end())
	{
		for (auto& desc : it->second)
		{
			if (!meshName.empty())
				desc.setEntity(meshName);

			if (additionalParams)
				desc.setAdditionalParams(*additionalParams, replace_parameters);

			desc.setType(newType);

			actors.push_back(mCreator->createActor(desc));
		}
	}
	else
	{
		DEBUG_OUTPUT(LVL_RED, "Delayed actors with old type \"" + oldType + 
			"\" could not be found");
	}

	return actors;
}

void DotSceneLoader::reserveSpace(rapidxml::xml_node<>* XMLNode, 
	const Ogre::String& attributeName)
{
	const unsigned int count = getAttributeUnsignedInt(XMLNode, attributeName);
	if (count != 0)
		mCreator->reserveForActors(count);
}

void DotSceneLoader::parseDotScene(const Ogre::String& filename, 
	const Ogre::String& groupName)
{
    Ogre::DataStreamPtr stream = 
		Ogre::ResourceGroupManager::getSingleton().openResource(filename, groupName);

	Ogre::String data = stream->getAsString();
    mXMLDoc.parse<0>(const_cast<char*>(data.c_str()));
 
    mXMLRoot = mXMLDoc.first_node("scene");
	if (mXMLRoot)
		processScene(mXMLRoot);
	else
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE,
			"Opening tag <scene> was not found in \"" + filename + 
			'\"', "DotSceneLoader::parseDotScene");
	}
}

void DotSceneLoader::processScene(rapidxml::xml_node<>* XMLRoot)
{
    rapidxml::xml_node<>* element;

	// Process settings
    element = XMLRoot->first_node("settings");
    if (element)
        processSettings(element);

	// Process actors  
    element = XMLRoot->first_node("actors");
    if (element)
        processActors(element);
 
    // Process lights
    element = XMLRoot->first_node("lights");
    if (element)
        processLights(element);

	// Process particles
    element = XMLRoot->first_node("particles");
    if (element)
        processParticles(element);
 
    // Process camera  
    element = XMLRoot->first_node("camera");
    if (element)
        processCamera(element);

	element = XMLRoot->first_node("staticgeometry");
	if (element)
		processStaticGeometry(element);
}

void DotSceneLoader::processSettings(rapidxml::xml_node<>* XMLRoot)
{
    rapidxml::xml_node<>* element;

	// Process skybox
    element = XMLRoot->first_node("skybox");
    if (element)
		getSceneManager()->setSkyBox(true, getAttribute(element, "name"));

	// Process ambient colour
    element = XMLRoot->first_node("ambient");
    if (element)
		getSceneManager()->setAmbientLight(parseColour(element));
}
 
void DotSceneLoader::processActors(rapidxml::xml_node<>* XMLNode)
{
	// reserve space for actor count
	reserveSpace(XMLNode, "count");

	// process individual actors
    rapidxml::xml_node<>* element;
 
    element = XMLNode->first_node("actor");
    while (element)
    {
        ActorClass desc = processActor(element);

		// create an actor or store it if of type "delayed"
		bool delayed = getAttributeBool(element, "delayed");
		if (delayed)
			delayActor(std::move(desc));
		else
			mCreator->createActor(desc);

        element = element->next_sibling("actor");
    }
}

ActorClass DotSceneLoader::processActor(rapidxml::xml_node<>* XMLNode)
{
	ActorClass desc;
	desc.setName(getAttribute(XMLNode, "name"));
	desc.setType(getAttribute(XMLNode, "type"));
	desc.setNode(mRootSceneNode, create_child_node);
 
	rapidxml::xml_node<>* element;
 
	// Process position  
	element = XMLNode->first_node("position");
	if (element)
		desc.setTranslate(parseVector3(element));
 
	// Process orientation  
	element = XMLNode->first_node("quaternion");
	if (element)
		desc.setRotate(parseQuaternion(element));
 
	// Process scale  
	element = XMLNode->first_node("scale");
	if (element)
		desc.setScale(parseVector3(element));
 
	// Process entity 
	element = XMLNode->first_node("entity");
	if (element)
		desc.setEntity(getAttribute(element, "mesh"));

	// Process params
	element = XMLNode->first_node("params");
	if (element)
		processParams(element, desc);

	// Process children
	element = XMLNode->first_node("children");
	if (element)
		processChildren(element, desc);

	return desc;
}

void DotSceneLoader::processParams(rapidxml::xml_node<>* XMLNode, ActorClass& desc)
{
	rapidxml::xml_node<>* element = XMLNode->first_node("param");
	rapidxml::xml_attribute<>* attribute = nullptr;
	while (element) // could also use for
	{
		attribute = element->first_attribute();
		Ogre::String name;
		Ogre::String value;
		if (attribute)
		{
			name = attribute->name();
			value = attribute->value();
		}
		else
		{
			OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, 
				"No parameter name or value for actor \"" + desc.getName() +
				"\", in \"" + mFilename + '\"', 
				"DotSceneLoader::processParams");
		}

		attribute = attribute->next_attribute("type");
		if (attribute)
		{ 
			Ogre::String type = attribute->value();
			if (type == "string")
			{
				desc.addParam(name, std::move(value), 
					PARAMETER_TYPE_STRING);
			}
			else if (type == "float")
			{
				desc.addParam(name, Ogre::StringConverter::parseReal(value), 
					PARAMETER_TYPE_REAL);
			}
			else if (type == "int")
			{
				desc.addParam(name, Ogre::StringConverter::parseInt(value), 
					PARAMETER_TYPE_INT);
			}
			else if (type == "vector3")
			{
				desc.addParam(name, Ogre::StringConverter::parseVector3(value),
					PARAMETER_TYPE_VECTOR3);
			}
			else
			{
				OGRE_EXCEPT(Ogre::Exception::ERR_NOT_IMPLEMENTED, 
					"Actor parameter type \"" + type + 
					"\" is not supported, for actor \"" + desc.getName() +
					"\" and parameter name \"" + name + 
					"\", in \"" + mFilename + '\"', 
					"DotSceneLoader::processParams");
			}
		}
		else
		{
			OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, 
				"Actor \"" + desc.getName() + 
				"\"'s parameter \"" + name +
				"\" does not have a type attribute, in \"" + mFilename + '\"',
				"DotSceneLoader::processParams");
		}

		element = element->next_sibling("param");
	}
}

void DotSceneLoader::processChildren(rapidxml::xml_node<>* XMLNode, ActorClass& desc)
{
	// reserve enough space for all children descriptions
	desc._reserve(getAttributeUnsignedInt(XMLNode, "count"));

	rapidxml::xml_node<>* element = XMLNode->first_node("actor");
	while (element)
	{
		desc.addChild(std::move(processActor(element)));

		element = element->next_sibling("actor");
	}
}

void DotSceneLoader::processLights(rapidxml::xml_node<>* XMLNode)
{
    rapidxml::xml_node<>* element;
 
    element = XMLNode->first_node("light");
    while (element)
    {
        processLight(element);
        element = element->next_sibling("light");
    }
}

void DotSceneLoader::processLight(rapidxml::xml_node<>* XMLNode)
{
    const Ogre::String name = getAttribute(XMLNode, "name");

    Ogre::Light* light = getSceneManager()->createLight(name);
	Ogre::SceneNode* sceneNode = mRootSceneNode->createChildSceneNode(name);

    rapidxml::xml_node<>* element;

	// Process power
	light->setPowerScale(getAttributeReal(XMLNode, "power"));

	// Process position  
    element = XMLNode->first_node("position");
    if (element)
		sceneNode->setPosition(parseVector3(element));
	
    // Process diffuse colour  
    element = XMLNode->first_node("colour");
    if (element)
        light->setDiffuseColour(parseColour(element));

	// Process type
    Ogre::String type = getAttribute(XMLNode, "type");
    if (type == "POINT")
	{
        light->setType(Ogre::Light::LT_POINT);

		// Process attenuation for point lights
		Ogre::Vector4 atten = Ogre::Vector4::ZERO;
		element = XMLNode->first_node("attenuation");
		if (element)
		{
			atten = parseAttenuation(element);
			light->setAttenuation(atten.x, atten.y, atten.z, atten.w);
		}

		if (mCreateDebugEntities)
		{ // debug object for light range
			InAppDebug::getSingleton().addDebugObject(sceneNode, false, 
				"sphere_d.mesh", Ogre::Vector3::ZERO, Ogre::Quaternion::IDENTITY,
				Ogre::Vector3(atten.x, atten.x, atten.x));
		}
	}
    else if (type == "DIRECTIONAL")
	{
        light->setType(Ogre::Light::LT_DIRECTIONAL);

		// set the light's forward vector before attaching it
		light->setDirection(OGRE_UNIT_VECTOR_FORWARD); 

		// Process direction 
		element = XMLNode->first_node("direction");
		if (element)
			sceneNode->setDirection(parseVector3(element));

		if (mCreateDebugEntities)
			InAppDebug::getSingleton().addArrow(sceneNode);
	}

	if (mCreateDebugEntities)
		InAppDebug::getSingleton().addBillboard(sceneNode);

	sceneNode->attachObject(light);
}

void DotSceneLoader::processParticles(rapidxml::xml_node<>* XMLNode)
{
	rapidxml::xml_node<>* element;
 
    element = XMLNode->first_node("particle");
    while (element)
    {
        processParticle(element);
        element = element->next_sibling("particle");
    }
}

void DotSceneLoader::processParticle(rapidxml::xml_node<>* XMLNode)
{
	// Get attributes
    const Ogre::String name = getAttribute(XMLNode, "name");
	const Ogre::String templateName = getAttribute(XMLNode, "templateName");

	// Create the particle system
	Ogre::ParticleSystem* particleSystem = 
		getSceneManager()->createParticleSystem(name, templateName);

	// Create the scene node
	Ogre::SceneNode* sceneNode = mRootSceneNode->createChildSceneNode(name);
	sceneNode->attachObject(particleSystem);
}
 
void DotSceneLoader::processCamera(rapidxml::xml_node<>* XMLNode)
{
	rapidxml::xml_node<>* element;
	Ogre::Camera* camera = getCamera();

    // Process position  
	element = XMLNode->first_node("position");
    if (element)
        camera->setPosition(parseVector3(element));

    // Process orientation  
    element = XMLNode->first_node("direction");
    if (element)
        camera->setDirection(parseVector3(element));

	InAppDebug::getSingleton().getCameraMan()._init();
}

void DotSceneLoader::processStaticGeometry(rapidxml::xml_node<>* XMLGeometryRoot)
{
	mStaticGeometry = getSceneManager()->createStaticGeometry("StaticGeo");

	processBatches(XMLGeometryRoot);
	
	mStaticGeometry->build();
}

void DotSceneLoader::processBatches(rapidxml::xml_node<>* XMLNode)
{
	rapidxml::xml_node<>* element;
 
    element = XMLNode->first_node("batch");
    while (element)
	{
		processBatch(element);
		element = element->next_sibling("batch");
	}
}

void DotSceneLoader::processBatch(rapidxml::xml_node<>* XMLNode)
{
	const Ogre::String mesh = XMLNode->first_attribute("mesh")->value();
	Ogre::Entity* batchEntity = getSceneManager()->createEntity(mesh, mesh);

    rapidxml::xml_node<>* element;

	element = XMLNode->first_node("entity");
	if (element)
		processEntity(element, batchEntity);

	mBatchEntities.push_back(batchEntity);
}

void DotSceneLoader::processEntity(rapidxml::xml_node<>* XMLNode, Ogre::Entity* entity)
{
    while (XMLNode)
	{
		mStaticGeometry->addEntity(
			entity, 
			parseVector3(XMLNode->first_node("position")),
			parseQuaternion(XMLNode->first_node("quaternion")),
			parseVector3(XMLNode->first_node("scale")));

		XMLNode = XMLNode->next_sibling("entity");
	} 
}

const Ogre::String DotSceneLoader::getAttribute(rapidxml::xml_node<>* XMLNode, 
	const Ogre::String& name) const
{
	const rapidxml::xml_attribute<>* attribute = XMLNode->first_attribute(name.c_str());
    if (attribute)
        return attribute->value();

	OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, 
		"Attribute \"" + name + "\" (Ogre::String) under node \"" + 
		XMLNode->name() + "\" was not found in \"" + mFilename + '\"', 
		"DotSceneLoader::getAttribute");
}
 
const Ogre::Real DotSceneLoader::getAttributeReal(rapidxml::xml_node<>* XMLNode, 
	const Ogre::String& name) const
{
	const rapidxml::xml_attribute<>* attribute = XMLNode->first_attribute(name.c_str());
    if (attribute)
        return Ogre::StringConverter::parseReal(attribute->value());

	OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, "Attribute \"" + name + 
		"\" (Ogre::Real) under node \"" + XMLNode->name() + 
		"\" was not found in \"" + mFilename + '\"',
		"DotSceneLoader::getAttribute");
}
 
const bool DotSceneLoader::getAttributeBool(rapidxml::xml_node<>* XMLNode, const 
	Ogre::String& name) const
{
	const rapidxml::xml_attribute<>* attribute = XMLNode->first_attribute(name.c_str());
    if (attribute)
		return Ogre::StringConverter::parseBool(attribute->value());

	OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, "Attribute \"" + name + 
		"\" (bool) under node \"" + XMLNode->name() +
		"\" was not found in \"" + mFilename + '\"', 
		"DotSceneLoader::getAttribute");
}

const unsigned int DotSceneLoader::getAttributeUnsignedInt(rapidxml::xml_node<>* XMLNode, 
	const Ogre::String& name) const
{
	const rapidxml::xml_attribute<>* attribute = XMLNode->first_attribute(name.c_str());
    if (attribute)
		return Ogre::StringConverter::parseUnsignedInt(attribute->value());

	OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, "Attribute \"" + name + 
		"\" (unsigned int) under node \"" + XMLNode->name() +
		"\" was not found in \"" + mFilename + '\"',
		"DotSceneLoader::getAttribute");
}
 
Ogre::Vector3 DotSceneLoader::parseVector3(rapidxml::xml_node<>* XMLNode) const
{
    return Ogre::Vector3(
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value()));
}
 
Ogre::Quaternion DotSceneLoader::parseQuaternion(rapidxml::xml_node<>* XMLNode) const
{
	return Ogre::Quaternion(
		Ogre::StringConverter::parseReal(XMLNode->first_attribute("w")->value()),
		Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value()),
		Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value()),
		Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value()));
}
 
Ogre::ColourValue DotSceneLoader::parseColour(rapidxml::xml_node<>* XMLNode) const
{
    return Ogre::ColourValue(
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("r")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("g")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("b")->value()),
        (XMLNode->first_attribute("a") != nullptr) ? 
			Ogre::StringConverter::parseReal(XMLNode->first_attribute("a")->value()) : 1.0f);
}

Ogre::Vector4 DotSceneLoader::parseAttenuation(rapidxml::xml_node<>* XMLNode) const
{
    return Ogre::Vector4(
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("range")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("constant")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("linear")->value()),
        Ogre::StringConverter::parseReal(XMLNode->first_attribute("quadratic")->value()));
}
 
void DotSceneLoader::unloadScene()
{
	// destroy the static geometry and all the nodes/entities related to it
	if (mStaticGeometry)
		getSceneManager()->destroyStaticGeometry(mStaticGeometry);

	for (const auto& entity : mBatchEntities)
		getSceneManager()->destroyEntity(entity);

	if (mRootSceneNode)
		eOgre::nuke_scene_node(getSceneManager(), mRootSceneNode);

	getSceneManager()->setSkyBoxEnabled(false);
}