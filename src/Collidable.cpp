// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Collidable.h"

#include "InAppDebug.h"
#include "CollisionManager.h"

Collidable::Collidable(const ActorFlagType actorFlag) 
  : mTypeFlag(actorFlag),
	mCollisionMask(true),
	mVisibleToListeners(true)
{
	CollisionManager::getSingleton().registerCollidable(this);
}

Collidable::~Collidable()
{
	try {
		CollisionManager::getSingleton().removeCollidable(this);
	} 
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy a collidable");
	}
}

Collidable::Collidable(const Collidable& rhs)
  : mCollisionResults(rhs.mCollisionResults),
	mTypeFlag(rhs.mTypeFlag),
	mCollisionMask(rhs.mCollisionMask),
	mVisibleToListeners(rhs.mVisibleToListeners)
{
	CollisionManager::getSingleton().registerCollidable(this);
}

Collidable& Collidable::operator=(const Collidable& rhs)
{
	if (this != std::addressof(rhs))
	{
		mCollisionResults = rhs.mCollisionResults;
		mTypeFlag = rhs.mTypeFlag;
		mCollisionMask = rhs.mCollisionMask;
		mVisibleToListeners = rhs.mVisibleToListeners;
	}

	return *this;
}

Collidable::Collidable(Collidable&& rhs)
  : mCollisionResults(std::move(rhs.mCollisionResults)),
	mTypeFlag(rhs.mTypeFlag),
	mCollisionMask(rhs.mCollisionMask),
	mVisibleToListeners(rhs.mVisibleToListeners)
{
	CollisionManager::getSingleton().registerCollidable(this);
}

Collidable& Collidable::operator=(Collidable&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		// free existing resources of the source
		// ...

		// copy data from the source object
		mCollisionResults = std::move(rhs.mCollisionResults);
		mTypeFlag = rhs.mTypeFlag;
		mCollisionMask = rhs.mCollisionMask;
		mVisibleToListeners = rhs.mVisibleToListeners;

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		// ...
	}

	return *this;
}

std::ostream& operator<<(std::ostream& os, const Collidable& collidable) NOEXCEPT
{
	os << collidable.getName() << '\t'
	   << collidable.mCollisionMask << '\t'
	   << estd::to_binary(collidable.mTypeFlag) << '\t'
	   << std::boolalpha << collidable.mVisibleToListeners << '\t';

	if (!collidable.mCollisionResults.empty())
	{
		for (const auto& collision : collidable.mCollisionResults)
			os << collision.first->getName();
	}
	else
		os << "none";

	os << '\t';

	return os;
}

void Collidable::addCollision(Collidable* collidable)
{
	mCollisionResults.emplace_back(collidable, true);
}

void Collidable::clearCollisions() NOEXCEPT
{
	mCollisionResults.clear();
}

bool Collidable::hasCollisions() const NOEXCEPT
{
	return (!mCollisionResults.empty());
}

const CollidablePtrList Collidable::getCollisions(const Ogre::String& type)
{
	CollidablePtrList result;
	for (const auto& pair : mCollisionResults)
	{
		if (pair.first->getActorType() == type)
			result.push_back(pair.first);
	}

	return result;
}

bool Collidable::isCollisionMasked(ActorFlagType typeFlag) const NOEXCEPT
{
	if (typeFlag == NULL) // this member function is only called for collidables
	{
		DEBUG_OUTPUT(LVL_YELLOW, "Collidable with name \"" + getName() +
			"\" does not have a valid type flag assigned", true);
	}

	return !(mCollisionMask.isSet(typeFlag));
}

bool Collidable::hasCollided(const Ogre::String& nameOrType) const
{
	return (std::find_if(mCollisionResults.begin(), mCollisionResults.end(), 
		[&nameOrType](const CollisionList::value_type pair) NOEXCEPT {
			return (pair.first->getName() == nameOrType || 
					pair.first->getActorType() == nameOrType); 
		}
	) != mCollisionResults.end());
}

bool Collidable::hasCollided(const Collidable* collidable) const
{
	return (std::find_if(mCollisionResults.begin(), mCollisionResults.end(), 
		[collidable](const CollisionList::value_type& pair) NOEXCEPT {
			return (pair.first == collidable);
		}
	) != mCollisionResults.end());
}

void Collidable::reset()
{
	for (auto& pair : mCollisionResults)
		pair.second = false;
}