// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "SimpleMenu.h"

#include "InAppDebug.h"
#include "Widget.h"
#include "Dropdown.h"
#include "Button.h"
#include "Config.h"

SimpleMenu::SimpleMenu(const Ogre::String& name)
  : mListener(nullptr),
	mActiveWidget(nullptr),
	mName(name),
	mOverlayManager(Ogre::OverlayManager::getSingletonPtr()),
	mTray(mOverlayManager->create(name + SIMPLEMENU_TRAY_SUFFIX)),
	mTrayContainer(static_cast<Ogre::OverlayContainer*>(
		mOverlayManager->createOverlayElementFromTemplate(SIMPLEMENU_TEMPLATE_NAME, 
			SIMPLEMENU_TYPE_NAME, name + SIMPLEMENU_INSTANCE_SUFFIX))),
	mJoystickActive(false),
	mNeedsResize(true)
{
	mTrayContainer->setHorizontalAlignment(Ogre::GuiHorizontalAlignment::GHA_CENTER);
	mTrayContainer->setVerticalAlignment(Ogre::GuiVerticalAlignment::GVA_CENTER);

	mTray->setZOrder(200);
	mTray->add2D(mTrayContainer);
}

SimpleMenu::~SimpleMenu()
{
	mWidgets.clear(); // noexcept

	try {
		mOverlayManager->destroy(mTray);
	} 
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy simple menu");
	}
}

void SimpleMenu::reset()
{
	for (const auto& widget : mWidgets)
		widget->reset();

	if (mJoystickActive)
		mActiveWidget = mWidgets.front().get();

	if (mNeedsResize)
		resizeTray();
}

void SimpleMenu::createButton(const Ogre::String& name, const Ogre::DisplayString& caption)
{
	for (const auto& widget : mWidgets)
	{
		if (widget->getType() == WIDGET_TYPE_BUTTON)
		{
			DEBUG_OUTPUT(LVL_PERF, 
				"Creating multiple buttons one-by-one for SimpleMenu \"" + 
				mName + "\" is slower, use SimpleMenu::createButtons");
			break;
		}
	}

	mWidgets.push_back(std::make_unique<Button>(name, mName, caption, 
		mTrayContainer, getNextTop(), mWidgets.size(), mListener));

	mNeedsResize = true;
}

void SimpleMenu::createButtons(const std::initializer_list<Ogre::String> names, 
	const std::initializer_list<Ogre::DisplayString> captions)
{
	assert(names.size() == captions.size() && 
		"invalid button containers supplied");

	auto namesIt = names.begin();
	auto captionsIt = captions.begin();
	for (size_t i = 0; i < names.size(); ++i)
	{
		mWidgets.push_back(std::make_unique<Button>(*namesIt, mName, *captionsIt, 
			mTrayContainer, getNextTop(), mWidgets.size(), mListener));

		++namesIt; 
		++captionsIt;
	}

	mNeedsResize = true;
}

void SimpleMenu::createDropdown(const Ogre::String& name, const Ogre::DisplayString& caption, 
	const std::initializer_list<Ogre::DisplayString> items, const size_t itemIndex)
{
	mWidgets.push_back(std::make_unique<Dropdown>(name, mName, caption, 
		mTrayContainer, getNextTop(), items, itemIndex, mWidgets.size(), mListener));

	mNeedsResize = true;
}

void SimpleMenu::resizeTray()
{
	Ogre::Real width = 0.0f, height = WIDGET_PADDING;
	for (const auto& widget : mWidgets)
	{
		width = std::max(widget->getWidth(), width);
		height += widget->getHeight() + WIDGET_PADDING / 2;
	}

	width += WIDGET_PADDING * 2;
	height += WIDGET_PADDING / 2;

	mTrayContainer->setWidth(width);
	mTrayContainer->setHeight(height);
	mTrayContainer->setLeft(-width / 2);
	mTrayContainer->setTop(-height / 2);

	mNeedsResize = false;
}

const Ogre::Real SimpleMenu::getNextTop() const
{ 
	if (mWidgets.empty())
		return WIDGET_PADDING;
	else
		return mWidgets.back()->getBottom() + (WIDGET_PADDING / 2);
}

// InputHandler.h
bool SimpleMenu::mouseMoved(const SDL_MouseMotionEvent& arg)   
{
	mJoystickActive = false;

	for (const auto& widget : mWidgets) 
	{
		if (!widget->mouseMoved(arg))
			return false;
	}

	return true;
}

// InputHandler.h
bool SimpleMenu::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	mJoystickActive = false;

	for (const auto& widget : mWidgets)
	{
		if (!widget->mousePressed(arg, id))
			return false;
	}

	return true;
}

// InputHandler.h
bool SimpleMenu::mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	mJoystickActive = false;

	for (const auto& widget : mWidgets)
	{
		if (!widget->mouseReleased(arg, id))
			return false;
	}

	return true;
}

// InputHandler.h
bool SimpleMenu::keyPressed(const SDL_KeyboardEvent& arg)
{
	mJoystickActive = false;

	for (const auto& widget : mWidgets)
		widget->keyPressed(arg);

	return true;
}

// InputHandler.h
bool SimpleMenu::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
{
	mJoystickActive = true;

	if (mActiveWidget)
		mActiveWidget->joyButtonPressed(evt, button);

	return true;
}

// InputHandler.h
bool SimpleMenu::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	mJoystickActive = true;

	if (mActiveWidget)
		mActiveWidget->joyButtonReleased(evt, button);

	return true;
}

// InputHandler.h
bool SimpleMenu::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
{
	mJoystickActive = true;

	if (mActiveWidget)
		mActiveWidget->joyPovMoved(arg, index);

	return true;
}