// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Dropdown.h"

Dropdown::Dropdown(const Ogre::String& name, const Ogre::String& menuName, 
	const Ogre::DisplayString& caption, Ogre::OverlayContainer* tray, 
	const Ogre::Real top, const std::initializer_list<Ogre::DisplayString> items, 
	const ItemList::size_type itemIndex, const Widget::size_type widgetIndex, 
	SimpleMenu::Listener* listener)
  : Widget(name, menuName, caption, DROPDOWN_TEMPLATE_NAME, 
		DROPDOWN_WIDTH, DROPDOWN_HEIGHT, top, DROPDOWN_CAPTION_SUFFIX, widgetIndex, 
		listener),
	mContainer(static_cast<Ogre::OverlayContainer*>(
		getRootElement())),
	mSmallBox(static_cast<Ogre::BorderPanelOverlayElement*>(
		mContainer->getChild(getRootName() + DROPDOWN_SMALLBOX_SUFFIX))),
	mSmallTextArea(static_cast<Ogre::TextAreaOverlayElement*>(
		mSmallBox->getChild(getRootName() + DROPDOWN_SMALLTEXTAREA_SUFFIX))),
	mExpandedBox(static_cast<Ogre::BorderPanelOverlayElement*>(
		mContainer->getChild(getRootName() + DROPDOWN_EXPANDEDBOX_SUFFIX))),
	mState(DROPDOWN_STATE_UP)
{
	mSmallBox->setWidth(DROPDOWN_WIDTH - WIDGET_PADDING);
            
	mExpandedBox->setWidth(mSmallBox->getWidth() + WIDGET_PADDING);
	mExpandedBox->hide();

	tray->addChild(getRootElement());

	setItems(items, itemIndex);
}

Dropdown::~Dropdown()
{
}

Dropdown::ItemList::size_type Dropdown::getValue() const 
{ 
	auto it = std::find(mItems.begin(), mItems.end(), mActiveItem); 
	return std::distance(mItems.begin(), it);
}

const Ogre::DisplayString& Dropdown::getValueCaption() const NOEXCEPT
{
	return mActiveItem.second->getCaption();
}

void Dropdown::reset()
{
	mExpandedBox->hide();
	mSmallBox->show();

	setState(mSmallBox, DROPDOWN_STATE_UP);
}

void Dropdown::setItems(const std::initializer_list<Ogre::DisplayString> captions,
	const Widget::size_type defaultIndex)
{
	assert(mItems.empty() && 
		"select menu items cannot be reset");

	for (const auto& cap : captions)
	{
		Ogre::BorderPanelOverlayElement* element = static_cast<Ogre::BorderPanelOverlayElement*>(
			Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate(
			DROPDOWN_ITEM_TEMPLATE_NAME, DROPDOWN_ITEM_TEMPLATE_TYPE_NAME, 
			mExpandedBox->getName() + DROPDOWN_ITEM_TEMPLATE_INSTANCE_INFIX + cap));

		Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(
			element->getChild(element->getName() + DROPDOWN_ITEM_TEXTAREA_SUFFIX));
		textArea->setCaption(cap);

		element->setTop(6 + mItems.size() * (mSmallBox->getHeight() - 8));
		element->setWidth(mExpandedBox->getWidth() - WIDGET_PADDING);

		mExpandedBox->addChild(element);
		mItems.push_back(std::make_pair(element, textArea));
	}

	// position the dropdown
	const Ogre::Real height = mItems.size() * (mSmallBox->getHeight() - 8) + 20;
	mExpandedBox->setHeight(height);
	mExpandedBox->setLeft(mSmallBox->getLeft() - 4);
	mExpandedBox->setTop(mSmallBox->getTop() + 3);

	mActiveItem = mItems[defaultIndex];

	selectActiveItem();
}

void Dropdown::selectActiveItem()
{
	getListener()->itemSelected(this);
	mSmallTextArea->setCaption(mActiveItem.second->getCaption());
}

void Dropdown::setActiveItem(const ItemList::value_type& newElement)
{
	setState(mActiveItem.first, DROPDOWN_STATE_UP);
	setState(newElement.first, DROPDOWN_STATE_OVER);

	mActiveItem = newElement;
}

void Dropdown::setState(Ogre::BorderPanelOverlayElement* element, const State state)
{
	switch (state)
	{
	case DROPDOWN_STATE_UP:
		element->setMaterialName(DROPDOWN_MATERIAL_UP);
		element->setBorderMaterialName(DROPDOWN_MATERIAL_UP);
		break;
	case DROPDOWN_STATE_OVER:
		element->setMaterialName(DROPDOWN_MATERIAL_DOWN);
		element->setBorderMaterialName(DROPDOWN_MATERIAL_DOWN);
		break;
	default:
		break;
	}

	mState = state;
}

const Dropdown::State Dropdown::getState(Ogre::BorderPanelOverlayElement* element) const NOEXCEPT
{
	if (element->getMaterialName() == DROPDOWN_MATERIAL_UP)
		return DROPDOWN_STATE_UP;
	else
		return DROPDOWN_STATE_OVER;
}

// InputHandler.h
bool Dropdown::mouseMoved(const SDL_MouseMotionEvent& arg) 
{
	if (mExpandedBox->isVisible())
	{
		if (!isCursorOver(mExpandedBox, arg.x, arg.y, 0))
			reset();
		else
		{
			for (const auto& element : mItems)
			{
				if (isCursorOver(element.first, arg.x, arg.y))
				{
					setActiveItem(element);
					return false;
				}
			}
		}
	}
	else
	{
		if (isCursorOver(mSmallBox, arg.x, arg.y))
			setState(mSmallBox, DROPDOWN_STATE_OVER);
		else
		{
			if (mState == DROPDOWN_STATE_OVER)
				setState(mSmallBox, DROPDOWN_STATE_UP);
		}
	}

	return true;
}

// InputHandler.h
bool Dropdown::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	if (mExpandedBox->isVisible())
	{
		for (const auto& element : mItems)
		{
			if (getState(element.first) == DROPDOWN_STATE_OVER)
			{
				selectActiveItem();

				reset();

				if (isCursorOver(mSmallBox, arg.x, arg.y))
					setState(mSmallBox, DROPDOWN_STATE_OVER);

				return false;
			}
		}
	}
	else
	{
		if (getState(mSmallBox) == DROPDOWN_STATE_OVER)
		{
			mExpandedBox->show();
			mSmallBox->hide();

			setActiveItem(mItems[NULL]);
		
			return false;
		}
	}

	return true;
}

// InputHandler.h
bool Dropdown::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
{
	return true;
}

// InputHandler.h
bool Dropdown::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	return true;
}

// InputHandler.h
bool Dropdown::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
{
	return true;
}