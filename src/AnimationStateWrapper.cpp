// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "AnimationStateWrapper.h"

#include "InAppDebug.h"

AnimationStateWrapper::AnimationStateWrapper(Ogre::Entity* parentEntity, 
	AnimationSection animSection, const Ogre::String& startingAnimation, 
	bool loop, bool inverse)
  : mSection(animSection),
    mParentEntity(parentEntity),
	mAnimationState(nullptr),
	mInverse(inverse),
	mBlend(false)
{
	changeTo(startingAnimation, loop, inverse, false);

	mParentEntity->getSkeleton()->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);
}

AnimationStateWrapper::~AnimationStateWrapper() 
{
}

AnimationStateWrapper::AnimationStateWrapper(AnimationStateWrapper&& rhs) NOEXCEPT
{
	*this = std::move(rhs);
}

AnimationStateWrapper& AnimationStateWrapper::operator=(AnimationStateWrapper&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		// ...

		// free existing resources of the source
		// ...

		// copy data from the source object
		mPrevData = std::move(rhs.mPrevData);
		mParentEntity = rhs.mParentEntity;
		mAnimationState = rhs.mAnimationState;
		mSection = rhs.mSection;
		mInverse = rhs.mInverse;
		mBlend = rhs.mBlend;

		// release pointer ownership from the source object so that the
		// destructor does not free the memory multiple times
		// these below are not necessary since they're non-owning pointers
		rhs.mParentEntity = nullptr;
		rhs.mAnimationState = nullptr;
	}

	return *this;
}

void AnimationStateWrapper::update(const Ogre::Real deltaSeconds) NOEXCEPT
{
	if (mAnimationState)
	{
		mAnimationState->addTime((mInverse) ? -deltaSeconds : deltaSeconds);

		progressBlending(deltaSeconds);
	}
}

void AnimationStateWrapper::reset() NOEXCEPT
{
	if (mAnimationState)
		mAnimationState->setTimePosition(0);
}

void AnimationStateWrapper::changeTo(const Ogre::String& name, bool loop, 
	bool isInversed, bool blendIn)
{
	if (name != Ogre::StringUtil::BLANK)
	{
		if (mSection != ANIMATION_COMPLETE)
		{
			if (mSection == ANIMATION_TOP && 
				name.find(ANIMATION_STATE_WRAPPER_PREFIX_TOP) == std::string::npos)
			{
				DEBUG_OUTPUT(LVL_YELLOW, "AnimationStateWrapper is responsible "
					"for TOP section, but the animation \"" + name + 
					"\" doesn't have a prefix \"top\"");
			}

			if (mSection == ANIMATION_BOTTOM && 
				name.find(ANIMATION_STATE_WRAPPER_PREFIX_BOT) == std::string::npos)
			{
				DEBUG_OUTPUT(LVL_YELLOW, "AnimationStateWrapper is responsible "
					"for BOTTOM section, but the animation \"" + name + 
					"\" doesn't have a prefix \"top\"");
			}
		}

		// only if not already set or has finished
		// state here can be nullptr so you can't call any of its member functions
		if (mAnimationState != mParentEntity->getAnimationState(name) || hasEnded()) 
		{
			// if last animation exists, disable it
			if (mAnimationState)
			{
				if (!blendIn)
					mAnimationState->setEnabled(false);

				mPrevData = PreviousStateData(*this);
			}

			// set to the newly created animation state
			mAnimationState = init(name, loop, isInversed, blendIn);
		}
	}
}

bool AnimationStateWrapper::hasHalfEnded() const NOEXCEPT
{
	if (mAnimationState)
	{
		Ogre::Real timePos = mAnimationState->getTimePosition();
		Ogre::Real length = mAnimationState->getLength();

		return ((mAnimationState->getEnabled() && 
			(mInverse)) ? (timePos <= length / 2) : (timePos >= length / 2));
	}
	else
		return false;
}

bool AnimationStateWrapper::hasEnded() const NOEXCEPT
{
	if (mAnimationState)
	{
		return ((mInverse) ? (mAnimationState->getTimePosition() == 0) : 
			mAnimationState->hasEnded() || !mAnimationState->getEnabled());
	}
	else
		return false;
}

void AnimationStateWrapper::progressBlending(const Ogre::Real deltaSeconds) NOEXCEPT
{
	if (mBlend)
	{
		mPrevData->addTime((mPrevData.mInversed) ? -deltaSeconds : deltaSeconds);

		if (mAnimationState->getEnabled() && 
			mAnimationState->getWeight() < 1.0f)
		{
			mAnimationState->setWeight(mAnimationState->getWeight() + 
				ANIMATION_STATE_WRAPPER_SPEED_FADE_IN * deltaSeconds);

			if (mAnimationState->getWeight() > 1.0f)
				mAnimationState->setWeight(1.0f); // clamp the weight
		}

		if (mPrevData && mPrevData->getEnabled() && 
			mPrevData->getWeight() > 0)
		{
			mPrevData->setWeight(mPrevData->getWeight() - 
				ANIMATION_STATE_WRAPPER_SPEED_FADE_IN * deltaSeconds);

			if (mAnimationState->getWeight() < 0)
			{
				mPrevData->setWeight(0); // clamp the weight
				mPrevData->setEnabled(false);
			}
		}
	}
}

Ogre::AnimationState* AnimationStateWrapper::init(const Ogre::String& name, 
	bool loop, bool inverse, bool blendIn)
{
	// exceptions here are caught by Ogre
	Ogre::AnimationState* state = mParentEntity->getAnimationState(name);
	state->setLoop(loop);
	state->setEnabled(true);

	state->setTimePosition((inverse) ? state->getLength() : 0);
	state->setWeight((blendIn) ? 0.0f : 1.0f);

	mInverse = inverse;
	mBlend = blendIn;

	return state;
}

/*void AnimationStateWrapper::inverseBoneTransforms(
	const Ogre::String& firstBoneName, const Ogre::String& secondBoneName)
{
	Ogre::Skeleton *skeleton = entity->getSkeleton();
	Ogre::Bone *rightBone = skeleton->getBone(firstBoneName);
	Ogre::Bone *leftBone = skeleton->getBone(secondBoneName);

	Ogre::Quaternion temp = leftBone->getOrientation();
	leftBone->setOrientation(rightBone->getOrientation());
	leftBone->yaw(Ogre::Degree(180));

	rightBone->setOrientation(temp);
	rightBone->yaw(Ogre::Degree(180));
}*/