// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "AppStateManager.h"

#include "AppState.h"
#include "MenuState.h"
#include "PauseState.h"
#include "CinematicState.h"
#include "MapSelectState.h"
#include "SettingsState.h"
#include "InAppDebug.h"
#include "Config.h"
#include "Core.h"

AppStateManager* AppStateManager::msSingleton = nullptr;

AppStateManager::AppStateManager(AppState::Listener* listener)
  : mCursor(Ogre::OverlayManager::getSingleton().create(CURSOR_OVERLAY_NAME)),
	mCursorContainer(static_cast<Ogre::OverlayContainer*>(
		Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate(
			"Menus/Cursor", "Panel", "CursorContainer"))),
	mCurrentNode(nullptr),
    mListener(listener)
{
	assert(mListener && 
		"a valid listener is required");

	msSingleton = this;

	mCursor->add2D(mCursorContainer);
	mCursor->setZOrder(400);
	showCursor();

	// create the state notes
	mGameNode = std::make_shared<AppNode>();
	AppNodePtr pauseNode = 
		std::make_shared<AppNode>(std::make_unique<PauseState>(mListener));
	AppNodePtr settingsNode = 
		std::make_shared<AppNode>(std::make_unique<SettingsState>(mListener), true);
	AppNodePtr menuNode = 
		std::make_shared<AppNode>(std::make_unique<MenuState>(mListener));
	AppNodePtr cinematicNode = 
		std::make_shared<AppNode>(std::make_unique<CinematicState>(mListener));
	AppNodePtr mapSelectNode = 
		std::make_shared<AppNode>(std::make_unique<MapSelectState>(mListener));
	AppNodePtr quitNode = 
		std::make_shared<AppNode>();

	// describes the hierarchy of the menus: previous, next...
	mGameNode->setNext(pauseNode);
	mGameNode->setPrevious(pauseNode);

	pauseNode->setNext({mGameNode, settingsNode, menuNode, quitNode});
	pauseNode->setPrevious(mGameNode);

	mapSelectNode->setNext(mGameNode);
	mapSelectNode->setPrevious(menuNode);

	menuNode->setNext({mapSelectNode, mapSelectNode, settingsNode, quitNode});
	cinematicNode->setNext(menuNode);

	// set the starting node
	mCurrentNode = cinematicNode;
}

AppStateManager::~AppStateManager()
{
	try {
		hideCursor();
		Ogre::OverlayManager::getSingleton().destroy(mCursor);
	}
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy appstatemanager");
	}

	msSingleton = nullptr;
}

AppStateManager* AppStateManager::getSingletonPtr() NOEXCEPT
{
	return msSingleton;
}

AppStateManager& AppStateManager::getSingleton()
{
	assert(msSingleton);
	return (*msSingleton);
}

void AppStateManager::refreshCursor() NOEXCEPT
{
	int x, y;
	SDL_GetMouseState(&x, &y);
	mCursorContainer->setPosition(
		static_cast<Ogre::Real>(x), static_cast<Ogre::Real>(y));
}

void AppStateManager::showCursor()
{
	if (!mCursor->isVisible())
	{
		mCursor->show();
		refreshCursor();
	}
}

void AppStateManager::hideCursor() NOEXCEPT
{
	mCursor->hide();
}

void AppStateManager::nextState(const AppResultType& result)
{
	if (result && mCurrentNode->hasNext())
		mCurrentNode = mCurrentNode->next(result, mCurrentNode);
}

void AppStateManager::_previousState()
{
	showCursor();

	if (mCurrentNode->hasPrevious())
		mCurrentNode = mCurrentNode->previous();
}

bool AppStateManager::isActive() const NOEXCEPT
{
	return !(mCurrentNode == mGameNode);
}

bool AppStateManager::update(const Ogre::Real deltaSeconds)
{
	if (isActive())
	{
		mCurrentNode->update(deltaSeconds);

		nextState(mCurrentNode->getResult());

		return true; // trap the loop and update the states only
	}
	else
		hideCursor();
		
	return false; // render the scene
}

void AppStateManager::enterMenu()
{
	if (!isActive())
		_previousState();
}

// InputHandler.h
bool AppStateManager::mouseMoved(const SDL_MouseMotionEvent& arg)   
{
	if (isActive())
	{
		refreshCursor();
		mCurrentNode->mouseMoved(arg);

		return false;
	}

	return true;
}

// InputHandler.h
bool AppStateManager::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	if (isActive())
	{
		mCurrentNode->mousePressed(arg, id);
		return false;
	}

	return true;
}

// InputHandler.h
bool AppStateManager::mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	if (isActive())
	{
		mCurrentNode->mouseReleased(arg, id);
		return false;
	}

	return true;
}

// InputHandler.h
bool AppStateManager::textInput(const SDL_TextInputEvent& arg) 
{
	if (isActive())
	{
		mCurrentNode->textInput(arg);
		return false;
	}

	return true;
}

// InputHandler.h
bool AppStateManager::keyPressed(const SDL_KeyboardEvent& arg) 
{
	switch(arg.keysym.sym)
	{
	case APPSTATEMANAGER_BUTTON_KB_PAUSE:
		_previousState();

		if (mCurrentNode == mGameNode)
			eSDL_FlushMotionEvents();

		Core::getSingleton().resetDelta();
		break;
	default:
		break;
	}

	if (isActive())
	{
		mCurrentNode->keyPressed(arg);
		return false;
	}

	return true;
}

// InputHandler.h
bool AppStateManager::keyReleased(const SDL_KeyboardEvent& arg) 
{
	if (isActive())
	{
		mCurrentNode->keyReleased(arg);
		return false;
	}

	return true;
}

// InputHandler.h
bool AppStateManager::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
{
	if (isActive())
	{
		mCurrentNode->joyButtonPressed(evt, button);
		return false;
	}
	else
	{
		switch (button)
		{
		case APPSTATEMANAGER_BUTTON_JS_PAUSE:
		case APPSTATEMANAGER_BUTTON_JS_PAUSE_ALT:
			_previousState();
			
			if (mCurrentNode == mGameNode)
				eSDL_FlushMotionEvents();

			Core::getSingleton().resetDelta();
			break;
		default:
			break;
		}
	}

	return true;
}

// InputHandler.h
bool AppStateManager::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	if (isActive())
	{
		mCurrentNode->joyButtonReleased(evt, button);
		return false;
	}

	return true;
}

// InputHandler.h
bool AppStateManager::joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) 
{
	if (isActive())
	{
		mCurrentNode->joyAxisMoved(arg, axis);
		return false;
	}

	return true;
}

// InputHandler.h
bool AppStateManager::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
{
	if (isActive())
	{
		mCurrentNode->joyPovMoved(arg, index);
		return false;
	}

	return true;
}