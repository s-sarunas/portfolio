// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Shoot.h"

#include "Paddle.h"
#include "Scene.h"

const Ogre::String Shoot::msName = "Shoot";
Ogre::ulong Shoot::msBulletCount = NULL;

Shoot::Shoot(Paddle* user)
  : Ability(user, ability_traits<Shoot>::num_phases),
	mTopAnimation(getUser()->getCharacter()->getTopAnimation()),
	mTimer(Ogre::Timer()),
	mNumBulletsShot(NULL),
	mShooting(false)
{
	Ability::nextPhase();
}
 
Shoot::~Shoot()
{
}

void Shoot::_phaseChanged(const PhaseIndex newPhase)
{
	switch(newPhase)
	{
	case 1:
		mTopAnimation.changeTo(CHARACTER_ANIMATION_TOP_EQUIP_NAME, false);

		mNumBulletsShot = 0;
		mShooting = false;
		break;
	case 2:
		break;
	case 3:
		mTopAnimation.changeTo(CHARACTER_ANIMATION_TOP_EQUIP_NAME, false, true);
		getUser()->getCharacter()->getWeapon()->getFlash()->hide();
		break;
	case 4:
		mTopAnimation.changeTo(CHARACTER_ANIMATION_TOP_STAND_NAME, true);
		break;
	default:
		break;
	}
}

bool Shoot::process(const Ogre::Real deltaSeconds)
{
	// the destruction of Paddle should not be delayed until after the ability 
	// has ended therefore the weak_ptr is locked each time it is used as 
	// opposed to once when the ability is used
	Weapon* weapon = getUser()->getCharacter()->getWeapon();
	Flash* flash = weapon->getFlash();
	switch (getCurrentPhase())
	{
	case 1:	// set idle animation after equipping
		if (mTopAnimation.hasHalfEnded())
			weapon->attachToHandle();

		if (mTopAnimation.hasEnded())
			Ability::nextPhase();

		return true;
		break;
	case 2:	// shoot
		if (flash->getVisible() && 
			mTimer.getMilliseconds() > SHOOT_FLASH_DURATION_MS)
		{
			flash->hide();
		}

		if (mNumBulletsShot >= SHOOT_BULLET_INITIAL_COUNT)
			Ability::nextPhase();
		else if (mShooting)
			shootBullet();

		return true;
		break;
	case 3: // if all the bullets were shot, destruction events
		if (mTopAnimation.hasHalfEnded())
			weapon->attachToSheath();

		if (mTopAnimation.hasEnded())
			Ability::nextPhase();

		return true;
		break;
	case 4:
		return false;
		break;
	default:
		break;
	}

	return false;
}

void Shoot::startShooting()
{
	mTopAnimation.changeTo(CHARACTER_ANIMATION_TOP_SHOOT_NAME, false);
	mShooting = true;
}

void Shoot::shootBullet()
{
	if (mTimer.getMilliseconds() > SHOOT_COOLDOWN_MS)
	{
		Weapon* const weapon = getUser()->getCharacter()->getWeapon();

		mTopAnimation.reset(); // reset the shooting animation
		weapon->getFlash()->show(); // show the flash

		// create a bullet
		Ogre::Vector3 muzzlePosition, muzzleScale;
		Ogre::Quaternion muzzleOrientation;
		weapon->getMuzzleTransforms(
			muzzlePosition, 
			muzzleOrientation, 
			muzzleScale);

		const Ogre::Vector3 direction = 
			muzzleOrientation * OGRE_UNIT_VECTOR_FORWARD;

		ActorClass desc;
		desc.setName(ACTOR_BULLET_TYPE_NAME + '_' + 
			Ogre::StringConverter::toString(msBulletCount));
		desc.setType(ACTOR_BULLET_TYPE_NAME);
		desc.setEntity(SHOOT_BULLET_MESH_FILENAME);
		desc.setTransforms(muzzlePosition, muzzleOrientation, muzzleScale);
		desc.addParam("direction", direction, PARAMETER_TYPE_VECTOR3);
		desc.setScale(Ogre::Vector3(1.2f, 1.2f, 1.2f));

		getUser()->getCharacter()->createChild(desc);

		// set the parameters
		++mNumBulletsShot;
		++msBulletCount;

		mTimer.reset();
	}
}

// InputHandler.h
bool Shoot::keyPressed(const SDL_KeyboardEvent& arg)
{
	if (getCurrentPhase() == 2)
	{
		switch(arg.keysym.sym)
		{
		case SHOOT_BUTTON_KB_SHOOT:
			startShooting();
			break;
		case SHOOT_BUTTON_KB_EQUIP:
			Ability::nextPhase();
			break;
		default:
			break;
		}
	}

	return true;
}

// InputHandler.h
bool Shoot::keyReleased(const SDL_KeyboardEvent& arg)
{
	mShooting = false;

	return true;
}

// InputHandler.h
bool Shoot::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button)
{
	if (getCurrentPhase() == 2)
	{
		switch(button)
		{
		case SHOOT_BUTTON_JS_SHOOT:
			startShooting();
			break;
		case SHOOT_BUTTON_JS_EQUIP:
			Ability::nextPhase();
			break;
		default:
			break;
		}
	}

	return true;
}

// InputHandler.h
bool Shoot::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	mShooting = false;

	return true;
}
