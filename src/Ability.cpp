// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Ability.h"

Ability::Ability(Paddle* user, PhaseIndex numPhases) 
  : mCurrentPhase(ABILITY_PHASE_DISABLED),
	mUserPaddle(user),
	mMaxPhases(numPhases)
{
}

Ability::~Ability()
{
}

void Ability::setPhase(const PhaseIndex phase)
{
	assert(phase <= mMaxPhases && 
		"ability phase above the valid range");

	mCurrentPhase = phase;
	_phaseChanged(mCurrentPhase);
}

void Ability::nextPhase()
{
	if (mCurrentPhase == mMaxPhases)
		mCurrentPhase = ABILITY_PHASE_DISABLED;
	else
		_phaseChanged(++mCurrentPhase);
}

void Ability::previousPhase()
{
	assert(mCurrentPhase != ABILITY_PHASE_DISABLED && 
		"ability phase below the valid range");

	_phaseChanged(--mCurrentPhase);
}