// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Ray.h"

#include "InAppDebug.h"
#include "GameMath.h"

Ray<Actor>::Ray(const Actor* actor, const Ogre::Vector3& direction)
try
  : mName(actor->getName() + "_ray"),
	mParentActor(actor),
    mDirection(direction),
	mAdjustment(actor->getEntity()->getWorldBoundingBox(true).getHalfSize() * direction)
{
}
catch (...)
{
	OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, 
		"Ray cannot be created for an invalid actor.", 
		"Ray<Actor>::Ray");
}

Ray<Actor>::~Ray()
{
}

Ray<Actor>::Ray(const Ray& rhs, Actor* newParentActor)
  : mName(rhs.mName + "_copy"), // can throw
	mParentActor(newParentActor),
	mDirection(rhs.mDirection),
	mAdjustment(rhs.mAdjustment)
{
}

Ray<Actor>::Ray(Ray&& rhs) NOEXCEPT
  : mName(std::move(rhs.mName)),
	mParentActor(rhs.mParentActor),
	mDirection(rhs.mDirection),
	mAdjustment(rhs.mAdjustment)
{
	rhs.mParentActor = nullptr;
}

Ray<Actor>& Ray<Actor>::operator=(Ray&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		// free existing resources of the source
		// ...

		// copy data from the source object
		mName = std::move(rhs.mName);
		mParentActor = rhs.mParentActor;
		mDirection = rhs.mDirection;
		const_cast<Ogre::Vector3&>(mAdjustment) = rhs.mAdjustment; // not movable

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		rhs.mParentActor = nullptr;
	}

	return *this;
}

NODISCARD bool operator==(const Ray<Actor>& lhs, 
	const Ray<Actor>& rhs) NOEXCEPT
{
	return (lhs.getParentActor() == rhs.getParentActor() &&
			lhs.getDirection() == rhs.getDirection() &&
			lhs.getAdjustment() == rhs.getAdjustment());
}

NODISCARD bool operator!=(const Ray<Actor>& lhs, 
	const Ray<Actor>& rhs) NOEXCEPT
{
	return !(lhs == rhs);
}

const Ogre::Real Ray<Actor>::cast(const Ogre::Vector3& vert0, 
	const Ogre::Vector3& vert1, const Ogre::Vector3& vert2) const
{
	return Math::rayTriangleIntersection(getOrigin(), mDirection, 
		vert0, vert1, vert2);
}

const Ogre::Vector3 Ray<Actor>::getPoint(Ogre::Real t) NOEXCEPT 
{
	return getOrigin() + (mDirection * t);
}

std::ostream& operator<<(std::ostream& os, const Ray<Actor>& r) NOEXCEPT
{
	using std::endl;

	os << "Ray(" << r.mName << ")\n"
	   << "  Origin:\t" << r.getOrigin() << endl
	   << "    Adjustment:\t" << r.mAdjustment << endl
	   << "  Direction:\t" << r.mDirection << endl;

	return os;
}

////////////////////////////////////////////////////////////////////////////////

Ray<Ogre::Node>::Ray(const Ogre::Node* node, const Ogre::Vector3& direction, 
	const Ogre::Vector3& adjustment)
try
  : mName(node->getName() + "_ray"),
	mParent(node),
    mDirection(direction),
	mAdjustment(adjustment)
{
}
catch (...)
{
	OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, 
		"Ray cannot be created for an invalid scene node.", 
		"Ray<Ogre::Node>::Ray");
}

Ray<Ogre::Node>::~Ray()
{
}

Ray<Ogre::Node>::Ray(const Ray& rhs, Ogre::Node* parentNode)
  : mName(rhs.mName + "_copy"), // can throw
	mParent(parentNode),
	mDirection(rhs.mDirection),
	mAdjustment(rhs.mAdjustment)
{
}

Ray<Ogre::Node>::Ray(Ray&& rhs) NOEXCEPT
  : mName(std::move(rhs.mName)),
	mParent(rhs.mParent),
	mDirection(rhs.mDirection),
	mAdjustment(rhs.mAdjustment)
{
	rhs.mParent = nullptr;
}

Ray<Ogre::Node>& Ray<Ogre::Node>::operator=(Ray&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		// free existing resources of the source
		// ...

		// copy data from the source object
		mName = std::move(rhs.mName);
		mParent = rhs.mParent;
		mDirection = rhs.mDirection;
		mAdjustment = rhs.mAdjustment;

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		rhs.mParent = nullptr;
	}

	return *this;
}

NODISCARD bool operator==(const Ray<Ogre::Node>& lhs, 
	const Ray<Ogre::Node>& rhs) NOEXCEPT
{
	return (lhs.getParent() == rhs.getParent() &&
			lhs.getDirection() == rhs.getDirection() &&
			lhs.getAdjustment() == rhs.getAdjustment());
}

NODISCARD bool operator!=(const Ray<Ogre::Node>& lhs, 
	const Ray<Ogre::Node>& rhs) NOEXCEPT
{
	return !(lhs == rhs);
}

const Ogre::Real Ray<Ogre::Node>::cast(const Ogre::Vector3& vert0, 
	const Ogre::Vector3& vert1, const Ogre::Vector3& vert2) const
{
	return Math::rayTriangleIntersection(getOrigin(), mDirection, 
		vert0, vert1, vert2);
}

const Ogre::Vector3 Ray<Ogre::Node>::getPoint(Ogre::Real t) NOEXCEPT 
{
	return getOrigin() + (mDirection * t);
}

std::ostream& operator<<(std::ostream& os, const Ray<Ogre::Node>& r) NOEXCEPT
{
	using std::endl;

	os << "Ray(" << r.mName << ")\n"
	   << "  Origin:\t" << r.getOrigin() << endl
	   << "    Adjustment:\t" << r.mAdjustment << endl
	   << "  Direction:\t" << r.mDirection << endl;

	return os;
}

////////////////////////////////////////////////////////////////////////////////

Ray<Ogre::Vector3>::Ray(const Ogre::String& name, const Ogre::Vector3& origin, 
	const Ogre::Vector3& direction)
  : mName(name),
    mOrigin(origin),
    mDirection(direction)
{
}

Ray<Ogre::Vector3>::~Ray()
{
}

Ray<Ogre::Vector3>::Ray(const Ray& rhs)
  : mOrigin(Ogre::Vector3::ZERO),
	mDirection(Ogre::Vector3::ZERO)
{
	*this = rhs; // reuse
}

Ray<Ogre::Vector3>& Ray<Ogre::Vector3>::operator=(const Ray& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class copy assignment operator
		// ...

		// copy data from the source object
		mName = rhs.mName + "_copy";
		mOrigin = rhs.mOrigin;
		mDirection = rhs.mDirection;
	}

	return *this;
}

Ray<Ogre::Vector3>::Ray(Ray&& rhs) NOEXCEPT
  : mOrigin(Ogre::Vector3::ZERO),
	mDirection(Ogre::Vector3::ZERO)
{
	*this = std::move(rhs); // reuse
}

Ray<Ogre::Vector3>& Ray<Ogre::Vector3>::operator=(Ray&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		// free existing resources of the source
		// ...

		// copy data from the source object
		mName = std::move(rhs.mName);
		mOrigin = std::move(rhs.mOrigin);
		mDirection = std::move(rhs.mDirection);

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		// ...
	}

	return *this;
}

NODISCARD bool operator==(const Ray<Ogre::Vector3>& lhs, 
	const Ray<Ogre::Vector3>& rhs) NOEXCEPT
{
	return (lhs.getOrigin() == rhs.getOrigin() &&
			lhs.getDirection() == rhs.getDirection());
}

NODISCARD bool operator!=(const Ray<Ogre::Vector3>& lhs, 
	const Ray<Ogre::Vector3>& rhs) NOEXCEPT
{
	return !(lhs == rhs);
}

const Ogre::Real Ray<Ogre::Vector3>::cast(const Ogre::Vector3& vert0, 
	const Ogre::Vector3& vert1, const Ogre::Vector3& vert2) const
{
	return Math::rayTriangleIntersection(mOrigin, mDirection,
		vert0, vert1, vert2);
}

const Ogre::Vector3 Ray<Ogre::Vector3>::getPoint(Ogre::Real t) NOEXCEPT 
{
	return mOrigin + (mDirection * t);
}

std::ostream& operator<<(std::ostream& os, const Ray<Ogre::Vector3>& r) NOEXCEPT
{
	using std::endl;

	os << "Ray(" << r.mName << ")\n"
	   << "  Origin:\t" << r.mOrigin << endl
	   << "  Direction:\t" << r.mDirection << endl;

	return os;
}