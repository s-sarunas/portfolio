// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "AbilitiesManager.h"

AbilitiesManager::AbilitiesManager()
{
}

AbilitiesManager::~AbilitiesManager()
{
}

void AbilitiesManager::update(const Ogre::Real deltaSeconds)
{
	if (!mActiveAbilities.empty())
	{
		mActiveAbilities.erase(std::remove_if(
			mActiveAbilities.begin(), mActiveAbilities.end(), 
			[deltaSeconds](const AbilityPtrList::value_type& element) {
				return (!element->process(deltaSeconds));
			}
		), mActiveAbilities.end());
	}
}

// InputHandler.h
bool AbilitiesManager::mouseMoved(const SDL_MouseMotionEvent& arg)  
{
	for_each_active_ability(
		[&](Ability* ability) {
			ability->mouseMoved(arg); 
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	for_each_active_ability(
		[&](Ability* ability) { 
			ability->mousePressed(arg, id); 
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	for_each_active_ability(
		[&](Ability* ability) { 
			ability->mouseReleased(arg, id); 
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::textInput(const SDL_TextInputEvent& arg) 
{
	for_each_active_ability(
		[&](Ability* ability) { 
			ability->textInput(arg); 
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::keyPressed(const SDL_KeyboardEvent& arg) 
{
	for_each_active_ability(
		[&](Ability* ability) { 
			ability->keyPressed(arg);
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::keyReleased(const SDL_KeyboardEvent& arg) 
{
	for_each_active_ability(
		[&](Ability* ability) { 
			ability->keyReleased(arg); 
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
{
	for_each_active_ability(
		[&](Ability* ability) { 
			ability->joyButtonPressed(evt, button); 
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	for_each_active_ability(
		[&](Ability* ability) { 
			ability->joyButtonReleased(evt, button);
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) 
{
	for_each_active_ability(
		[&](Ability* ability) { 
			ability->joyAxisMoved(arg, axis); 
		}
	);

	return true;
}

// InputHandler.h
bool AbilitiesManager::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
{
	for_each_active_ability(
		[&](Ability* ability) {
			ability->joyPovMoved(arg, index);
		}
	);

	return true;
}