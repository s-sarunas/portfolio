// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "StaticActor.h"

StaticActor::StaticActor(const ActorClass& desc)
  : Actor(desc)
{
}

StaticActor::~StaticActor()
{
}

StaticActor::StaticActor(StaticActor&& rhs) NOEXCEPT
  : Actor(std::move(rhs))
{
}

StaticActor& StaticActor::operator=(StaticActor&& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		Actor::operator=(std::move(rhs));

		// free existing resources of the source
		// ...

		// copy data from the source object
		// ...

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		// ...
	}

	return *this;
}

std::shared_ptr<Actor> StaticFactory::createInstance(const ActorClass& desc)
{
	assert(!desc.hasParams() && 
		"static actor's additional params should be empty");

	return std::make_shared<StaticActor>(desc);
}