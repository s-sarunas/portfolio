// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Dash.h"

#include "DynamicActor.h"
#include "Paddle.h"
#include "Border.h"

// CollisionManager::Listener
void Dash::collisionOccured(Collidable* one, Collidable* two)
{
	if (Ability::isActive())
	{
		if (one == getUser())
		{
			if (two->getActorType() == ACTOR_BORDER_TYPE_NAME)
			{
				reverseDirection();
			}
		}
	}
}

const Ogre::String Dash::msName = "Dash";

Dash::Dash(Paddle* user)
  : Ability(user, ability_traits<Dash>::num_phases),
	mAcceleration(NULL),
	mInitialMovementSpeed(getUser()->getMovementSpeed()),
	mInitialBorder(getBorder())
{
	CollisionManager::getSingleton().addListener(this);

	if (mInitialBorder)
		nextPhase();
}

Dash::~Dash()
{
	CollisionManager::getSingleton().removeListener(this);
}

void Dash::_phaseChanged(const PhaseIndex newPhase)
{
	switch(newPhase)
	{
	case 1:
	{
		getUser()->stop();
		getUser()->setMovementSpeed(
			RNG(DASH_SPEED_INITIAL_MIN, DASH_SPEED_INITIAL_MAX));

		// deceleration
		mAcceleration = -(getUser()->getMovementSpeed() / DASH_DURATION_SECONDS); 

		// move away from the border
		const Ogre::Vector3 awayVector = 
			getUser()->getNode()->getPosition() -		// user's position
			mInitialBorder->getNode()->getPosition();	// border's position

		Ogre::Vector3 direction = awayVector * getUser()->getRight();
		direction.normalise();

		getUser()->setDirection(direction, TRANSFORM_SPACE_WORLD);
		getUser()->setAutoMove(true);

		getUser()->getAnimation().changeTo(PADDLE_ANIMATION_JUMP_NAME, false);
		
		Ability::nextPhase();
		break;
	}
	case 2:
		break;
	case 3:
		getUser()->start();
		getUser()->setAutoMove(false);
		getUser()->setMovementSpeed(mInitialMovementSpeed);
		break;
	default:
		break;
	}
}

bool Dash::process(const Ogre::Real deltaSeconds)
{
	switch(getCurrentPhase())
	{
	case 1:
		break;
	case 2: 
		getUser()->addMovementSpeed(mAcceleration * deltaSeconds);

		getUser()->move(deltaSeconds, true);

		if (getUser()->getMovementSpeed() < 0)
			Ability::nextPhase();

		return true;
		break;
	case 3:
		return false;
		break;
	default:
		break;
	}

	return false;
}

Border* Dash::getBorder()
{
	if (getUser()->hasCollisions())
	{
		for (const auto& pair : getUser()->getCollisions())
		{
			if (pair.first->getActorType() == ACTOR_BORDER_TYPE_NAME)
				return dynamic_cast<Border*>(pair.first);
		}
	}
	
	return nullptr;
}

void Dash::reverseDirection()
{
	getUser()->setDirection(-getUser()->getWorldDirection(), 
		TRANSFORM_SPACE_WORLD);
}