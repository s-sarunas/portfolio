// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Character.h"

#include "InAppDebug.h"

Character::Character(const ActorClass& desc, const Ogre::String& weaponFilename)
  : Actor(desc),
	StaticActor(desc),
	Collidable(desc.getTypeFlag()),
	Animated(getEntity(), 
		CHARACTER_ANIMATION_TOP_STAND_NAME, 
		CHARACTER_ANIMATION_BOT_STAND_NAME),
	mWeapon(nullptr)
{
	ActorClass weaponDesc;
	weaponDesc.setName(ACTOR_WEAPON_TYPE_NAME + '_' + desc.getName());
	weaponDesc.setType(ACTOR_WEAPON_TYPE_NAME);
	weaponDesc.setEntity(weaponFilename);
	weaponDesc.setTagPoint(getEntity(), CHARACTER_BONE_SHEATH_NAME);
	weaponDesc.addParam("handle_bone_name", CHARACTER_BONE_HANDLE_NAME, 
		PARAMETER_TYPE_STRING);

	mWeapon = dynamic_cast<Weapon*>(createChild(weaponDesc));

	getNode()->scale(0.8f, 0.8f, 0.8f);
}

Character::~Character()
{
}

Character::Character(Character&& rhs)
  : Actor(std::move(rhs)),
    StaticActor(std::move(rhs)),
	Collidable(std::move(rhs)),
	Animated(std::move(rhs)),
	mWeapon(rhs.mWeapon)
{
	Actor::removeChild(mWeapon);

	rhs.mWeapon = nullptr;
}

Character& Character::operator=(Character&& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		Actor::operator=(std::move(rhs));
		StaticActor::operator=(std::move(rhs));
		Collidable::operator=(std::move(rhs));
		Animated::operator=(std::move(rhs));

		// free existing resources of the source
		Actor::removeChild(mWeapon);

		// copy data from the source object
		mWeapon = rhs.mWeapon;

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		rhs.mWeapon = nullptr;
	}

	return *this;
}

const Ogre::Vector3 Character::getBonePosition(const Ogre::String& boneName) const
{
	return (
		getNode()->_getDerivedOrientation() 
		* 
		(getEntity()->getSkeleton()->getBone(boneName)->_getDerivedPosition() * 
			getNode()->_getDerivedScale())
		+ 
		getNode()->_getDerivedPosition());
}

void Character::update(const Ogre::Real deltaSeconds)
{
	StaticActor::update(deltaSeconds);
	Animated::update(deltaSeconds);

	// if not moving, stand 
	if (getBotAnimation().hasEnded())
		getBotAnimation().changeTo(CHARACTER_ANIMATION_BOT_STAND_NAME);
}

void Character::handleCollision(Collidable* collidable, bool isColliding)
{
}

std::shared_ptr<Actor> CharacterFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Character>(desc, 
		desc.getParam("weapon_filename"));
}
