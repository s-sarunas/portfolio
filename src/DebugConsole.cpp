// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "DebugConsole.h"

#include "InAppDebug.h"
#include "Core.h"
#include "Scene.h"
#include "AppStateManager.h"
#include "CollisionManager.h"
#include "Region.h"
#include "Butterfly.h"
#include "Paddle.h"
#include "Ball.h"

DebugConsole* DebugConsole::msSingleton = nullptr;

DebugConsole::DebugConsole()
  : mOperators(initOperators()),
	mSpecialStrings({{
		DEBUG_CONSOLE_NEW_COMMAND_STRING, 
		DEBUG_CONSOLE_CURSOR_STRING, 
		DEBUG_CONSOLE_HELP_STRING}}),
	mRunning(true),
	mActive(false),
	mOutputClosed(true)
{
    msSingleton = this;

	mThread = std::make_unique<std::thread>(&DebugConsole::run, this);
}

DebugConsole::~DebugConsole()
{
	try {
		mRunning = false;
		mMessageAvailable.notify_one();

		mThread->join();
	} 
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy debug console");
	}

	msSingleton = nullptr;
}

DebugConsole* DebugConsole::getSingletonPtr() NOEXCEPT
{
    return msSingleton;
}

DebugConsole& DebugConsole::getSingleton()
{
    assert(msSingleton);
    return (*msSingleton);
}

DebugConsole& DebugConsole::operator<<(char c) NOEXCEPT
{
	try {
		addToMessageQueue(std::string(1, c));
	}
	catch (...) {
		// do nothing, avoid exceptions in error logging and allow this 
		// in noexcept functions like the destructor
	}

	return *this;
}

DebugConsole& DebugConsole::operator<<(const char* str) NOEXCEPT
{
	try {
		addToMessageQueue(std::move(std::string(str)));
	}
	catch (...) {
		// do nothing, avoid exceptions in error logging and allow this 
		// in noexcept functions like the destructor
	}

	return *this;
}

DebugConsole& DebugConsole::operator<<(char* str) NOEXCEPT
{
	try {
		addToMessageQueue(std::move(std::string(str)));
	}
	catch (...) {
		// do nothing, avoid exceptions in error logging and allow this 
		// in noexcept functions like the destructor
	}

	return *this;
}

DebugConsole& DebugConsole::operator<<(const Ogre::String& str) NOEXCEPT
{
	addToMessageQueue(str);
	return *this;
}

DebugConsole& DebugConsole::operator<<(Ogre::String&& str) NOEXCEPT
{
	addToMessageQueue(std::move(str));
	return *this;
}

DebugConsole& DebugConsole::operator<<(const std::vector<std::string>& vec) NOEXCEPT
{
	try {
		for (const auto& str : vec)
		{
			addToMessageQueue(str + ' ');
		}
	}
	catch (...) {
		// do nothing, avoid exceptions in error logging and allow this 
		// in noexcept functions like the destructor
	}

	return *this;
}

DebugConsole& DebugConsole::operator<<(const std::ostringstream& oss) NOEXCEPT
{
	try {
		addToMessageQueue(std::move(oss.str())); // std::stringbuf::str copies
	}
	catch (...) {
		// do nothing, avoid exceptions in error logging and allow this 
		// in noexcept functions like the destructor
	}

	return *this;
}

DebugConsole& DebugConsole::operator<<(const std::ostream& os) NOEXCEPT
{
	try {
		std::streambuf* sbuf = os.rdbuf();
		std::streambuf::pos_type size = sbuf->pubseekoff(0, os.end);
		sbuf->pubseekoff(0, os.beg); // rewind
		std::unique_ptr<char[]> buffer(new char[static_cast<size_t>(size)]);
		sbuf->sgetn(buffer.get(), size);

		*this << buffer.get();
	}
	catch (...) {
		// do nothing, avoid exceptions in error logging and allow this 
		// in noexcept functions like the destructor
	}

	return *this;
}

void DebugConsole::closeOutput() NOEXCEPT
{
	// manually close the output stream
	mOutputClosed = true;
	cout << endl;
	newLine();
}

void DebugConsole::setEnabled(bool enabled) NOEXCEPT
{
	// done by the calling thread
	mActive.exchange(enabled);

	if (enabled)
		cout << mSpecialStrings[SPECIAL_STR_CURSOR];
	else
		clearCursor();
}

void DebugConsole::clearLine()
{
	cout << '\r';

	// this is done at compile-time if at least /O2 or /O1 with /Oi compiler 
	// optimisation flags are enabled (also works with strlen()) (compile-time 
	// by default on GCC with strlen()), cannot however be declared constexpr 
	// in C++17 to assure that, because functions used are not constexpr 
	// (even under the optimisations)
	const size_t additionalSize = 
		std::char_traits<char>::length(mSpecialStrings[SPECIAL_STR_COMMAND]) + 
		std::char_traits<char>::length(mSpecialStrings[SPECIAL_STR_CURSOR]);

	std::fill_n(std::ostream_iterator<char>(std::cout),
		mRunningInput.length() + additionalSize, ' ');

	cout << '\r';
}

void DebugConsole::clearCursor() NOEXCEPT
{
	cout << "\b \b";
}

void DebugConsole::nextLine() NOEXCEPT
{
	cout << endl;
	newLine();
}

void DebugConsole::newLine() NOEXCEPT
{
	cout << mSpecialStrings[SPECIAL_STR_COMMAND] << mRunningInput;
	
	if (mActive) 
		cout << mSpecialStrings[SPECIAL_STR_CURSOR];
}

void DebugConsole::init()
{
	// create a console with iostream
    if (!AllocConsole())
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_INTERNAL_ERROR, 
			"A console window cannot be allocated.",
			"DebugConsole::init");
	}

	// maximise the console
	ShowWindow(GetConsoleWindow(), SW_SHOWMAXIMIZED);

	// do not sync with C-style input libraries
	std::ios_base::sync_with_stdio(false); 
	// do not tie cin with cout - not using cin for input
	std::cin.tie(NULL); 

	// open the console for output
	mConsoleOutStream.open("CONOUT$", std::ofstream::out | std::ofstream::binary); 
	// set the persistent flag unitbuf to force flushing after every 
	// output operation (not set by default)
	cout.setf(std::ios::unitbuf); 
	// redirect global cout to the console
	std::cout.rdbuf(mConsoleOutStream.rdbuf()); 

	// std::bind(&ClassName::memberFunctionName, this) for member functions
	// std::invoke can be used here to avoid explicit binding in C++17
	mChords.emplace_back("autocomplete", 
		Chord::Keys{SDLK_LCTRL, SDLK_SPACE}, 
		[this]() {
			Ogre::StringVector processedInput = processInput(mRunningInput);
			if (!mRunningInput.empty() && 
				(processedInput.size() == 1 && mRunningInput.back() == ' ' ||
				processedInput.size() == 2 && mRunningInput.back() != ' '))
			{ // autocomplete operands
				auto it = mOperators.find(processedInput.front());
				if (it != mOperators.end())
				{ // if a valid operand exists
					autoComplete<OperandType>(it->second, processedInput);
				}
			}
			else if (mRunningInput.empty() || 
				processedInput.size() == 1 && 
				!mRunningInput.empty() && mRunningInput.back() != ' ')
			{ // autocomplete operators
				autoComplete<OperatorType>(mOperators, processedInput);
			}
		}
	);

	mChords.emplace_back("clear_input", 
		Chord::Keys{SDLK_LCTRL, SDLK_x}, 
		[this]() {
			clearLine();
			mRunningInput.clear();
			mHistory.reset();
			newLine();
		}
	);

	mChords.emplace_back("interrupt", 
		Chord::Keys{SDLK_LCTRL, SDLK_c}, 
		std::bind(&DebugConsole::closeOutput, this));

	mChords.emplace_back("help", 
		Chord::Keys{SDLK_LCTRL, SDLK_h}, 
		[this]() {
			clearCursor();
			cout << '\n';
			printHelp();
			newLine();
		}
	);

	mChords.emplace_back("delete_previous_word", 
		Chord::Keys{SDLK_LCTRL, SDLK_BACKSPACE}, 
		[this]() {
			clearLine();

			const size_t pos = mRunningInput.find_last_of(" ");
			if (pos != mRunningInput.npos)	
				mRunningInput.erase(pos, mRunningInput.npos);
			else
				mRunningInput.clear();

			newLine();
		}
	);
}

DebugConsole::OperatorType DebugConsole::initOperators() NOEXCEPT
{
	// NOTE: nothing is captured by the lambdas, so a C-style function pointer 
	// can also be used instead of an std::function
	// NOTE: additional braces could be removed in C++17
	return OperatorType{
		{"output", OperandType{
			{"scene_hierarchy", [](const Ogre::StringVector&) {
				InAppDebug::outputSceneHierarchy(); 
			}},
			{"scene_actors", [](const Ogre::StringVector&) {
				Scene* access = Scene::getSingletonPtr();
				if (access) 
					InAppDebug::outputActors(access->getActorCollection());
				else
					cout << "Scene is not active.";
			}},
			{"collision_actors", [](const Ogre::StringVector&) {
				CollisionManager* access = CollisionManager::getSingletonPtr();
				if (access)
					InAppDebug::outputActors(access->getCollidables());
				else
					cout << "CollisionManager is not active.";
			}},
			/*{"custom_parameter", [](const Ogre::StringVector& args) {
				if (!args.empty())
				{
					InAppDebug::outputCustomParameter(
						Ogre::StringConverter::parseUnsignedInt(args.front()));
				}
				else
					cout << "Arguments: [parameter_index].";
			}},*/
			{"configuration", [](const Ogre::StringVector&) {
				std::stringstream ss;
				ss << getConfig();

				InAppDebug::outputTable({"Name", "Value", "Type"}, ss);
			}},
			{"average_time", [](const Ogre::StringVector&) { 
				gsDebugTimer.outputAverage();
			}},
			{"resource_leaks", [](const Ogre::StringVector&) { 
				Core::getSingleton().checkResourceLeaks();
			}},
		}},
		{"add", OperandType{
			{"entity", [](const Ogre::StringVector& args) {
				if (args.size() == 2)
				{
					Ogre::MovableObject* movableObject = nullptr;
					for (const auto& factory : 
						Ogre::Root::getSingleton().getMovableObjectFactoryIterator())
					{
						for (const auto& movableObj : 
							getSceneManager()->getMovableObjectIterator(factory.first))
						{
							if (movableObj.first == args[1])
								movableObject = movableObj.second;
						}
					}

					if (movableObject)
					{
						Ogre::SceneNode* sceneNode = 
							getSceneManager()->getSceneNode(args[0]);

						if (sceneNode)
							sceneNode->attachObject(movableObject);
						else
						{
							cout << "Scene node with name " << args[0] << 
								"could not be found";
						}
					}
					else
					{
						cout << "Entity with name " << args[1] << 
							"could not be found";
					}
				}	
				else
					cout << "Arguments: [scene_node_name] [entity_name]";
			}},
			{"object", [](const Ogre::StringVector& args) {
				if (!args.empty())
				{
					if (args.size() == 3)
					{
						Ogre::Vector3 vec3(
							Ogre::StringConverter::parseReal(args[0]),
							Ogre::StringConverter::parseReal(args[1]),
							Ogre::StringConverter::parseReal(args[2]));

						if (InAppDebug::getSingleton().addDebugObject(vec3))
							cout << "Object added at: " << vec3 << ".";
					}
					else
					{
						Scene* access = Scene::getSingletonPtr();
						if (access) 
						{
							try {
								Ogre::SceneNode* sceneNode = 
									getSceneManager()->getSceneNode(args[0]);
								if (InAppDebug::getSingleton().addDebugObject(sceneNode))
								{
									cout << "Object parented to scene node \"" << 
										sceneNode->getName() << "\".";
								}
							}
							catch (const Ogre::Exception&) {
								cout << "A scene node with name \"" << 
									args.front() << "\" was not found.";
							}
						}
						else
							cout << "Scene is not active.";
					}
				}
				else
					cout << "Arguments: [actor_name/position (format: x y z)].";
			}},
			{"arrow", [](const Ogre::StringVector& args) {
				if (!args.empty())
				{
					Scene* access = Scene::getSingletonPtr();
					if (access) 
					{
						Actor* actor = access->getActor(args.front());
						if (actor)
						{
							if (InAppDebug::getSingleton().addArrow(actor->getSceneNode()))
							{
								cout << "Arrow parented to actor \"" << 
									actor->getName() << "\".";
							}
						}
						else
						{
							cout << "Actor with name \"" << args.front() << 
								"\" does not exist.";
						}
					}
					else
						cout << "Scene is not active.";
				}
				else
					cout << "Arguments: [actor_name].";
			}},
			{"billboard", [](const Ogre::StringVector& args) {
				if (args.size() != 2)
				{
					Scene* access = Scene::getSingletonPtr();
					if (access) 
					{
						Actor* actor = access->getActor(args.front());
						if (actor)
						{
							if (InAppDebug::getSingleton().addBillboard(
								actor->getSceneNode(), args.back()))
							{
								cout << "Billboard parented to actor \"" << 
									actor->getName() << "\".";
							}
						}
						else
						{
							cout << "Actor with name \"" << args.front() <<
								"\" does not exist.";
						}
					}
					else
						cout << "Scene is not active.";
				}
				else
				{
					cout << "Arguments: [actor_name] [billboard_material_name] "
						"(e.g. Light1 light_billboard).";
				}
			}},
			{"compositor", [](const Ogre::StringVector& args) {
				if (args.size() > 0)
				{
					Ogre::CompositorManager* manager = 
						Ogre::CompositorManager::getSingletonPtr();
					Ogre::Viewport* viewport = getCamera()->getViewport();

					int position = -1;
					if (args.size() == 2)
						position = Ogre::StringConverter::parseInt(args[1], -1);

					Ogre::CompositorInstance* instance = 
						manager->addCompositor(viewport, args[0], position);

					if (instance) 
					{
						instance->setEnabled(true);
						cout << "Compositor added to position " << position << 
							" (where -1 is the end).";
					}
					else
						cout << "Compositor not found.";
				}
				else
				{
					cout << "Compositor name and (optional) position in the "
						"compositor chain required.\nAvailable compositors: "
						"BlackAndWhite";
				}
			}},
		}},
		{"call", OperandType{
			{"debug_break", [](const Ogre::StringVector&) {
#ifdef _DEBUG
				DebugBreak();
#else
				cout << "Debug break is disabled on non-debug configurations.";
#endif
			}},
		}},
		{"toggle", OperandType{
			{"all_bounding_boxes", [](const Ogre::StringVector&) { 
				bool result = !getSceneManager()->getShowBoundingBoxes();
				getSceneManager()->showBoundingBoxes(result);
				cout << "Bounding boxes " << DEBUG_CONSOLE_TO_STRING_ENABLE(result);
			}},
			{"batch_bounding_boxes", [](const Ogre::StringVector&) { 
				bool visible = false;

				Scene* access = Scene::getSingletonPtr();
				if (access)
				{
					visible = 
						access->getLoader()->toggleStaticGeometryBoundingBoxes();
				}
				else
					cout << "Scene is not active.";

				cout << "StaticGeometry bounding boxes " << 
					DEBUG_CONSOLE_TO_STRING_ENABLE(visible);
			}},
			{"debug_objects", [](const Ogre::StringVector&) {
				// no need to check for debugAccess validity, 
				// since InAppDebug manages DebugConsole
				InAppDebug* debugAccess = InAppDebug::getSingletonPtr();

				// all objects managed by InAppDebug (arrows, billboards etc.)
				debugAccess->flipObjectVisibility();

				bool visible = debugAccess->getObjectsVisible();
				Scene* sceneAccess = Scene::getSingletonPtr();
				if (sceneAccess) 
				{
					// debuggables
					sceneAccess->for_each_actor<Debuggable>(
						[visible](Debuggable* debuggable) {
							debuggable->setDebuggableVisibility((visible));
						}
					);
				}
				else
				{
					cout << "Scene is not active, objects under its control "
						"were not affected.\n";
				}

				cout << "Debug objects are now " << 
					DEBUG_CONSOLE_TO_STRING_VISIBLE(visible);
			}},
			{"debug_table", [](const Ogre::StringVector& args) {
				bool result = InAppDebug::getSingleton().getTable()->toggle();
				cout << "Debug table is " << 
					DEBUG_CONSOLE_TO_STRING_VISIBLE(result);
			}},
			{"axes_xray", [](const Ogre::StringVector& args) {
				auto access = Ogre::MaterialManager::getSingletonPtr();
				if (access)
				{
					Ogre::Pass* pass = nullptr;
					bool enabled = false;
					for (const auto& materialName : {"x", "y", "z"})
					{ // disable depth checking - script inheritance is not 
					  // active after loading
						// hardcoded indices
						pass = access->getByName(materialName)->
							getTechnique(0)->getPass(0);
						enabled = !pass->getDepthCheckEnabled();

						pass->setDepthCheckEnabled(enabled);
					}

					cout << "Debug axes' x-ray " << 
						DEBUG_CONSOLE_TO_STRING_ENABLE(!enabled);
				}
				else
					cout << "MaterialManager is not active.";
			}},
			{"showcase", [](const Ogre::StringVector& args) {
				if (!args.empty())
				{
					const Ogre::String& showcase = args.front();
					if (showcase == "1")
					{
						Scene* access = Scene::getSingletonPtr();
						if (access) 
						{
							ActorPtrList* list = 
								access->getActorList(ACTOR_BUTTERFLY_TYPE_NAME);
							if (list && !list->empty())
							{
								const bool enabled = dynamic_cast<Butterfly*>(
									list->front().get())->_toggleShowcase();

								cout << "Showcase number 1 " << 
									DEBUG_CONSOLE_TO_STRING_ENABLE(enabled);
							}
							else
							{
								cout << "An actor of type \"Butterfly\" is "
									"needed for this showcase.";
							}
						}
						else
							cout << "Scene is not active.";
					}
					else
						cout << "Unrecognised showcase.";
				}
				else
					cout << "Arguments: [showcase_index].";
			}},
		}},
		{"set", OperandType{
			{"num_updates", [](const Ogre::StringVector& args) {
				Core* access = Core::getSingletonPtr();
				if (access) 
				{
					if (!args.empty())
					{
						int numUpdates = 
							Ogre::StringConverter::parseInt(args.front());

						access->_setDebugNumUpdates(numUpdates);
						cout << "Num updates = " << numUpdates;
					}
					else
						cout << "Arguments: [number_of_updates].";
				}
				else
					cout << "Core singleton is not valid.";
			}},
			{"fps_limit", [](const Ogre::StringVector& args) {
				Core* access = Core::getSingletonPtr();
				if (access) 
				{
					if (!args.empty())
					{
						const Ogre::Real newLimit = 
							Ogre::StringConverter::parseReal(args.front());

						access->setFpsLimit(newLimit);
						cout << "Fps limit = " << newLimit;
					}
					else
						cout << "Arguments: [fps_limit].";
				}
				else
					cout << "Core singleton is not valid.";
			}},
			{"score", [](const Ogre::StringVector& args) {
				Scene* access = Scene::getSingletonPtr();
				if (access) 
				{
					if (args.size() == 2)
					{
						Paddle* paddle = 
							dynamic_cast<Paddle*>(access->getActor(args[0]));
						if (paddle)
						{
							paddle->setScore(
								Ogre::StringConverter::parseUnsignedInt(args[1]));

							cout << "Score set.";
						}
						else
						{
							cout << "Actor with name \"" + args[0] + 
								"\" could not be found or is not a paddle.";
						}
					}
					else
						cout << "Arguments: [paddle_actor_name] [score].";
				}
				else
					cout << "Scene is not active.";
			}},
			{"movement_speed", [](const Ogre::StringVector& args) {
				Scene* access = Scene::getSingletonPtr();
				if (access) 
				{
					if (args.size() == 2)
					{
						Actor* actor = access->getActor(args[0]);
						DynamicActor* dynamic = 
							dynamic_cast<DynamicActor*>(actor);
						if (dynamic)
						{
							dynamic->setMovementSpeed(
								Ogre::StringConverter::parseReal(args[1]));

							cout << "Speed base set.";
						}
						else
						{
							cout << "Actor with name \"" + args[0] + 
								"\" could not be found or is not a dynamic actor.";
						}
					}
					else
						cout << "Arguments: [dynamic_actor_name] [base_speed].";
				}
				else
					cout << "Scene is not active.";
			}},
			{"visibility", [](const Ogre::StringVector& args) {
				Scene* access = Scene::getSingletonPtr();
				if (access) 
				{
					if (args.size() == 2)
					{
						Actor* actor = access->getActor(args[0]);
						if (actor)
						{
							actor->setVisible(
								Ogre::StringConverter::parseBool(args[1]));

							cout << "Actor with name \"" << actor->getName() << 
								"\" is now " << DEBUG_CONSOLE_TO_STRING_VISIBLE(
								actor->getVisible());
						}
						else
						{
							cout << "Actor with name \"" + args[0] + 
								"\" could not be found.";
						}
					}
					else
						cout << "Arguments: [actor_name] [boolean].";
				}
				else
					cout << "Scene is not active.";
			}},
			{"collision_output", [](const Ogre::StringVector& args) {
				CollisionManager* access = CollisionManager::getSingletonPtr();
				if (access) 
				{
					if (!args.empty())
					{
						bool flag = Ogre::StringConverter::parseBool(args.front());
						access->setDebugOutput(flag);

						cout << "Collision manager's debug output set to " << 
							std::boolalpha << flag;
					}
					else
						cout << "Arguments: [boolean].";
				}
				else
					cout << "CollisionManager singleton is not valid.";
			}},
			{"node_position", [](const Ogre::StringVector& args) {
				if (args.size() == 4)
				{
					Scene* access = Scene::getSingletonPtr();
					if (access) 
					{
						try {
							const Ogre::Vector3 position = Ogre::Vector3(
								Ogre::StringConverter::parseReal(args[1]),
								Ogre::StringConverter::parseReal(args[2]),
								Ogre::StringConverter::parseReal(args[3]));

							Ogre::SceneNode* result = eOgre::find_child_scene_node(
								access->getRootSceneNode(), 
								[&args](const Ogre::SceneNode* sceneNode) {
									return (sceneNode->getName() == args[0]);
								}
							);

							if (result)
								result->setPosition(position);
							else
							{
								cout << "Actor with name \"" << args[0] << 
									"\" not found";
							}
						} 
						catch (const Ogre::Exception& e) {
							cout << "Set actor_position failed: " <<
								e.getFullDescription();
						}
					}
					else
						cout << "Scene is not active.";
				}
				else
					cout << "Arguments: [actor_name] [position (format: x y z)].";
			}},
		}},
		{"update", OperandType{
			{"scene", [](const Ogre::StringVector& args) { 
				Scene* access = Scene::getSingletonPtr();
				if (access) 
				{
					if (!args.empty())
					{
						Ogre::Real deltaSeconds = 
							Ogre::StringConverter::parseReal(args.front());

						access->update(deltaSeconds);

						cout << "Scene updated with deltaSeconds = " << 
							deltaSeconds << ".";
					}
					else
						cout << "Arguments: [delta_seconds].";
				}
				else
					cout << "Scene is not active.";
			}}
		}},
		{"reset", OperandType{
			{"scene", [](const Ogre::StringVector&) {
				Scene* access = Scene::getSingletonPtr();
				if (access) 
				{
					access->reset();
					cout << "Scene reset.";
				}
				else
					cout << "Scene is not active.";
			}},
			{"camera", [](const Ogre::StringVector&) {
				InAppDebug::getSingleton().getCameraMan().reset();
				cout << "Camera reset.";
			}}
		}},
		{"destroy", OperandType{
			{"actor", [](const Ogre::StringVector& args) {
				if (!args.empty())
				{
					Scene* access = Scene::getSingletonPtr();
					if (access) 
					{
						Actor* actor = access->getActor(args.front());
						if (actor)
						{
							actor->setDestroyFlag(true);
							cout << "The actor was destroyed.";
						}
						else
						{
							cout << "Actor with name \"" << args.front() << 
								"\" does not exist.";
						}
					}
					else
						cout << "Scene is not active.";
				}
				else
					cout << "Arguments: [actor_name].";
			}},
		}},
		{"create", OperandType{
			{"paddle", [](const Ogre::StringVector& args) {
				if (args.size() == 6)
				{
					Scene* access = Scene::getSingletonPtr();
					if (access) 
					{
						try {
							const Ogre::Vector3 position = Ogre::Vector3(
								Ogre::StringConverter::parseReal(args[2]),
								Ogre::StringConverter::parseReal(args[3]),
								Ogre::StringConverter::parseReal(args[4]));

							const bool isAI = 
								Ogre::StringConverter::parseBool(args[3], false);

							ActorClass desc;
							desc.setName(args[0]);
							desc.setEntity(args[1]);
							desc.setTranslate(position);

							access->createPaddle(desc, isAI, "fox.mesh");
						} 
						catch (const Ogre::Exception& e) {
							cout << "Create actor failed: " << 
								e.getFullDescription();
						}
					}
					else
						cout << "Scene is not active.";
				}
				else
				{
					cout << "Arguments: [actor_name] [filename.mesh] [position "
						"(format: x y z)] [is_ai (boolean)].";
				}
			}},
		}},
		{"clear", OperandType{
			{"compositor_chain", [](const Ogre::StringVector&) {
				Ogre::Viewport* viewport = getCamera()->getViewport();
				Ogre::CompositorChain* chain = 
					Ogre::CompositorManager::getSingleton().getCompositorChain(viewport);
				if (chain)
				{
					chain->removeAllCompositors();
					cout << "Compositor chain cleared.";
				}
				else
				{
					cout << "No compositor chain could be found attached to "
						"the viewport.";
				}
			}},
			{"debug_actors", [](const Ogre::StringVector&) {
				InAppDebug::getSingleton().clearActors();
				cout << "Debug actors cleared.";
			}},
			{"debug_table", [](const Ogre::StringVector&) {
				InAppDebug::getSingleton().clearTable();
				cout << "Debug table cleared of non-persistent rows.";
			}},
			{"timer_results", [](const Ogre::StringVector&) {
				const auto name = gsDebugTimer.getName();
				const auto numResults = gsDebugTimer.getResults().size();

				gsDebugTimer.clear();
				cout << numResults << " debug timer \"" << name << 
					"\" results cleared.";
			}},
			{"console", [](const Ogre::StringVector&) {
				system("cls");
			}}
		}}
	};
}

void DebugConsole::run()
{
	init();

	*this << "\nCONSOLE:\n";
	*this << " Tilde/Backquote\ttoggles console input focus.\n";
	*this << "    Up/Down Arrow\ttraverses through history.\n";
	*this << "    Ctrl+Space\t\tautocompletes.\n";
	*this << "    Ctrl+Backspace\tdeletes previous word.\n";
	*this << "    Ctrl+X\t\tclears input.\n";
	*this << "    Ctrl+H\t\toutputs help.\n";

	*this << "\nCONTROLS:\n";
	*this << " J/K or mouse\tmoves the player to the left/right.\n";
	*this << " Q\t\tactivates the Dash ability, the paddle has to be touching\n"
		"\t\ta border.\n";
	*this << " E\t\tactivates the Shoot ability, use R to shoot.\n";
	*this << " TAB\t\tstarts the ball.\n";
	*this << " ESCAPE\t\topens the pause menu.\n";

	*this << "\nDEBUG:\n";
	*this << " F1\t\ttoggles free-look camera. Use WASD or arrow keys to move.\n";
	*this << " F2\t\ttoggles a showcase of cubic hermite splines with natural end\n"
		"\t\tconditions with numerical integration (only in grass.scene).\n";
	*this << " F3\t\ttoggles a compositor script for the final render target\n"
		"\t\tblack-and-white effect using hlsl shaders.\n";
	*this << " NUMPAD_5\tfreezes updating.\n";
	*this << " NUMPAD_4/6\truns an update backward/forward.\n";

	/* 		
        Could use a lock-free concurrent linked list using CAS here:
            However, using boost's single-consumer/single-producer 
            boost::lockfree::spsc_queue reduces performance by 25%
    */

	/* 
        This is the main loop of the worker thread.
        
        Using move sematics to pass messages from an intermediate queue that is 
        shared and needs locking to the processing queue that is not shared to 
        minimize the impact of message output on the main thread. 
    */

	std::unique_lock<std::mutex> lock(mIntermediateQueue.mMutex);
	while (mRunning)
	{
		// use move semantics to transfer messages from the intermediate queue 
		// to the processing queue
		if (transferMessageOwnership()) 
		{ // if any messages were transfered or in case of a spurious wake up, 
		  // the check is performed and back to waiting

			// unlock the intermediate queue, so that other threads can 
			// continue to push messages
			lock.unlock(); 

			// output the messages from the processing queue to the console
			handleMessages(); 

			// lock the intermediate queue back for the conditional_variable
			lock.lock(); 
		}

		// essentially a spinlock with a delay
		mMessageAvailable.wait_for(lock, std::chrono::milliseconds(20)); 
	}

	FreeConsole();
}

void DebugConsole::output(const Ogre::String& message)
{
	clearLine();
	cout << message << endl;
	newLine();
}

void DebugConsole::flush(const Ogre::String& buffer)
{
	mRunningInput += buffer;
	cout << '\b';
	cout << buffer << mSpecialStrings[SPECIAL_STR_CURSOR];
}

void DebugConsole::handleAutocomplete(const Ogre::String& word)
{
	// search from the last to ignore repeated words
	// + 1 to leave the trailing space alone
	Ogre::String incompleteWord = mRunningInput.substr(
		mRunningInput.find_last_not_of(word) + 1, 
		mRunningInput.npos); 

	Ogre::String difference = word.substr(incompleteWord.length(), word.npos);

	flush(difference + ' ');
}

bool DebugConsole::transferMessageOwnership() NOEXCEPT
{
	if (!mIntermediateQueue.get().empty())
	{
		mProcessingQueue = std::move(mIntermediateQueue.get());
		return true;
	}

	return false;
}

void DebugConsole::handleMessages()
{
	if (!mProcessingQueue.empty())
	{
		if (mOutputClosed) // if input is active
			clearLine();

		// process the messages
		while (!mProcessingQueue.empty())
		{
			Ogre::String& msg = mProcessingQueue.front();
			if (!msg.empty())
			{
				cout << msg;

				// in the while loop in case several empty strings are  
				// pushed into the queue at the end
				mOutputClosed = (msg.back() == '\n');	
			}

			mProcessingQueue.pop(); // remove from the queue
		}

		if (mOutputClosed)
			newLine();
	}
}

template <typename MapType>
void DebugConsole::autoComplete(const MapType& dictionary, 
	const Ogre::StringVector& inputVector)
{
	if (!inputVector.empty()) // in case running input is only blank space
	{
		auto compLower = 
			[](const MapType::value_type& pair, const std::string& input) {
				return (pair.first.compare(0, 
					std::min(pair.first.length(), input.length()), input) < 0);
			};

		auto compUpper = 
			[](const std::string& input, const MapType::value_type& pair) {
				return (pair.first.compare(0, 
					std::min(pair.first.length(), input.length()), input) > 0);
			};

		// binary search using string::compare for comparison with input as a 
		// substring - O(log(n)) * (<=2)
		MapType::const_iterator begin = 
			lower_bound(dictionary.begin(), dictionary.end(), inputVector.back(), compLower);

		const MapType::const_iterator end = 
			upper_bound(begin, dictionary.end(), inputVector.back(), compUpper);

		if (begin == end)
		{ // no matches - output all possibilities
			for (const auto& pair : dictionary)
				output(pair.first);
		}
		else if (std::distance(begin, end) == 1)
		{ // a single match is found
			handleAutocomplete((*begin).first);
		}
		else
		{ // multiple matches found - output them
			while (begin != end)
				output((*begin++).first);
		}
	}
}

Ogre::StringVector DebugConsole::processInput(const Ogre::String& input)
{
	return Ogre::StringUtil::split(input, " ");
}

bool DebugConsole::executeCommand(const Ogre::String& cmd)
{
	const bool inExecution = (!mCommandInExecution.empty());

	if (!inExecution)
		clearLine();
	
	const bool result = execute(cmd);

	if (!inExecution)
		nextLine();
	else
		cout << endl;

	return result;
}

bool DebugConsole::execute()
{
	clearCursor();

	cout << endl;

	const bool result = execute(mRunningInput);

	if (result)
	{
		mHistory.add(std::move(mRunningInput));
		nextLine();
	}
	else
		mHistory.add(mRunningInput);

	return result;
}

bool DebugConsole::execute(const Ogre::String& input)
{
	auto success = [this]() -> bool {
		mCommandInExecution.clear();
		return true;
	};

	auto failure =  [this]() -> bool {
		mCommandInExecution.clear();
		return false;
	};

	Ogre::StringVector processedInput = processInput(input);
	if (processedInput.size() >= 2)
	{ // execute a command
		mCommandInExecution = input;

		try {
			const Ogre::StringVector args(processedInput.begin() + 2, processedInput.end());

			// mOperators.at("operator_name").at("operand_name")(arguments...);
			mOperators.at(processedInput.at(0)).at(processedInput.at(1))(args);

			DEBUG_OUTPUT(LVL_CONSOLE, "Executed command \"" + input + '\"');

			return success();
		} 
		catch (const Ogre::Exception& e) {
			DEBUG_OUTPUT(LVL_CONSOLE, "Command \"" + input + 
				"\" cannot be executed: " + e.getDescription());
		}
		catch (...) {
			const Ogre::String& operatorValue = processedInput[0];
			const OperatorType::const_iterator operatorIt = 
				mOperators.find(operatorValue);

			if (operatorIt == mOperators.end())
			{
				cout << "Invalid operator \"" << operatorValue << "\"" << 
					mSpecialStrings[SPECIAL_STR_HELP_SUFFIX];
			}
			else
			{
				const Ogre::String& operandValue = processedInput[1];
				const OperandType::const_iterator operandIt = 
					operatorIt->second.find(operandValue);

				if (operandIt == operatorIt->second.end())
				{
					cout << "Invalid operand \"" << operandValue << "\"" << 
						mSpecialStrings[SPECIAL_STR_HELP_SUFFIX];
				}
				else
					cout << "Uncaught command exception!";
			}

		}
		
		nextLine();
	}
	else if (processedInput.size() == 1)
	{ 
		// output operands if the operator is valid
		auto it = mOperators.find(processedInput.at(0));
		if (it != mOperators.end())
		{
			for (const auto& pair : it->second)
				cout << pair.first << endl;

			newLine();
		} // handle special cases
		else if (processedInput.front() == "/?" || processedInput.front() == "?")
		{
			printHelp();

			return success();
		}
		else
		{ // finally, output usage
			cout << "usage: [operator] [operand] [arguments...]" << 
				mSpecialStrings[SPECIAL_STR_HELP_SUFFIX];
			nextLine();
		}
	}

	return failure();
}

void DebugConsole::printHelp()
{
	cout << "Available commands:\n";
	for (const auto& op : mOperators)
	{
		cout << op.first << endl;
		for (const auto& operand : op.second)
		{
			cout << "  " << operand.first << endl;
		}
	}
}

// InputHandler.h
bool DebugConsole::textInput(const SDL_TextInputEvent& arg) 
{
	// done by the calling thread
	if (mActive)
	{
		// left/right ctrl + any button (e.g. space) should not be a 
		// printable character backquote is a special character, do not print it
		if (std::find(mButtonsHeld.begin(), mButtonsHeld.end(), 
				SDLK_LCTRL) == mButtonsHeld.end() &&
			std::find(mButtonsHeld.begin(), mButtonsHeld.end(), 
				SDLK_RCTRL) == mButtonsHeld.end() &&
			std::find(mButtonsHeld.begin(), mButtonsHeld.end(), 
				SDLK_BACKQUOTE) == mButtonsHeld.end())
		{
			if (mOutputClosed)
			{
				// raw char array arg.text[32], 32 is defined as 
				// SDL_TEXTINPUTEVENT_TEXT_SIZE
				flush(arg.text); 
			}
		}

		return false;
	}

	return true;
}

// InputHandler.h
bool DebugConsole::keyPressed(const SDL_KeyboardEvent& arg) 
{
	// done by the calling thread
	switch (arg.keysym.sym)
	{
	case SDLK_BACKQUOTE:
		mButtonsHeld.push_back(arg.keysym.sym);

		toggle();

		return false;
		break;
	case SDLK_F2:
		executeCommand("toggle showcase 1");
		break;
	case SDLK_F3:
	{
		Ogre::Viewport* viewport = getCamera()->getViewport();
		Ogre::CompositorChain* chain = 
			Ogre::CompositorManager::getSingleton().getCompositorChain(viewport);

		if (chain && chain->getNumCompositors() > 0)
			executeCommand("clear compositor_chain");
		else
			executeCommand("add compositor BlackAndWhite");
	}
		break;
	default:
		break;
	};

	if (mActive)
	{
		mButtonsHeld.push_back(arg.keysym.sym);

		if (mButtonsHeld.size() > 1)
		{ // handle any chords
			for (auto& chord : mChords)
			{
				if (chord.hit(mButtonsHeld))
					mButtonsHeld.pop_back();
			}
		}
		else
		{ // handle a single button press
			switch (mButtonsHeld.back())
			{
			case SDLK_BACKSPACE:
				if (!mRunningInput.empty())
				{
					clearCursor();
					cout << "\b_";
					mRunningInput.pop_back();
				}
				break;
			case SDLK_RETURN:
			case SDLK_KP_ENTER:
				if (mOutputClosed)
					execute();
				else
					closeOutput();
				break;
			case SDLK_UP:
				clearLine();
				mRunningInput = mHistory.previous();
				newLine();
				break;
			case SDLK_DOWN:
				clearLine();
				mRunningInput = mHistory.next();
				newLine();
				break;
			default:
				break;
			}
		}

		return false;
	}

	return true;
}

// InputHandler.h
bool DebugConsole::keyReleased(const SDL_KeyboardEvent& arg) 
{
	// done by the calling thread
	if (mActive)
	{
		mButtonsHeld.erase(
			std::remove(mButtonsHeld.begin(), mButtonsHeld.end(), arg.keysym.sym), 
			mButtonsHeld.end());

		return false;
	}

	return true;
}