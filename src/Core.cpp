// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Core.h"

#include "Scene.h"
#include "Config.h"
#include "AppStateManager.h"

#include "SDL_syswm.h"

// Ogre::FrameListener
bool Core::frameStarted(const Ogre::FrameEvent& evt)
{
	return true;
}

// Ogre::FrameListener
bool Core::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	if (mWindowHasFocus)
		mAppStateManager->update(evt.timeSinceLastFrame);
	else // sleep if the window has lost focus to save CPU%
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

	return true;
}

// Ogre::FrameListener
bool Core::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

// AppStateManager::Listener
void Core::handleMenuEvents(const MenuEvent menuEvent, const StringInterface* params)
{
	switch (menuEvent)
	{
	case MENU_EVENT_STATE_PREVIOUS:
		mAppStateManager->_previousState();
		break;
	case MENU_EVENT_START_GAME:
		createScene(params->getParameter("scene_name"));
		break;
	case MENU_EVENT_RETURN_TO_MENU:
		destroyScene();
		break;
	case MENU_EVENT_RETURN_TO_GAME:
		mUpdatePaused = false;

		eSDL_FlushMotionEvents();
		resetDelta();
		break;
	case MENU_EVENT_QUIT:
		mQuitPending = true;
		break;
	default:
		break;
	}
}

// AppStateManager::Listener
void Core::handleMenuEvents(Config::Property&& prop)
{
	getConfig().setProperty(std::move(prop));
}

Core* Core::msSingleton = nullptr;

Core::Core()
  : mViewport(nullptr),
	mWindow(nullptr),
	mSDLWindow(nullptr),
	mFpsLimit(NULL),
	mFrameTargetMicroseconds(NULL),
	mNumUpdates(NULL),
	mWindowHasFocus(true),
	mUnloadResources(true),
	mShowConfig(true),
	mUpdatePaused(false),
	mQuitPending(false)
{
	msSingleton = this;
}

Core::~Core()
{
	try {
		destroyScene();

		if (mSDLWindow)
		{
			// Restore desktop resolution on exit
			SDL_SetWindowFullscreen(mSDLWindow, 0);
			SDL_DestroyWindow(mSDLWindow);
			mSDLWindow = 0;
		}

		for (auto& joystick : mJoysticks)
			SDL_JoystickClose(joystick);

		SDL_Quit();
	} 
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy core");
	}

	msSingleton = nullptr;
}

Core* Core::getSingletonPtr() NOEXCEPT
{
	return msSingleton;
}

Core& Core::getSingleton()
{
	assert(msSingleton);
	return (*msSingleton);
}

void Core::chooseSceneManager()
{
	// Create the scene manager, in this case the generic one
	setSceneManager(mRoot->createSceneManager(Ogre::ST_GENERIC, "Generic"));
}

void Core::createListeners()
{
	mRoot->addFrameListener(this);
}

void Core::createViewport()
{
	// Create a viewport, all window
	mViewport = mWindow->addViewport(getCamera());
	mViewport->setBackgroundColour(CORE_VIEWPORT_BACKGROUND_COLOR);

	// Alter the camera aspect ratio to match the viewport
	getCamera()->setAspectRatio(Ogre::Real(mViewport->getActualWidth()) / 
		Ogre::Real(mViewport->getActualHeight()));
}

void Core::initialiseSDL()
{
	// initialise the SDL
    if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | 
		SDL_INIT_GAMECONTROLLER | SDL_INIT_EVENTS) > 0)
    {
        OGRE_EXCEPT(Ogre::Exception::ERR_INTERNAL_ERROR, 
			"Cannot initialize SDL2: " + std::string(SDL_GetError()), 
			"Core::initialiseSDL");
    }

	Ogre::ConfigOptionMap& configOptions = 
		mRoot->getRenderSystem()->getConfigOptions();

	// get the width and height of the window
	int width = 1280;
	int height = 720;
	const Ogre::ConfigOptionMap::const_iterator option = 
		configOptions.find("Video Mode");
	if (option != configOptions.end())
	{
		// ignore leading space
		const Ogre::String::size_type start = 
			option->second.currentValue.find_first_of("012356789");

		// get the width and height
		Ogre::String::size_type widthEnd = 
			option->second.currentValue.find(' ', start);

		// we know that the height starts 3 characters after the width and goes 
		// until the next space
		Ogre::String::size_type heightEnd = 
			option->second.currentValue.find(' ', widthEnd + 3);

		// now we can parse out the values
		width = Ogre::StringConverter::parseInt(
			option->second.currentValue.substr(0, widthEnd));
		height = Ogre::StringConverter::parseInt(
			option->second.currentValue.substr(widthEnd + 3, heightEnd));
	}

	// get the x and y positions of the window and whether it's fullscreen
	int posX, posY;
	const bool fullscreen = false;/*Ogre::StringConverter::parseBool(
		configOptions["Full Screen"].currentValue);*/
    if (fullscreen)
    {
        posX = SDL_WINDOWPOS_UNDEFINED_DISPLAY(0);
        posY = SDL_WINDOWPOS_UNDEFINED_DISPLAY(0);
    }
	else
	{
		posX = SDL_WINDOWPOS_CENTERED_DISPLAY(0);
		posY = SDL_WINDOWPOS_CENTERED_DISPLAY(0);
	}

	// create the SDL window
	Ogre::String windowTitle = CORE_WINDOW_TITLE;
    mSDLWindow = SDL_CreateWindow(windowTitle.c_str(), posX, posY,
		width, height, SDL_WINDOW_SHOWN);

	if (!mSDLWindow)
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_INTERNAL_ERROR, 
			"Cannot create an SDL window: " + std::string(SDL_GetError()), 
			"Core::initialiseSDL");
	}

	// fill in the WMinfo struct
    SDL_SysWMinfo wmInfo;
    SDL_VERSION(&wmInfo.version);
    if (SDL_GetWindowWMInfo(mSDLWindow, &wmInfo) == SDL_FALSE)
    {
        OGRE_EXCEPT(Ogre::Exception::ERR_INTERNAL_ERROR, 
			"Could not get SDL2 window WM info: " + std::string(SDL_GetError()), 
			"Core::initialiseSDL");
    }

	// get the HWND
    Ogre::String winHandle;
	if (wmInfo.subsystem == SDL_SYSWM_WINDOWS)
    {
        winHandle = Ogre::StringConverter::toString((uintptr_t)wmInfo.info.win.window);
	}
	else
	{ 
		OGRE_EXCEPT(Ogre::Exception::ERR_INTERNAL_ERROR, 
			"Unexpected SDL2 WM subsystem", 
			"Core::initialiseSDL");
	}

	Ogre::NameValuePairList miscParams;
	miscParams.emplace("externalWindowHandle", winHandle);
	miscParams.emplace("title", windowTitle);
	miscParams.emplace("FSAA", configOptions["FSAA"].currentValue);
	miscParams.emplace("vsync", configOptions["VSync"].currentValue);

	mWindow = mRoot->createRenderWindow(windowTitle, width, height, 
		fullscreen, &miscParams);

	SDL_ShowCursor(SDL_DISABLE);
	SDL_SetWindowGrab(mSDLWindow, SDL_TRUE);
}

void Core::setupResources()
{
	// Load resource paths from config file
	Ogre::ConfigFile cf;
	cf.load(CORE_RESOURCES_FILENAME);

	// Go through all sections & setting in the file
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	Ogre::String secName, typeName, archName;
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
		for (auto it = settings->cbegin(); it != settings->end(); ++it)
		{
			typeName = it->first;
			archName = it->second;

			Ogre::ResourceGroupManager::getSingleton().
				addResourceLocation(archName, typeName, secName);
		}
	}
}

void Core::loadResources()
{
	// initialise overlays (also parses them, has to be before group initialisation)
	mOverlaySystem = std::make_unique<Ogre::OverlaySystem>();
	getSceneManager()->addRenderQueueListener(mOverlaySystem.get());

	// Initalise all resource groups
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	
	// fonts are not loaded on startup by the resource manager, 
	// so no text appears in the ogre overlays
	Ogre::ResourceManager::ResourceMapIterator it = 
		Ogre::FontManager::getSingleton().getResourceIterator();
	while (it.hasMoreElements())
	{
		it.getNext()->load();
	}
}

bool Core::configure()
{
	mRoot->restoreConfig();

	if (mShowConfig)
		return mRoot->showConfigDialog();
	else
		return true;
}

bool Core::setup()
{
	mLogManager = std::make_unique<Ogre::LogManager>();
	mLogManager->createLog(CORE_LOG_FILENAME, true, false, false);

	mRoot = std::make_unique<Ogre::Root>(
		CORE_PLUGINS_FILENAME, CORE_CFG_FILENAME, Ogre::StringUtil::BLANK);

	// load resources, such as .cfg that is used in configure() below
	setupResources(); 

	if (configure())
		mRoot->initialise(false);
	else
		return false;

	initialiseSDL();

	chooseSceneManager();

	createCamera();
	createViewport();
	
	loadResources();
	createListeners();

	mAppStateManager = std::make_unique<AppStateManager>(this);

	mInAppDebug._init(mWindow);
	
	return true;
}

void Core::createScene(const Ogre::String& sceneFilename) NOEXCEPT
{
	if (!mScene)
	{
		DEBUG_OUTPUT(LVL_GREEN, "A scene is being created");

		try {
			mScene = std::make_unique<Scene>(
				sceneFilename, 
				static_cast<GameModes>(
					getConfig().getProperty<unsigned int>("game_mode")),
				mJoysticks,
				mUnloadResources);

			resetDelta();
		} 
		catch (const Ogre::Exception& e) {
			DEBUG_OUTPUT(LVL_RED, "The scene could not be created: " + 
				e.getFullDescription());

			return;
		} 

		DEBUG_OUTPUT(LVL_GREEN, "The scene has been created");
	}
}

void Core::destroyScene()
{
	if (mScene)
	{
		DEBUG_OUTPUT(LVL_GREEN, "The scene is being destroyed");

		mScene.reset(); // call the scene's destructor

		DEBUG_OUTPUT(LVL_GREEN, "The scene has been destroyed");

		checkResourceLeaks();
	}
	else
	{
		DEBUG_OUTPUT(LVL_GREEN, "The scene is not created");
	}
}

void Core::checkResourceLeaks()
{
	// check whether all scene nodes were destroyed
	Ogre::String nodeLeaks;
	eOgre::for_each_child_scene_node(getSceneManager()->getRootSceneNode(), 
		[&nodeLeaks, this](Ogre::SceneNode* sceneNode, unsigned int depth) {
			if (sceneNode == mInAppDebug.getRootSceneNode())
				return;

			nodeLeaks.append(' ' + sceneNode->getName());
		}
	);

	if (!nodeLeaks.empty())
	{
		DEBUG_OUTPUT(LVL_RED, "Scene node(s) leaked:" + nodeLeaks);
	}

	// check whether all movable objects were destroyed
	Ogre::String movableObjectLeaks;
	for (const auto& factory : mRoot->getMovableObjectFactoryIterator())
	{
		for (const auto& movableObject : 
			getSceneManager()->getMovableObjectIterator(factory.first))
		{
			movableObjectLeaks.append(' ' + movableObject.first + '(' + 
				movableObject.second->getMovableType() + ')');
		}
	}

	if (!movableObjectLeaks.empty())
	{
		DEBUG_OUTPUT(LVL_RED, "Movable object(s) leaked:" + movableObjectLeaks);
	}
}

void Core::createCamera()
{
	// Create the camera
	setCamera(getSceneManager()->createCamera(CORE_PRIMARY_CAMERA_NAME));
	getCamera()->setNearClipDistance(CORE_NEAR_CLIP_DISTANCE);
}

void Core::processCmdArgs(const std::vector<std::string>& cmdArguments)
{
	// output command line arguments
	DEBUG_WRITELN("Command-line arguments: " << cmdArguments);

	const auto getArgument = [](const std::string& flag) -> std::string {
		return flag.substr(flag.find(':') + 1, flag.npos);
	};

	// process command line arguments
	size_t pos = 0;
	for (const auto& flag : cmdArguments)
	{
		if (flag.find("/fps") != flag.npos)
		{
			std::string arg = getArgument(flag);
			try {
				setFpsLimit(std::stof(arg));
			} 
			catch (...) {
				setFpsLimit(0.0f);

				DEBUG_WRITELN("Invalid argument to /fps " << arg);
			}
		}
		else if (flag == "/nounload")
			mUnloadResources = false;
		else if (flag == "/noconfig")
			mShowConfig = false;
		else
			DEBUG_WRITELN("Unrecognized command-line argument: " << flag);
	}
}

void Core::_go(std::vector<std::string> cmdArgs)
{
	processCmdArgs(cmdArgs);

	if (!setup())
		return;

	Ogre::Real frameMicroseconds = 0.0f;
	Ogre::Real deltaSeconds = 0.0f;

	// Custom game loop
	while (!mQuitPending)
	{
		mLoopTimer.reset();

		messagePump();

		// update the scene
		if (!mUpdatePaused)
		{
			updateOnce(deltaSeconds);
		}
		else if (mNumUpdates != 0)
		{
			if (mNumUpdates > 0)
			{ // forward
				updateOnce(deltaSeconds);
				--mNumUpdates;
			}
			else
			{ // backward
				updateOnce(-deltaSeconds);
				++mNumUpdates;
			}
		}

		// render a frame
		mRoot->renderOneFrame(deltaSeconds); 

		mInAppDebug.update(deltaSeconds);

		frameMicroseconds = static_cast<Ogre::Real>(mLoopTimer.getMicroseconds());
		mInAppDebug.getTable()->setValue("frame_time", 
			Ogre::StringConverter::toString(frameMicroseconds * 0.001) + "ms");

		// limit fps by sleeping inbetween frames 
		if (mFpsLimit != 0.0f && frameMicroseconds < mFrameTargetMicroseconds)
		{
			std::this_thread::sleep_for(std::chrono::microseconds(
				static_cast<long long>(mFrameTargetMicroseconds - frameMicroseconds)));
		}

		if (!mUpdatePaused || mNumUpdates != 0)
		{
			// delta has to be in seconds, since Ogre takes seconds by default 
			// in addTime functions
			deltaSeconds = static_cast<Ogre::Real>(mLoopTimer.getMicroseconds() * 0.000001);

#ifdef _DEBUG
			if (mWindowHasFocus)
			{
				// reset the delta to a reasonable amount e.g. in case of a 
				// break due to a breakpoint etc.
				if (deltaSeconds > CORE_MAXDELTA_SECONDS)
					deltaSeconds = CORE_MAXDELTA_SECONDS;
			}
#endif

			mInAppDebug.getTable()->setValue("delta_time", 
				Ogre::StringConverter::toString(deltaSeconds * 1000) + "ms");
		}
	}
}

void Core::updateOnce(const Ogre::Real deltaSeconds)
{
	if (mScene && !mAppStateManager->isActive())
		mScene->update(deltaSeconds);
}

void Core::messagePump()
{
	SDL_Event evt;
    while(SDL_PollEvent(&evt))
    {
        switch(evt.type)
        {
        case SDL_WINDOWEVENT:
            handleWindowEvents(evt.window);
            break;
        case SDL_QUIT:
			mQuitPending = true;
            break;
        default:
            break;
        }

        handleInputEvents(evt);
    }
}

void Core::handleWindowEvents(const SDL_WindowEvent& evt)
{
	switch(evt.event)
    {
    case SDL_WINDOWEVENT_SIZE_CHANGED:
        int w, h;
        SDL_GetWindowSize(mSDLWindow, &w, &h);
        mWindow->windowMovedOrResized();
        break;
	case SDL_WINDOWEVENT_MOVED:
    case SDL_WINDOWEVENT_RESIZED:
        mWindow->windowMovedOrResized();
        break;
    case SDL_WINDOWEVENT_CLOSE:
        break;
    case SDL_WINDOWEVENT_SHOWN:
        mWindow->setVisible(true);
        break;
    case SDL_WINDOWEVENT_HIDDEN:
        mWindow->setVisible(false);
        break;
	// fired when mouse enters the window
	case SDL_WINDOWEVENT_ENTER: 
		break;
	// fired when window gains focus by alt-tabbing
	case SDL_WINDOWEVENT_FOCUS_GAINED: 
		mWindowHasFocus = true;

		eSDL_FlushMotionEvents();
		resetDelta();
		break;
	case SDL_WINDOWEVENT_LEAVE:
		break;
	case SDL_WINDOWEVENT_FOCUS_LOST:
		mWindowHasFocus = false;

		if (mScene)
		{
			mUpdatePaused = true;
			mAppStateManager->enterMenu();
		}
		break;
	default:
		break;
    }
}

void Core::handleInputEvents(const SDL_Event& evt)
{
	switch (evt.type)
	{
	//case SDL_MOUSEWHEEL:
	case SDL_MOUSEMOTION:
		if (mAppStateManager->mouseMoved(evt.motion))
		{
			if (mInAppDebug.mouseMoved(evt.motion))
			{
				if (mScene)
					mScene->mouseMoved(evt.motion);
			}
		}

		break;
	case SDL_MOUSEBUTTONDOWN:
		if (mScene)
			mScene->mousePressed(evt.button, evt.button.button);

		mAppStateManager->mousePressed(evt.button, evt.button.button);
		break;
	case SDL_MOUSEBUTTONUP:
		if (mScene)
			mScene->mouseReleased(evt.button, evt.button.button);

		mAppStateManager->mouseReleased(evt.button, evt.button.button);
		break;
	case SDL_KEYDOWN:
		if(!evt.key.repeat)
		{
			mAppStateManager->keyPressed(evt.key);
			if (mInAppDebug.keyPressed(evt.key))
			{ // console is not active
				if (mScene)
					mScene->keyPressed(evt.key);

				switch(evt.key.keysym.sym)
				{
				case SDLK_KP_5:
					mUpdatePaused = !mUpdatePaused;
					mNumUpdates = 0;
					break;
				case SDLK_KP_4:
					if (mUpdatePaused)
						mNumUpdates -= 1;
					break;
				case SDLK_KP_6:
					if (mUpdatePaused)
						mNumUpdates += 1;
					break;
				default:
					break;
				}
			}
		}
		break;
	case SDL_KEYUP:
		if(!evt.key.repeat)
		{
			mAppStateManager->keyReleased(evt.key);
			if (mInAppDebug.keyReleased(evt.key))
			{
				if (mScene)
					mScene->keyReleased(evt.key);
			}
		}
		break;
	case SDL_TEXTINPUT:
		mAppStateManager->textInput(evt.text);
		if (mInAppDebug.textInput(evt.text))
		{
			if (mScene)
				mScene->textInput(evt.text);
		}
		break;
	case SDL_JOYAXISMOTION:
	//case SDL_JOYBALLMOTION:
		if (mScene)
			mScene->joyAxisMoved(evt.jaxis, evt.jaxis.axis);

		mAppStateManager->joyAxisMoved(evt.jaxis, evt.jaxis.axis);
		break;
	case SDL_JOYBUTTONDOWN:
		if (mScene)
			mScene->joyButtonPressed(evt.jbutton, evt.jbutton.button);

		mAppStateManager->joyButtonPressed(evt.jbutton, evt.jbutton.button);
		break;
	case SDL_JOYBUTTONUP:
		if (mScene)
			mScene->joyButtonReleased(evt.jbutton, evt.jbutton.button);

		mAppStateManager->joyButtonReleased(evt.jbutton, evt.jbutton.button);
		break;
	case SDL_JOYHATMOTION:
		if (mScene)
			mScene->joyPovMoved(evt.jhat, evt.jhat.hat);

		mAppStateManager->joyPovMoved(evt.jhat, evt.jhat.hat);
		break;
	case SDL_JOYDEVICEADDED:
	{
		SDL_Joystick* joystick = SDL_JoystickOpen(evt.jdevice.which);
		if (joystick)
		{
			SDL_GameControllerAddMappingsFromFile("gamecontrollerdb.txt");
			mJoysticks.push_back(joystick);

			if (mScene)
				mScene->assignInput(joystick);

			DEBUG_WRITELN("\nDetected a new joystick:");
			DEBUG_WRITELN(" Name: " << SDL_JoystickName(joystick));
			DEBUG_WRITELN(" Number of axes: " << SDL_JoystickNumAxes(joystick));
			DEBUG_WRITELN(" Number of buttons: " << SDL_JoystickNumButtons(joystick));
			DEBUG_WRITELN(" Number of balls: " << SDL_JoystickNumBalls(joystick));
			DEBUG_WRITELN(" Number of hats: " << SDL_JoystickNumHats(joystick));

			DEBUG_WRITELN("\nCONTROL SCHEME:");
			DEBUG_WRITELN(" Left stick/D-Pad\tmoves the player.");
			DEBUG_WRITELN(" A/cross\t\tactivates the Dash ability, the paddle has "
				"to be\n\t\t\ttouching a border.");
			DEBUG_WRITELN(" X/square\t\tactivates the Shoot ability, use "
				"Y/triangle to shoot.");
			DEBUG_WRITELN(" Right bumper\t\tstarts the ball.");
			DEBUG_WRITELN(" Back\t\t\topens the pause menu.");
		}
		else
		{
			DEBUG_OUTPUT(LVL_YELLOW, "The joystick was detected, but could "
				"not be opened");
		}
	}
		break;
	case SDL_JOYDEVICEREMOVED:
		// NOTE: not way to use evt.jdevice.which here to get the joystick in 
		// question, have to loop and check
		for (auto& joystick : mJoysticks)
		{
			if (SDL_JoystickGetAttached(joystick) == SDL_FALSE)
			{
				DEBUG_WRITELN("Joystick with name \"" << 
					SDL_JoystickName(joystick) << "\" has been closed.");
				SDL_JoystickClose(joystick);

				mJoysticks.erase(
					std::remove(mJoysticks.begin(), mJoysticks.end(), joystick), 
					mJoysticks.end());

				if (mScene)
					mScene->unassignInput(joystick);

				break;
			}
		}
		break;
	default:
		break;
	}
}

void Core::resetDelta() NOEXCEPT
{
	mLoopTimer.reset();
}

void Core::setFpsLimit(const Ogre::Real newLimit) NOEXCEPT 
{ 
	mFpsLimit = newLimit;

	if (mFpsLimit != 0.0f)
		mFrameTargetMicroseconds = (1.0f/mFpsLimit * 1000000);
	else 
		mFrameTargetMicroseconds = 0.0f;
}