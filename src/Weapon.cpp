// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Weapon.h"

Weapon::Weapon(const ActorClass& desc, const Ogre::String& handleBoneName)
  : Actor(desc), 
	StaticActor(desc),
	mParentEntity(const_cast<Ogre::Entity*>(desc.getParentEntity())),
	mParentSceneNode(mParentEntity->getParentSceneNode()),
	mSheathBone(mParentEntity->getSkeleton()->getBone(desc.getBoneName())),
	mHandleBone(mParentEntity->getSkeleton()->getBone(handleBoneName)),
    mFlash(nullptr)
{
	ActorClass flashDesc;
	flashDesc.setName(ACTOR_FLASH_TYPE_NAME + '_' + getEntity()->getName());
	flashDesc.setType(ACTOR_FLASH_TYPE_NAME);
	flashDesc.setEntity(FLASH_MESH_FILENAME);
	flashDesc.setTagPoint(getEntity(), WEAPON_BONE_MUZZLE_NAME);
	flashDesc.setRotate(Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3::UNIT_X));
	flashDesc.setVisible(false);

	mFlash = dynamic_cast<Flash*>(createChild(flashDesc));
} 

Weapon::~Weapon()
{
}

Weapon::Weapon(Weapon&& rhs)
  : Actor(std::move(rhs)), 
	StaticActor(std::move(rhs)),
	mParentEntity(rhs.mParentEntity),
	mParentSceneNode(rhs.mParentSceneNode),
	mSheathBone(rhs.mSheathBone),
	mHandleBone(rhs.mHandleBone),
	mFlash(rhs.mFlash)
{
	Actor::removeChild(mFlash);

	rhs.mFlash = nullptr;
}

Weapon& Weapon::operator=(Weapon&& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		Actor::operator=(std::move(rhs));
		StaticActor::operator=(std::move(rhs));

		// free existing resources of the source
		Actor::removeChild(mFlash);

		// copy data from the source object
		mParentEntity = rhs.mParentEntity;
		mParentSceneNode = rhs.mParentSceneNode;
		mSheathBone = rhs.mSheathBone;
		mHandleBone = rhs.mHandleBone;
		mFlash = rhs.mFlash;

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		// non-owning so not necessary
		rhs.mFlash = nullptr;
	}

	return *this;
}

void Weapon::attachToHandle()
{
	reattachToBone(mHandleBone->getName());
}

void Weapon::attachToSheath()
{
	reattachToBone(mSheathBone->getName());
}

void Weapon::getMuzzleTransforms(Ogre::Vector3& pos, Ogre::Quaternion& orient,
	Ogre::Vector3& scale)
{
	// charNode -> charEntity -> Bone("handle") -> entity(weapon) -> Bone("muzzle")
	// charNode -> Bone("handle") -> Bone("muzzle")

	// orientation * (position * scale) + parent position
	// orientated position + parent position
	
	const Ogre::Bone* muzzleBone = 
		getEntity()->getSkeleton()->getBone(WEAPON_BONE_MUZZLE_NAME);

	// scale is affected by scaling the parent scene node or the muzzle bone itself
	scale = muzzleBone->_getDerivedScale() * mParentSceneNode->_getDerivedScale();

	// find the position of the bone "handle"
    Ogre::Vector3 handlePosition = (
        mParentSceneNode->_getDerivedOrientation() 
        * 
        (mHandleBone->_getDerivedPosition() * mParentSceneNode->_getDerivedScale())
        +
        mParentSceneNode->_getDerivedPosition()
    );

    // find the orientation of the bone "muzzle"
    orient = (
        mParentSceneNode->_getDerivedOrientation() 
        * 
        mHandleBone->_getDerivedOrientation()
        *
        muzzleBone->_getDerivedOrientation()
    );

    // find the bone "muzzle" position
    // muzzle orientation * (muzzle position * charNode scale) + parent position
    pos = (
        orient
        * 
        (muzzleBone->_getDerivedPosition() * mParentSceneNode->_getDerivedScale())
        +
        handlePosition
    );

	/* 
		Alternative method, however it's expensive to extract the scale and the 
		orientation afterwards:

		Returns a right-handed matrix representing the transformations to get 
		to the weapon's muzzle, relative to the mParentSceneNode, with scale
		and orientation in the top left 3x3 and translation in the 
		right-most column
	*/
	//return mParentSceneNode->_getFullTransform() * 
	//	   mHandleBone->_getFullTransform() * 
	//	   muzzleBone->_getFullTransform();
}

std::shared_ptr<Actor> WeaponFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Weapon>(desc, 
		desc.getParam("handle_bone_name"));
}
