// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Actor.h"

#include "InAppDebug.h"
#include "Scene.h"

ActorClass::ActorClass()
 :  mEntity(nullptr),
	mNode(getSceneManager()->getRootSceneNode()),
	mParentEntity(nullptr),
	mNodeTranslate(Ogre::Vector3::ZERO),
	mNodeRotate(Ogre::Quaternion::IDENTITY),
	mNodeScale(Ogre::Vector3::UNIT_SCALE),
	mIsNodeParent(true),
	mIsTagPoint(false),
	mIsVisible(true),
	mTypeFlag(NULL),
	mCreator(nullptr)
{
}

ActorClass::ActorClass(const Ogre::String& name, const Ogre::String& type, 
	const Ogre::String& meshFilename, Ogre::Node* node, 
	use_existing_node_t)
  : mName(name),
	mType(type),
	mMeshFilename(meshFilename),
	mEntity(nullptr),
	mNode(node),
	mParentEntity(nullptr),
	mNodeTranslate(Ogre::Vector3::ZERO),
	mNodeRotate(Ogre::Quaternion::IDENTITY),
	mNodeScale(Ogre::Vector3::UNIT_SCALE),
	mIsNodeParent(false),
	mIsTagPoint(false),
	mIsVisible(true),
	mTypeFlag(NULL),
	mCreator(nullptr)
{
}

ActorClass::ActorClass(const Ogre::String& name, const Ogre::String& type, 
	const Ogre::String& meshFilename, Ogre::Node* parentNode, 
	create_child_node_t, const Ogre::Vector3& translate, 
	const Ogre::Quaternion& rotate, const Ogre::Vector3& scale)
  : mName(name),
	mType(type),
	mMeshFilename(meshFilename),
	mEntity(nullptr),
	mNode(parentNode),
	mParentEntity(nullptr),
	mNodeTranslate(translate),
	mNodeRotate(rotate),
	mNodeScale(scale),
	mIsNodeParent(true),
	mIsTagPoint(false),
	mIsVisible(true),
	mTypeFlag(NULL),
	mCreator(nullptr)
{
}

ActorClass::ActorClass(const Ogre::String& name, const Ogre::String& type, 
	const Ogre::String& meshFilename, const Ogre::String& boneName,
	Ogre::Entity* parentEntity, const Ogre::Vector3& translate, 
	const Ogre::Quaternion& rotate, const Ogre::Vector3& scale)
  : mName(name),
	mType(type),
	mMeshFilename(meshFilename),
	mEntity(nullptr),
	mNode(nullptr),
	mBoneName(boneName),
	mParentEntity(parentEntity),
	mNodeTranslate(translate),
	mNodeRotate(rotate),
	mNodeScale(scale),
	mIsNodeParent(false),
	mIsTagPoint(true),
	mIsVisible(true),
	mTypeFlag(NULL),
	mCreator(nullptr)
{
}

ActorClass::ActorClass(const Ogre::String& name, const Ogre::String& type,
	Ogre::Entity* entity, Ogre::Node* node, use_existing_node_t)
  : mName(name),
	mType(type),
	mEntity(entity),
	mNode(node),
	mParentEntity(nullptr),
	mNodeTranslate(Ogre::Vector3::ZERO),
	mNodeRotate(Ogre::Quaternion::IDENTITY),
	mNodeScale(Ogre::Vector3::UNIT_SCALE),
	mIsNodeParent(false),
	mIsTagPoint(false),
	mIsVisible(true),
	mTypeFlag(NULL),
	mCreator(nullptr)
{
}

ActorClass::ActorClass(const Ogre::String& name, const Ogre::String& type, 
	Ogre::Entity* entity, Ogre::Node* parentNode, create_child_node_t,
	const Ogre::Vector3& translate, const Ogre::Quaternion& rotate, 
	const Ogre::Vector3& scale)
  : mName(name),
	mType(type),
	mEntity(entity),
	mNode(parentNode),
	mParentEntity(nullptr),
	mNodeTranslate(translate),
	mNodeRotate(rotate),
	mNodeScale(scale),
	mIsNodeParent(true),
	mIsTagPoint(false),
	mIsVisible(true),
	mTypeFlag(NULL),
	mCreator(nullptr)
{
}

ActorClass::ActorClass(const Ogre::String& name, const Ogre::String& type, 
	Ogre::SceneNode* sceneNode)
  : ActorClass() // delegate to the default constructor (ctor-based init is deprecated)
{
	initFromSceneNode(sceneNode);
}

ActorClass::ActorClass(const Ogre::String& name, const Ogre::String& type,
	Ogre::Entity* entity, const Ogre::String& boneName,
	Ogre::Entity* parentEntity, const Ogre::Vector3& translate, 
	const Ogre::Quaternion& rotate, const Ogre::Vector3& scale)
  : mName(name),
	mType(type),
	mEntity(entity),
	mNode(nullptr),
	mBoneName(boneName),
	mParentEntity(parentEntity),
	mNodeTranslate(translate),
	mNodeRotate(rotate),
	mNodeScale(scale),
	mIsNodeParent(false),
	mIsTagPoint(true),
	mIsVisible(true),
	mTypeFlag(NULL),
	mCreator(nullptr)
{
}

ActorClass::ActorClass(ActorClass&& rhs) NOEXCEPT
  : mChildren(std::move(rhs.mChildren)),
	mAdditionalParams(std::move(rhs.mAdditionalParams)),
	mName(std::move(rhs.mName)),
	mType(std::move(rhs.mType)),
	mMeshFilename(std::move(rhs.mMeshFilename)),
	mEntity(rhs.mEntity),
	mNode(rhs.mNode),
	mBoneName(std::move(rhs.mBoneName)),
	mParentEntity(rhs.mParentEntity),
	mNodeTranslate(rhs.mNodeTranslate),
	mNodeRotate(rhs.mNodeRotate),
	mNodeScale(rhs.mNodeScale),
	mIsNodeParent(rhs.mIsNodeParent),
	mIsTagPoint(rhs.mIsTagPoint),
	mIsVisible(rhs.mIsVisible),
	mTypeFlag(rhs.mTypeFlag),
	mCreator(rhs.mCreator)
{
}

ActorClass& ActorClass::operator=(ActorClass&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		// ...

		// free existing resources of the source
		// ...

		// copy data from the source object
		mChildren = std::move(rhs.mChildren);
		mAdditionalParams = std::move(rhs.mAdditionalParams);
		mName = std::move(rhs.mName);
		mType = std::move(rhs.mType);
		mMeshFilename = std::move(rhs.mMeshFilename);
		mEntity = rhs.mEntity;
		mNode = rhs.mNode;
		mBoneName = std::move(rhs.mBoneName);
		mParentEntity = rhs.mParentEntity;
		mNodeTranslate = rhs.mNodeTranslate;
		mNodeRotate = rhs.mNodeRotate;
		mNodeScale = rhs.mNodeScale;
		mIsNodeParent = rhs.mIsNodeParent;
		mIsTagPoint = rhs.mIsTagPoint;
		mIsVisible = rhs.mIsVisible;
		mTypeFlag = rhs.mTypeFlag;
		mCreator = rhs.mCreator;

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		rhs.mEntity = nullptr;
		rhs.mNode = nullptr;
		rhs.mParentEntity = nullptr;
	}

	return *this;
}

void ActorClass::initFromSceneNode(Ogre::SceneNode* sceneNode)
{
	const auto numAttached = sceneNode->numAttachedObjects();
	if (numAttached == 0)
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, "Node with name \"" + 
			sceneNode->getName() + "\" has no objects attached for Actor \"" + 
			mName + '\"', "ActorClass::ActorClass");
	}
	else if (numAttached > 1)
	{
		DEBUG_OUTPUT(LVL_GREEN, "Node with name \"" + sceneNode->getName() + 
			"\" has more than one attached object for Actor \"" + mName + 
			"\", using the first \"Entity\"");

		// assign the entity from the scene node 
		Ogre::SceneNode::ObjectIterator it = sceneNode->getAttachedObjectIterator();
		while (it.hasMoreElements())
		{
			Ogre::MovableObject* movableObject = it.getNext();
			if (movableObject->getMovableType() == "Entity")
			{
				mEntity = static_cast<Ogre::Entity*>(movableObject);
				break;
			}
		}
	}
	else // numAttached == 1
	{
		Ogre::MovableObject* movableObject = sceneNode->getAttachedObject(0);
		if (movableObject->getMovableType() == "Entity")
			mEntity = static_cast<Ogre::Entity*>(movableObject);
		else
		{
			OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, 
				"Node with name \"" + sceneNode->getName() + 
				"\" and attached object \"" + movableObject->getName() + 
				"\" is of type \"" + movableObject->getMovableType() + 
				"\", an \"Entity\" is required, for Actor \"" + mName + '\"', 
				"ActorClass::ActorClass");
		}
	}

	mNode = sceneNode;
	mIsNodeParent = false;
}

void ActorClass::setAdditionalParams(StringInterface& params, discard_parameters_t)
{ 
	if (mAdditionalParams)
		mAdditionalParams.reset(&params);
	else
		mAdditionalParams = std::make_unique<StringInterface>(std::move(params));
}

void ActorClass::setAdditionalParams(StringInterface&& params, replace_parameters_t)
{
	if (!mAdditionalParams)
		mAdditionalParams = std::make_unique<StringInterface>();

	mAdditionalParams->merge(std::move(params));
}

void ActorClass::setAdditionalParams(const StringInterface& params, replace_parameters_t)
{
	if (!mAdditionalParams)
		mAdditionalParams = std::make_unique<StringInterface>();

	mAdditionalParams->merge(params);
}

void ActorClass::_changeType(const Ogre::String& type) const NOEXCEPT 
{ 
	if (mType != Ogre::StringUtil::BLANK)
	{
		DEBUG_OUTPUT(LVL_YELLOW, "Actor type changed from \"" + mType + 
			"\" to \"" + type + '\"');
	}

	mType = type; 
}

void MovableObjectListenerImpl::_addLightTypes(Ogre::MovableObject* obj)
{
	Ogre::LightList* lightList = obj->_getLightList();
	if (!lightList->empty())
	{
		// using float here, since Ogre does not support int for shader parameters
		std::array<float, SCENE_LIGHTS_MAX> lightTypes;

		// safe to dereference here, since the light list is on the stack
		fillLightArray(*lightList, lightTypes);

		const Ogre::Entity* entity = static_cast<const Ogre::Entity*>(obj);
		for (size_t i = 0; i < entity->getNumSubEntities(); ++i)
		{
			entity->getSubEntity(i)->setCustomParameter(0, 
				Ogre::Vector4(lightTypes.data()));
		}
	}
}

void MovableObjectListenerImpl::_addLightTypes(Ogre::Renderable* renderable)
{
	const Ogre::LightList& lightList = renderable->getLights();
	if (!lightList.empty())
	{
		// using float here, since Ogre does not support int for shader parameters
		std::array<float, SCENE_LIGHTS_MAX> lightTypes;
		fillLightArray(lightList, lightTypes);

		renderable->setCustomParameter(0, Ogre::Vector4(lightTypes.data()));
	}
}

// Ogre::MovableObject::Listener
const Ogre::LightList* MovableObjectListenerImpl::objectQueryLights(
	const Ogre::MovableObject* obj)
{
	_addLightTypes(const_cast<Ogre::MovableObject*>(obj));

	// return nullptr - this does not populate the light list, 
	// continue with the standard Ogre::MovableObject::queryLights
	return nullptr; 
}

MovableObjectListenerImpl Actor::mMovableObjectListenerImpl;

Actor::Actor(const ActorClass& desc)
try
  : mName(desc.mName),
	mNode(nullptr),
	mEntity(nullptr),
	mCreator(desc.mCreator),
	mParent(nullptr),
	mIsTagPoint(desc.mIsTagPoint),
	mIsVisible(true),
	mDestroy(false)
{
	assert(desc.mName != Ogre::StringUtil::BLANK && 
		"actor name cannot be blank");

	assert(desc.mCreator &&
		"actor must have a valid creator");

	// create the actor hierarchy from top, down, creating children if any first
	reserveForActors(desc.mChildren.size());

	for (const auto& child : desc.mChildren)
		createChild(child);

	if (desc)
	{ // if actor is not empty
		assert((desc.mEntity || desc.mMeshFilename != Ogre::StringUtil::BLANK) && 
			"actor must be provided with either a valid entity or a .mesh filename");

		// process the entity
		if (desc.mEntity)
			mEntity = desc.mEntity;
		else 
			mEntity = getSceneManager()->createEntity(desc.mName, desc.mMeshFilename);

		if (desc.mIsTagPoint)
		{ // actor is an entity/tag point
			assert(desc.mBoneName != Ogre::StringUtil::BLANK &&
				"actor created with a tag point must have a valid bone name");

			assert(desc.mParentEntity &&
				"actor created with a tag point must have a valid parent entity");

			mNode = desc.mParentEntity->attachObjectToBone(desc.mBoneName, mEntity);
		}
		else
		{ // actor is an entity/scene node
			assert(desc.mNode &&
				"actor created with a scene node must have a valid scene node "
				"(usable or parent)");

			if (desc.mIsNodeParent)
				mNode = desc.mNode->createChild(desc.mName);
			else
				mNode = desc.mNode;

			initSceneNode(desc);
		}

		if (desc.mNodeTranslate != Ogre::Vector3::ZERO)
			mNode->translate(desc.mNodeTranslate);

		if (desc.mNodeRotate != Ogre::Quaternion::IDENTITY)
			mNode->rotate(desc.mNodeRotate);

		if (desc.mNodeScale != Ogre::Vector3::UNIT_SCALE)
			mNode->scale(desc.mNodeScale);

		setVisible(desc.mIsVisible);

		mNode->setInitialState();
		mEntity->setListener(&mMovableObjectListenerImpl);
	}

	DEBUG_OUTPUT(LVL_GREEN, "Actor \"" + mName + "\" created");
}
catch (const Ogre::Exception& e)
{
	DEBUG_OUTPUT(LVL_RED, "Cannot create an actor with name \"" + 
		desc.getName() + "\": " + e.getDescription());

	Actor::~Actor();
}
catch (...)
{
	DEBUG_OUTPUT(LVL_RED, "Cannot create an actor with name \"" + 
		desc.getName() + "\": Uncaught exception");

	Actor::~Actor();
}

Actor::~Actor()
{
	try {
		if (mEntity)
		{
			//mEntity->setListener(nullptr);
			getSceneManager()->destroyEntity(mEntity);
		}

		// entity deletes all of its tag points (nodes), only deal with scene nodes
		if (!mIsTagPoint)
		{
			Ogre::SceneNode* sceneNode = getSceneNode();
			if (sceneNode)
				getSceneManager()->destroySceneNode(sceneNode);
		}

		clearChildren();
	}
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy an actor");
	}

	DEBUG_OUTPUT(LVL_GREEN, "Actor \"" + mName + "\" destroyed");
}

Actor::Actor(const Actor& rhs, Ogre::Node* parentNode)
  : mChildren(rhs.mChildren),
	mName(getNextCopyName(rhs.mName)),
	mNode((parentNode) ? parentNode->createChild(mName) : 
		rhs.mNode->getParent()->createChild(mName)),
	mEntity(rhs.mEntity->clone(mName)),
	mUpdateFlags(rhs.mUpdateFlags),
	mCreator(rhs.mCreator),
	mParent(rhs.mParent),
	mIsTagPoint(rhs.mIsTagPoint),
	mIsVisible(rhs.mIsVisible),
	mDestroy(rhs.mDestroy)
{
	// NOTE: Ogre::Node has no copy or move semantics, below does not 
	// copy children etc.
	mNode->setInheritOrientation(rhs.mNode->getInheritOrientation());
	mNode->setInheritScale(rhs.mNode->getInheritScale());
	mNode->setListener(rhs.mNode->getListener());
	mNode->setOrientation(rhs.mNode->getOrientation());
	mNode->setPosition(rhs.mNode->getPosition());
	mNode->setScale(rhs.mNode->getScale());

	getSceneNode()->detachAllObjects();
	getSceneNode()->attachObject(mEntity);

	mNode->_update(true, (parentNode != nullptr));

	setVisible(mIsVisible);
}

Actor& Actor::operator=(const Actor& rhs)
{
	// NOTE: cannot take param 'rhs' by value, clashes with move assignment
	// cannot reuse Actor's copy constructor, since Actor has virtual functions
	if (this != std::addressof(rhs))
	{
		// call the base class copy assignment operator 
		// ...

		// copy the children
		mChildren = rhs.mChildren;

		// copy and suffix the name
		mName = getNextCopyName(rhs.mName);

		// copy only some of the main attributes of the scene node
		// NOTE: no copy semantics for Ogre::SceneNode
		mNode->setPosition(rhs.mNode->getPosition());
		mNode->setOrientation(rhs.mNode->getOrientation());
		mNode->setScale(rhs.mNode->getScale());
		// NOTE: no Ogre::SceneNode::isVisible
		//mNode->setVisible(rhs.mNode->isVisible()); 

		// deep copy the entity
		mEntity = rhs.mEntity->clone(mName);
		getSceneNode()->attachObject(mEntity);

		mUpdateFlags = rhs.mUpdateFlags;
		mCreator = rhs.mCreator;
		mParent = rhs.mParent;
		mIsTagPoint = rhs.mIsTagPoint;
		mIsVisible = rhs.mIsVisible;
		mDestroy = rhs.mDestroy;
	}

	return *this;
}

Actor::Actor(Actor&& rhs) NOEXCEPT
  : mChildren(std::move(rhs.mChildren)),
	mName(std::move(rhs.mName)),
	mNode(rhs.mNode),
	mEntity(rhs.mEntity),
	mUpdateFlags(rhs.mUpdateFlags),
	mCreator(rhs.mCreator),
	mParent(rhs.mParent),
	mIsTagPoint(rhs.mIsTagPoint),
	mIsVisible(rhs.mIsVisible),
	mDestroy(rhs.mDestroy)
{
	// release pointer ownership from the source object so that the 
	// destructor does not free the memory multiple times
	// not necessary since the pointers are non-owning
	rhs.mNode = nullptr;
	rhs.mEntity = nullptr;
}

Actor& Actor::operator=(Actor&& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		// ...

		// free existing resources of the source
		Scene::getSingleton().destroyActor(this);

		// copy data from the source object
		mChildren = std::move(rhs.mChildren);
		mName = std::move(rhs.mName);
		mNode = rhs.mNode;
		mEntity = rhs.mEntity;
		mUpdateFlags = rhs.mUpdateFlags;
		mCreator = rhs.mCreator;
		mParent = rhs.mParent;
		mIsTagPoint = rhs.mIsTagPoint;
		mIsVisible = rhs.mIsVisible;
		mDestroy = rhs.mDestroy;

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		rhs.mNode = nullptr;
		rhs.mEntity = nullptr;
	}

	return *this;
}

void Actor::initSceneNode(const ActorClass& desc)
{
	Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(mNode);

	// check if the entity is attached to the scene node
	if (mEntity->isAttached())
	{
		if (mEntity->getParentNode() != mNode)
		{
			mEntity->detachFromParent();
			sceneNode->attachObject(mEntity);
		}
	}
	else
		sceneNode->attachObject(mEntity);

	// check if the node is attached to a parent node
	if (!sceneNode->isInSceneGraph())
		getSceneManager()->getRootSceneNode()->addChild(mNode);
}

void Actor::update(const Ogre::Real deltaSeconds)
{
	for (ActorPtrList::size_type i = 0; i < mChildren.size();)
	{
		Actor* child = mChildren[i].get();
		if (child->getDestroyFlag())
			removeChild(child);
		else
		{
			child->update(deltaSeconds);
			++i;
		}
	}
}

void Actor::reset()
{
	mNode->resetToInitialState();
	mUpdateFlags.setAll();

	for (const auto& child : mChildren)
		child->reset();
}

void Actor::show()
{
	if (!mIsVisible)
	{
		if (mIsTagPoint)
			mEntity->setVisible(true);
		else
			getSceneNode()->setVisible(true);

		mIsVisible = true;
	}
}

void Actor::hide()
{
	if (mIsVisible)
	{
		// NOTE: cannot detach the entity from the scene node to 
		// increase performance, since it returns to object space, i.e. (0, 0, 0)
		if (mIsTagPoint)
			mEntity->setVisible(false);
		else
			getSceneNode()->setVisible(false);

		mIsVisible = false;
	}
}

Ogre::SceneNode* Actor::getSceneNode() const 
{
	assert(!mIsTagPoint && 
		"actor's node is not a scene node");

	return static_cast<Ogre::SceneNode*>(mNode); 
}

Ogre::TagPoint* Actor::getTagPoint() const 
{ 
	assert(mIsTagPoint && 
		"actor's node is not a tag point");

	return static_cast<Ogre::TagPoint*>(mNode);
}

Actor* Actor::getChild(const Ogre::String& nameOrType)
{
	for (const auto& child : mChildren)
	{
		if (child->getName() == nameOrType ||
			child->getActorType() == nameOrType)
			return child.get();
	}

	OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, 
		"Child with name or type \"" + nameOrType + 
		"\" was not found for actor \"" + mName + '\"', 
		"Actor::getChild");

}

Actor* Actor::createActor(const ActorClass& desc)
{
	return createChild(desc);
}

Actor* Actor::createChild(const ActorClass& desc)
{
	std::shared_ptr<Actor> actor = createUnmanagedActor(desc);
	addChild(actor);

	return actor.get();
}

std::shared_ptr<Actor> Actor::createUnmanagedActor(const ActorClass& desc)
{
	return mCreator->createUnmanagedActor(desc);
}

void Actor::reserveForActors(const size_t numActors)
{
	mChildren.reserve(mChildren.size() + numActors);
}

bool Actor::hasChildren() NOEXCEPT
{
	return (!mChildren.empty());
}

void Actor::addChild(std::shared_ptr<Actor> actor)
{
	actor->setParent(this);

	// moves the shared_ptr copy to the container
	mChildren.push_back(std::move(actor)); 
}

void Actor::removeChild(Actor* actor)
{
	const ActorPtrList::const_iterator it = std::find_if(
		mChildren.begin(), mChildren.end(), 
		[&actor](const ActorPtrList::value_type& smartPtr) NOEXCEPT {
			return (smartPtr.get() == actor);
		}
	);

	if (it != mChildren.end())
	{
		// remove from actor's children
		mChildren.erase(it);
	}
	else
	{
		DEBUG_OUTPUT(LVL_GREEN, "An actor with name \"" + actor->getName() + 
			"\" could not be found");
	}
}

void Actor::clearChildren() NOEXCEPT
{
	mChildren.clear();
}

Actor* Actor::find(const Ogre::String& name)
{
	if (mName == name)
		return this;

	if (this->hasChildren())
	{
		for (const auto& child : mChildren)
		{
			Actor* result = child->find(name);
			if (result)
				return result; 
		}
	}

	return nullptr;
}

void Actor::attachToBone(Ogre::MovableObject* object, 
	const Ogre::Vector3 translate, const Ogre::Quaternion rotate)
{
	Ogre::Entity* parentEntity = getTagPoint()->getParentEntity();
	parentEntity->attachObjectToBone(getTagPoint()->getParent()->getName(), object);

	getNode()->translate(translate);
	getNode()->rotate(rotate);
}

void Actor::reattachToBone(const Ogre::String& boneName, 
	Ogre::Entity* entityWithBone)
{
	Ogre::Entity* parentEntity = getTagPoint()->getParentEntity();
	parentEntity->detachObjectFromBone(mEntity);

	if (entityWithBone)
		mNode = entityWithBone->attachObjectToBone(boneName, mEntity);
	else
		mNode = parentEntity->attachObjectToBone(boneName, mEntity);
}

void Actor::changeEntity(const Ogre::String& meshFilename)
{
	assert(!mIsTagPoint && 
		"changing entity on tag points is not supported");

	getSceneManager()->destroyEntity(mEntity);
	mEntity = getSceneManager()->createEntity(mName, meshFilename);
}

// a workaround to enumarate the copies of Actors in copy assignment 
// and copy constructor
/*Ogre::String Actor::incrementCopyName(Ogre::String copyName)
{
	Ogre::String newSuffix;
	if (copyName.find("copy") == copyName.npos) // if "copy" not found in the name
	{
		newSuffix = "_copy_1";
	}
	else // if "copy" found
	{
		// extract the copy number and increment it by 1
		std::size_t numberPosition = copyName.find_last_of("_") + 1;
		unsigned int number = 
			Ogre::StringConverter::parseInt(copyName.substr(numberPosition)) + 1;

		newSuffix = Ogre::StringConverter::toString(number);

		// erase the old number
		copyName.erase(numberPosition); // string::replace() does not fit here
	}

	return copyName + newSuffix;
}*/

ActorFactory::ActorFactory() 
{
}

ActorFactory::~ActorFactory() 
{
}

/*	 
	Predicates on std::find and std::sort (among others) are still required for 
	std::vector<std::shared_ptr<Actor>> and Actor*:
		std::find dereferences an iterator to get the container element,
			which is shared_ptr<Actor> in this case and not Actor*, therefore a
			std::find_if with a predicate still has to be used for correct behaviour.
		std::sort works with a shared_ptr to get() the underlying pointer, 
			thus comparing memory addresses, therefore a std::sort with a 
			binary comparison function still has to be used for correct behaviour.
*/

NODISCARD inline bool operator==(const Actor& lhs, const Actor& rhs) NOEXCEPT
{
	// no two actors can have the same name; follows from Ogre's behaviour
	return (lhs.getName() == rhs.getName());
}

NODISCARD inline bool operator!=(const Actor& lhs, const Actor& rhs) NOEXCEPT
{
	return !(lhs == rhs);
}

NODISCARD inline bool operator<(const Actor& lhs, const Actor& rhs) NOEXCEPT
{
	return (lhs.getName() < rhs.getName());
}

NODISCARD inline bool operator>(const Actor& lhs, const Actor& rhs) NOEXCEPT
{
	return rhs < lhs;
}

NODISCARD inline bool operator<=(const Actor& lhs, const Actor& rhs) NOEXCEPT
{
	return !(rhs < lhs);
}

NODISCARD inline bool operator>=(const Actor& lhs, const Actor& rhs) NOEXCEPT
{
	return !(lhs < rhs);
}

std::ostream& operator<<(std::ostream& os, const Actor& actor) NOEXCEPT
{
	os << actor.mName << '\t'
	   << actor.getActorType() << '\t'
	   << actor.getActorGroup() << '\t';

	if (actor.mNode)
		os << actor.mNode->getName();
	else
		os << "NULL";

	os << '\t';

	if (actor.mEntity)
		os << actor.mEntity->getName();
	else
		os << "NULL";

	os << '\t';

	os << actor.mUpdateFlags << '\t';

	if (actor.mParent)
		os << actor.mParent->getName();
	else
		os << "NULL";

	os << '\t';

	os << std::boolalpha
	   << actor.mIsTagPoint << '\t'
	   << actor.mIsVisible << '\t'
	   << actor.mDestroy;

	return os;
}

void swap(Actor& lhs, Actor& rhs) NOEXCEPT
{
	if (std::addressof(lhs) != std::addressof(rhs))
	{
		std::swap(lhs.mChildren, rhs.mChildren);
		std::swap(lhs.mName, rhs.mName);
		std::swap(lhs.mNode, rhs.mNode);
		std::swap(lhs.mEntity, rhs.mEntity);
		std::swap(lhs.mUpdateFlags, rhs.mUpdateFlags);
		std::swap(lhs.mCreator, rhs.mCreator);
		std::swap(lhs.mParent, rhs.mParent);
		std::swap(lhs.mIsTagPoint, rhs.mIsTagPoint);
		std::swap(lhs.mIsVisible, rhs.mIsVisible);
		std::swap(lhs.mDestroy, rhs.mDestroy);
	}
}