// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Terrain.h"

#include "InAppDebug.h"

Terrain::Terrain(const ActorClass& desc, const Ogre::String& polysoupFilename)
  : Actor(desc),
	StaticActor(desc),
	mPolysoup(getSceneManager()->createEntity(desc.getName() + "_polysoup", polysoupFilename))
{
	const Ogre::Matrix4& fullTransform = getNode()->_getFullTransform();
	if (fullTransform != Ogre::Matrix4::IDENTITY)
	{
		// NOTE: otherwise the composite tranformation of the polysoup together 
		// with the terrain would have to be applied to each vertex separately 
		// using Ogre::Matrix4::transformAffine 
		DEBUG_OUTPUT(LVL_YELLOW, "Terrain with name \"" + getName() + 
			"\" must not have any world transformations "
			"(they are ignored for collision detection)");
	}

	// get the mesh and the submesh
	const Ogre::MeshPtr mesh = mPolysoup->getMesh();

	Ogre::SubMesh* subMesh = nullptr;
	try {
		subMesh = mesh->getSubMesh(0);
	} 
	catch (Ogre::Exception&) {
		OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, 
			"Could not get submesh with index 0 from polysoup with mesh \"" + 
			mesh->getName() + "\".", "Terrain::Terrain");
	}

	// get the vertex data
	std::vector<Ogre::Vector3> vertices;
	//std::vector<Ogre::Vector3> normals; // vertex normals

	Ogre::VertexData* vertexData = nullptr;
	if (subMesh->useSharedVertices)
		vertexData = mesh->sharedVertexData;
	else
		vertexData = subMesh->vertexData;

	const size_t vertexCount = vertexData->vertexCount;
	vertices.reserve(vertexCount);
	//normals.reserve(vertexCount);

	// vertex declaration is the way the vertex is described, 
	// as in Direct9 FlexibleVertexFormat, it has type, size, source...
	const Ogre::VertexElement* positionElement = 
		vertexData->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);
	/*const Ogre::VertexElement* normalElement = 
		vertexData->vertexDeclaration->findElementBySemantic(Ogre::VES_NORMAL);*/

	Ogre::HardwareVertexBufferSharedPtr vertexBuffer = 
		vertexData->vertexBufferBinding->getBuffer(positionElement->getSource());

	unsigned char* vertex = static_cast<unsigned char*>(
		vertexBuffer->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
	//unsigned char* normal = vertex + normalElement->getOffset();

	float* real;
	size_t vertexSize = vertexBuffer->getVertexSize();
	// advances the vertex and the normal buffers by the size of a 
	// vertex declaration
	for (size_t i = 0; i < vertexCount; ++i, vertex += vertexSize/*, normal += vertexSize*/)
	{
		// baseVertexPointerToElement sets "real" to the start position in the 
		// buffer where the element <position> under <vertex> begins
		positionElement->baseVertexPointerToElement(vertex, &real);
		vertices.emplace_back(real[0], real[1], real[2]);

		//positionElement->baseVertexPointerToElement(normal, &real);
		//normals.emplace_back(real[0], real[1], real[2]);
	}

	vertexBuffer->unlock();

	// get the index data
	Ogre::HardwareIndexBufferSharedPtr indexBuffer = subMesh->indexData->indexBuffer;

	if (indexBuffer->getType() == Ogre::HardwareIndexBuffer::IndexType::IT_32BIT)
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_NOT_IMPLEMENTED, 
			"32bit mesh indices are not implemented for mesh \"" + 
			mesh->getName() + "\".", "Terrain::Terrain");
	}

	uint16_t* index = static_cast<uint16_t*>(
		indexBuffer->lock(Ogre::HardwareBuffer::LockOptions::HBL_READ_ONLY));

	const size_t indexCount = subMesh->indexData->indexCount;
	const size_t faceCount = indexCount / 3;
	for (size_t i = 0; i < faceCount; ++i, index += sizeof(uint16_t))
	{
		mFaces.emplace_back(
			vertices[index[i + 0]], 
			vertices[index[i + 1]], 
			vertices[index[i + 2]]);
	}

	indexBuffer->unlock();
}

Terrain::~Terrain() 
{
	try {
		if (mPolysoup)
			getSceneManager()->destroyEntity(mPolysoup);
	} 
	catch (...) {
		// do nothing - destructors should not throw
	}
}

// Debuggable.h
void Terrain::initDebuggable()
{
}

// Debuggable.h
void Terrain::showDebuggable()
{
	getSceneNode()->attachObject(mPolysoup);
}

// Debuggable.h
void Terrain::hideDebuggable()
{
	getSceneNode()->detachObject(mPolysoup);
}

std::shared_ptr<Actor> TerrainFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Terrain>(desc,
		desc.getParam("polysoup_filename"));
}