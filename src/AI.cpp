// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "AI.h"

#include "Ball.h"
#include "Region.h"
#include "InAppDebug.h"

// CollisionManager::Listener
void AI::collisionOccured(Collidable* one, Collidable* two)
{
	if (one->getActorType() == ACTOR_BALL_TYPE_NAME)
	{
		if (two->getActorType() == ACTOR_PLAYER_TYPE_NAME ||
			two->getActorType() == ACTOR_AI_TYPE_NAME)
		{
			// get end point of the ball for the computer to move to
			updateEndPoint(static_cast<Ball*>(one)); 
		}
	}

	Paddle::collisionOccured(one, two);
}

// Config::Listener
void AI::propertyChanged(const Config::Property& prop)
{
	if (prop.first == "ai_difficulty")
		changeDifficulty(static_cast<Difficulty>(prop.second.getValue<unsigned int>()));

	Paddle::propertyChanged(prop);
}

// NOTE: MSVC2013 does not support user-defined literals,
// C++14 defines std::chrono literals by default
using std::chrono::milliseconds;

const AI::Settings AI::msEasySettings(
	DIFFICULTY_EASY,
	make_property(15.0f, 25.0f),						// speed
	make_property(8.0f, 14.0f),							// idle speed
	make_property(0.5f, 0.8f),							// hit offset %
	make_property(milliseconds(250), milliseconds(350))	// reaction time
);

const AI::Settings AI::msMediumSettings(
	DIFFICULTY_MEDIUM,
	make_property(25.0f, 35.0f), 
	make_property(10.0f, 16.0f), 
	make_property(0.3f, 0.7f), 
	make_property(milliseconds(150), milliseconds(250))
);

const AI::Settings AI::msHardSettings(
	DIFFICULTY_HARD,
	make_property(35.0f, 45.0f), 
	make_property(14.0f, 20.0f), 
	make_property(0.1f, 0.5f),
	make_property(milliseconds(50), milliseconds(150))
);

AI::AI(const ActorClass& desc)
  :	Actor(desc),
	Paddle(desc),
	mCurrentPhase(Phase::IDLING),
	mSettings(getSettings(
		static_cast<Difficulty>(getConfig().getProperty<unsigned int>("ai_difficulty")))),
	mMoveToPosition(Ogre::Vector3::ZERO),
	mHitPoint(Ogre::Vector3::ZERO),
	mMaxLeft((getRegion()->getBoundingBox().getMaximum() * getRight()).length()),
	mMaxRight((getRegion()->getBoundingBox().getMinimum() * getRight()).length())
{
}

AI::~AI()
{
}

AI::AI(AI&& rhs)
  : Actor(std::move(rhs)),
    Paddle(std::move(rhs)),
	mCurrentPhase(mCurrentPhase),
	mSettings(rhs.mSettings),
	mLogicTimer(std::move(rhs.mLogicTimer)),
	mMoveToPosition(rhs.mMoveToPosition),
	mHitPoint(rhs.mHitPoint),
	mMaxLeft(rhs.mMaxLeft),
	mMaxRight(rhs.mMaxRight)
{
}

AI& AI::operator=(AI&& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		Actor::operator=(std::move(rhs));
		Paddle::operator=(std::move(rhs));

		// free existing resources of the source
		// ...

		// copy data from the source object
		mCurrentPhase = mCurrentPhase;
		mSettings = rhs.mSettings;
		mLogicTimer = std::move(rhs.mLogicTimer);
		mMoveToPosition = rhs.mMoveToPosition;
		mHitPoint = rhs.mHitPoint;
		const_cast<Ogre::Real&>(mMaxLeft) = rhs.mMaxLeft;
		const_cast<Ogre::Real&>(mMaxRight) = rhs.mMaxRight;

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		// ...
	}

	return *this;
}

void AI::updateEndPoint(const Ball* currentBall) NOEXCEPT
{
	if (currentBall->getEndCollidable() == getRegion())
	{ 
		mHitPoint = currentBall->getEndPosition();
		adjustHitPoint();

		setPhase(Phase::MOVING_TO_HIT_BALL);
	}
}

void AI::adjustHitPoint() NOEXCEPT
{
	const Ogre::Real halfLength = getLength() / 2.0f;
	Ogre::Real movementSize = RNG(-halfLength, halfLength);

	movementSize *= mSettings->getRandomHitOffset();
	mHitPoint += movementSize * getRight();
}

void AI::setPhase(Phase phase, bool delayReaction) NOEXCEPT
{
	switch(phase)
	{
	case Phase::MOVING_TO_HIT_BALL:
		DEBUG_TABLE_SET("ai_state_" + getName(), "MOVING_TO_HIT_BALL");
		updatePlayValues();
		break;
	case Phase::WAITING_FOR_BALL:
		break;
	case Phase::MOVING_TO_CENTRE:
		DEBUG_TABLE_SET("ai_state_" + getName(), "MOVING_TO_CENTRE");
		break;
	case Phase::IDLE_MOVING_TO_RANDOM:
		DEBUG_TABLE_SET("ai_state_" + getName(), "IDLE_MOVING_TO_RANDOM");
		updateIdleValues();
		break;
	case Phase::IDLING:
		DEBUG_TABLE_SET("ai_state_" + getName(), "IDLING");
		break;
	default:
		break;
	}

	if (delayReaction)
		idleFor(mSettings->getRandomReactionTime());

	mCurrentPhase = phase;
}

template <typename DurationType>
void AI::idleFor(const DurationType& duration)
{
	mLogicTimer.start(duration);
}

void AI::updateIdleValues() NOEXCEPT
{
	// find the random idle position
	mMoveToPosition = getDefaultPosition() + 
		(RNG(-mMaxLeft, mMaxRight) * getRight());

	// set the random idle speed
	setMovementSpeed(mSettings->getRandomIdleSpeed());
}

void AI::updatePlayValues() NOEXCEPT
{
	setMovementSpeed(mSettings->getRandomSpeed());
}

const AI::Settings* AI::getSettings(const Difficulty difficulty) NOEXCEPT
{
	switch (difficulty)
	{
	case DIFFICULTY_EASY:
		return &msEasySettings;
		break;
	case DIFFICULTY_MEDIUM:
		return &msMediumSettings;
		break;
	case DIFFICULTY_HARD:
		return &msHardSettings;
		break;
	default:
		break;
	}

	return nullptr;
}

void AI::handleCollision(Collidable* collidable, bool isColliding)
{
	if (isColliding)
	{
		if (collidable->getActorType() == ACTOR_BORDER_TYPE_NAME)
		{ 
			switch (mCurrentPhase)
			{
			case Phase::MOVING_TO_HIT_BALL:
				// if the actual end point of the ball is in the corner and the 
				// exact position is not reachable, do not move in place
				setPhase(Phase::WAITING_FOR_BALL);
				break;
			case Phase::WAITING_FOR_BALL:
				break;
			case Phase::MOVING_TO_CENTRE:
				break;
			case Phase::IDLE_MOVING_TO_RANDOM:
				// if AI collides with a border and cannot reach the randomly 
				// generated position when idling
				setPhase(Phase::IDLING);
				break;
			case Phase::IDLING:
				break;
			default:
				break;
			}
		}
	}
	else
	{
		if (collidable->getActorType() == ACTOR_BALL_TYPE_NAME)
			setPhase(Phase::MOVING_TO_CENTRE, true);
	}

	Paddle::handleCollision(collidable, isColliding);
}

void AI::update(const Ogre::Real deltaSeconds)
{
	mLogicTimer.update(deltaSeconds);

	if (mLogicTimer.hasEnded())
	{
		switch(mCurrentPhase)
		{
		case Phase::MOVING_TO_HIT_BALL:
			if (moveTo(deltaSeconds, mHitPoint))
				setPhase(Phase::WAITING_FOR_BALL);
			break;
		case Phase::WAITING_FOR_BALL:
			/* do nothing - wait for a collision callback */
			break;
		case Phase::MOVING_TO_CENTRE:
			if (moveTo(deltaSeconds, getDefaultPosition()))
				setPhase(Phase::IDLING);
			break;
		case Phase::IDLE_MOVING_TO_RANDOM: 
			if (moveTo(deltaSeconds, mMoveToPosition))
				setPhase(Phase::IDLING);
			break;
		case Phase::IDLING:
			setPhase(Phase::IDLE_MOVING_TO_RANDOM);
			break;
		default:
			break;
		};	
	}

	Paddle::update(deltaSeconds);
}

void AI::reset()
{
	mCurrentPhase = Phase::IDLING;

	Paddle::reset();
}

std::shared_ptr<Actor> AIFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<AI>(desc);
}