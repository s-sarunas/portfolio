// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "DynamicActor.h"

#include "InAppDebug.h"
#include "CollisionManager.h"

DynamicClass::DynamicClass()
  :	mLocalDirection(Ogre::Vector3::ZERO),
	mMovementSpeed(NULL),
	mSpeedEquationMultiplier(NULL),
	mAutoMove(true),
	mCanMove(false)
{
}

DynamicClass::DynamicClass(Ogre::Real movementSpeed, bool autoMove, 
	const Ogre::Vector3& localDirection, Ogre::Real speedMultiplier, bool canMove)
  : mLocalDirection(localDirection),
	mMovementSpeed(movementSpeed),
	mSpeedEquationMultiplier(speedMultiplier),
	mAutoMove(autoMove),
	mCanMove(canMove)
{
	assert(speedMultiplier >= 0.0f && 
		"dynamic actor's speed multiplier has to be more than or equal to 0");
	assert(movementSpeed != 0.0f && 
		"dynamic actor's base speed cannot be 0, use a static actor instead");

	mLocalDirection.normalise();
}

DynamicClass::~DynamicClass()
{
}

DynamicClass::DynamicClass(const DynamicClass& rhs)
{
	mLocalDirection = rhs.mLocalDirection;
	mMovementSpeed = rhs.mMovementSpeed;
	mSpeedEquationMultiplier = rhs.mSpeedEquationMultiplier;
	mAutoMove = rhs.mAutoMove;
	mCanMove = rhs.mCanMove;
}

void DynamicActor::notifyActorMoved(DynamicActor* actor)
{
	if (!mListeners.empty())
	{
		for (const auto& listener : mListeners)
			listener->actorMoved(actor);
	}
}

DynamicActor::DynamicActor(const ActorClass& desc, const DynamicClass& dyn)
  : Collidable(desc.getTypeFlag()),
	mVelocity(Ogre::Vector3::ZERO),
	mSpeedEquation(DYNAMIC_ACTOR_SPEED_COEFFICIENT),
	mStep(Ogre::Vector3::ZERO),
	mActualSpeed(NULL),
	mDefaultValues(dyn),
	mSpeedDirty(true),
	mVelocityDirty(true)
{
	resetParameters();
}

DynamicActor::~DynamicActor()
{
}

DynamicActor::DynamicActor(const DynamicActor& rhs, Ogre::SceneNode* parentNode)
  : Collidable(rhs),
	mVelocity(rhs.mVelocity),
	mWorldDirection(rhs.mWorldDirection),
	mSpeedEquation(rhs.mSpeedEquation),
	mStep(rhs.mStep),
	mStatusEffects(rhs.mStatusEffects),
	mMovementSpeed(rhs.mMovementSpeed),
	mSpeedEquationMultiplier(rhs.mSpeedEquationMultiplier),
	mActualSpeed(rhs.mActualSpeed),
	mSpeedDirty(rhs.mSpeedDirty),
	mVelocityDirty(rhs.mVelocityDirty),
	mAutoMove(rhs.mAutoMove),
	mCanMove(rhs.mCanMove),
	mDefaultValues(rhs.mDefaultValues),
	mListeners(rhs.mListeners)
{
}

DynamicActor& DynamicActor::operator=(const DynamicActor& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class copy assignment operator
		Collidable::operator=(rhs);

		// copy data from the source object
		mVelocity = rhs.mVelocity;
		mWorldDirection = rhs.mWorldDirection;
		mSpeedEquation = rhs.mSpeedEquation;
		mStep = rhs.mStep;
		mStatusEffects = rhs.mStatusEffects;
		mMovementSpeed = rhs.mMovementSpeed;
		mSpeedEquationMultiplier = rhs.mSpeedEquationMultiplier;
		mActualSpeed = rhs.mActualSpeed;
		mSpeedDirty = rhs.mSpeedDirty;
		mVelocityDirty = rhs.mVelocityDirty;
		mAutoMove = rhs.mAutoMove;
		mCanMove = rhs.mCanMove;
		const_cast<DynamicClass&>(mDefaultValues) = rhs.mDefaultValues;
		mListeners = rhs.mListeners;
	}

	return *this;
}

DynamicActor::DynamicActor(DynamicActor&& rhs)
  : Collidable(std::move(rhs)),
	mVelocity(rhs.mVelocity),
	mWorldDirection(rhs.mWorldDirection),
	mSpeedEquation(rhs.mSpeedEquation),
	mStep(std::move(rhs.mStep)),
	mStatusEffects(std::move(rhs.mStatusEffects)),
	mMovementSpeed(rhs.mMovementSpeed),
	mSpeedEquationMultiplier(rhs.mSpeedEquationMultiplier),
	mActualSpeed(rhs.mActualSpeed),
	mSpeedDirty(rhs.mSpeedDirty),
	mVelocityDirty(rhs.mVelocityDirty),
	mAutoMove(rhs.mAutoMove),
	mCanMove(rhs.mCanMove),
	mDefaultValues(rhs.mDefaultValues),
	mListeners(std::move(rhs.mListeners))
{
}

DynamicActor& DynamicActor::operator=(DynamicActor&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		Collidable::operator=(std::move(rhs));

		// free existing resources of the source
		// ...

		// copy data from the source object
		mVelocity = rhs.mVelocity;
		mWorldDirection = rhs.mWorldDirection;
		mSpeedEquation = rhs.mSpeedEquation;
		mStep = std::move(rhs.mStep);
		mStatusEffects = std::move(rhs.mStatusEffects);
		mMovementSpeed = rhs.mMovementSpeed;
		mSpeedEquationMultiplier = rhs.mSpeedEquationMultiplier;
		mActualSpeed = rhs.mActualSpeed;
		mSpeedDirty = rhs.mSpeedDirty;
		mVelocityDirty = rhs.mVelocityDirty;
		mAutoMove = rhs.mAutoMove;
		mCanMove = rhs.mCanMove;
		const_cast<DynamicClass&>(mDefaultValues) = rhs.mDefaultValues;
		mListeners = std::move(rhs.mListeners);

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		// ...
	}

	return *this;
}

void DynamicActor::handleCollision(Collidable* collidable, bool isColliding)
{
	if (isColliding)
		stepBack();
}

void DynamicActor::resetParameters() NOEXCEPT
{
	// initialize the dynamic actor with the default values
	(*this) = mDefaultValues; 
}

DynamicActor& DynamicActor::operator=(const DynamicClass& rhs) NOEXCEPT
{
	mMovementSpeed = rhs.mMovementSpeed;
	mSpeedEquationMultiplier = rhs.mSpeedEquationMultiplier;
	mSpeedDirty = true;
	mVelocityDirty = true;
	mAutoMove = rhs.mAutoMove;
	mCanMove = rhs.mCanMove;

	if (mAutoMove)
		setDirection(rhs.mLocalDirection, TRANSFORM_SPACE_LOCAL);
	else
		mWorldDirection = Ogre::Vector3::ZERO;

	return *this;
}

void DynamicActor::update(const Ogre::Real deltaSeconds)
{
	// process all status effects
	if (!mStatusEffects.empty())
	{
		mStatusEffects.erase(std::remove_if(
			mStatusEffects.begin(), mStatusEffects.end(), 
			[this, deltaSeconds](StatusEffect& effect) NOEXCEPT {
				effect.update(deltaSeconds);

				if (effect.ended())
				{
					switch (effect.getStatus())
					{
					case STATUS_EFFECT_STUNNED:
					case STATUS_EFFECT_CASTING:
						start();
						break;
					default:
						break;
					}

					return true;
				}

				return false;
			}
		), mStatusEffects.end());
	}

	Actor::update(deltaSeconds);
}

void DynamicActor::reset() 
{
	Actor::reset();
	Collidable::reset();

	resetParameters();
}

bool DynamicActor::moveTo(const Ogre::Real deltaSeconds, const Ogre::Vector3& pos,
	bool forceMove)
{
	const Ogre::Vector3 nodePos = getNode()->getPosition() * UNIT_VECTOR_MOVEPLANE;
	const Ogre::Vector3 endPos = pos * UNIT_VECTOR_MOVEPLANE;

	if (!nodePos.positionEquals(endPos, DYNAMIC_ACTOR_POSITION_TOLERANCE))
	{
		move(deltaSeconds, (endPos - nodePos).normalisedCopy(), TRANSFORM_SPACE_WORLD);
		return false;
	}

	return true;
}

void DynamicActor::move(const Ogre::Real deltaSeconds,
	const Ogre::Vector3& direction, const TransformSpace relativeTo, 
	bool forceMove)
{
	setDirection(direction, relativeTo);
	move(deltaSeconds, forceMove);
}

void DynamicActor::move(const Ogre::Real deltaSeconds, bool forceMove)
{
	if (mCanMove || forceMove)
	{
		if (mWorldDirection != Ogre::Vector3::ZERO)
		{
			recalculateVelocity();

			stepForward(deltaSeconds);
		}
	}
}

void DynamicActor::reflect(Collidable* collidable)
{
	// get the area of intersection
	const Ogre::AxisAlignedBox intersection = 
		getBoundingBox().intersection(collidable->getBoundingBox());

	// get the center of the area
	const Ogre::Vector3 normalOrigin = 
		eOgre::getCenter(intersection.getMinimum(), intersection.getMaximum());

	// find the normal of the collision using the node's position
	Ogre::Vector3 normal = getNode()->getPosition() - normalOrigin;
	normal.normalise();

	// find the reflection vector using the normal
	// no need to check the dot product since the direction 
	// is always away from the normal
	const Ogre::Vector3 targetDir = getForward().reflect(normal);
	
	// set the new direction in world space
	setSceneNodeDirection(targetDir);
}

void DynamicActor::reflect(DynamicActor* dynamicActor, 
	const Ogre::Degree& degreesPerUnit)
{
	// get the float value offset from the centre of the dynamicActor
	const Ogre::Vector3 offsetVector = 
		getNode()->getPosition() - dynamicActor->getNode()->getPosition();

	const Ogre::Real offset = offsetVector.length();

	// calculate the absolute angle
	Ogre::Radian angle = offset * degreesPerUnit;

	// get the direction of the offset
	const Ogre::Real dot = offsetVector.dotProduct(dynamicActor->getLeft());
	if (dot < 0.0f)
		angle = -angle;

	// get the dynamicActor's world space forward vector
	const Ogre::Vector3 normal = dynamicActor->getForward();

	// calculate the new world space direction
	const Ogre::Vector3 worldDirection = 
		Ogre::Quaternion(angle, UNIT_VECTOR_UP) * normal;

	// set the new direction in world space
	setSceneNodeDirection(worldDirection);
}

void DynamicActor::incrementSpeed() NOEXCEPT 
{ 
	if (mSpeedEquationMultiplier != NULL)
	{
		mSpeedEquationMultiplier += mDefaultValues.mSpeedEquationMultiplier;
		mSpeedDirty = true;
	}
}

Ogre::Real DynamicActor::recalculateSpeed(const Ogre::Real speedBase) NOEXCEPT
{
	if (mSpeedEquationMultiplier != NULL)
		return mSpeedEquation.substitute(mSpeedEquationMultiplier) + speedBase;

	return speedBase;
}

void DynamicActor::recalculateSpeed() NOEXCEPT
{
	if (mSpeedDirty)
	{
		mActualSpeed = recalculateSpeed(mMovementSpeed);

		mSpeedDirty = false;
	}
}

void DynamicActor::recalculateVelocity() NOEXCEPT
{
	if (mVelocityDirty || mSpeedDirty)
	{
		mVelocity = mWorldDirection * recalculateSpeed(mMovementSpeed);

		mVelocityDirty = false;
	}
}

void DynamicActor::recalculateDirection() NOEXCEPT
{
	if (mAutoMove)
		setDirection(mDefaultValues.mLocalDirection, TRANSFORM_SPACE_LOCAL);

	mVelocityDirty = true;
}

void DynamicActor::stepForward(const Ogre::Real deltaSeconds)
{
	mStep = mVelocity * deltaSeconds;

	if (!mAutoMove)
		mWorldDirection = Ogre::Vector3::ZERO;

	translate(mStep * UNIT_VECTOR_MOVEPLANE);
}

void DynamicActor::stepBack()
{
	setPosition(getNode()->getPosition() - mStep);
}

void DynamicActor::translate(const Ogre::Vector3& vector)
{
	if (vector != Ogre::Vector3::ZERO)
	{
		getNode()->translate(vector, Ogre::Node::TS_WORLD);

		getUpdateFlags().set(MIXIN_COLLIDABLE_FLAG | MIXIN_GROUNDED_FLAG);
		notifyActorMoved(this);
	}
}

void DynamicActor::setPosition(const Ogre::Vector3& vector)
{
	getNode()->setPosition(vector);

	getUpdateFlags().set(MIXIN_COLLIDABLE_FLAG | MIXIN_GROUNDED_FLAG);
	notifyActorMoved(this);
}

void DynamicActor::setDirection(const Ogre::Vector3& direction, 
	const TransformSpace relativeTo) NOEXCEPT 
{ 
	switch (relativeTo)
	{
	case TRANSFORM_SPACE_LOCAL:
		mWorldDirection = getNode()->getOrientation() * direction; 
		break;
	case TRANSFORM_SPACE_WORLD:
		mWorldDirection = direction; 
		break;
	default:
		break;
	}

	mVelocityDirty = true;
}

void DynamicActor::setOrientation(const Ogre::Quaternion& orientation) 
{
	getNode()->setOrientation(orientation);
	recalculateDirection();
}

void DynamicActor::setSceneNodeDirection(const Ogre::Vector3& direction)
{
	getSceneNode()->setDirection(direction, 
		Ogre::Node::TS_WORLD, UNIT_VECTOR_FORWARD);

	recalculateDirection();
}