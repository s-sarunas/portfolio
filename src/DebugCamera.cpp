// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "DebugCamera.h"

#include "InAppDebug.h"

DebugCamera::DebugCamera()
  : mCamera(nullptr),
	mDefaultPosition(Ogre::Vector3::ZERO),
	mDefaultDirection(Ogre::Vector3::ZERO),
	mVelocity(Ogre::Vector3::ZERO),
	mEnabled(false)
{
}

DebugCamera::~DebugCamera()
{
}

void DebugCamera::_init()
{
	mCamera = getCamera();

	// NOTE: have to keep tranforms separate, since there is no way
	// to apply a transformation matrix to the camera
	mDefaultPosition = mCamera->getPosition();
	mDefaultDirection = mCamera->getDirection();
}

void DebugCamera::update(const Ogre::Real deltaSeconds) NOEXCEPT
{
	if (mEnabled)
	{
		decelerate(deltaSeconds);

		if (!mDirectionFlags.empty())
		{
			// find the composite direction
			Ogre::Vector3 dir = Ogre::Vector3::ZERO;
			if (mDirectionFlags.isSet(DEBUG_CAMERA_FORWARD)) 
				dir += mCamera->getDirection();
			if (mDirectionFlags.isSet(DEBUG_CAMERA_BACKWARD))
				dir -= mCamera->getDirection();
			if (mDirectionFlags.isSet(DEBUG_CAMERA_RIGHT))
				dir += mCamera->getRight();
			if (mDirectionFlags.isSet(DEBUG_CAMERA_LEFT))
				dir -= mCamera->getRight();

			if (dir != Ogre::Vector3::ZERO)
			{
				dir.normalise();

				accelerate(dir, deltaSeconds);
			}
		}

		// move the camera
		if (mVelocity != Ogre::Vector3::ZERO) 
			mCamera->move(mVelocity * deltaSeconds);
	}
}

void DebugCamera::reset()
{
	mCamera->setPosition(mDefaultPosition);
	mCamera->setDirection(mDefaultDirection);
}

void DebugCamera::accelerate(const Ogre::Vector3& direction, 
	const Ogre::Real deltaSeconds) NOEXCEPT
{
	// don't exceed the top velocity
	if (mVelocity.squaredLength() < DEBUG_CAMERA_SPEED_MAX_SQUARED)
		mVelocity += direction * DEBUG_CAMERA_SPEED_ACCELERATE * deltaSeconds;
}

void DebugCamera::decelerate(const Ogre::Real deltaSeconds) NOEXCEPT
{
	if (mVelocity != Ogre::Vector3::ZERO)
	{
		mVelocity -= mVelocity * DEBUG_CAMERA_SPEED_DECELERATE_PERCENT * deltaSeconds;

		// stop completely if the velocity is small enough
		if (mVelocity.squaredLength() < DEBUG_CAMERA_SPEED_STOP_THRESHOLD_SQUARED)
			mVelocity = Ogre::Vector3::ZERO;
	}
}

void DebugCamera::setEnabled(bool enabled) NOEXCEPT 
{
	mEnabled = enabled;

	mVelocity = Ogre::Vector3::ZERO;
	mDirectionFlags.clearAll();
}

// InputHandler.h
bool DebugCamera::mouseMoved(const SDL_MouseMotionEvent& arg) 
{
	if (mEnabled)
	{
		mCamera->yaw(Ogre::Degree(-arg.xrel * DEBUG_CAMERA_SPEED_ROTATION));
		mCamera->pitch(Ogre::Degree(-arg.yrel * DEBUG_CAMERA_SPEED_ROTATION));

		return false;
	}

	return true;
}

// InputHandler.h
bool DebugCamera::keyPressed(const SDL_KeyboardEvent& arg)
{
	if (arg.keysym.sym == SDLK_F1)
	{
		flipEnabled();
		DEBUG_WRITELN("Debug camera " << (mEnabled ? "enabled" : "disabled"));
	}

	if (mEnabled)
	{
		switch (arg.keysym.sym)
		{
		case SDLK_w:
		case SDLK_UP:
			mDirectionFlags.set(DEBUG_CAMERA_FORWARD);
			return false;
			break;
		case SDLK_s:
		case SDLK_DOWN:
			mDirectionFlags.set(DEBUG_CAMERA_BACKWARD);
			return false;
			break;
		case SDLK_a:
		case SDLK_LEFT:
			mDirectionFlags.set(DEBUG_CAMERA_LEFT);
			return false;
			break;
		case SDLK_d:
		case SDLK_RIGHT:
			mDirectionFlags.set(DEBUG_CAMERA_RIGHT);
			return false;
			break;
		default:
			break;
		}
	}

	return true;
}

// InputHandler.h
bool DebugCamera::keyReleased(const SDL_KeyboardEvent& arg)
{
	if (mEnabled)
	{
		switch (arg.keysym.sym)
		{
		case SDLK_w:
		case SDLK_UP:
			mDirectionFlags.clear(DEBUG_CAMERA_FORWARD);
			return false;
			break;
		case SDLK_s:
		case SDLK_DOWN:
			mDirectionFlags.clear(DEBUG_CAMERA_BACKWARD);
			return false;
			break;
		case SDLK_a:
		case SDLK_LEFT:
			mDirectionFlags.clear(DEBUG_CAMERA_LEFT);
			return false;
			break;
		case SDLK_d:
		case SDLK_RIGHT:
			mDirectionFlags.clear(DEBUG_CAMERA_RIGHT);
			return false;
			break;
		default:
			break;
		}
	}

	return true;
}