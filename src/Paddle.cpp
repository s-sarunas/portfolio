// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Paddle.h"

#include "InAppDebug.h"
#include "CollisionManager.h"
#include "Scene.h"
#include "Shoot.h"
#include "Region.h"
#include "Scoreboard.h"

// CollisionManager::Listener
void Paddle::collisionOccured(Collidable* one, Collidable* two)
{
    if (two == mCharacter)
    {
		if (one->getActorType() == ACTOR_BULLET_TYPE_NAME)
		{
            DynamicActor::applyStatusEffect(
				SHOOT_BULLET_STUN_DURATION, STATUS_EFFECT_STUNNED);
		}
    }
}

// Config::Listener
void Paddle::propertyChanged(const Config::Property& prop)
{
	if (prop.first == "paddle_character")
		changeEntity(prop.second.getValue() + ".mesh");
	else if (prop.first == "paddle_speed_multiplier")
		setSpeedMultiplier(prop.second.getValue<Ogre::Real>());
}

Paddle::Paddle(const ActorClass& desc)
  : Actor(desc),
	DynamicActor(desc, DynamicClass(PADDLE_SPEED_STARTING, false, 
		UNIT_VECTOR_RIGHT, PADDLE_SPEED_INCREASE_MULTIPLIER)),
	Animated(getEntity()),
	Grounded(false),
	mJoystick(nullptr),
	mScore(NULL),
	mLength((getBoundingBox().getSize() * getRight()).length()),
	mCharacter(nullptr),
	mRegion(nullptr),
	mScoreboard(nullptr)
{
	getNode()->scale(Ogre::Vector3(1.1f, 1.1f, 1.1f));

	// create child actors
	const Ogre::String& nodeName = getNode()->getName();

	// create a character
	ActorClass characterDesc;
	characterDesc.setName(ACTOR_CHARACTER_TYPE_NAME + '_' + nodeName);
	characterDesc.setType(ACTOR_CHARACTER_TYPE_NAME);
	characterDesc.setEntity(getConfig().getProperty("paddle_character"));
	characterDesc.setNode(getNode(), create_child_node);
	characterDesc.setTranslate(UNIT_VECTOR_UP * 3.0f);
	characterDesc.addParam("weapon_filename", "weapon.mesh", PARAMETER_TYPE_STRING);

	mCharacter = dynamic_cast<Character*>(createChild(characterDesc));

	mRegion = dynamic_cast<Region*>(getChild(ACTOR_REGION_TYPE_NAME));

	// create a scoreboard
	ActorClass scoreboardDesc;
	scoreboardDesc.setName(ACTOR_SCOREBOARD_TYPE_NAME + '_' + nodeName);
	scoreboardDesc.setType(ACTOR_SCOREBOARD_TYPE_NAME);
	scoreboardDesc.setEntity(SCOREBOARD_MESH_NAME);

	mScoreboard = dynamic_cast<Scoreboard*>(createChild(scoreboardDesc));

	getCollisionMask().set(ACTOR_BALL_TYPE_FLAG | ACTOR_BORDER_TYPE_FLAG);

	CollisionManager::getSingleton().addListener(this);
	getConfig().addListener(this);

	// for debugging only
	/*DEBUG_TABLE_ADD(desc.mName + "_top_anim", 
		[this]() -> Ogre::String {
			Ogre::AnimationState* state = nullptr;
			if (mCharacter)
				 state = *mCharacter->getTopAnimation();

			if (state)
			{
				return state->getAnimationName() + '(' + 
					Ogre::StringConverter::toString(state->getTimePosition()) + 
					')';
			}
			else
				return "nullptr";
		}
	);

	DEBUG_TABLE_ADD(desc.mName + "_bot_anim", 
		[this]() -> Ogre::String {
			Ogre::AnimationState* state = nullptr;
			if (mCharacter)
				state = *mCharacter->getBotAnimation();

			if (state)
			{
				return state->getAnimationName() + '(' + 
					Ogre::StringConverter::toString(state->getTimePosition()) + 
					')';
			}
			else
				return "nullptr";
		}
	);*/
}

Paddle::~Paddle()
{
	getConfig().removeListener(this);
	CollisionManager::getSingleton().removeListener(this);
}

Paddle::Paddle(Paddle&& rhs)
  : Actor(std::move(rhs)),
	DynamicActor(std::move(rhs)),
	Animated(std::move(rhs)),
	Grounded(std::move(rhs)),
	mMoveFlags(rhs.mMoveFlags),
	mMouseMotion(std::move(mMouseMotion)),
	mJoystick(rhs.mJoystick),
	mScore(rhs.mScore),
	mLength(rhs.mLength),
	mCharacter(rhs.mCharacter),
	mRegion(rhs.mRegion),
	mScoreboard(rhs.mScoreboard),
	mAbilitiesManager(std::move(rhs.mAbilitiesManager))
{
	Actor::removeChild(mCharacter);
	Actor::removeChild(mRegion);
	Actor::removeChild(mScoreboard);

	rhs.mJoystick = nullptr;
	rhs.mCharacter = nullptr;
	rhs.mRegion = nullptr;
	rhs.mScoreboard = nullptr;
}

Paddle& Paddle::operator=(Paddle&& rhs)
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		Actor::operator=(std::move(rhs));
		DynamicActor::operator=(std::move(rhs));
		Animated::operator=(std::move(rhs));
		Grounded::operator=(std::move(rhs));

		// free existing resources of the source
		Actor::removeChild(mCharacter);
		Actor::removeChild(mRegion);
		Actor::removeChild(mScoreboard);

		// copy data from the source object
		mMoveFlags = rhs.mMoveFlags;
		mMouseMotion = std::move(rhs.mMouseMotion);
		mJoystick = rhs.mJoystick;
		mScore = rhs.mScore;
		mLength = rhs.mLength;
		mCharacter = rhs.mCharacter;
		mRegion = rhs.mRegion;
		mScoreboard = rhs.mScoreboard;
		mAbilitiesManager = std::move(rhs.mAbilitiesManager);

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		rhs.mJoystick = nullptr;
		rhs.mCharacter = nullptr;
		rhs.mRegion = nullptr;
		rhs.mScoreboard = nullptr;
	}

	return *this;
}

void Paddle::translate(const Ogre::Vector3& vector)
{
	DynamicActor::translate(vector);

	// in addition to DynamicActor::translate(), the animation has to be 
	// changed for this actor
	if (vector != Ogre::Vector3::ZERO)
	{
		mCharacter->getBotAnimation().changeTo(
			CHARACTER_ANIMATION_BOT_RUN_NAME, false);
	}
}

void Paddle::update(const Ogre::Real deltaSeconds)
{
	DynamicActor::update(deltaSeconds);
	Animated::update(deltaSeconds);
	Grounded::update(deltaSeconds);

	mAbilitiesManager.update(deltaSeconds);

	if (!isAI() && canMove())
	{
		// NOTE: analogue input events are flooded in the buffered input with 
		// each having very small relative motion thus creating a lot of 
		// unnecessary calculations reducing performance, unbuffered workaround:
		Sint16 joystickValue = 0;
		if (mJoystick)
			joystickValue = SDL_JoystickGetAxis(mJoystick, PADDLE_JS_MOVEMENT_AXIS);

		if (joystickValue != 0) 
		{ // joystick input
			// NOTE: there is a small discrepancy with integer max being bigger 
			// than min in magnitude, could possibly use:
			// abs(std::numeric_limits<>::min()) for negatives, but not worth 
			// it due to floating point precision
			const Ogre::Real relative = static_cast<Ogre::Real>(-joystickValue) *
				(1.0f / std::numeric_limits<decltype(joystickValue)>::max());

			setDirection(Ogre::Vector3(relative, 0.0f, 0.0f));
			setAutoMove(true);
		}
		else 
		{
			if (!mMouseMotion.empty())
			{ // mouse input
				// find the harmonic average of the mouse input
				const Ogre::Vector2 inverseSum = std::accumulate(
					mMouseMotion.begin(), mMouseMotion.end(), Ogre::Vector2::ZERO, 
					[](const Ogre::Vector2 lhs, const Ogre::Vector2 rhs) NOEXCEPT {
						return lhs + (1.0f / rhs);
					}
				);

#pragma warning (push)
				// NOTE: generates a C4244 warning, because size_t (an integer)
				// cannot exactly be represented as a float (Ogre::Real) due to
				// floating point precision, not an issue in this case
#pragma warning (disable : 4244)
				const Ogre::Vector2 harmonicMean = mMouseMotion.size() / inverseSum;
#pragma warning (pop)

				mMouseMotion.clear();

				Ogre::Real relX = estd::clamp(
					harmonicMean.x * PADDLE_MOUSE_SENSITIVITY,
					-PADDLE_MOUSE_RESOLUTION, PADDLE_MOUSE_RESOLUTION);

				relX /= PADDLE_MOUSE_RESOLUTION;

				/*Ogre::Real relY = estd::clamp(
					harmonicMean.y * PADDLE_MOUSE_SENSITIVITY, 
					-PADDLE_MOUSE_RESOLUTION, PADDLE_MOUSE_RESOLUTION);

				relY /= PADDLE_MOUSE_RESOLUTION;*/

				setDirection(Ogre::Vector3(-relX, 0.0f, 0.0f/*relY*/));
			}
			else if (!mMoveFlags.empty())
			{ // keyboard input
				// get the compound velocity
				// NOTE: positive x-axis is to the left of the paddle 
				// (right-handed system)
				Ogre::Vector3 direction = Ogre::Vector3::ZERO;
				/*if (mMoveFlags.isSet(PADDLE_MOVE_FORWARD)) 
					velocity += UNIT_VECTOR_FORWARD;
				if (mMoveFlags.isSet(PADDLE_MOVE_BACKWARD))
					velocity -= UNIT_VECTOR_FORWARD;*/
				if (mMoveFlags.isSet(PADDLE_MOVE_RIGHT))
					direction += UNIT_VECTOR_RIGHT;
				if (mMoveFlags.isSet(PADDLE_MOVE_LEFT))
					direction -= UNIT_VECTOR_RIGHT;

				setDirection(direction);
			}

			setAutoMove(false);
		}

		move(deltaSeconds);
	}
}

void Paddle::reset()
{
	resetParameters();
}

void Paddle::handleCollision(Collidable* collidable, bool isColliding)
{
	if (collidable->getActorType() == ACTOR_BALL_TYPE_NAME)
		incrementSpeed();

	DynamicActor::handleCollision(collidable, isColliding);
}

void Paddle::setScore(score_type score)
{ 
	mScore = score; 
	mScoreboard->setText(Ogre::StringConverter::toString(mScore)); 
}

// Debuggable.h
void Paddle::setDebuggableVisibility(bool flag)
{
	mRegion->setDebuggableVisibility(flag);
}

// InputHandler.h
bool Paddle::mouseMoved(const SDL_MouseMotionEvent& arg)   
{
	mAbilitiesManager.mouseMoved(arg);

	mMouseMotion.push_back(Ogre::Vector2(
		static_cast<Ogre::Real>(arg.xrel), 
		static_cast<Ogre::Real>(arg.yrel)));

	return true;
}

// InputHandler.h
bool Paddle::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	mAbilitiesManager.mousePressed(arg, id);

	return true;
}

// InputHandler.h
bool Paddle::mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	mAbilitiesManager.mouseReleased(arg, id);

	return true;
}

// InputHandler.h
bool Paddle::textInput(const SDL_TextInputEvent& arg) 
{
	mAbilitiesManager.textInput(arg);

	return true;
}

// InputHandler.h
bool Paddle::keyPressed(const SDL_KeyboardEvent& arg) 
{
	mAbilitiesManager.keyPressed(arg);

	switch(arg.keysym.sym)
	{
	case PADDLE_BUTTON_KB_RIGHT:
		mMoveFlags.set(PADDLE_MOVE_RIGHT);
		break;
	case PADDLE_BUTTON_KB_LEFT:
		mMoveFlags.set(PADDLE_MOVE_LEFT);
		break;
	case PADDLE_BUTTON_KB_ABILITY_DASH:
		mAbilitiesManager.useAbility<Dash>(this);
		break;
	case PADDLE_BUTTON_KB_ABILITY_SHOOT:
		mAbilitiesManager.useAbility<Shoot>(this);
		break;
	default:
		break;
	}

	return true;
}

// InputHandler.h
bool Paddle::keyReleased(const SDL_KeyboardEvent& arg) 
{
	mAbilitiesManager.keyReleased(arg);

	switch (arg.keysym.sym)
	{
	case PADDLE_BUTTON_KB_RIGHT:
		mMoveFlags.clear(PADDLE_MOVE_RIGHT);
		break;
	case PADDLE_BUTTON_KB_LEFT:
		mMoveFlags.clear(PADDLE_MOVE_LEFT);
		break;
	default:
		break;
	}

	return true;
}

// InputHandler.h
bool Paddle::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
{
	mAbilitiesManager.joyButtonPressed(evt, button);

	switch(button)
	{
	case PADDLE_BUTTON_JS_ABILITY_DASH:
		mAbilitiesManager.useAbility<Dash>(this);
		break;
	case PADDLE_BUTTON_JS_ABILITY_SHOOT:
		mAbilitiesManager.useAbility<Shoot>(this);
		break;
	default:
		break;
	}

	return true;
}

// InputHandler.h
bool Paddle::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	mAbilitiesManager.joyButtonReleased(evt, button);

	return true;
}

// InputHandler.h
bool Paddle::joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) 
{
	mAbilitiesManager.joyAxisMoved(arg, axis);

	return true;
}

// InputHandler.h
bool Paddle::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
{
	mAbilitiesManager.joyPovMoved(arg, index);

	switch (arg.value)
	{
	case SDL_HAT_CENTERED:
		mMoveFlags.clear(PADDLE_MOVE_RIGHT | PADDLE_MOVE_LEFT);
		break;
	case SDL_HAT_UP:
		break;
	case SDL_HAT_RIGHT:
	case SDL_HAT_RIGHTDOWN:
	case SDL_HAT_RIGHTUP:
		mMoveFlags.set(PADDLE_MOVE_RIGHT);
		mMoveFlags.clear(PADDLE_MOVE_LEFT);
		break;
	case SDL_HAT_DOWN:
		break;
	case SDL_HAT_LEFT:
	case SDL_HAT_LEFTDOWN:
	case SDL_HAT_LEFTUP:
		mMoveFlags.set(PADDLE_MOVE_LEFT);
		mMoveFlags.clear(PADDLE_MOVE_RIGHT);
		break;
	default:
		break;
	}

	return true;
}

std::shared_ptr<Actor> PaddleFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Paddle>(desc);
}