// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Border.h"

Border::Border(const ActorClass& desc, const Ogre::String& type)
  : Actor(desc),
	StaticActor(desc),
	Collidable(desc.getTypeFlag()),
	mType(type) 
{
}

Border::~Border() 
{
}

void Border::handleCollision(Collidable* collidable, bool isColliding)
{
}

std::shared_ptr<Actor> BorderFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Border>(desc, 
		desc.getParam("border_type"));
}