// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Bullet.h"

#include "StaticActor.h"
#include "InAppDebug.h"

Bullet::Bullet(const ActorClass& desc, const Ogre::Vector3& direction)
  : Actor(desc),
	DynamicActor(desc, DynamicClass(BULLET_SPEED, true, direction))
{
	getCollisionMask().setAll(); // a bullet collides with everything
}

Bullet::~Bullet()
{
}

void Bullet::update(const Ogre::Real deltaSeconds)
{
	DynamicActor::update(deltaSeconds);

	move(deltaSeconds);

	checkBounds();
}

void Bullet::handleCollision(Collidable* collidable, bool isColliding)
{
	DynamicActor::handleCollision(collidable, isColliding);

	setDestroyFlag(true);
}

void Bullet::checkBounds() NOEXCEPT
{
	if (getNode()->getPosition().squaredLength() == BULLET_SELF_DESTRUCT_LIMIT_SQUARED)
	{
		DEBUG_OUTPUT(LVL_YELLOW, "Bullet with name \"" + getName() + 
			"\" reached out of bounds", true);

		setDestroyFlag(true);
	}
}

std::shared_ptr<Actor> BulletFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Bullet>(desc, 
		desc.getParam<Ogre::Vector3>("direction"));
}
