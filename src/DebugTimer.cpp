// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "DebugTimer.h"

#include <numeric>

#include "InAppDebug.h"

DebugTimer::DebugTimer()
{
}

DebugTimer::~DebugTimer()
{
}

void DebugTimer::start(const Ogre::String& name) NOEXCEPT
{
	if (mName != name)
		mResults.clear();

	mName = name;
	mTimer.reset();
}

void DebugTimer::end() NOEXCEPT
{
	try {
		Ogre::ulong time = mTimer.getMicroseconds();

		DEBUG_WRITELN("Debug timer \"" << mName << "\" ended with time: " << 
			time << "microseconds.");

		mResults.push_back(time);
	} 
	catch (...) {
		DEBUG_WRITELN("Cannot end debug timer \"" << mName << "\".");
	}
}

void DebugTimer::outputAverage()
{
	if (!mResults.empty())
	{
		auto num = mResults.size();
		auto avg = std::accumulate(mResults.begin(), mResults.end(), NULL) / num;

		DEBUG_WRITELN("Average times from " << num << " timer results: " << 
			avg << " microseconds.");
	}
	else
		DEBUG_WRITELN("Debug timer results are empty.");
}

void DebugTimer::clear() NOEXCEPT
{
	mResults.clear();
}