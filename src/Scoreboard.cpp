// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Scoreboard.h"

#include "InAppDebug.h"

// Paddle.h
void Scoreboard::scored(const unsigned int newScore)
{
	setText(Ogre::StringConverter::toString(newScore));
}

// Ogre::Camera::Listener
void Scoreboard::cameraPreRenderScene(Ogre::Camera* cam)
{
	// Ogre::MovableObject::Listener doesn't fire on a Ogre::Camera,
	// so have to check manually if camera transforms have changed
	Ogre::Matrix4 worldTransforms;
	getCamera()->getWorldTransforms(&worldTransforms);
	if (worldTransforms != mLastCameraTransforms)
		updateTransforms();
}

// Ogre::Camera::Listener
void Scoreboard::cameraPostRenderScene(Ogre::Camera* cam)
{
}

// Ogre::Camera::Listener
void Scoreboard::cameraDestroyed(Ogre::Camera* cam)
{
}

Ogre::Real Scoreboard::msScreenOffsetX = -0.1f;

Scoreboard::Scoreboard(const ActorClass& desc)
try
  : Actor(desc),
	StaticActor(desc),
	mScreenPosition(getNextScreenOffset(), 1.0f), 
	mText("0"),
	mTextureManager(Ogre::TextureManager::getSingletonPtr()),
	mFont(Ogre::FontManager::getSingleton().getByName(SCOREBOARD_FONT_NAME)),
	mTextDrawRectangle(SCOREBOARD_DRAW_RECTANGLE_POSITION),
	mAngle(Ogre::Degree(0)),
	mRotating(false),
	mTextDirty(true)
{
	// get the font texture
	try {
		mFontTexture = mTextureManager->getByName(
			mFont->getMaterial()->getTechnique(0)->getPass(0)->
			getTextureUnitState(0)->getTextureName());
	} 
	catch (const Ogre::Exception&) {
		OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, "Texture for font \"" + 
			mFont->getName() + "\" could not be found at index 0", 
			"Scoreboard::Scoreboard");
	}
	mFontBuffer = mFontTexture->getBuffer(); // get the font buffer

	// create an empty texture for the text
	mTextTexture = mTextureManager->createManual(
		getName() + "_texture", 
		RESOURCE_GROUP_GENERAL,
		Ogre::TextureType::TEX_TYPE_2D, 
		128, 128, 
		0, 
		Ogre::PixelFormat::PF_A8R8G8B8, 
		Ogre::TextureUsage::TU_DYNAMIC);

	mDestBuffer = mTextTexture->getBuffer(); // get the texture buffer

	// attach the created texture to the entity
	try {
		Ogre::SubEntity* subEntity = getEntity()->getSubEntity(0);
		Ogre::Material* templateMaterial = subEntity->getMaterial().get();
		Ogre::MaterialPtr newMaterial = templateMaterial->clone(
			templateMaterial->getName() + "_copy");

		newMaterial->getTechnique(0)->getPass(0)->getTextureUnitState(1)->
			setTexture(mTextTexture);

		subEntity->setMaterial(newMaterial);

	} 
	catch (const Ogre::Exception&) {
		OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, 
			"Cannot change material of the Scoreboard with name \"" + 
			getName() + '\"', "Scoreboard::Scoreboard");
	}

	getNode()->setScale(0.105f, 0.105f, 0.105f);
	getCamera()->addListener(this);

	updateText();
	updateTransforms();
}
catch (...)
{
	DEBUG_OUTPUT(LVL_RED, "Cannot create a scoreboard with name \"" + 
		desc.getName() + '\"');

	if (mTextTexture.get()) 
	{
		// Ogre::TexturePtr mTextTexture is Ogre's custom shared_ptr and 
		// does not provide implicit upcasts, so Ogre::SharedPtr<Ogre::Resource>
		// and Ogre::SharedPtr<Ogre::Texture> have no direct link, 
		// i.e. have to manually upcast
		mTextureManager->remove(static_cast<Ogre::ResourcePtr>(mTextTexture));
	}

	getCamera()->removeListener(this);
}

Scoreboard::~Scoreboard()
{
	try {
		mTextureManager->remove(static_cast<Ogre::ResourcePtr>(mTextTexture));

		reduceScreenOffset();
	} 
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy a \"Scoreboard\" actor");
	}

	getCamera()->removeListener(this);
}

void Scoreboard::update(const Ogre::Real deltaSeconds)
{
	StaticActor::update(deltaSeconds);

	// cannot use orientation as the angle, since moving the camera affects the 
	// orientation
	if (mRotating)
	{
		if (mAngle <= Ogre::Degree(360))
			mAngle += Ogre::Degree(SCOREBOARD_ROTATION_SPEED_SCALAR * deltaSeconds);
		else
		{
			mAngle = Ogre::Degree(0);
			mRotating = false;
		}

		updateOrientation();
	}

	if (mTextDirty && mAngle >= Ogre::Degree(180))
		updateText();
}

void Scoreboard::setText(const Ogre::String& str)
{
	if (mText != str)
	{
		mText = str;
		mTextDirty = true;
		mRotating = true;
	}
}

void Scoreboard::updateTransforms()
{
	// matrix to reverse projection and camera position
	const Ogre::Camera* camera = getCamera();
	const Ogre::Matrix4 inversePVMatrix = 
		(camera->getProjectionMatrix() * camera->getViewMatrix()).inverse();

	getNode()->setPosition(inversePVMatrix * 
		Ogre::Vector3(mScreenPosition.x, mScreenPosition.y, 0.0f));

	if (!mRotating)
		updateOrientation();

	camera->getWorldTransforms(&mLastCameraTransforms);
}

void Scoreboard::updateOrientation()
{
	getNode()->setOrientation(getCamera()->getOrientation() * 
		Ogre::Quaternion(mAngle, Ogre::Vector3::UNIT_Y));
}

void Scoreboard::updateText()
{
	/* 
		TODO: You may not specify a subrect when using D3DLOCK_DISCARD.
			can only discard the entire texture, which takes up another 
			lock-unlock of the texture, since subrectangle is used in the 
			code later 
	*/
	memset(mDestBuffer->lock(
		Ogre::HardwareBuffer::LockOptions::HBL_NORMAL), 
		0xff000000, 
		mDestBuffer->getSizeInBytes());
	mDestBuffer->unlock();

	Ogre::PixelBox destPixelBox = mDestBuffer->lock(
		mTextDrawRectangle, 
		Ogre::HardwareBuffer::LockOptions::HBL_WRITE_ONLY);
	Ogre::PixelBox fontPixelBox = mFontBuffer->lock(
		Ogre::Image::Box(0,0,0,0), 
		Ogre::HardwareBuffer::LockOptions::HBL_READ_ONLY);

	Ogre::uint8* fontData = static_cast<Ogre::uint8*>(fontPixelBox.data);
	Ogre::uint8* destData = static_cast<Ogre::uint8*>(destPixelBox.data);

	// get how many bytes per pixel
	const size_t fontPixelSize = Ogre::PixelUtil::getNumElemBytes(fontPixelBox.format);
	const size_t destPixelSize = Ogre::PixelUtil::getNumElemBytes(destPixelBox.format);

	// get how many bytes in each row
	const size_t fontRowPitchBytes = fontPixelBox.rowPitch * fontPixelSize;
	const size_t destRowPitchBytes = destPixelBox.rowPitch * destPixelSize;

	// find the pixel coordinates of each glyph in the font texture and store 
	// them in glyphCoords
	size_t textWidth = 0;
	std::vector<Ogre::Box> glyphCoords;
	for (const auto& c : mText)
	{
		if (std::isspace(c)) 
			continue; // ignore space
		else if (std::isprint(c))
		{
			Ogre::Font::UVRect fontGlyphUVs = mFont->getGlyphTexCoords(c);
			if (fontGlyphUVs.isNull()) // if the glyph does not exist
				fontGlyphUVs = mFont->getGlyphTexCoords(SCOREBOARD_UNKNOWN_GLYPH_CHAR);

			Ogre::Box glyph;
			glyph.left = static_cast<Ogre::uint32>(fontGlyphUVs.left *
				mFontTexture->getSrcWidth());
			glyph.top = static_cast<Ogre::uint32>(fontGlyphUVs.top * 
				mFontTexture->getSrcHeight());
			glyph.right = static_cast<Ogre::uint32>(fontGlyphUVs.right * 
				mFontTexture->getSrcWidth());
			glyph.bottom = static_cast<Ogre::uint32>(fontGlyphUVs.bottom * 
				mFontTexture->getSrcHeight());

			textWidth += glyph.getWidth();
			glyphCoords.push_back(glyph);
		}
		else
		{
			DEBUG_OUTPUT(LVL_YELLOW, "Scoreboard character \"" + 
				std::string(1, c) + "\" in text \"" + mText + 
				"\" cannot be applied");
		}
	}

	// TODO: should copy the entire glyph onto the texture in one go, or do it 
	// line-by-line to improve performance

	// draw each glyph on the destination texture pixel by pixel
	size_t cursorX = (mTextDrawRectangle.getWidth() - textWidth) / 2;
	for (const auto& glyph : glyphCoords)
	{
		for (size_t i = 0; i < glyph.getHeight(); ++i) // for each row
		{
			for (size_t j = 0; j < glyph.getWidth(); ++j) // for each column
			{
				Ogre::Real alpha = SCOREBOARD_TEXT_COLOR.a * (fontData[
					(i + glyph.top)		//	 current row + top of glyph
					*					//	 times
					fontRowPitchBytes	//	 how many bytes fit in a line (move by lines)
					+					// plus
					(j + glyph.left)	//	 current column + left of glyph
					*					//	 times
					fontPixelSize		//	 font pixel size (move by pixels)
					+					// plus
					1					//	 1 byte (the next alpha value, 
				]						//	     the font texture format is PF_BYTE_LA
				/ 255.0f);				// uint8 max is 255, divide by 255 to 
										//   get alpha in the range (0,1)

				Ogre::Real invalpha = 1.0f - alpha;
				// find again the offset to the current pixel now in 
				// destination texture
				size_t offset = (i /*+ cursorY*/) * destRowPitchBytes + 
							    (j + cursorX) * destPixelSize;

				Ogre::ColourValue pixel;
				// get the colour of current pixel at the found offset
				Ogre::PixelUtil::unpackColour(&pixel, destPixelBox.format, &destData[offset]);
				// calculate the new colour of the pixel
				pixel = (pixel * invalpha) + (SCOREBOARD_TEXT_COLOR * alpha);
				// set the new colour of the pixel at the found offset
				Ogre::PixelUtil::packColour(pixel, destPixelBox.format, &destData[offset]);
			}
		}

		// advance the cursor by the width of the glyph
        cursorX += glyph.getWidth();
	}

	// cleanup
	mDestBuffer->unlock();
	mFontBuffer->unlock();

	mTextDirty = false;
}

std::shared_ptr<Actor> ScoreboardFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Scoreboard>(desc);
}
