// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Debuggable.h"

Debuggable::Debuggable()
  : mLoaded(false)
{
}

Debuggable::~Debuggable()
{
}

void Debuggable::setDebuggableVisibility(bool flag)
{
	if (flag)
	{
		if (!mLoaded)
		{
			initDebuggable();
			mLoaded = true;
		}

		showDebuggable();
	}
	else
		hideDebuggable();
}