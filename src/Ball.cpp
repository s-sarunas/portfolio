// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Ball.h"

#include "InAppDebug.h"
#include "Border.h"
#include "CollisionManager.h"
#include "Scene.h"
#include "Paddle.h"
#include "Region.h"
#include "DebugConsole.h"

// this skips most of the saturated green colour as it's dominant in the scene
// NOTE: this adds more customizability compared to moving the hue and the 
// saturation based on values
const Ogre::Real Ball::msColourMatrix[][3] = {
   // red, green, blue
	{ 0.0f, 0.0f, 1.0f }, // blue
  //{ 0.5f, 0.5f, 1.0f }, // light blue
	{ 0.0f, 1.0f, 1.0f }, // teal
	{ 0.5f, 1.0f, 1.0f }, // light teal
	{ 1.0f, 1.0f, 0.5f }, // light yellow
  //{ 1.0f, 0.5f, 0.0f }, // orange
  //{ 1.0f, 1.0f, 0.0f }, // yellow
	{ 1.0f, 0.5f, 0.5f }, // light red
	{ 1.0f, 0.0f, 0.0f }, // red
	{ 1.0f, 0.5f, 1.0f }, // light purple
	{ 1.0f, 0.0f, 1.0f }, // purple
	{ 1.0f, 1.0f, 1.0f } // white
};

const size_t Ball::msNumColours = 
	sizeof(msColourMatrix) / (sizeof(Ogre::Real) * 3);

const Ogre::Real Ball::msSpeedPerColour = 
	BALL_COLOUR_FINAL_SPEED / (msNumColours - 1);

// CollisionManager::Listener
void Ball::collisionOccured(Collidable* one, Collidable* two)
{
	// deliberately done here to avoid dummy ball callbacks
	if (one == this &&
		two->getActorType() == ACTOR_REGION_TYPE_NAME &&
		getGameMode() == GAME_MODE_FREEPLAY)
	{
		if (mLastPaddle)
		{
			// handle paddle's score
			if (mLastPaddle->getRegion() == two)
				mLastPaddle->decreaseScore(); // self goal
			else
				mLastPaddle->increaseScore();
		}
	}
}

// Config::Listener
void Ball::propertyChanged(const Config::Property& prop)
{
	if (prop.first == "ball_speed_multiplier")
		setSpeedMultiplier(prop.second.getValue<Ogre::Real>());
}

Ball::Ball(const ActorClass& desc)
  : Actor(desc),
	DynamicActor(desc, DynamicClass(BALL_SPEED_STARTING, true, UNIT_VECTOR_FORWARD, 
		BALL_SPEED_INCREASE_MULTIPLIER, false)),
	Grounded(true),
	mEndCollidable(nullptr),
	mEndPosition(Ogre::Vector3::ZERO),
	mColour(Ogre::ColourValue(
		msColourMatrix[0][0],
		msColourMatrix[0][1],
		msColourMatrix[0][1])),
	mLastPaddle(nullptr),
	mIsPrimary(true)
{
	// balls do not interact with other balls
	getCollisionMask().set(ACTOR_AI_TYPE_FLAG | ACTOR_PLAYER_TYPE_FLAG | 
		ACTOR_BORDER_TYPE_FLAG | ACTOR_REGION_TYPE_FLAG); 

	updateColour();

	getConfig().addListener(this);
	CollisionManager::getSingleton().addListener(this);
}

Ball::~Ball()
{
	getConfig().removeListener(this);
	CollisionManager::getSingleton().removeListener(this);
}

Ball::Ball(const Ball& rhs, bool varyOrientation, bool varySpeed, bool newMaterial) 
  : Actor(rhs),
    DynamicActor(rhs),
	Grounded(rhs, this),
	mEndCollidable(rhs.mEndCollidable),
	mEndPosition(rhs.mEndPosition),
	mColour(rhs.mColour),
	mAxisAlignedBox(rhs.mAxisAlignedBox),
	mLastPaddle(rhs.mLastPaddle),
	mIsPrimary(false)
{
	if (newMaterial)
	{
		for (size_t i = 0; i < rhs.getEntity()->getNumSubEntities(); ++i)
		{
			const Ogre::MaterialPtr& material = 
				rhs.getEntity()->getSubEntity(i)->getMaterial();

			getEntity()->getSubEntity(i)->setMaterial(material->clone(getName()));
		}
	}

	if (varyOrientation)
		getNode()->yaw(Ogre::Degree(
			RNG(BALL_COPY_REL_ROTATION_MIN_DEGREES, BALL_COPY_REL_ROTATION_MAX_DEGREES)));

	if (varySpeed)
	{
		setMovementSpeed(
			RNG(BALL_COPY_REL_SPEED_MIN, BALL_COPY_REL_SPEED_MAX));
		updateColour();
	}

	getConfig().addListener(this);
	CollisionManager::getSingleton().addListener(this);
}

Ball::Ball(Ball&& rhs)
  : Actor(std::move(rhs)),
    DynamicActor(std::move(rhs)),
	Grounded(std::move(rhs)),
	mEndCollidable(rhs.mEndCollidable),
	mEndPosition(rhs.mEndPosition),
	mColour(std::move(rhs.mColour)),
	mAxisAlignedBox(rhs.mAxisAlignedBox),
	mLastPaddle(rhs.mLastPaddle),
	mIsPrimary(rhs.mIsPrimary)
{
	getConfig().addListener(this);
	CollisionManager::getSingleton().addListener(this);
}

Ball& Ball::operator=(Ball&& rhs)
{
	// if not assigning to itself
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		Actor::operator=(std::move(rhs));
		DynamicActor::operator=(std::move(rhs));
		Grounded::operator=(std::move(rhs));

		// free existing resources of the source
		// handled by Actor
		// ...

		// copy data from the source object
		mEndCollidable = rhs.mEndCollidable;
		mEndPosition = rhs.mEndPosition;
		mColour = rhs.mColour;
		mAxisAlignedBox = rhs.mAxisAlignedBox;
		mLastPaddle = rhs.mLastPaddle;
		mIsPrimary = rhs.mIsPrimary;

		// release pointer ownership from the source object so that the 
		// destructor does not free the memory multiple times
		rhs.mEndCollidable = nullptr;
	}

	return *this;
}

/*void Ball::checkGroundCollision()
{
	// A ray down unit y
	bounding_shape sphere = entity->getWorldBoundingSphere(true);

	Ogre::Ray heightRay(node->getPosition(), Ogre::Vector3::NEGATIVE_UNIT_Y);
	rayQuery->setRay(heightRay);
	queryResult = rayQuery->execute();

	if (!queryResult.empty() && 
		queryResult.begin()->worldFragment && 
		queryResult.begin()->worldFragment->singleIntersection.y != NULL) 
	{
		Ogre::Real distance = 
			queryResult.begin()->worldFragment->singleIntersection.y - 
			sphere.getRadius();

		//node->setPosition(
			Ogre::Vector3(node->getPosition().x, distance, node->getPosition().z));
		node->translate(Ogre::Vector3(0, distance, 0));
		cout << "translated\n";
	}

	//delete sphere;
	Ogre::SceneQuery *scenequery = sceneManager->createrays
}*/

void Ball::updateEndParams()
{
	// Using a Ray-AABB test here would not work since a border on a hill 
	// would not be taken into account. A ray parallel to the ground might, 
	// but it would have to be adjusted every iteration, since the terrain 
	// is unpredictable.
	// A dummy copy of the current ball is used to simulate it and get 
	// the point of goal for the computer to move to
	Ball dummy(*this, false, false, false);

	// ignore collisions with AI and Players, so the point of goal could be found
	dummy.getCollisionMask().clear(ACTOR_AI_TYPE_FLAG | ACTOR_PLAYER_TYPE_FLAG);

	// hide this actor's collision results from listeners
	dummy.setVisibleToListeners(false);

	// trap the loop until the dummy ball collides with a region
	eOgre::CountdownTimer failsafeTimer(BALL_DUMMY_FAILSAFE_DURATION);
	while (!failsafeTimer.hasEnded() && !dummy.hasCollided(ACTOR_REGION_TYPE_NAME))
	{
		CollisionManager::getSingleton().handleOneCollision(&dummy);

		failsafeTimer.update(BALL_DUMMY_UPDATE_RATE_SECONDS);
		dummy.update(BALL_DUMMY_UPDATE_RATE_SECONDS);
	}

	if (!failsafeTimer.hasEnded())
	{
		mEndCollidable = dummy.getCollisions(ACTOR_REGION_TYPE_NAME).front();

		// NOTE: have to take away (dummy.getStep() * 2) here due to almost non-existent
		// collision detection, i.e. the exact point of collision cannot be found
		mEndPosition = dummy.getNode()->getPosition() - (dummy.getStep() * 2);
	}
	else
	{
		DEBUG_OUTPUT(LVL_YELLOW, "The ball \"" + getName() + 
			"\" could not determine its outcome");
	}
}

void Ball::speedChanged()
{
	updateColour();
}

void Ball::updateColour()
{
	const Ogre::Real interpolationValue = getActualSpeed() / msSpeedPerColour;

	Ogre::Real intPart = NULL;
	const Ogre::Real fracPart = std::modf(interpolationValue, &intPart);

	const size_t i = static_cast<size_t>(intPart);
	if (i < msNumColours - 1)
	{
		const Ogre::ColourValue currentColourRange(
			msColourMatrix[i][0], msColourMatrix[i][1], msColourMatrix[i][2]);
		const Ogre::ColourValue nextColourRange(
			msColourMatrix[i + 1][0], msColourMatrix[i + 1][1], msColourMatrix[i + 1][2]);

		Ogre::ColourValue colourDifference = nextColourRange - currentColourRange;

		applyColour(currentColourRange + (colourDifference * fracPart)); // lerp
	}
	else
	{
		applyColour(Ogre::ColourValue(
			msColourMatrix[msNumColours - 1][0],
			msColourMatrix[msNumColours - 1][1], 
			msColourMatrix[msNumColours - 1][2]));
	}
}

void Ball::applyColour(const Ogre::ColourValue& newColour)
{
	mColour = newColour;

	for (size_t i = 0; i < getEntity()->getNumSubEntities(); ++i)
		getEntity()->getSubEntity(i)->getMaterial()->setDiffuse(mColour);
}

void Ball::update(const Ogre::Real deltaSeconds)
{
	DynamicActor::update(deltaSeconds);
	Grounded::update(deltaSeconds);

	move(deltaSeconds);
}

void Ball::reset()
{
	DynamicActor::reset();

	updateColour();
}

const Ogre::AxisAlignedBox& Ball::getBoundingBox()
{
	mAxisAlignedBox = getEntity()->getBoundingBox(); // copy
	const Ogre::Vector3& pos = getNode()->getPosition();

	mAxisAlignedBox.setExtents(
		mAxisAlignedBox.getMinimum() + pos, mAxisAlignedBox.getMaximum() + pos);

	return mAxisAlignedBox;
}

void Ball::handleCollision(Collidable* collidable, bool isColliding)
{
	if (isColliding)
	{
		if (collidable->getActorType() == ACTOR_PLAYER_TYPE_NAME ||
			collidable->getActorType() == ACTOR_AI_TYPE_NAME)
		{
			mLastPaddle = static_cast<Paddle*>(collidable);
			DynamicActor::reflect(mLastPaddle, Ogre::Degree(
				BALL_REFLECT_ANGLE_RANGE_DEGREES / mLastPaddle->getLength()));

			incrementSpeed();
			updateColour();

			if (getGameMode() == GAME_MODE_FREEPLAY)
				updateEndParams();
		}
		else if (collidable->getActorType() != ACTOR_REGION_TYPE_NAME)
			DynamicActor::reflect(collidable);
	}

	DynamicActor::handleCollision(collidable, isColliding);

	if (collidable->getActorType() == ACTOR_BORDER_TYPE_NAME)
	{
		const Border* border = static_cast<Border*>(collidable);
		if (mIsPrimary && border->getBorderType() == BORDER_TYPE_CLONER)
		{
			for (Ogre::ushort i = 0; i < BORDER_CLONER_DUPLICATES; ++i)
			{ // call the copy constructor
				std::shared_ptr<Ball> copy = 
					std::make_shared<Ball>(*this, true, true, true);

				Scene::getSingleton().addActor(copy);
			}

			mLastPaddle->increaseScore(BORDER_CLONER_DUPLICATES);
		}
	}
}

// InputHandler.h
bool Ball::keyReleased(const SDL_KeyboardEvent& arg) 
{
	if (arg.keysym.sym == BALL_BUTTON_KB_START)
		start();

	return true;
}

// InputHandler.h
bool Ball::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	if (button == BALL_BUTTON_JS_START || button == BALL_BUTTON_JS_START_ALT)
		start();

	return true;
}

std::shared_ptr<Actor> BallFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Ball>(desc);
}