// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "InAppDebug.h"

#include "Actor.h"
#include "Collidable.h"
#include "Scene.h"

using namespace std;

InAppDebug* InAppDebug::msSingleton = nullptr;

const std::array<Ogre::String, MESSAGE_LEVEL_MAX> InAppDebug::msMessageLevels = 
	{ "GREEN", "CONSOLE", "PERFORMANCE", "YELLOW", "RED" };
const std::hash<Ogre::String> InAppDebug::msStringHash;
InAppDebug::HashList InAppDebug::msMessageHashes;

InAppDebug::InAppDebug()
  : mRootSceneNode(nullptr),
	mObjectsVisible(true),
	mConsole(std::make_unique<DebugConsole>())
{
	msSingleton = this;
}

InAppDebug::~InAppDebug()
{
	msSingleton = nullptr;
}

InAppDebug* InAppDebug::getSingletonPtr() NOEXCEPT
{
	return msSingleton;
}

InAppDebug& InAppDebug::getSingleton()
{
	assert(msSingleton);
	return (*msSingleton);
}

void InAppDebug::_init(const Ogre::RenderWindow* window)
{
	// create a default billboard others copy
	Ogre::Plane billboard;
	billboard.normal = Ogre::Vector3::UNIT_Y;
	billboard.d = 0;

	mBillboardMesh = Ogre::MeshManager::getSingleton().createPlane(
		"plane_billboard", RESOURCE_GROUP_GENERAL, billboard, 1.75f, 1.75f, 1, 
		1, true, 1, 1.0f, 1.0f, Ogre::Vector3::UNIT_Z);

	mRootSceneNode = getSceneManager()->getRootSceneNode()->
		createChildSceneNode(DEBUG_ROOT_NAME);

	mTable = std::make_unique<DebugTable>(window);

	DEBUG_TABLE_ADD("intermediate_queue", 
		[this]() { 
			return Ogre::StringConverter::toString(
				mConsole->getIntermediateQueue().size()
			);
		}
	);

	DEBUG_TABLE_ADD("processing_queue", 
		[this]() { 
			return Ogre::StringConverter::toString(
				mConsole->getProcessingQueue().size()
			);
		}
	);
}

void InAppDebug::clearTable()
{
	if (mTable)
		mTable->clearNonPersistentRows();
}

void InAppDebug::clearActors()
{
	// clear debug actors
	mActors.clear();

	// nuke the debug root node
	if (mRootSceneNode)
		eOgre::nuke_scene_node(getSceneManager(), mRootSceneNode, false);
}

void InAppDebug::cleanup()
{
	clearTable();
	clearActors();
}

void InAppDebug::saveTextureToFile(const Ogre::TexturePtr texture, 
	const Ogre::String& filename)
{
	try {
		Ogre::Image img;
		texture->convertToImage(img);
		img.save(filename);
	} 
	catch (const Ogre::RenderingAPIException& e) {
		OGRE_EXCEPT(Ogre::Exception::ERR_RENDERINGAPI_ERROR, 
			"Writing texture \"" + texture->getName() + "\" to file failed. "
			"Is the buffer locked?: " + e.getFullDescription(), 
			"InAppDebug::saveTextureToFile");
	} 
	catch (const Ogre::Exception& e) {
		OGRE_EXCEPT(Ogre::Exception::ERR_CANNOT_WRITE_TO_FILE, 
			"Texture \"" + texture->getName() + 
			"\" could not be saved to file \"" + filename +
			"\": " + e.getFullDescription(), 
			"InAppDebug::saveTextureToFile");
	}
}

int InAppDebug::getNumResourcesWithStr(Ogre::ResourceManager* manager, 
	const Ogre::String& str)
{
	if (manager)
	{
		Ogre::ResourceManager::ResourceMapIterator it = 
			manager->getResourceIterator();

		unsigned int n = 0;
		for (const auto& resource : it)
		{
			if (resource.second->getName().find(str) != std::string::npos)
				n++;
		}

		return n;
	}
		
	return -1;
}

void InAppDebug::flipObjectVisibility()
{
	setObjectsVisible(!mObjectsVisible);
}

void InAppDebug::setObjectsVisible(bool visible)
{
	mRootSceneNode->setVisible(visible);

	// set visibility for debug actors
	for (const auto& actor : mActors)
		actor->setVisible(visible);

	mObjectsVisible = visible;
}

Actor* InAppDebug::addDebugObject(Ogre::SceneNode* parentNode, 
	const bool visible, const Ogre::String& meshName, 
	const Ogre::Vector3& translation, const Ogre::Quaternion& orientation, 
	const Ogre::Vector3& scale) NOEXCEPT
{
	try {
		ActorClass desc;
		desc.setName(parentNode->getName() + "_debug");
		desc.setEntity(meshName);
		desc.setNode(parentNode, create_child_node);
		desc.setTransforms(translation, orientation, scale);
		desc.setVisible(visible);

		return createActor(desc);
	} 
	catch (Ogre::Exception&) {
		// do nothing - exceptions are handled by actor's constructor, 
		// which rethrows by default
	}

	return nullptr;
}

Actor* InAppDebug::addDebugObject(const Ogre::Vector3& position, 
	const bool visible, const Ogre::String& meshName, 
	const Ogre::Quaternion& orientation, const Ogre::Vector3& scale) NOEXCEPT
{
	try {
		ActorClass desc;
		desc.setName('(' + Ogre::StringConverter::toString(position) + ")_object");
		desc.setEntity(meshName);
		desc.setNode(mRootSceneNode, create_child_node);
		desc.setTransforms(position, orientation, scale);
		desc.setVisible(visible);

		return createActor(desc);
	} 
	catch (Ogre::Exception&) {
		// do nothing - exceptions are handled by actor's constructor, 
		// which rethrows by default
	}

	return nullptr;
}

Actor* InAppDebug::addBillboard(Ogre::SceneNode* parentNode, 
	const Ogre::String& materialName) NOEXCEPT
{
	try {
		ActorClass desc;
		desc.setName(parentNode->getName() + "_billboard");

		Ogre::Entity* entity = getSceneManager()->createEntity(
			desc.getName(), mBillboardMesh);
		entity->setMaterialName(materialName);
		desc.setEntity(entity);

		desc.setNode(parentNode, create_child_node);

		return createActor(desc);
	} 
	catch (Ogre::Exception&) {
		// do nothing - exceptions are handled by actor's constructor,
		// which rethrows by default
	}

	return nullptr;
}

Actor* InAppDebug::addArrow(Ogre::SceneNode* parentNode) NOEXCEPT
{
	try {
		ActorClass desc;
		desc.setName(parentNode->getName() + "_arrow");
		desc.setEntity("arrow.mesh");
		desc.setNode(parentNode, create_child_node);

		return createActor(desc);
	} 
	catch (Ogre::Exception&) {
		// do nothing - exceptions are handled by actor's constructor, 
		// which rethrows by default
	}

	return nullptr;
}

Actor* InAppDebug::addArrow(const Ogre::Vector3& direction, 
	const Ogre::Vector3& position) NOEXCEPT
{
	try {
		ActorClass desc;
		desc.setName('(' + Ogre::StringConverter::toString(direction) + ") (" + 
			Ogre::StringConverter::toString(position) + ")_arrow");
		desc.setEntity("arrow.mesh");
		desc.setNode(mRootSceneNode, create_child_node);
		desc.setTranslate(position);

		Actor* actor = createActor(desc);
		actor->getSceneNode()->setDirection(direction);

		return actor;
	} 
	catch (Ogre::Exception&) {
		// do nothing - exceptions are handled by actor's constructor, 
		// which rethrows by default
	}

	return nullptr;
}

Actor* InAppDebug::createActor(const ActorClass& desc)
{
	std::shared_ptr<Actor> actor = createUnmanagedActor(desc);

	mActors.push_back(actor);

	return actor.get();
}

std::shared_ptr<Actor> InAppDebug::createUnmanagedActor(const ActorClass& desc)
{
	desc._changeType(ACTOR_DEBUG_TYPE_NAME);
	desc._setCreator(this);

	return std::make_shared<StaticActor>(desc);
}

void InAppDebug::reserveForActors(const size_t numActors)
{
	mActors.reserve(mActors.size() + numActors);
}

void InAppDebug::update(const Ogre::Real deltaSeconds)
{
	updateBillboards();
	mTable->update();
	mCamera.update(deltaSeconds);
}

void InAppDebug::updateBillboards()
{
	for (const auto& actor : mActors)
	{
		const Ogre::String& name = actor->getName();
		if (name.find("_billboard") != name.npos)
		{
			Ogre::Vector3 direction = 
				getCamera()->getPosition() - 
				actor->getNode()->_getDerivedPosition();

			actor->getSceneNode()->setDirection(
				direction, Ogre::Node::TS_WORLD, Ogre::Vector3::UNIT_Y);
		}
	}
}

const unsigned int InAppDebug::calcNumNodes(Ogre::SceneNode* root) NOEXCEPT 
{
	unsigned int c = 0;
	eOgre::for_each_child_scene_node(root, 
		[&c](Ogre::SceneNode* sceneNode, unsigned int depth) NOEXCEPT { 
			c++; 
		}
	);

	return c;
}

void InAppDebug::outputTable(const Ogre::StringVector& headers, const std::stringstream& ss)
{
	// resets cout formatting flags on return using RAII
	boost::io::ios_flags_saver saver(cout); 

	size_t numRows = 0;
	const std::vector<std::string> data = 
		estd::split(ss.rdbuf()->str(), "\t", "\n", &numRows); 
	if (!data.empty())
	{
		size_t numColumns = 0;
		std::vector<std::string>::const_iterator it = data.begin();
		while ((*it++) != "\n")
			++numColumns;

		const size_t numElements = data.size() - numRows;
		assert((numElements / numColumns) == numRows && 
			"incorrect stringstream argument formatting: '\t' - new cell, '\n' - new row");

		const std::string unnamedHeaderName = "Unnamed";

		// find the max length of the column
		std::vector<size_t> columnLengths;
		size_t length = NULL;
		for (size_t j = 0; j < numColumns; ++j) // for each column
		{
			if (j < headers.size())
				length = headers[j].length();
			else
				length = unnamedHeaderName.length();

			for (size_t i = 0; i < numRows; ++i) // for each row
			{
				// row * stride + column + row (# of \n)
				length = std::max(length, data[i * numColumns + j + i].length()); 
			}
																				
			columnLengths.push_back(length + DEBUG_OUTPUT_TABLE_GAP);
		}

		// output headers
		for (size_t h = 0; h < numColumns; ++h) // for each header
		{
			cout << estd::column(columnLengths[h]);

			if (h < headers.size())
				cout << headers[h];
			else
				cout << unnamedHeaderName;
		}

		cout << endl;

		// output the line break
		const size_t maxRowLength = 
			std::accumulate(columnLengths.begin(), columnLengths.end(), 0);
		cout << estd::linebreak(maxRowLength);

		// output the table
		for (size_t i = 0; i < numRows; ++i) // for each row
		{
			for (size_t j = 0; j < numColumns; ++j) // for each column
			{
				// row * stride + column + row (# of \n)
				cout << estd::column(columnLengths[j]) <<
					data[i * numColumns + j + i];	
			}
																						
			cout << endl;
		}

		cout << estd::linebreak(maxRowLength);
	}
}

void InAppDebug::outputMessage(MESSAGE_LEVEL level, const Ogre::String& message, 
	const Ogre::String& source, bool noDuplicates) NOEXCEPT
{
	try {
		const Ogre::String outputText = msMessageLevels[level] + ": " + 
			message + ", in " + source + '.';

		if (noDuplicates)
		{
			const size_t hash = InAppDebug::msStringHash(outputText);
			if (std::find(msMessageHashes.begin(), msMessageHashes.end(), hash) 
				!= msMessageHashes.end())
			{
				return;
			}

			msMessageHashes.push_back(hash);
		}

		Ogre::LogManager::getSingleton().logMessage(outputText);

		if (level > DEBUG_OUTPUT_CONSOLE_ERROR_LEVEL)
		{
			DEBUG_WRITELN(std::move(outputText));
		}
	}
	catch (...) {
		// do not let the exception leave this function, 
		// as this is used in destructors
	}
}

void InAppDebug::outputSceneHierarchy()
{
	std::stringstream ss;
	unsigned int counter = NULL;

	Ogre::SceneNode* rootNode = getSceneManager()->getRootSceneNode();

	ss << rootNode->getName() << "\tempty\n";

	eOgre::for_each_child_scene_node(rootNode, 
		[&](Ogre::SceneNode* sceneNode, unsigned int depth) { 
			ss << Ogre::String(depth * 2, ' ') << sceneNode->getName() << '\t';

			if (sceneNode->numAttachedObjects() > 0)
			{
				for (const auto& obj : sceneNode->getAttachedObjectIterator())
					ss << obj.first << '(' << obj.second->getMovableType() << ") ";
			}
			else
				ss << "empty";

			ss << '\n';

			++counter;
		}, 1 // starting depth is 1
	);

	outputTable({"Name", "Attached"}, ss);

	DEBUG_WRITELN("Number of scene nodes: " << counter);
}

void InAppDebug::outputActors(const ActorCollectionMap& actorCollection)
{
	std::stringstream ss;
	for (const auto& actorList : actorCollection)
	{
		for (const auto& actor : actorList.second)
		{
			actor->for_all(
				[&ss](Actor* actor, unsigned int depth) {
					ss << Ogre::String(depth * 2, ' ') << *actor << '\n';
				}
			);
		}
	}

	outputTable({"Name", "Type", "Group", "SceneNode", "Entity", "UpdateFlags", 
		"Parent", "TagPoint", "Visible", "Destroy"}, ss);
}

void InAppDebug::outputActors(const CollidablePtrList& collidableList)
{
	std::stringstream ss;
	for (const auto& collidable : collidableList)
		ss << *collidable << '\n';

	outputTable({"Name", "CollisionMask", "TypeFlag", "ListenerVisible", 
		"CollisionResults"}, ss);
}

// InputHandler.h
bool InAppDebug::mouseMoved(const SDL_MouseMotionEvent& arg)
{
	return mCamera.mouseMoved(arg);
}

// InputHandler.h
bool InAppDebug::textInput(const SDL_TextInputEvent& arg) 
{
	return mConsole->textInput(arg);
}

// InputHandler.h
bool InAppDebug::keyPressed(const SDL_KeyboardEvent& arg) 
{
	return (mConsole->keyPressed(arg) && mCamera.keyPressed(arg));
}

// InputHandler.h
bool InAppDebug::keyReleased(const SDL_KeyboardEvent& arg) 
{
	return (mConsole->keyReleased(arg) && mCamera.keyReleased(arg));
}