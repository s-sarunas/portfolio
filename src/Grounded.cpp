// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Grounded.h"

#include "InAppDebug.h"
#include "Terrain.h"
#include "DynamicActor.h"
#include "Scene.h"

Grounded::Grounded(bool rotate)
  : mRay(Ray<Actor>(this)),
	mRotate(rotate)
{
	init();
}

Grounded::~Grounded()
{
}

Grounded::Grounded(const Grounded& rhs, Actor* newActorParent)
  : mRay(rhs.mRay, newActorParent),
	mRotate(rhs.mRotate)
{
	init();
}

Grounded::Grounded(Grounded&& rhs) NOEXCEPT
  : mRay(std::move(rhs.mRay)),
	mRotate(rhs.mRotate)
{
	init();
}

Grounded& Grounded::operator=(Grounded&& rhs) NOEXCEPT
{
	if (this != std::addressof(rhs))
	{
		// call the base class move assignment operator 
		// ...

		// free existing resources of the source
		// ...

		// copy data from the source object
		mRay = std::move(rhs.mRay);
		mRotate = rhs.mRotate;

		// release pointer ownership from the source object so that the
		// destructor does not free the memory multiple times
		// ...
	}

	return *this;
}

void Grounded::init()
{
	// set this to maintain the up vector when rotating,
	// e.g. -120 degree pitch = yaw 180, pitch 60
	if (mRotate)
		getSceneNode()->setFixedYawAxis(true, UNIT_VECTOR_UP);

	_needsUpdate();
}

void Grounded::update(const Ogre::Real deltaSeconds)
{
	if (getActorGroup() == ACTOR_STATIC_GROUP_NAME)
	{
		DEBUG_OUTPUT(LVL_PERF, "Grounding a non-dynamic actor \"" + getName() + 
			"\" unnecessarily reduces performance", true);
	}

	if (_isUpdatePending())
		ground(this, mRay, mRotate); 

	Mixin::_updated();
}

void Grounded::groundActor(Actor* actor, bool rotate)
{
	groundActor<Actor>(actor, Ray<Actor>(actor), rotate);
}