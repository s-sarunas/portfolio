// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "MapSelectState.h"

#include "SimpleMenu.h"
#include "Config.h"
#include "Button.h"
#include "Dropdown.h"

// SimpleMenu::Listener
void MapSelectState::itemSelected(const Dropdown* dropdown)
{
	mCallbackParams.setParameter(dropdown->getName(), 
		dropdown->getValueCaption().asUTF8(), PARAMETER_TYPE_STRING);
}

// SimpleMenu::Listener
void MapSelectState::buttonHit(const Button* button) 
{
	const Ogre::String& name = button->getName();
	if (name == "play")
		notifyListener(MENU_EVENT_START_GAME, &mCallbackParams);
	else if (name == "back")
		notifyListener(MENU_EVENT_STATE_PREVIOUS);

	end(NULL);
}

MapSelectState::MapSelectState(AppState::Listener* listener)
  : AppState(listener, "MapSelectMenu")
{
	SimpleMenu* menu = getSimpleMenu();
	menu->createDropdown("scene_name", "Map name", 
		{"grassy.scene", "4player.scene"}, NULL);
	menu->createButtons({"play", "back"}, {"Play", "Back"});
}

MapSelectState::~MapSelectState()
{
}