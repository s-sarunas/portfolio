// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Scene.h"

#include "Paddle.h"
#include "Ball.h"
#include "AI.h"
#include "Butterfly.h"
#include "Region.h"
#include "Terrain.h"
#include "Bullet.h"
#include "Weapon.h"
#include "Ray.h"

// CollisionManager::Listener
void Scene::collisionOccured(Collidable* one, Collidable* two)
{
	if (one->getActorType() == ACTOR_BALL_TYPE_NAME &&
		two->getActorType() == ACTOR_REGION_TYPE_NAME)
	{
		switch (getGameMode())
		{
		case GAME_MODE_FREEPLAY:
			reset(); // reset the entire scene
			break;
		case GAME_MODE_CHALLENGE:
			{ // destroy the ball
				Ball* currentBall = static_cast<Ball*>(one);
				if (currentBall->isPrimary())
				{ // change the primary ball
					// find the next ball in the list that's not being destroyed
					ActorPtrList* ballList = getActorList(ACTOR_BALL_TYPE_NAME);
					ActorPtrList::const_iterator it = ballList->begin();
					while (it != ballList->end() && 
						   ((*it).get() == currentBall || (*it)->getDestroyFlag()))
					{
						++it;
					}

					if (it != ballList->end())
					{ // a valid next ball is found
						Ball* nextBall = dynamic_cast<Ball*>((*it).get());
						nextBall->setPrimary(true);

						currentBall->setPrimary(false);
						currentBall->setDestroyFlag(true);

						if (mPlayer)
							mPlayer->decreaseScore(1);
					}
					else 
						currentBall->reset();
				}
				else
				{
					currentBall->setDestroyFlag(true);

					if (mPlayer)
						mPlayer->decreaseScore(1);
				}
			}
			break;
		default:
			break;
		}
	}
}

Scene* Scene::msSingleton = nullptr;

Scene::Scene(const Ogre::String& sceneFilename, const GameModes gameMode, 
	const JoystickList& joystickList, bool unloadResources)
  :	mPlayer(nullptr),
	mRootSceneNode(getSceneManager()->getRootSceneNode()->
		createChildSceneNode(parseFilename(sceneFilename))),
	mNextActorTypeFlag(ACTOR_LAST_TYPE_FLAG << 1),
	mUnload(unloadResources)
{
	msSingleton = this;

	setGameMode(gameMode);

	mActorFactoryMap.max_load_factor(SCENE_UNORDERED_MAP_MAX_LOAD_FACTOR);
	mActorFactoryMap.reserve(ACTOR_NUM_TYPES);

	addActorFactory(std::make_unique<StaticFactory>());
	addActorFactory(std::make_unique<ButterflyFactory>());
	addActorFactory(std::make_unique<BallFactory>());
	addActorFactory(std::make_unique<PaddleFactory>());
	addActorFactory(std::make_unique<CharacterFactory>());
	addActorFactory(std::make_unique<AIFactory>());
	addActorFactory(std::make_unique<BorderFactory>());
	addActorFactory(std::make_unique<RegionFactory>());
	addActorFactory(std::make_unique<BulletFactory>());
	addActorFactory(std::make_unique<TerrainFactory>());
	addActorFactory(std::make_unique<ScoreboardFactory>());
	addActorFactory(std::make_unique<WeaponFactory>());
	addActorFactory(std::make_unique<FlashFactory>());

	// no rehashing - its empty
	mActorCollection.max_load_factor(SCENE_UNORDERED_MAP_MAX_LOAD_FACTOR); 
	// reserve enough buckets for each actor type
	mActorCollection.reserve(mActorFactoryMap.size()); 

	mLoader = std::make_unique<DotSceneLoader>(this, sceneFilename, 
		mRootSceneNode, RESOURCE_GROUP_GENERAL, true);

	// get the pointer to the human player's paddle
	ActorPtrList* playerList = getActorList(ACTOR_PLAYER_TYPE_NAME);
	if (playerList)
	{
		assert(playerList->size() <= 1 && 
			"only a single human player is supported");

		if (!playerList->empty())
		{
			mPlayer = dynamic_cast<Paddle*>(playerList->front().get());
		}
		else
		{
			DEBUG_OUTPUT(LVL_RED, "No active player was found");
		}
	}

	if (getGameMode() == GAME_MODE_FREEPLAY)
	{ 
		// load AI
		mLoader->loadDelayedActors(ACTOR_AI_TYPE_NAME, ACTOR_AI_TYPE_NAME);
	}
	else if (getGameMode() == GAME_MODE_CHALLENGE)
	{ 
		// load challenge borders
		StringInterface params;
		params.addParameter("border_type", BORDER_TYPE_CLONER, 
			PARAMETER_TYPE_STRING);

		std::vector<Actor*> borders = mLoader->loadDelayedActors(
			ACTOR_AI_TYPE_NAME, 
			ACTOR_BORDER_TYPE_NAME, 
			SCENE_CHALLENGE_BORDER_MESH_FILENAME, 
			&params);
		
		// ground the borders 
		for (auto& actor : borders)
			Grounded::groundActor(actor, false);

		// change the ruleset
		getConfig().setProperty("paddle_speed_multiplier", 0.0f, 
			PARAMETER_TYPE_REAL);
		getConfig().setProperty("ball_speed_multiplier", 4.0f, 
			PARAMETER_TYPE_REAL);

		// set the initial score
		mPlayer->setScore(1);
	}

	for (const auto& joystick : joystickList)
		assignInput(joystick);

	// render once to avoid the first frame being unlit and showing ungrounded
	// actors
	getSceneManager()->_renderScene(getCamera(), getCamera()->getViewport(), false);

	// update the light lists of static geometry regions
	mLoader->updateStaticGeometryLightList();

	DEBUG_TABLE_ADD("num_balls", 
		[this]() { 
			return Ogre::StringConverter::toString(getNumActors(ACTOR_BALL_TYPE_NAME)); 
		}
	);

	mCollisionManager.addListener(this);

	eSDL_FlushMotionEvents();
} 

Scene::~Scene()
{
	try {
		// cleanup the debug objects, has to be done before destroying the scene
		InAppDebug::getSingleton().cleanup(); 

		if (mUnload)
			unloadAllUsedResources();
	}
	catch (...) {
		DEBUG_OUTPUT(LVL_RED, "Could not destroy scene");
	}

	mCollisionManager.removeListener(this);

	msSingleton = nullptr;
}

Scene* Scene::getSingletonPtr() NOEXCEPT
{
	return msSingleton;
}

Scene& Scene::getSingleton()
{
	assert(msSingleton);
	return (*msSingleton);
}

const Ogre::String Scene::parseFilename(const Ogre::String& filename)
{
	Ogre::String result = filename;
	Ogre::String::size_type pos = result.find(".scene");
	if (pos == result.npos)
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, 
			"Scene name \"" + filename + "\" is not valid.",
			"DotSceneLoader::parseFilename");
	}
	else
	{
		Ogre::StringUtil::toUpperCase(result);
		result.replace(pos, result.length() - 1, "_SCENE_ROOT");
	}

	return result;
}

void Scene::addActorFactory(std::unique_ptr<ActorFactory>&& factory, bool overwrite)
{
	const Ogre::String& type = factory->getType();
	const ActorFactoryMap::const_iterator it = mActorFactoryMap.find(type);
	if (!overwrite && it != mActorFactoryMap.end())
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_DUPLICATE_ITEM, 
			"A factory with type \"" + type + "\" already exists.",
			"Scene::addActorFactory");
	}

	mActorFactoryMap[type] = std::move(factory); // pass ownership

	DEBUG_OUTPUT(LVL_GREEN, "Actor factory with type \"" + type + 
		"\" registered");
}

void Scene::removeActorFactory(ActorFactory* factory)
{
	const Ogre::String& type = factory->getType();
	if (mActorFactoryMap.erase(type))
	{
		DEBUG_OUTPUT(LVL_GREEN, "Actor factory with type \"" + type + 
			"\" removed");
	}
	else
	{
		DEBUG_OUTPUT(LVL_YELLOW, "Actor factory with type \"" + type + 
			"\" was not found");
	}
}

const bool Scene::hasActorFactory(ActorFactory* factory)
{
	return !(mActorFactoryMap.find(factory->getType()) == mActorFactoryMap.end());
}

ActorFactory* Scene::getActorFactory(const Ogre::String& type)
{
	const ActorFactoryMap::const_iterator it = mActorFactoryMap.find(type);
	if (it == mActorFactoryMap.end())
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, "A factory with type \"" + 
			type + "\" does not exist",
			"Scene::getActorFactory");
	}

	return it->second.get();
}

const ActorPtrList::size_type Scene::getNumActors(const Ogre::String& type) const
{
	const ActorPtrList* actorList = getActorList(type);
	if (actorList)
		return actorList->size();

	return NULL;
}

Actor* Scene::getActor(const Ogre::String& name, const Ogre::String& type) const
{ 
	const ActorPtrList* actorList = getActorList(type);
	if (actorList)
	{
		for (const auto& actor : (*actorList))
		{
			if (actor->getName() == name)
			{
				return actor.get(); // found
			}
		}
	}

	return nullptr;
}

Actor* Scene::getActor(const Ogre::String& name) const
{
	for (const auto& actorList : mActorCollection)
	{
		for (const auto& actor : actorList.second)
		{
			Actor* result = actor->find(name);
			if (result)
				return result;
		}
	}

	return nullptr;
}

Butterfly* Scene::createButterfly(ActorClass& desc, const Ogre::String& pathFilename, 
	const Ogre::String& animationName)
{
	desc.addParam("path_filename", pathFilename, PARAMETER_TYPE_STRING);
	desc.addParam("animation_name", animationName, PARAMETER_TYPE_STRING);

	desc.setType(ACTOR_BUTTERFLY_TYPE_NAME);

	return dynamic_cast<Butterfly*>(createActor(desc));
}

Paddle* Scene::createPaddle(ActorClass& desc, bool isAIControlled, 
	const Ogre::String& characterMeshFilename)
{
	desc.setType((isAIControlled) ? ACTOR_AI_TYPE_NAME : ACTOR_PLAYER_TYPE_NAME);

	return dynamic_cast<Paddle*>(createActor(desc));
}

Character* Scene::createCharacter(ActorClass& desc, 
	const Ogre::String& weaponFilename)
{
    desc.addParam("weapon_filename", weaponFilename, PARAMETER_TYPE_STRING);
	
	desc.setType(ACTOR_CHARACTER_TYPE_NAME);

    return dynamic_cast<Character*>(createActor(desc));
}

Ball* Scene::createBall(ActorClass& desc)
{
	desc.setType(ACTOR_BALL_TYPE_NAME);

	return dynamic_cast<Ball*>(createActor(desc));
}

Border* Scene::createBorder(ActorClass& desc, const Ogre::String& type)
{
	desc.addParam("border_type", type, PARAMETER_TYPE_STRING);

	desc.setType(ACTOR_BORDER_TYPE_NAME);

	return dynamic_cast<Border*>(createActor(desc));
}

Region* Scene::createRegion(ActorClass& desc)
{
	desc.setType(ACTOR_REGION_TYPE_NAME);

	return dynamic_cast<Region*>(createActor(desc));
}

Bullet* Scene::createBullet(ActorClass& desc, const Ogre::Vector3& direction)
{
	desc.addParam("direction", direction, PARAMETER_TYPE_VECTOR3);

	desc.setType(ACTOR_BULLET_TYPE_NAME);

	return dynamic_cast<Bullet*>(createActor(desc));
}

Terrain* Scene::createTerrain(ActorClass& desc, const Ogre::String& polysoupFilename)
{
	desc.addParam("polysoup_filename", polysoupFilename, PARAMETER_TYPE_STRING);

	desc.setType(ACTOR_TERRAIN_TYPE_NAME);

	return dynamic_cast<Terrain*>(createActor(desc));
}

Scoreboard* Scene::createScoreboard(ActorClass& desc)
{
	desc.setType(ACTOR_SCOREBOARD_TYPE_NAME);

	return dynamic_cast<Scoreboard*>(createActor(desc));
}

const ActorPtrList* Scene::getActorList(const Ogre::String& type) const
{
	const ActorCollectionMap::const_iterator it = mActorCollection.find(type);
	if (it != mActorCollection.end())
		return &it->second;

	return nullptr;
}

ActorPtrList* Scene::getActorList(const Ogre::String& type)
{
	const ActorCollectionMap::iterator it = mActorCollection.find(type);
	if (it != mActorCollection.end())
		return &it->second;

	return nullptr;
}

Actor* Scene::createActor(const ActorClass& desc)
{
	std::shared_ptr<Actor> actor = createUnmanagedActor(desc);

	// manage the actor by adding it to the collection
	addActor(actor); 

	return actor.get();
}

std::shared_ptr<Actor> Scene::createUnmanagedActor(const ActorClass& desc)
{
	if (getActor(desc.getName()))
	{
		OGRE_EXCEPT(Ogre::Exception::ERR_DUPLICATE_ITEM, "An actor with name \"" + 
			desc.getName() + "\" already exists and cannot be created", 
			"Scene::createUnmanagedActor");
	}
	else
	{
		// create using a factory
		ActorFactory* factory = getActorFactory(desc.getType());

		// assign creator's params
		desc._setCreator(this);
		desc._setTypeFlag(factory->getTypeFlag());

		// return an actor instance
		return factory->createInstance(desc);
	}
}

void Scene::reserveForActors(const size_t numActors)
{
	// do nothing
	// no way to reserve space without knowing actor count for each actor type
}

void Scene::addActor(std::shared_ptr<Actor> actor)
{
	DEBUG_OUTPUT(LVL_GREEN, "An actor with name \"" + actor->getName() + 
		"\" is being registered with the scene");

	// push the actor to the collection
	mActorCollection[actor->getActorType()].push_back(std::move(actor));
}

void Scene::destroyActor(Actor* actor)
{
	ActorPtrList* actorList = getActorList(actor->getActorType());
	if (actorList)
	{
		const ActorPtrList::iterator it = 
			std::find_if(actorList->begin(), actorList->end(), 
				[&actor](const ActorPtrList::value_type& smartPtr) NOEXCEPT { 
					return (smartPtr.get() == actor); 
				}
			);

		if (it != actorList->end())
		{
			auto count = it->use_count();
			if (dynamic_cast<Collidable*>(it->get()) != nullptr) 
			{ // if use count exceeds 2: the Scene and the CollisionManager, notify
				if (count > 2)
				{
					DEBUG_OUTPUT(LVL_YELLOW, "Actor \"" + actor->getName() + 
						"\" (Collidable) shared_ptr use count is more than 2 (" + 
						Ogre::StringConverter::toString(count) + 
						") before destroying");
				}
			}
			else
			{
				if (count > 1)
				{
					DEBUG_OUTPUT(LVL_YELLOW, "Actor \"" + actor->getName() + 
						"\" (non-Collidable) shared_ptr use count is more than 1 (" + 
						Ogre::StringConverter::toString(count) +
						") before destroying");
				}
			}

			DEBUG_OUTPUT(LVL_GREEN, "An actor with name \"" + (*it)->getName() + 
				"\" is being unregistered from the scene");

			// erase from the list
			actorList->erase(it);
		}
		else
		{
			DEBUG_OUTPUT(LVL_RED, "An actor with name \"" + actor->getName() + 
				"\" could not be found in the actor list with type \"" + 
				actor->getActorType() + '\"');
		}
	}
	else
	{
		DEBUG_OUTPUT(LVL_RED, "An actor list with type \"" + actor->getActorType() + 
			"\" could not be found");
	}
}

void Scene::destroyActorType(const Ogre::String& type)
{
	mActorCollection.erase(type);
}

ActorFlagType Scene::allocateNextActorTypeFlag() NOEXCEPT
{
	// check if overshifted
	assert(mNextActorTypeFlag != NULL && 
		"last actor type flag used"); 

	ActorFlagType flag = mNextActorTypeFlag;
	mNextActorTypeFlag <<= 1;
	return flag;
}

void Scene::unloadAllUsedResources()
{
	Ogre::ResourceGroupManager* resourceManager = 
		Ogre::ResourceGroupManager::getSingletonPtr();
	
	// the only resource group that declares all the resources required by 
	// only the scene; fonts and the UI textures are in group "Essential"
	// and are not unloaded since they're used in the menus
	resourceManager->unloadResourceGroup(RESOURCE_GROUP_GENERAL);
}

void Scene::reset()
{
	for_each_actor(
		[](Actor* actor) {
			actor->reset();
		}
	);
}

void Scene::update(const Ogre::Real deltaSeconds)
{
	// update actors
	for_each_actor(
		[deltaSeconds, this](Actor* actor) -> bool {
			bool flag = actor->getDestroyFlag();
			if (flag)
				destroyActor(actor);
			else
				actor->update(deltaSeconds);

			return !flag;
		}
	);

	// check for collisions
	mCollisionManager.handleCollisions();
}

void Scene::assignInput(SDL_Joystick* joystick) NOEXCEPT
{
	// NOTE: this is where the player who wants to use this input should
	// be asked to press any button
	if (mPlayer)
		mPlayer->setJoystick(joystick);
}

void Scene::unassignInput(SDL_Joystick* joystick) NOEXCEPT
{
	// NOTE: only a single human player is supported
	if (mPlayer)
		mPlayer->setJoystick(nullptr);
}

// InputHandler.h
bool Scene::mouseMoved(const SDL_MouseMotionEvent& arg)   
	{
		if (mPlayer)
			mPlayer->mouseMoved(arg);

	return true;
}

// InputHandler.h
bool Scene::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
	{
		if (mPlayer)
			mPlayer->mousePressed(arg, id);

	return true;
}

// InputHandler.h
bool Scene::mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
	{
		if (mPlayer)
			mPlayer->mouseReleased(arg, id);

	return true;
}

// InputHandler.h
bool Scene::textInput(const SDL_TextInputEvent& arg) 
	{
		if (mPlayer)
			mPlayer->textInput(arg);

	return true;
}

// InputHandler.h
bool Scene::keyPressed(const SDL_KeyboardEvent& arg) 
	{
		if (mPlayer)
			mPlayer->keyPressed(arg);

	return true;
}

// InputHandler.h
bool Scene::keyReleased(const SDL_KeyboardEvent& arg) 
	{
		if (mPlayer)
			mPlayer->keyReleased(arg);

		for_each_actor<Ball>(ACTOR_BALL_TYPE_NAME, 
			[&arg](Ball* ball) { 
				ball->keyReleased(arg); 
			}
		);

	return true;
}

// InputHandler.h
bool Scene::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
	{
		if (mPlayer)
			mPlayer->joyButtonPressed(evt, button);

	return true;
}

// InputHandler.h
bool Scene::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
	{
		if (mPlayer)
			mPlayer->joyButtonReleased(evt, button);

		for_each_actor<Ball>(ACTOR_BALL_TYPE_NAME, 
			[&evt, button](Ball* ball) { 
				ball->joyButtonReleased(evt, button); 
			}
		);

	return true;
}

// InputHandler.h
bool Scene::joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) 
	{
		if (mPlayer)
			mPlayer->joyAxisMoved(arg, axis);

	return true;
}

// InputHandler.h
bool Scene::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
	{
		if (mPlayer)
			mPlayer->joyPovMoved(arg, index);

	return true;
}