// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Ground.h"

Ground::Ground()
  : entity(0)
{
	entity = sceneManager->createEntity("groundEntity", "battleground.mesh");
	entity->setQueryFlags(GROUND_ENTITY);
	node = sceneManager->getRootSceneNode()->createChildSceneNode("groundNode");
	node->attachObject(entity);
	//node->scale(Ogre::Vector3(20.0f, 20.0f, 20.0f));
}