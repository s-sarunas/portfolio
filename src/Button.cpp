// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Button.h"

Button::Button(const Ogre::String& name, const Ogre::String& menuName, 
	const Ogre::DisplayString& caption, Ogre::OverlayContainer* tray,
	const Ogre::Real top, Widget::size_type widgetIndex, 
	SimpleMenu::Listener* listener)
  : Widget(name, menuName, caption, BUTTON_TEMPLATE_NAME, 
		BUTTON_WIDTH, BUTTON_HEIGHT, top, BUTTON_CAPTION_SUFFIX, widgetIndex,
		listener),
	mState(BUTTON_STATE_UP)
{
	getRootCaption()->setTop(-getRootCaption()->getCharHeight() / 2);

	tray->addChild(getRootElement());
}

Button::~Button()
{
}

void Button::setState(const State state)
{
	switch (state)
	{
	case BUTTON_STATE_OVER:
		getRootElement()->setBorderMaterialName(BUTTON_MATERIAL_OVER_NAME);
		getRootElement()->setMaterialName(BUTTON_MATERIAL_OVER_NAME);
		break;
	case BUTTON_STATE_UP:
		getRootElement()->setBorderMaterialName(BUTTON_MATERIAL_UP_NAME);
		getRootElement()->setMaterialName(BUTTON_MATERIAL_UP_NAME);
		break;
	case BUTTON_STATE_DOWN:
		getRootElement()->setBorderMaterialName(BUTTON_MATERIAL_DOWN_NAME);
		getRootElement()->setMaterialName(BUTTON_MATERIAL_DOWN_NAME);
		break;
	default:
		break;
	}

	mState = state;
}

// InputHandler.h
bool Button::mouseMoved(const SDL_MouseMotionEvent& arg)   
{
	if (isCursorOver(getRootElement(), arg.x, arg.y))
	{
		if (mState == BUTTON_STATE_UP)
			setState(BUTTON_STATE_OVER);
	}
	else
		setState(BUTTON_STATE_UP);

	return true;
}

// InputHandler.h
bool Button::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	if (mState == BUTTON_STATE_OVER)
	{
		setState(BUTTON_STATE_DOWN);
		return false;
	}

	return true;
}

// InputHandler.h
bool Button::mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
	if (mState == BUTTON_STATE_DOWN)
	{
		getListener()->buttonHit(this);
		return false;
	}

	return true;
}

// InputHandler.h
bool Button::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
{
	return true;
}

// InputHandler.h
bool Button::joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) 
{
	return true;
}

// InputHandler.h
bool Button::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
{
	return true;
}