// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "CinematicState.h"

#include "InAppDebug.h"

CinematicState::CinematicState(AppState::Listener* listener)
  : AppState(listener, Ogre::StringUtil::BLANK, CINEMATIC_STATE_OVERLAY_NAME),
	mTextureUnit(nullptr),
    mCurrentFadeAmount(NULL)
{
	mTextureUnit = Ogre::MaterialManager::getSingleton().getByName(
		CINEMATIC_STATE_OVERLAY_MATERIAL_NAME)->getTechnique(0)->getPass(0)->
		getTextureUnitState(0);
}

CinematicState::~CinematicState()
{
}

void CinematicState::process(const Ogre::Real deltaSeconds)
{
	mTextureUnit->setAlphaOperation(Ogre::LBX_SOURCE1, Ogre::LBS_MANUAL, 
		Ogre::LBS_CURRENT, increaseFadePercent(deltaSeconds));
}

void CinematicState::end(const AppResultType::value_type result)
{
	InAppDebug::getSingleton().getTable()->show();
	AppState::end(result);
}

// InputHandler.h
bool CinematicState::mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) 
{
    end();

	return true;
}

// InputHandler.h
bool CinematicState::keyPressed(const SDL_KeyboardEvent& arg) 
{
    end();

	return true;
}

// InputHandler.h
bool CinematicState::joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) 
{
    end();

	return true;
}

// InputHandler.h
bool CinematicState::joyPovMoved(const SDL_JoyHatEvent& arg, int index) 
{
    end();

	return true;
}
