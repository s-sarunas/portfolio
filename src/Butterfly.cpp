// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "Butterfly.h"

#include "InAppDebug.h"

Butterfly::Butterfly(const ActorClass& desc, const Ogre::String& pathFilename, 
	const Ogre::String& animationName)
  : Actor(desc),
	StaticActor(desc),
	Animated(getEntity(), animationName), 
	mCurrentVertexIndex(NULL),
	mVertexCount(NULL),
	mCurrentPosition(Ogre::Vector3::ZERO),
	mNextPosition(Ogre::Vector3::ZERO),
	mT(NULL),
	mShowcaseEnabled(false)
{
	Ogre::ResourceGroupManager* groupManager = 
		Ogre::ResourceGroupManager::getSingletonPtr();

	Ogre::String fileContent = 
		groupManager->openResource(pathFilename, RESOURCE_GROUP_GENERAL)->getAsString();

	loadVertices(fileContent);

	if (!mPathVertices.empty())
		computeCubicSplines();
	else
	{
		DEBUG_OUTPUT(LVL_YELLOW, "No vertices were loaded for a butterfly with name \"" + 
			desc.getName() + "\" and path \"" + pathFilename + '\"');
	}
}

Butterfly::~Butterfly()
{
}

bool Butterfly::_toggleShowcase()
{ 
	setVisible(mShowcaseEnabled);
	mShowcaseEnabled = !mShowcaseEnabled; 

	if (!mShowcaseEnabled)
		DEBUG_EXECUTE("reset camera");

	return mShowcaseEnabled;
};

void Butterfly::loadVertices(Ogre::String& content)
{
	// load the content from the file to the buffer
	rapidxml::xml_document<> pathFile;
	pathFile.parse<0>(&content[0]);

	// access <vertices> and find the vertex count
	rapidxml::xml_node<>* verticesNode = 
		pathFile.first_node("path")->first_node("vertices");

	mVertexCount = Ogre::StringConverter::parseUnsignedInt(
		verticesNode->first_attribute("count")->value());

	// allocate a matrix with enough space
	mPathVertices.resize(boost::extents[mVertexCount][3]);

	// iterate through the child nodes, <vertex>
	rapidxml::xml_node<>* node = verticesNode->first_node("vertex");
	for (size_t i = 0; node && i < mVertexCount; node = node->next_sibling(), ++i)
	{
		// get the x, y and z atributes
		mPathVertices[i][0] = std::stof(node->first_attribute("x")->value());
		mPathVertices[i][1] = std::stof(node->first_attribute("y")->value());
		mPathVertices[i][2] = std::stof(node->first_attribute("z")->value());
	}
}

void Butterfly::initDebuggable()
{
	RealList::value_type invLength = 0.0f;
	for (Matrix::size_type r = 0; r < mVertexCount; ++r)
	{
		// show loaded vertex as a sphere
		InAppDebug::getSingleton().addDebugObject(
			Ogre::Vector3(mPathVertices[r][0], mPathVertices[r][1], mPathVertices[r][2]), 
			true,
			BUTTERFLY_CORNER_MESH_FILENAME);

		// show the entire spline interpolation path to be taken with cubes
		invLength = mInverseArcLenghts[r];
		for (RealList::value_type z = invLength; z <= 1; z += (invLength * 2.0f))
		{
			InAppDebug::getSingleton().addDebugObject(Ogre::Vector3(
				mEquations[r][0].substitute(z),
				mEquations[r][1].substitute(z),
				mEquations[r][2].substitute(z)),
				true,
				BUTTERFLY_PATH_MESH_FILENAME);
		}
	}
}

void Butterfly::resetA(Matrix& _A)
{
	// the last indices of rows/columns (square matrix)
	const Matrix::size_type n = _A.shape()[0] - 1; 
	const Matrix::size_type m = n;

	// Using std::valarray: _A = 0;
	memset(_A.origin(), 0, sizeof(Matrix::element) * _A.num_elements());

	// first row
	_A[0][0] = 4;		// first element
	_A[0][1] = 1;		// second element
	_A[0][m] = 1;		// last element

	// tridiagonal matrix of 1, 4, 1 in between
	for (Matrix::size_type i = 1; i <= n - 1; i++) 
	{
		_A[i][i - 1] = 1;
		_A[i][i] = 4;
		_A[i][i + 1] = 1;
	}
	
	// last row
	_A[n][0] = 1;		// first element
	_A[n][m - 1] = 1;	// one before last
	_A[n][m] = 4;		// last
}

void Butterfly::populateB(Matrix& _b)
{
	// m is last index of the columns in the matrix
	const Matrix::size_type m = _b.shape()[1] - 1;

	// first column
	_b[0][0] = 3 * (mPathVertices[1][0] - mPathVertices[m][0]);
	_b[1][0] = 3 * (mPathVertices[1][1] - mPathVertices[m][1]);
	_b[2][0] = 3 * (mPathVertices[1][2] - mPathVertices[m][2]);

	// columns in between
	for (size_t j = 1; j <= m - 1; ++j)
	{
		_b[0][j] = 3 * (mPathVertices[j + 1][0] - mPathVertices[j - 1][0]);
		_b[1][j] = 3 * (mPathVertices[j + 1][1] - mPathVertices[j - 1][1]);
		_b[2][j] = 3 * (mPathVertices[j + 1][2] - mPathVertices[j - 1][2]);
	}

	// last column
	_b[0][m] = 3 * (mPathVertices[0][0] - mPathVertices[m - 1][0]);
	_b[1][m] = 3 * (mPathVertices[0][1] - mPathVertices[m - 1][1]);
	_b[2][m] = 3 * (mPathVertices[0][2] - mPathVertices[m - 1][2]);
}

void Butterfly::computeCubicSplines()
{
	Matrix::size_type m = mVertexCount, n = m;

	// +1 column for augmentation of matrix b
	Matrix A(boost::extents[n][m + 1]); 

	Matrix b(boost::extents[3][m]);
	populateB(b); 

	Matrix x(boost::extents[3][m]);

	for (Matrix::size_type s = 0; s < 3; ++s)
	{ // find the derivatives for each of the three rows 
	  // corresponding to x, y and z (row-major)
		resetA(A);

		Math::solveLinearEquations(A, b[s], x[s]);
	}
	
	// plug the derivatives back to find the equations (column-major)
	mEquations.resize(boost::extents[mVertexCount][3]);
	for (Matrix::size_type e = 0, f = e + 1; e < m; ++e, ++f)
	{ // for each piece of the path 

		if (e == m - 1) // if the last vertex is being processed
			f = 0; // use the first vertex as the next

		// x
		mEquations[e][0] = Cubic<>(
            2.0f * (mPathVertices[e][0] - mPathVertices[f][0]) + x[0][e] + x[0][f],
            3.0f * (mPathVertices[f][0] - mPathVertices[e][0]) - (2.0f * x[0][e]) - x[0][f],
            x[0][e], 
            mPathVertices[e][0]);

		// y
        mEquations[e][1] = Cubic<>(
            2.0f * (mPathVertices[e][1] - mPathVertices[f][1]) + x[1][e] + x[1][f],
            3.0f * (mPathVertices[f][1] - mPathVertices[e][1]) - (2.0f * x[1][e]) - x[1][f],
            x[1][e], 
            mPathVertices[e][1]);

		// z
		mEquations[e][2] = Cubic<>(
            2.0f * (mPathVertices[e][2] - mPathVertices[f][2]) + x[2][e] + x[2][f],
            3.0f * (mPathVertices[f][2] - mPathVertices[e][2]) - (2.0f * x[2][e]) - x[2][f],
            x[2][e], 
            mPathVertices[e][2]);
	}

	computeArcLength();

	mCurrentPosition = 
		Ogre::Vector3(
			mPathVertices[0][0], 
			mPathVertices[0][1],
			mPathVertices[0][2]);
}

void Butterfly::computeArcLength()
{
	mInverseArcLenghts.reserve(mVertexCount);
	for (EquationMatrix::size_type i = 0; i < mVertexCount; ++i)
	{
		mInverseArcLenghts.push_back(1.0f /
			Ogre::Vector3(
				Math::numericalIntegration(mEquations[i][0], 0.0f, 1.0f),
				Math::numericalIntegration(mEquations[i][1], 0.0f, 1.0f),
				Math::numericalIntegration(mEquations[i][2], 0.0f, 1.0f)
			).length()
		);
	}
}

void Butterfly::update(const Ogre::Real deltaSeconds)
{
	StaticActor::update(deltaSeconds);
	Animated::update(deltaSeconds * BUTTERFLY_SPEED_ANIMATION_SCALAR);
	
	if (mVertexCount > 0)
	{
		mCurrentPosition = mNextPosition;
		
		mT += BUTTERFLY_SPEED_SCALAR * 
			mInverseArcLenghts[mCurrentVertexIndex] * deltaSeconds;

		mNextPosition = Ogre::Vector3(
			mEquations[mCurrentVertexIndex][0].substitute(mT), 
			mEquations[mCurrentVertexIndex][1].substitute(mT),
			mEquations[mCurrentVertexIndex][2].substitute(mT));

		if (mShowcaseEnabled)
		{
			getCamera()->setDirection(Ogre::Vector3::ZERO - mCurrentPosition);
			getCamera()->setPosition(mCurrentPosition);
		}

		getSceneNode()->setDirection(mNextPosition - mCurrentPosition, 
			Ogre::Node::TransformSpace::TS_WORLD, Ogre::Vector3::NEGATIVE_UNIT_X);
		getNode()->setPosition(mCurrentPosition);

		if (mT >= 1.0f)
			nextVertex();
	}
}

void Butterfly::nextVertex() NOEXCEPT
{
	mT -= 1.0f;

	++mCurrentVertexIndex;
	if (mCurrentVertexIndex == mVertexCount)
		mCurrentVertexIndex = 0;
}

std::shared_ptr<Actor> ButterflyFactory::createInstance(const ActorClass& desc)
{
	return std::make_shared<Butterfly>(desc, 
		desc.getParam("path_filename"),
		desc.getParam("animation_name"));
}