// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef BALL_H
#define BALL_H

#include "HeaderPrefix.h"

#include "Ray.h"

// headers defining base classes
#include "DynamicActor.h"
#include "InputHandler.h"
#include "Grounded.h"
#include "Config.h"
#include "CollisionManager.h"

#define BALL_SPEED_STARTING 35.0f
// int and float multiplication and division by 2 is very cheap: bit shift 
// is used for ints and for floats the exponent gets increased by 1 
#define BALL_SPEED_INCREASE_MULTIPLIER 5.0f
// the angle range for reflection against a dynamic actor, e.g. a paddle ( _\__/_ )
#define BALL_REFLECT_ANGLE_RANGE_DEGREES 35.0f 
#define BALL_COPY_REL_ROTATION_MIN_DEGREES 20.0f
#define BALL_COPY_REL_ROTATION_MAX_DEGREES 30.0f
#define BALL_COPY_REL_SPEED_MIN 10.0f
#define BALL_COPY_REL_SPEED_MAX 35.0f
// at this speed the ball will reach the final colour 
// NOTE: should be found using compile-time evaluation of the ball's 
// speed equation at the asymptote (if a rational speed function is used)
#define BALL_COLOUR_FINAL_SPEED 100.0f 
#define BALL_DUMMY_UPDATE_RATE_SECONDS (1 / 60.0f)
// of actual travel time
#define BALL_DUMMY_FAILSAFE_DURATION std::chrono::seconds(30) 
#define BALL_BUTTON_KB_START SDLK_TAB
#define BALL_BUTTON_JS_START SDL_CONTROLLER_AXIS_TRIGGERRIGHT
#define BALL_BUTTON_JS_START_ALT SDL_CONTROLLER_AXIS_TRIGGERLEFT

class Ball: public DynamicActor, public Grounded, public InputHandler, 
	public Config::Listener, public CollisionManager::Listener
{
public:
	Ball(const ActorClass& desc); 
	~Ball();
	
	Ball(const Ball& rhs, bool varyOrientation = false, bool varySpeed = false, 
		bool newMaterial = false);
	Ball& operator=(const Ball&) = delete;
	Ball(Ball&& rhs);
	Ball& operator=(Ball&& rhs);

	void updateColour();

	void setPrimary(bool flag) NOEXCEPT { mIsPrimary = flag; }

	// Actor.h
	void update(const Ogre::Real deltaSeconds) override;
	void reset() override;
	NODISCARD const Ogre::AxisAlignedBox& getBoundingBox() override;
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{ 
		return ACTOR_BALL_TYPE_NAME;
	}

	// Collidable.h
	void handleCollision(Collidable* collidable, bool isColliding) override;

	// InputHandler.h
    bool keyReleased(const SDL_KeyboardEvent& arg) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;

	NODISCARD const Collidable* getEndCollidable() const NOEXCEPT 
	{ 
		return mEndCollidable;
	}
	NODISCARD const Ogre::Vector3& getEndPosition() const NOEXCEPT 
	{ 
		return mEndPosition;
	}
	NODISCARD const Ogre::ColourValue& getColour() const NOEXCEPT 
	{ 
		return mColour;
	}
	NODISCARD const bool isPrimary() const NOEXCEPT 
	{
		return mIsPrimary;
	}

private:
	void updateEndParams();
	void applyColour(const Ogre::ColourValue& newColour);

	// DynamicActor.h
	void speedChanged() override;

	Collidable* mEndCollidable;
	Ogre::Vector3 mEndPosition;
	Ogre::ColourValue mColour;
	Ogre::AxisAlignedBox mAxisAlignedBox;
	Paddle* mLastPaddle;
	bool mIsPrimary;

	static const Ogre::Real msColourMatrix[][3];
	static const size_t msNumColours;
	static const Ogre::Real msSpeedPerColour;

	// CollisionManager::Listener
	void collisionOccured(Collidable* one, Collidable* two) override;

	// Config::Listener
	void propertyChanged(const Config::Property& prop) override;

protected:
};

class NODISCARD BallFactory: public ActorFactory
{
public:
	BallFactory() {}
	~BallFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{ 
		return ACTOR_BALL_TYPE_NAME;
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{
		return ACTOR_BALL_TYPE_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif