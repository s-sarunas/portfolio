// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef GAME_PREREQUISITES_H
#define GAME_PREREQUISITES_H

#include "OgrePrerequisites.h"
#include "OgreOverlayPrerequisites.h"

#include <ios>
#include <chrono>

#include "boost\io\ios_state.hpp"

// should be 'noexcept' in C++17
#define NOEXCEPT throw()
#define NOEXCEPT_COND(cond, x) /*noexcept(cond), remove x (avoids C4002)*/

#define NODISCARD // [[nodiscard]] only available in C++17
// #pragma deprecated does not take function signatures into account
#define DEPRECATED __declspec(deprecated) 

#define RNG(fLow, fHigh) Ogre::Math::RangeRandom(fLow, fHigh)

static const Ogre::Vector3 UNIT_VECTOR_RIGHT(Ogre::Vector3::NEGATIVE_UNIT_X);
static const Ogre::Vector3 UNIT_VECTOR_UP(Ogre::Vector3::UNIT_Y);
static const Ogre::Vector3 UNIT_VECTOR_FORWARD(Ogre::Vector3::UNIT_Z);
static const Ogre::Vector3 UNIT_VECTOR_MOVEPLANE(1.0f, 0.0f, 1.0f);

#define OGRE_UNIT_VECTOR_FORWARD Ogre::Vector3::NEGATIVE_UNIT_Z
#define OGRE_UNIT_VECTOR_RIGHT Ogre::Vector3::UNIT_X
#define OGRE_UNIT_VECTOR_UP Ogre::Vector3::UNIT_Y

// everything except the scene, e.g. fonts, UI textures
static const Ogre::String RESOURCE_GROUP_ESSENTIAL = "Essential"; 
// everything that is loaded by creating a scene
static const Ogre::String RESOURCE_GROUP_GENERAL = "General"; 

enum GameModes
{
	GAME_MODE_FREEPLAY,
	GAME_MODE_CHALLENGE
};

// forward declarations
class Ability;
class Dash;
class Mirror;
class Shoot;
class AbilitiesManager;
class AI;
class AIFactory;
class Ball;
class Bullet;
class BallFactory;
class Paddle;
class PaddleFactory;
class Border;
class BorderFactory;
class Butterfly;
class ButterflyFactory;
class Character;
class Flash;
class Region;
class Scoreboard;
class ActorCreator;
class MovableObjectListenerImpl;
struct ActorClass;
class Actor;
class Collidable;
class DynamicActor;
class Grounded;
class SharedProperties;
class StaticActor;
class AnimationStateWrapper;
class Terrain;
class CollisionManager;
class Config;
class Core;
class DotSceneLoader;
class InAppDebug;
class DebugCamera;
class DebugConsole;
class DebugTable;
class DebugTimer;
class InputHandler;
namespace Math
{
	class RandomNumberGenerator;

	template <size_t N, typename Ty>
	struct Polynomial;

	template <typename Ty = RealType>
	using Quadratic = Polynomial<2, Ty>;

	template <typename Ty = RealType>
	using Cubic = Polynomial<3, Ty>;
};
template <typename OriginType>
class Ray;
class Scene;
class StringInterface;
class Weapon;
class AppNode;
class AppState;
class CinematicState;
class MapSelectState;
class MenuState;
class PauseState;
class SettingsState;
class Widget;
class Button;
class Dropdown;
class AppStateManager;
class SimpleMenu;

extern NODISCARD Ogre::SceneManager* const getSceneManager() NOEXCEPT;
extern NODISCARD Ogre::Camera* const getCamera() NOEXCEPT;
extern NODISCARD Config& getConfig() NOEXCEPT;
extern NODISCARD const GameModes getGameMode() NOEXCEPT;

// allows to use these types without the need to include their respective headers
// std::vector for quick iteration

typedef std::vector<Collidable*> CollidablePtrList;			
// bool indicates whether the collision should be processed (notified to actors 
// and listeners)
typedef std::vector<std::pair<Collidable*, bool>> CollisionList;
typedef std::vector<std::shared_ptr<Actor>> ActorPtrList; 
typedef std::vector<SDL_Joystick*> JoystickList;

// std::unordered_map - no ordering or traversal is needed
typedef std::unordered_map<Ogre::String, ActorPtrList> ActorCollectionMap; 

namespace estd
{
	// will be available in C++17
	// NOTE: allows bools and chars to pass through
	template <typename Ty>
	NODISCARD Ty clamp(const Ty n, const Ty lower, const Ty upper)
	{
		static_assert(std::is_arithmetic<Ty>::value,
			"incorrect type passed");

		assert(lower < upper && 
			"lower value is more than upper value");

		return std::max(lower, std::min(n, upper));
	}

	template <typename InIt, typename ValTy>
	void clamp(InIt first, const InIt last, const ValTy lower, const ValTy upper)
	{
		static_assert(
			std::is_same<InIt::iterator_category, std::input_iterator_tag>::value ||
			std::is_same<InIt::value_type, ValTy>::value,
			"incorrect type passed");

		assert(lower < upper && 
			"lower value is more than upper value");

		// NOTE: does not test the validity of the iterators or their range
		for (; first != last; ++first)
			*first = std::max(lower, std::min(*first, upper));
	}

	// checks if weak_ptr was not initialized or has expired (cheaper than 
	// weak_ptr::lock())
	template <typename Ty>
	NODISCARD bool is_empty(const std::weak_ptr<Ty>& weak) NOEXCEPT 
	{
		using wt = std::weak_ptr<Ty>;
		// find the order (in code) between the initialization of two weak_ptrs 
		// and if it's ==, then they're equivalent:
		// weak.owner_before(not empty_weak) && empty_weak.owner_before(not weak)
		// empty_weak was not set to weak (uninitialized) and 
		// weak was not set to empty_weak (expired)
		return !weak.owner_before(wt{}) && !wt{}.owner_before(weak);
	}

	// NOTE: could use std::string_view here to avoid copying or erasing from 
	// the original string and std::string_view::remove_prefix, remove_suffix,
	// starts_with and ends_with or string::starts_with and ends_with in C++17

	// removes a substring from a string, leaving the original string intact
	template <typename char_type = std::string::value_type>
	NODISCARD static const std::basic_string<char_type> remove_substring(
		std::basic_string<char_type> str, 
		const std::basic_string<char_type>& sub) 
	{
		typedef std::basic_string<char_type> string_type;

		const typename string_type::size_type pos = str.find(sub);
		if (pos != str.npos)
			str.erase(pos, sub.length());

		return str;
	}

	// retrieves the prefix from a string, leaving the original string intact
	// if no separators exist in the string, the result is empty
	template <typename char_type = std::string::value_type>
	NODISCARD static const std::basic_string<char_type> get_prefix(
		const std::basic_string<char_type>& str, 
		const char_type separator = '_')
	{
		typedef std::basic_string<char_type> string_type;

		const typename string_type::const_iterator it = 
			std::find(str.begin(), str.end(), separator);

		string_type res;
		// copies to a string instead of erasing
		std::copy(str.begin(), it, std::back_inserter(res));

		return res;
	}

	// returns whether a given prefix exists in a string separated by a 
	// given separator
	template <typename char_type = std::string::value_type>
	NODISCARD static bool has_prefix(const std::basic_string<char_type>& str, 
		const std::basic_string<char_type>& prefix,
		const char_type separator = '_')
	{
		assert(str.size() >= prefix.size() && 
			"string has to be longer or equal in length to the prefix");

		typedef std::basic_string<char_type> string_type;

		const typename string_type::const_iterator it = 
			std::find(str.begin(), str.end(), separator);

		if (it != str.end())
		{ // separator was found
			// random-access iterators
			if (prefix.size() == std::distance(str.begin(), it)) 
			{ // found prefix and given prefix's lengths match
				return std::equal(str.begin(), it, prefix.begin());
			}
		}
		
		return false;
	}

	// retrieves the suffix from a string, leaving the original string intact
	// if no separators exist in the string, the result is empty
	template <typename char_type = std::string::value_type>
	NODISCARD static const std::basic_string<char_type> get_suffix(
		const std::basic_string<char_type>& str, const char_type separator = '_')
	{
		typedef std::basic_string<char_type> string_type;

		const typename string_type::const_reverse_iterator it = 
			std::find(str.rbegin(), str.rend(), separator);

		string_type res; 
		// copies to a string instead of erasing
		std::copy(it.base(), str.end(), std::back_inserter(res));

		return res;
	}

	// returns whether a given suffix exists in a string separated by a 
	// given separator
	template <typename char_type = std::string::value_type>
	NODISCARD static bool has_suffix(const std::basic_string<char_type>& str, 
		const std::basic_string<char_type>& suffix, 
		const char_type separator = '_')
	{
		assert(str.size() >= suffix.size() && 
			"string has to be longer or equal in length to the suffix");

		typedef std::basic_string<char_type> string_type;

		const typename string_type::const_reverse_iterator it = 
			std::find(str.rbegin(), str.rend(), separator);

		if (it != str.rend())
		{ // separator was found
			// random-access iterators
			if (suffix.size() == std::distance(str.rbegin(), it)) 
			{ // found suffix and given suffix's lengths match
				return std::equal(str.rbegin(), it, suffix.rbegin());
			}
		}
		
		return false;
	}

	// different to Ogre::StringUtil::split in that it adds an empty string if 
	// two delimiters are in a row, thus preserving the structure in the output
	NODISCARD static std::vector<std::string> split(const std::string& str, 
		const std::string& splitOnDelims = "\t\n", const std::string& preserveDelims = "", 
		size_t* numPreserved = nullptr)
	{
		const std::string allDelims = splitOnDelims + preserveDelims;

		if (numPreserved)
			(*numPreserved) = 0;

		std::vector<std::string> result;
		size_t start = 0, pos = 0;
		while (start < str.size())
		{
			pos = str.find_first_of(allDelims, start);
			if (pos == str.npos)
			{ // copy up to the end
				result.push_back(str.substr(start, pos));
				break;
			}
			else
			{
				// copy up to the delimiter
				if (pos != start)
					result.push_back(str.substr(start, pos - start));

				// copy the delim if its in the string of preserved delims
				if (preserveDelims.find_first_of(str[pos]) != str.npos)
				{
					result.push_back(std::string(1, str[pos]));

					if (numPreserved)
						++(*numPreserved);
				}
			}

			start = pos + 1;
		} 

		return result;
	}

	template <typename int_type, typename char_type = std::string::value_type>
	NODISCARD static const std::basic_string<char_type> to_binary(const int_type integer)
	{
		typedef std::basic_string<char_type> string_type;

		const typename string_type::size_type numBits = sizeof(int_type) * CHAR_BIT;
		string_type result;
		result.reserve(numBits);

		for (typename string_type::size_type i = numBits; i > 0; --i)
			result += ((integer & (1 << (i - 1))) ? '1' : '0');

		return result;
	}

	namespace detail
	{
		// recursive template for tuple_do_for
		template <size_t N, typename Tuple, typename Func>
		struct tuple_do_for_t
		{
			static /*constexpr*/ void apply(const Tuple& tuple, Func func)
			{
				func(std::get<N>(tuple));

				tuple_do_for_t<N - 1, Tuple, Func>::apply(tuple, func);
			}
		};

		// finalizing template for tuple_do_for
		template <typename Tuple, typename Func>
		struct tuple_do_for_t<0, Tuple, Func>
		{
			// function apply is used in a struct to allow partial 
			// specializations for the function
			static /*constexpr*/ void apply(const Tuple& tuple, Func func)
			{
				func(std::get<0>(tuple));
			}
		};

		// used if the tuple element type and the provided type match
		template <typename Type, size_t N, typename Tuple, typename Func>
		typename std::enable_if<
			std::is_same<typename std::tuple_element<N, Tuple>::type, Type>::value
		>::type
		/*constexpr*/ tuple_do_for_type_call(const Tuple& tuple, Func func)
		{
			func(std::get<N>(tuple));
		};

		// used if the tuple element type and the provided types do not match
		template <typename Type, size_t N, typename Tuple, typename Func>
		typename std::enable_if<
			!std::is_same<typename std::tuple_element<N, Tuple>::type, Type>::value
		>::type
		/*constexpr*/ tuple_do_for_type_call(const Tuple& tuple, Func func)
		{
			/* do nothing */
		};

		// recursive template for tuple_do_for_type
		template <typename Type, size_t N, typename Tuple, typename Func>
		struct tuple_do_for_type_t
		{
			static /*constexpr*/ void apply(const Tuple& tuple, Func func)
			{
				// attempt to substitute the type, use the SFINAE principle
				detail::tuple_do_for_type_call<Type, N>(tuple, func);

				// recursion
				tuple_do_for_type_t<Type, N - 1, Tuple, Func>::apply(tuple, func);
			}
		};

		// finalizing template for tuple_do_for_type
		template <typename Type, typename Tuple, typename Func>
		struct tuple_do_for_type_t<Type, 0, Tuple, Func>
		{
			static /*constexpr*/ void apply(const Tuple& tuple, Func func)
			{
				// attempt to substitute the type, use the SFINAE principle
				detail::tuple_do_for_type_call<Type, 0>(tuple, func);
			}
		};
	};

	/*
		Below tuple_* recursive templates result in the same assembly as if done
		manually in GCC (with at least -O1 flag). However, using a lambda with 
		a capture adds a few more mov instructions for each call (tested only on GCC)

		NOTE: MSVC19 does not reduce the code fully during compile-time (even 
		with optimisation flags)
	*/

	/*
		Do for N tuple elements of the same type the provided function takes, 
		moving from the last to the first
	*/
	template <size_t N, typename Tuple, typename Func>
	static /*constexpr*/ void tuple_do_for(const Tuple& tuple, Func func)
	{
		// N - 1 to turn it from tuple_size to index in the tuple
		detail::tuple_do_for_t<N - 1, Tuple, Func>::apply(tuple, func); 
	};

	/*
		Do for all tuple elements of given Type, moving from the last to the first
	*/
	template <typename Type, typename Tuple, typename Func>
	static /*constexpr*/ void tuple_do_for_type(const Tuple& tuple, Func func)
	{
		static_assert(estd::is_any<Type, Tuple>::value, 
			"the tuple does not hold such a type");

		// N - 1 to turn it from tuple_size to index in the tuple
		detail::tuple_do_for_type_t<Type, std::tuple_size<Tuple>::value - 1,
			Tuple, Func>::apply(tuple, func);
	};

	// defined by default in C++17
	template <bool B>
	using bool_constant = std::integral_constant<bool, B>;

	/* 
		Alternatives in C++17 for the below type checking:
		Fold expressions (expanding parameter pack with operators, i.e. || 
			and comma) are available in C++17
		std::disjunction (logical OR on a sequence of traits) is available in C++17
	*/
	// recursive templates to parameter expand variadic templates and check 
	// if type Ty is present
	template <typename Ty, typename Head, typename... Tail>
	struct is_any : 
		estd::bool_constant<std::is_same<Ty, Head>::value || is_any<Ty, Tail...>::value> {};

	template <typename Ty, typename Head> // finalising template
	struct is_any<Ty, Head> : std::is_same<Ty, Head> {};

	// specialization of is_any for std::tuple
	template <typename Ty, typename Head, typename... Tail>
	struct is_any <Ty, std::tuple<Head, Tail...>> : estd::is_any<Ty, Head, Tail...> {};

	// specializations of is_any for std::pair
	template <typename Ty, typename Head, typename... Tail>
	struct is_any <Ty, std::pair<Head, Tail...>> : estd::is_any<Ty, Head, Tail...> {};

	/*
		Recursive templates to remove all pointers from a type
	*/
	// primary and terminating template
	template <typename Ty>
	struct clear_all_pointers { typedef Ty type; };

	// defines every possible combination of: const, volatile, const volatile and none
#define _CLEAR_POINTERS(CV_OPT) \
	template <typename Ty> \
	struct clear_all_pointers<Ty* CV_OPT> : estd::clear_all_pointers<Ty> {};
_CLASS_DEFINE_CV(_CLEAR_POINTERS)

	/*
		Recursive templates to copy all pointers from one type to another
	*/
	// primary and terminating template
	template <typename From, typename To>
	struct copy_all_pointers { typedef To type; };

#define _COPY_ALL_POINTERS(CV_OPT) \
	template <typename From, typename To> \
	struct copy_all_pointers<From* CV_OPT, To> \
	{ \
		typedef typename copy_all_pointers<From, To>::type* type; \
	};
_CLASS_DEFINE_CV(_COPY_ALL_POINTERS)

	/*
		Copies over const and/or volatile qualifiers to a new type
		NOTE: alternative to below would be to exhaust all possible variations 
		of const and volatile using macros similar to _CLASS_DEFINE_CV 
		(12 possible combinations)
	*/
	template <typename From, typename To>
	class copy_cv
	{
		// helpers
		template<typename _from, typename _to>
		struct check_const
		{
			using type = 
				typename std::conditional<
					std::is_const<_from>::value, 
					typename std::add_const<_to>::type, 
					_to
				>::type;
		};

		template<typename _from, typename _to>
		struct check_volatile
		{
			using type = 
				typename std::conditional<
					std::is_volatile<_from>::value, 
					typename std::add_volatile<_to>::type, 
					_to
				>::type;
		};

		template<typename _from, typename _to>
		struct check_cv
		{
			typedef typename check_const<
				_from, 
				typename check_volatile<_from, _to>::type
			>::type type;
		};

		/* 
			Strip pointers and references to rebuild the type from left-to-right,
			maintaining the correct order of the qualifiers. This is also 
			needed for std::is_const and std::is_volatile to correctly 
			identify the underlying types
		*/
		using from_type =
			typename estd::clear_all_pointers<
				typename std::remove_reference<From>::type
			>::type;

		using to_type =
			typename estd::clear_all_pointers<
				typename std::remove_reference<To>::type
			>::type;

		/*
			Copy qualifiers from the the left side of the type
		*/
		using cv_qualified = // const volatile
			typename check_cv<from_type, to_type>::type;

		/*
			Readd any references or pointers from the original type
		*/
		using ref_type = // is & or &&
			typename std::conditional< 
				std::is_reference<To>::value, 
				typename std::conditional<
					std::is_rvalue_reference<To>::value, // is &&
					typename std::add_rvalue_reference<cv_qualified>::type,
					typename std::conditional<
						std::is_lvalue_reference<To>::value, // is &
						typename std::add_lvalue_reference<cv_qualified>::type,
						cv_qualified
					>::type
				>::type,
				cv_qualified
			>::type;

		using ptr_type = // *...
			typename estd::copy_all_pointers<To, ref_type>::type; 

		/*
			If the type is a pointer, copy qualifiers from the right side of the type
		*/
		using const_ptr = // * const
			typename std::conditional<
				std::is_pointer<From>::value,
				typename check_const<From, ptr_type>::type,
				ptr_type
			>::type;

		using const_volatile_ptr = // * volatile
			typename std::conditional<
				std::is_pointer<From>::value,
				typename check_volatile<From, const_ptr>::type,
				const_ptr
			>::type;

	public:
		using type = const_volatile_ptr;
	};

	namespace detail
	{
		template <typename B>
		static /*constexpr*/ void check_convertible() NOEXCEPT
		{
			// do not preprocess type B to add/remove pointers to make the 
			// client code more readable
			static_assert(!std::is_pointer<B>::value,
				"type given must not be a pointer");
		}

		template <typename B, typename A, typename FuncSuccess, typename FuncFailure>
		struct runtime_is_convertible_f_t
		{
			typedef typename estd::copy_cv<A, B*>::type result_type;

			NODISCARD static /*constexpr*/ result_type apply(A* const ptr, 
				FuncSuccess onSuccess, FuncFailure onFailure)
			{
				result_type result = dynamic_cast<result_type>(ptr);
				if (result)
					onSuccess(result);
				else
					onFailure();

				return result;
			}
		};

		template <typename B, typename A, typename FuncSuccess>
		struct runtime_is_convertible_t
		{
			typedef typename estd::copy_cv<A, B*>::type result_type;

			NODISCARD static /*constexpr*/ result_type apply(A* const ptr, 
				FuncSuccess onSuccess) 
			{
				result_type result = dynamic_cast<result_type>(ptr);
				if (result)
					onSuccess(result);

				return result;
			}
		};
	}

	/*
		Used with cross-casts and downcasts (base to derived) where 
		std::is_convertible would not return the correct result, 
		since the compiler does not have the full information about the
		pointer available only at run-time

		NOTE: this is not used
	*/
	template <typename B, typename A, typename FuncSuccess, typename FuncFailure>
	static /*constexpr*/ auto runtime_is_convertible(A* const ptr, 
		FuncSuccess onSuccess, FuncFailure onFailure) -> 
		typename detail::runtime_is_convertible_f_t<B, A, FuncSuccess, FuncFailure>::result_type
	{
		detail::check_convertible<B>();

		return detail::runtime_is_convertible_f_t<B, A, FuncSuccess, FuncFailure>
			::apply(ptr, onSuccess, onFailure);
	}

	template <typename B, typename A, typename FuncSuccess>
	static /*constexpr*/ auto runtime_is_convertible(A* const ptr, 
		FuncSuccess onSuccess) -> 
		typename detail::runtime_is_convertible_t<B, A, FuncSuccess>::result_type
	{
		detail::check_convertible<B>();

		return detail::runtime_is_convertible_t<B, A, FuncSuccess>
			::apply(ptr, onSuccess);
	}

	// specializations for std::shared_ptr
	/* 
		NOTE: taking std::shared_ptr by reference, which does not increase the 
		use_count or protect the pointer from being deleted while the function runs
	*/
	template <typename B, typename A, typename FuncSuccess, typename FuncFailure>
	static /*constexpr*/ std::shared_ptr<B> runtime_is_convertible(
		const std::shared_ptr<A>& ptr, FuncSuccess onSuccess, FuncFailure onFailure)
	{
		std::shared_ptr<B> result = std::dynamic_pointer_cast<B>(ptr);
		if (result)
			onSuccess(result);
		else
			onFailure();

		return result;
	}

	template <typename B, typename A, typename FuncSuccess>
	static /*constexpr*/ std::shared_ptr<B> runtime_is_convertible(
		const std::shared_ptr<A>& ptr, FuncSuccess onSuccess)
	{
		std::shared_ptr<B> result = std::dynamic_pointer_cast<B>(ptr);
		if (result)
			onSuccess(result);

		return result;
	}

	// specializations for std::unique_ptr
	template <typename B, typename A, typename FuncSuccess, typename FuncFailure>
	static /*constexpr*/ std::unique_ptr<B> runtime_is_convertible(
		const std::unique_ptr<A>& ptr, FuncSuccess onSuccess, FuncFailure onFailure)
	{
		std::unique_ptr<B> result = std::dynamic_pointer_cast<B>(ptr);
		if (result)
			onSuccess(result);
		else
			onFailure();

		return result;
	}

	template <typename B, typename A, typename FuncSuccess>
	static /*constexpr*/ std::unique_ptr<B> runtime_is_convertible(
		const std::unique_ptr<A>& ptr, FuncSuccess onSuccess)
	{
		std::unique_ptr<B> result = std::dynamic_pointer_cast<B>(ptr);
		if (result)
			onSuccess(result);

		return result;
	}

	namespace detail
	{
		// function object with overloaded operator<<, instead of ()
		struct columnmanip
		{
			columnmanip(std::ios_base& (*f)(std::ios_base&, const size_t), 
				const size_t w)
			  : func(f),
				width(w) {};

			std::ios_base& (*func)(std::ios_base&, const size_t);
			const size_t width;
		};

		template <typename Ch, typename Tr>
		std::basic_ostream<Ch, Tr>& operator<<(std::basic_ostream<Ch, Tr>& os, 
			const columnmanip& m)
		{
			m.func(os, m.width); // calls the function with the stream as argument
			return os;
		}

		template <typename Ch, typename Tr>
		struct breakmanip
		{
			breakmanip(std::basic_ostream<Ch, Tr>& (*func)(
				std::basic_ostream<Ch, Tr>&, const size_t, Ch, Ch), const size_t w, 
				Ch f, Ch e)
			  : function(func),
				width(w),
				fill(f),
				end(e) {};

			std::basic_ostream<Ch, Tr>& (*function)(std::basic_ostream<Ch, Tr>&, 
				const size_t, Ch, Ch);
			const size_t width;
			Ch fill;
			Ch end;
		};

		template <typename Ch, typename Tr>
		std::basic_ostream<Ch, Tr>& operator<<(std::basic_ostream<Ch, Tr>& os, 
			const breakmanip<Ch, Tr>& m)
		{
			m.function(os, m.width, m.fill, m.end);
			return os;
		}
	}

	// stream manipulator that outputs a given stream in a column
	// make sure to save cout's formatting flags, e.g. using 
	// boost::io::ios_flags_saver after using this
	// NOTE: manipulators do not affect the flags, therefore this is not 
	// the best design
	static detail::columnmanip column(const size_t width)
	{
		// explicit lambda return type is needed to return a reference
		auto h = [](std::ios_base& s, const size_t w) -> std::ios_base& 
		{ 
			s.width(w); // has to be set each output operation
			s.flags(std::ios::left); 

			return s; 
		};

		return detail::columnmanip(h, width);
	}

	// stream manipulator that outputs a line of given width
	template <typename Ch = char, typename Tr = std::char_traits<Ch>>
	static detail::breakmanip<Ch, Tr> linebreak(const size_t width, 
		Ch fillCh = '-', Ch endCh = '+')
	{
		// explicit lambda return type is needed to return a reference
		auto h = [](std::basic_ostream<Ch, Tr>& s, const size_t w, Ch f, Ch e) 
			-> std::basic_ostream<Ch, Tr>& 
		{ 
			s.fill(f);
			s.width(w - 1); // -1 to make space for the end character

			if (f != e)
				s << left << e << right << e << '\n';
			else
				s << f << '\n';

			s.fill(' ');

			return s; 
		};

		return detail::breakmanip<Ch, Tr>(h, width, fillCh, endCh);
	}

	// estd::unwrap removes the std::reference_wrapper object
	template <typename Ty>
	struct unwrap
	{
		typedef Ty type;
	};

	template <typename Ty>
	struct unwrap<std::reference_wrapper<Ty>>
	{
		typedef Ty& type;
	};

	// std::decay does the usual type transformations when arguments are passed 
	// to functions by value also std::reference_wrapper is removed if any
	template <typename Ty>
	using decay_unwrap = unwrap<typename std::decay<Ty>::type>;

	namespace detail
	{
		// std::map::insert_or_assign not available in the current version of 
		// the compiler (MSVC2013) returns a bool indicating whether the param
		// was inserted (true) or assigned (false) and the iterator to it, 
		// see std::map::insert_or_assign
		template <typename MapTy, typename KeyTy, typename MappedTy>
		std::pair<typename MapTy::iterator, bool> insert_or_assign(MapTy& map, 
			KeyTy&& key,						// forwarding reference
			MappedTy&& value)					// forwarding reference
		{
			const MapTy::iterator it = map.find(key);
			if (it != map.end())
			{ // the value exists - assign
				it->second = std::forward<MappedTy>(value);

				return std::make_pair(it, false);
			}
			else 
			{ // the value does not exist - insert
				return std::make_pair(
					map.emplace_hint(it, 
						std::forward<KeyTy>(key), 
						std::forward<MappedTy>(value)), 
					true);
			}
		}
	}

	// overloads to keep the key strongly typed
	template <typename MapTy, typename MappedTy>
	std::pair<typename MapTy::iterator, bool> insert_or_assign(MapTy& map, 
		typename const MapTy::key_type& key,	// lvalue
		MappedTy&& value)						// forwarding reference
	{ // lvalue key - do not move it
		return detail::insert_or_assign(map, 
			key, 
			std::forward<MappedTy>(value));
	}

	template <typename MapTy, typename MappedTy>
	std::pair<typename MapTy::iterator, bool> insert_or_assign(MapTy& map, 
		typename MapTy::key_type&& key,			// rvalue
		MappedTy&& value)						// forwarding reference
	{ // rvalue key - move it
		return detail::insert_or_assign(map, 
			std::move(key), 
			std::forward<MappedTy>(value));
	}

	template <typename Ty>
	struct is_iterator
	{
		static char test(...);

		// using SFINAE via a template parameter to test the typedefs and
		// avoid modifying the function signature
		// this overload will have higher overloading precendence and 
		// will always be considered before the general test(...)
		template <typename U,
			typename=typename std::iterator_traits<U>::difference_type,
			typename=typename std::iterator_traits<U>::pointer,
			typename=typename std::iterator_traits<U>::reference,
			typename=typename std::iterator_traits<U>::value_type,
			typename=typename std::iterator_traits<U>::iterator_category
		> static long test(U&&);

		// checks the return type of the function chosen by the compiler
		// std::declval avoids construction of Ty in unevaluated contexts
		static const/*expr*/ bool value = 
			std::is_same<
				decltype(test(std::declval<Ty>())), 
				long
			>::value;
	};

	template <typename Ty>
	struct is_initializer_list : std::false_type {};

	template <typename Ty>
	struct is_initializer_list<std::initializer_list<Ty>> : std::true_type {};

	template <typename Ty>
	struct passable_by_value
	{
		static const/*expr*/ bool value = 
			is_iterator<Ty>::value || 
			is_initializer_list<Ty>::value || 
			std::is_function<Ty>::value || // should be std::is_invocable in C++17
			!(std::is_class<Ty>::value);
	};
}

namespace eOgre
{
	// iterates recursively over all the children of the given scene node and
	// applies the given function
	template <typename Func>
	static void for_each_child_scene_node(Ogre::SceneNode* rootNode, Func func, 
		unsigned int depth = 0) NOEXCEPT_COND(
		std::is_nothrow_invocable<Func, Ogre::SceneNode*>::value) // C++17 only
	{
		if (rootNode)
		{
			Ogre::Node::ChildNodeIterator it = rootNode->getChildIterator();
			while (it.hasMoreElements())
			{
				Ogre::SceneNode* sceneNode = 
					dynamic_cast<Ogre::SceneNode*>(it.getNext());
				if (sceneNode)
				{
					func(sceneNode, depth);

					if (sceneNode->numChildren() > 0) // if it has children
					{
						++depth;

						// recurse
						for_each_child_scene_node(sceneNode, func, depth); 

						--depth;
					}
				}
			}
		}
	}

	// recursively iterates over all the children of the given scene node and
	// returns the first scene node for which the predicate returns true
	// otherwise, nullptr if the node was a Bone or a TagPoint or the predicate 
	// was never true
	template <typename Pred>
	NODISCARD static Ogre::SceneNode* find_child_scene_node(
		const Ogre::SceneNode* rootNode, const Pred pred) NOEXCEPT_COND(
		std::is_nothrow_invocable<Pred, const Ogre::SceneNode*>::value) // C++17 only
	{
		if (rootNode)
		{
			// NOTE: should be getConstChildIterator(), however this non-member 
			// function cannot be declared const
			Ogre::Node::ConstChildNodeIterator it = rootNode->getChildIterator(); 
			while (it.hasMoreElements())
			{
				const Ogre::SceneNode* sceneNode = 
					dynamic_cast<Ogre::SceneNode*>(it.getNext());
				if (sceneNode)
				{
					if (pred(sceneNode))
						return const_cast<Ogre::SceneNode*>(sceneNode);

					if (sceneNode->numChildren() > 0) // if it has children
					{
						// recurse
						const Ogre::SceneNode* result = 
							find_child_scene_node(sceneNode, pred); 

						if (result)
							return const_cast<Ogre::SceneNode*>(result);
					}
				}
			}
		}

		return nullptr;
	}

	// recursively iterates over all the children of the given scene node and 
	// attached movable objects, returns the first movable object for which the 
	// predicate returns true otherwise, nullptr if the node was a Bone or a 
	// TagPoint or the predicate was never true
	template <typename Pred>
	NODISCARD static Ogre::MovableObject* find_child_movable_object(
		const Ogre::SceneNode* rootNode, const Pred pred) NOEXCEPT_COND(
		std::is_nothrow_invocable<Func, Ogre::MovableObject*>::value) // C++17 only
	{
		if (rootNode)
		{
			// NOTE: should be getConstChildIterator(), however this non-member 
			// function cannot be declared const
			Ogre::Node::ConstChildNodeIterator nodeIt = rootNode->getChildIterator();
			while (nodeIt.hasMoreElements())
			{
				const Ogre::SceneNode* sceneNode = 
					dynamic_cast<Ogre::SceneNode*>(nodeIt.getNext());
				if (sceneNode)
				{
					Ogre::SceneNode::ConstObjectIterator objIt = 
						sceneNode->getAttachedObjectIterator();
					while (objIt.hasMoreElements())
					{
						const Ogre::MovableObject* movableObject = objIt.getNext();
						if (pred(movableObject))
							return const_cast<Ogre::MovableObject*>(movableObject);
					}

					if (sceneNode->numChildren() > 0) // if it has children
					{
						// recurse
						const Ogre::MovableObject* result = 
							find_child_movable_object(sceneNode, pred); 

						if (result)
							return const_cast<Ogre::MovableObject*>(result);
					}
				}
			}
		}

		return nullptr;
	}

	// recursively destroys everything under the given rootNode: child scene 
	// nodes and all movable objects attached to them nukeRoot indicates whether
	// to destroy the rootNode
	static void nuke_scene_node(Ogre::SceneManager* sceneManager,
		Ogre::SceneNode* rootNode, bool nukeRoot = true)
	{
		if (rootNode)
		{
			// destroy all movable object under the root node
			eOgre::for_each_child_scene_node(rootNode, 
				[&sceneManager](Ogre::SceneNode* sceneNode, unsigned int depth) {
					if (sceneNode->numAttachedObjects() > 0)
					{
						Ogre::SceneNode::ObjectIterator it = 
							sceneNode->getAttachedObjectIterator();
						while (it.hasMoreElements())
						{
							Ogre::MovableObject* movableObject = it.getNext();
							sceneManager->destroyMovableObject(movableObject);
						}
					}
				}
			);

			// destroy all the scene nodes under the scene root
			rootNode->removeAndDestroyAllChildren(); 

			// destroy the scene root
			if (nukeRoot)
				sceneManager->destroySceneNode(rootNode); 
		}
	}

	NODISCARD static Ogre::Vector3 getCenter(const Ogre::Vector3& min,
		const Ogre::Vector3& max) NOEXCEPT
	{
		return Ogre::Vector3(max + min) * 0.5f;
	}

	/*
		Using Ogre::Timer, since it somewhat fixes very common time jumps when 
		using the WINAPI functions and the (cpu cycles / frequency) formula
	*/
	class CountdownTimer
	{
	public:
		typedef std::chrono::milliseconds duration_type;

		CountdownTimer()
		  : mDuration(NULL),
			mTickCount(NULL),
			mEnded(true) {}

		template <typename DurationTy>
		CountdownTimer(DurationTy&& duration)
		  : mTickCount(NULL)
		{
			start(std::forward<DurationTy>(duration));
		}

		~CountdownTimer() = default;

		CountdownTimer(const CountdownTimer& rhs) = default;
		CountdownTimer& operator=(const CountdownTimer& rhs) = default;

		CountdownTimer(CountdownTimer&& rhs) NOEXCEPT
		  : mDuration(std::move(rhs.mDuration)),
			mTickCount(rhs.mTickCount),
			mEnded(rhs.mEnded)
		{
		}

		CountdownTimer& operator=(CountdownTimer&& rhs) NOEXCEPT
		{
			if (this != std::addressof(rhs))
			{
				// call the base class move assignment operator 
				// ...

				// free existing resources of the source
				// ...

				// copy data from the source object
				mDuration = std::move(rhs.mDuration);
				mTickCount = rhs.mTickCount;
				mEnded = rhs.mEnded;

				// release pointer ownership from the source object so that the 
				// destructor does not free the memory multiple times
				// ...
			}

			return *this;
		}

		void update(const Ogre::Real deltaSeconds) NOEXCEPT
		{
			if (!mEnded)
				mTickCount += static_cast<duration_type::rep>(deltaSeconds * 1000);
		}

		template <typename DurationTy = duration_type>
		typename std::enable_if<
			std::is_same<DurationTy, duration_type>::value
		>::type
		start(const DurationTy& duration) NOEXCEPT 
		{ 
			mDuration = duration;
			reset();
		}

		template <typename DurationTy = duration_type>
		typename std::enable_if<
			!std::is_same<DurationTy, duration_type>::value
		>::type
		start(const DurationTy& duration) 
		{ 
			static_assert(std::chrono::_Is_duration<DurationTy>::value,
				"duration must be an instance of std::chrono::duration");

			mDuration = std::chrono::duration_cast<duration_type>(duration);
			reset();
		}

		NODISCARD bool hasEnded() NOEXCEPT 
		{ 
			if (mEnded)
				return true;
			else
			{
				mEnded = (mTickCount > mDuration.count());
				return mEnded;
			}
		}

	private:
		inline void reset() NOEXCEPT
		{
			mTickCount = NULL;
			mEnded = false;
		}

		duration_type mDuration;
		duration_type::rep mTickCount;
		bool mEnded;
	};
}

// flushes any buffered input motion events, e.g. SDL_MOUSEMOTION,
// SDL_JOYAXISMOTION etc.
static void eSDL_FlushMotionEvents()
{
	SDL_PumpEvents();
    SDL_FlushEvent(SDL_MOUSEMOTION);
    SDL_FlushEvent(SDL_JOYAXISMOTION);
    //SDL_FlushEvent(SDL_JOYBALLMOTION); 
	//SDL_FlushEvent(SDL_CONTROLLERAXISMOTION);
	//SDL_FlushEvent(SDL_FINGERMOTION);

	// flush mouse relative motion
	//SDL_GetRelativeMouseState(nullptr, nullptr); 
}

template <typename IntTy>
struct BitSet
{
	BitSet() : mMask(NULL) {}
	BitSet(IntTy mask) : mMask(mask) {}
	~BitSet() {}

	BitSet(const BitSet& rhs) = default;
	BitSet& operator=(const BitSet& rhs) = default;

	NODISCARD std::string to_string() const
	{
		return estd::to_binary(mMask);
	}

	friend std::ostream& operator<<(std::ostream& os, const BitSet& mask) NOEXCEPT
	{
		os << mask.to_string();
		return os;
	}

	BitSet& clear(const IntTy flag) NOEXCEPT 
	{
		mMask &= ~flag; 
		return *this;
	}
	BitSet& set(const IntTy flag) NOEXCEPT 
	{ 
		mMask |= flag; 
		return *this; 
	}
	BitSet& flip(const IntTy flag) NOEXCEPT 
	{ 
		mMask ^= flag; 
		return *this; 
	}
	BitSet& clearAll() NOEXCEPT
	{ 
		mMask = NULL; 
		return *this; 
	}
	BitSet& setAll() NOEXCEPT 
	{ 
		mMask = std::numeric_limits<IntTy>::max();
		return *this; 
	}
	BitSet& setTo(const IntTy mask) NOEXCEPT
	{ 
		mMask = mask; 
		return *this; 
	}

	NODISCARD const bool isSet(const IntTy flag) const NOEXCEPT 
	{ 
		return ((mMask & flag) != NULL); 
	}
	NODISCARD const bool isEqual(const IntTy mask) const NOEXCEPT 
	{ 
		return (mMask == mask);
	}
	NODISCARD const bool empty() const NOEXCEPT 
	{ 
		return mMask == NULL;
	}
	NODISCARD const bool full() const NOEXCEPT 
	{ 
		return mMask == std::numeric_limits<IntTy>::max();
	}

	NODISCARD const IntTy getMask() const NOEXCEPT 
	{ 
		return mMask;
	}

private:
	IntTy mMask;
};

template <typename IntTy>
using BitMask = BitSet<IntTy>;

#endif