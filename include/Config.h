// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef CONFIG_H
#define CONFIG_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "StringInterface.h"

// A configuration using [string, value] pairs, where value can be a set of 
// predefined types

// Using private inheritance here to alter and hide some of the interface of the 
// StringInterface class, provide a listener and change the error messages of 
// the original class without the need for duplicate code.

class Config: private StringInterface
{
public:
	typedef StringInterface::DictionaryMap PropertyMap;
	typedef PropertyMap::value_type Property;

	class Listener
	{
	public:
		Listener() {}
		virtual ~Listener() {}

		virtual void propertyChanged(const Property& prop) = 0;
	};
	typedef std::set<Listener*> ListenerPtrList;

	void addListener(Listener* listener) { mListeners.insert(listener); }
	void removeListener(Listener* listener) NOEXCEPT { mListeners.erase(listener); }

	Config() = default;
	~Config() = default;

	// friend output operator, since it needs access to the private inheritance
	friend std::ostream& operator<<(std::ostream& os, const Config& config) NOEXCEPT
	{
		return operator<<(os, static_cast<const StringInterface&>(config));
	}

	template <typename ValTy>
	NODISCARD static Property make_property(const Ogre::String& name, 
		ValTy&& value, const ParameterType type)
	{
		return StringInterface::make_entry(name, 
			std::forward<ValTy>(value), type);
	}

	NODISCARD const Ogre::String& getProperty(const Ogre::String& name) const
	{
		try
		{
			return StringInterface::getParameter(name);
		} 
		catch (...) {
			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, 
				"Config property with name \"" + name + 
				"\" and type \"Ogre::String\" does not exist", 
				"Config::getProperty");
		}
	}

	template <typename ValTy>
	NODISCARD const ValTy getProperty(const Ogre::String& name) const
	{
		try
		{
			return StringInterface::getParameter<ValTy>(name);
		} 
		catch (...) {
			OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, 
				"Config property with name \"" + name + "\" does not exist", 
				"Config::getProperty");
		}
	}

	template <typename ValTy>
	void setProperty(const Ogre::String& name, 
		ValTy&& value, const ParameterType type)
	{
		auto result = StringInterface::setParameter(name, 
			std::forward<ValTy>(value), type);

		notifyPropertyChanged(*result.first);
	}

	void setProperty(const Config::Property& prop)
	{
		StringInterface::setParameter(prop);

		notifyPropertyChanged(prop);
	}

	void setProperty(Config::Property&& prop)
	{
		notifyPropertyChanged(prop);

		StringInterface::setParameter(std::move(prop));
	}

private:
	void notifyPropertyChanged(const Property& prop)
	{
		if (!mListeners.empty())
		{
			for (const auto& listener : mListeners)
				listener->propertyChanged(prop);
		}
	}

	ListenerPtrList mListeners;

protected:
};

#include "HeaderSuffix.h"

#endif