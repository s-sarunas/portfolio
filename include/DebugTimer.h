// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DEBUG_TIMER_H
#define DEBUG_TIMER_H

#include "HeaderPrefix.h"

// based on WinAPI functions
class DebugTimer
{
public:
	DebugTimer();
	~DebugTimer();

	typedef std::vector<unsigned long> TimerResults;

	// starting and immediately stopping the timer gives 2-4 microseconds  
	void start(const Ogre::String& name) NOEXCEPT;
	void end() NOEXCEPT;
	void clear() NOEXCEPT;

	void outputAverage();

	NODISCARD const Ogre::String& getName() const NOEXCEPT { return mName; }
	NODISCARD const Ogre::Timer& getTimer() const NOEXCEPT { return mTimer; }
	NODISCARD const TimerResults& getResults() const NOEXCEPT { return mResults; }

private:
	Ogre::String mName;
	Ogre::Timer mTimer; 
	TimerResults mResults;

protected:
};

static DebugTimer gsDebugTimer;

#ifdef _DEBUG
	#define DEBUG_TIMER_START(name) gsDebugTimer.start(name)
	#define DEBUG_TIMER_END gsDebugTimer.end()
#else
	// empty
	#define DEBUG_TIMER_START(name)
	#define DEBUG_TIMER_END
#endif

#include "HeaderSuffix.h"

#endif