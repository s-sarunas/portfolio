// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef BUTTERFLY_H
#define BUTTERFLY_H

#include "HeaderPrefix.h"

#include "rapidxml.hpp"

#include "GameMath.h"

// headers defining base classes
#include "StaticActor.h"
#include "Animated.h"
#include "Debuggable.h"

#define BUTTERFLY_SPEED_SCALAR 20.0f
#define BUTTERFLY_SPEED_ANIMATION_SCALAR 2.5f

#define BUTTERFLY_MESH_FILENAME "butterfly.mesh"
#define BUTTERFLY_PATH_FILENAME "first.path"
#define BUTTERFLY_ANIMATION_NAME "my_animation"

#define BUTTERFLY_CORNER_MESH_FILENAME "sphere_d.mesh"
#define BUTTERFLY_PATH_MESH_FILENAME "cube_d.mesh"

using namespace Math;

class Butterfly: public StaticActor, public Animated<1>, public Debuggable
{
public:
	Butterfly(const ActorClass& desc, const Ogre::String& pathFilename, 
		const Ogre::String& animationName);
	~Butterfly();

	bool _toggleShowcase();

	// Actor.h
	void update(const Ogre::Real deltaSeconds) override;
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{ 
		return ACTOR_BUTTERFLY_TYPE_NAME; 
	}

private:
	typedef boost::multi_array<Cubic<>, 2> EquationMatrix;

	void loadVertices(Ogre::String& content);
	void resetA(Matrix& _A);
	void populateB(Matrix& _b);
	// computes cubic hermite spline with natural end conditions
	void computeCubicSplines();
	void computeArcLength();
	void nextVertex() NOEXCEPT;

	Matrix mPathVertices;
	EquationMatrix mEquations;
	RealList mInverseArcLenghts;
	Matrix::size_type mCurrentVertexIndex;
	Matrix::size_type mVertexCount;
	Ogre::Vector3 mCurrentPosition;
	Ogre::Vector3 mNextPosition;
	Ogre::Real mT; // ranges from 0 to 1
	bool mShowcaseEnabled;

protected:
	// Debuggable.h
	void initDebuggable() override;
};

class NODISCARD ButterflyFactory: public ActorFactory
{
public:
	ButterflyFactory() {}
	~ButterflyFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{ 
		return ACTOR_BUTTERFLY_TYPE_NAME;
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return ACTOR_BUTTERFLY_TYPE_FLAG;
	}
};

#include "HeaderSuffix.h"

#endif