// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef ANIMATED_H
#define ANIMATED_H

#include "HeaderPrefix.h"

#include "AnimationStateWrapper.h"

// headers defining base classes
#include "Actor.h"
#include "Mixin.h"

// primary template
template <unsigned int numSections = 1>
class Animated;

// complete specialization for numSections = 1
template <>
class Animated <1>: virtual public Actor, public Mixin
{
public:
	void update(const Ogre::Real deltaSeconds);

	NODISCARD AnimationStateWrapper& getAnimation() NOEXCEPT 
	{ 
		return mAnimationState;
	}

private:
	AnimationStateWrapper mAnimationState;

protected:
	Animated(Ogre::Entity* entity, 
		const Ogre::String& animName = Ogre::StringUtil::BLANK, 
		bool loop = true, bool inverse = false);
	virtual ~Animated();

	Animated(const Animated& rhs) = delete;
	Animated(const Animated& rhs, Ogre::Entity* parentEntity) = delete;
	Animated& operator=(const Animated& rhs) = delete;
	Animated(Animated&& rhs) NOEXCEPT;
	Animated& operator=(Animated&& rhs) NOEXCEPT;

	// Mixin.h
	NODISCARD const MixinFlagType getMixinType() const NOEXCEPT 
	{
		return MIXIN_ANIMATED_FLAG; 
	}
};

// complete specialization for numSections = 2
template <>
class Animated <2>: virtual public Actor, public Mixin
{
public:
	void update(const Ogre::Real deltaSeconds);

	NODISCARD AnimationStateWrapper& getTopAnimation() NOEXCEPT 
	{ 
		return mTopAnimationState;
	}
	NODISCARD AnimationStateWrapper& getBotAnimation() NOEXCEPT
	{ 
		return mBotAnimationState; 
	}

private:
	AnimationStateWrapper mTopAnimationState;
	AnimationStateWrapper mBotAnimationState;

protected:
	Animated(Ogre::Entity* entity, 
		const Ogre::String& topAnimName = Ogre::StringUtil::BLANK, 
		const Ogre::String& botAnimName = Ogre::StringUtil::BLANK, 
		bool loop = true, bool inverse = false);
	virtual ~Animated();

	Animated(const Animated& rhs) = delete;
	Animated(const Animated& rhs, Ogre::Entity* parentEntity) = delete;
	Animated& operator=(const Animated& rhs) = delete;
	Animated(Animated&& rhs) NOEXCEPT;
	Animated& operator=(Animated&& rhs) NOEXCEPT;

	// Mixin.h
	NODISCARD const MixinFlagType getMixinType() const NOEXCEPT 
	{ 
		return MIXIN_ANIMATED_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif
