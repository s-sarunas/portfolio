// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef SETTINGSSTATE_H
#define SETTINGSSTATE_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "AppState.h"

class SettingsState: public AppState
{
public:
	SettingsState(AppState::Listener* listener);
	~SettingsState();

private:
protected:
	// SimpleMenu::Listener
	void itemSelected(const Dropdown* dropdown) override;
	void buttonHit(const Button* button) override;
};

#include "HeaderSuffix.h"

#endif
