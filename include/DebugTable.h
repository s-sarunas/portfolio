// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DEBUG_TABLE_H
#define DEBUG_TABLE_H

#include "HeaderPrefix.h"

//#define BOOST_MPL_LIMIT_VECTOR_SIZE 30 // max size by default is 20

#include "boost\variant.hpp"
#include "boost\mpl\vector.hpp"
#include "boost\mpl\contains.hpp"

// character widths may vary because OGRE supports proportional fonts
#define DEBUG_TABLE_CHARACTER_WIDTH 11 
#define DEBUG_TABLE_CHARACTER_HEIGHT 19
#define DEBUG_TABLE_GAP_VERTICAL_WIDTH 8
#define DEBUG_TABLE_GAP_FOOTER 13
#define DEBUG_TABLE_GAP_HORIZONTAL_WIDTH 7

#define DEBUG_TABLE_OVERLAY_NAME "DebugTable"
#define DEBUG_TABLE_OVERLAY_CONTAINER_NAME "DebugTable/Panel"
#define DEBUG_TABLE_CAPTION_TEMPLATE_NAME "DebugTable/Caption"
#define DEBUG_TABLE_VALUE_TEMPLATE_NAME "DebugTable/Value"

#define DEBUG_TABLE_ADD(caption, stringOrPointer) \
	InAppDebug::getSingleton().getTable()->addRow(caption, stringOrPointer);

#define DEBUG_TABLE_SET(caption, string) \
	InAppDebug::getSingleton().getTable()->setValue(caption, string);

class DebugTable
{
	// Ogre::String* and all overloads of Ogre::StringConverter::toString
	// NOTE: no const char* as it might lead to memory leaks (raw strings are 
	// wrapped into std::string if needed)
	// NOTE: a void* could've been used instead, however the interface would 
	// not have been type-safe
	typedef boost::mpl::vector<Ogre::String, Ogre::StringVector, 
		Ogre::ColourValue, Ogre::Quaternion, Ogre::Matrix4, Ogre::Matrix3, 
		Ogre::Vector4, Ogre::Vector3, Ogre::Vector2, bool, long, unsigned long, 
		size_t, int, Ogre::Degree, Ogre::Radian, double, Ogre::Real> 
		convertible_types;

	// add const to each of the types
	typedef	boost::mpl::transform<
		convertible_types, boost::add_const<boost::mpl::_1>
	>::type convertible_types_const;

	// add pointers to each of the types
	typedef	boost::mpl::transform<
		convertible_types_const, boost::add_pointer<boost::mpl::_1>
	>::type convertible_types_const_ptrs;

	// add std::function to the end of the types
	typedef boost::mpl::push_back<
		convertible_types_const_ptrs, std::function<Ogre::String()>
	>::type convertible_types_const_ptrs_function;

	// add owning Ogre::String to the beginning of the types 
	// (becomes the default type)
	typedef boost::mpl::push_front<
		convertible_types_const_ptrs_function, Ogre::String
	>::type supported_types;

public:
	DebugTable(const Ogre::RenderWindow* window);
	~DebugTable();

	void update();
	void clearNonPersistentRows();

	void show() { mOverlay->show(); }
	void hide() NOEXCEPT { mOverlay->hide(); }
	bool toggle();

	// std::variant is available in C++17
	typedef boost::make_variant_over<supported_types>::type VariantType;

	// raw strings are not supported directly, wraps them in a string for RAII
	void addRow(const Ogre::String& caption, char* value); 
	void addRow(const Ogre::String& caption, const char* value); 
	/*
		Sets a table value to a pointer to member or a 
			function pointer of the type: Ogre::String().
		Lambdas, functors, bind expressions will all work. 
		To bind a member function use: 
			std::bind(&Class::memberFunction, instancePtr).
		Use std::ref to pass reference arguments to the std::bind
		The pointer values are updated right after the scene is updated.
	*/
	void addRow(const Ogre::String& caption, const VariantType& value = msDefaultValue);

	// raw strings are wrapped into Ogre::String for resource management
	void setValue(const Ogre::String& caption, char* value);
	void setValue(const Ogre::String& caption, const char* value);
	// setValue() should only be used with Ogre::String and does not 
	// update automatically if/when the string changes
	void setValue(const Ogre::String& caption, const Ogre::String& value);

	static Ogre::String msDefaultValue;

private:
	typedef std::tuple<
		Ogre::OverlayElement*, 
		Ogre::OverlayElement*, 
		VariantType> TableRow;

	typedef std::vector<TableRow> TableType;

	template <typename Ty>
	NODISCARD bool variantTypeEquals(const TableRow& tuple) NOEXCEPT;

	//void updateTableWidth();
	void updateCaptionWidth(const Ogre::String::size_type newLength);
	Ogre::OverlayElement* const createOverlayElement(
		const Ogre::String& templateName, const Ogre::String& overlayName, 
		const Ogre::String& caption, Ogre::Real left, Ogre::Real top);

	TableType mRows;
	Ogre::OverlayManager* mOverlayManager;
	Ogre::Overlay* mOverlay;
	Ogre::OverlayContainer* mContainer;
	size_t mCaptionWidth;
	//size_t mTableWidth;
	size_t mNumPersistentRows;

protected:
};

#include "HeaderSuffix.h"

#endif