// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef MENUSTATE_H
#define MENUSTATE_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "AppState.h"

class MenuState: public AppState
{
public:
	MenuState(AppState::Listener* listener);
	~MenuState();

private:
protected:
	// SimpleMenu::Listener
	void buttonHit(const Button* button) override;
};

#include "HeaderSuffix.h"

#endif