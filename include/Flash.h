// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef FLASH_H
#define FLASH_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "StaticActor.h"

#define FLASH_LIGHT_COLOUR Ogre::ColourValue::Blue
#define FLASH_LIGHT_POWER 1.0f
#define FLASH_LIGHT_RANGE 5.0f
#define FLASH_LIGHT_ROLL_SPEED_MIN 80.0f
#define FLASH_LIGHT_ROLL_SPEED_MAX 180.0f

#define FLASH_MESH_FILENAME "weapon_flash.mesh"

class Flash: public StaticActor
{
public:
	Flash(const ActorClass& desc);
	~Flash();

	Flash(const Flash& rhs) = delete;
	Flash& operator=(const Flash& rhs) = delete;
	Flash(Flash&& rhs);
	Flash& operator=(Flash&& rhs);

	// Actor.h
	void show() override;
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{
		return ACTOR_FLASH_TYPE_NAME;
	}

private:
	Ogre::Light* mLight;

protected:
	NODISCARD Ogre::Light* getLight() const NOEXCEPT { return mLight; }
};

class NODISCARD FlashFactory: public ActorFactory
{
public:
	FlashFactory() {}
	~FlashFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{ 
		return ACTOR_FLASH_TYPE_NAME; 
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return NULL;
	}
};

#include "HeaderSuffix.h"

#endif