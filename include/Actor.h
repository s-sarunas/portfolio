// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef ACTOR_H
#define ACTOR_H

#include "HeaderPrefix.h"

#include "StringInterface.h"

// a workaround for unique name restriction in Ogre
/*constexpr*/ static const Ogre::String gsCopySuffix = "_copy";
/*constexpr*/ static uintmax_t gsCopyCounter = 0; // global copy counter
NODISCARD /*constexpr*/ static const Ogre::String getCurrentCopyName(Ogre::String rhsName)
{
	if (!rhsName.empty())
	{
		Ogre::String::size_type pos = rhsName.find(gsCopySuffix);
		if (pos != rhsName.npos)
		{ // already has a suffix
			rhsName.erase(pos + gsCopySuffix.length(), rhsName.length());
		}
		else
		{ // does not have a suffix
			rhsName += gsCopySuffix;
		}

		rhsName += std::to_string(gsCopyCounter);
	}

	return rhsName;
}

NODISCARD /*constexpr*/ static const Ogre::String getNextCopyName(const Ogre::String& rhsName)
{
	Ogre::String copyName = getCurrentCopyName(rhsName);
	gsCopyCounter++;

	return copyName;
}

struct use_existing_node_t {};
struct create_child_node_t {};

struct discard_parameters_t {};
struct replace_parameters_t {};

// to disambiguate constructors and provide clarity to the overload resolution
// static since only one instance is required
static const use_existing_node_t use_existing_node;
static const create_child_node_t create_child_node;

static const discard_parameters_t discard_parameters;
static const replace_parameters_t replace_parameters;

// definitions similar to macros, but remain in memory so the strings do not 
// need to be constructed every time they are used (passed by reference), 
// also less error-prone than using hand-written raw strings
static const Ogre::String ACTOR_DEBUG_TYPE_NAME = "debug";
static const Ogre::String ACTOR_AI_TYPE_NAME = "ai";
static const Ogre::String ACTOR_BALL_TYPE_NAME = "ball";
static const Ogre::String ACTOR_PLAYER_TYPE_NAME = "player";
static const Ogre::String ACTOR_BORDER_TYPE_NAME = "border";
static const Ogre::String ACTOR_BUTTERFLY_TYPE_NAME = "butterfly";
static const Ogre::String ACTOR_REGION_TYPE_NAME = "region";
static const Ogre::String ACTOR_BULLET_TYPE_NAME = "bullet";
static const Ogre::String ACTOR_CHARACTER_TYPE_NAME = "character";
static const Ogre::String ACTOR_FLASH_TYPE_NAME = "flash";
static const Ogre::String ACTOR_TERRAIN_TYPE_NAME = "terrain";
static const Ogre::String ACTOR_SCOREBOARD_TYPE_NAME = "scoreboard";
static const Ogre::String ACTOR_WEAPON_TYPE_NAME = "weapon";

static const size_t ACTOR_NUM_TYPES = 13;

static const Ogre::String ACTOR_STATIC_GROUP_NAME = "static";
static const Ogre::String ACTOR_DYNAMIC_GROUP_NAME = "dynamic";

typedef uint16_t ActorFlagType;

// (non-class) enum leaks into the global namespace, but const-qualified 
// variables at namespace scope have internal linkage, while enumerators have 
// external linkage so an enum has to be used or several instances of the 
// variables will be generated.
// http://en.cppreference.com/w/cpp/language/storage_duration#Linkage
enum NODISCARD ActorTypeFlags : ActorFlagType
{
	ACTOR_STATIC_TYPE_FLAG =		1 << 0,
	ACTOR_DYNAMIC_TYPE_FLAG =		1 << 1,

	ACTOR_AI_TYPE_FLAG =			1 << 2,
	ACTOR_BALL_TYPE_FLAG =			1 << 3,
	ACTOR_PLAYER_TYPE_FLAG =		1 << 4,
	ACTOR_BORDER_TYPE_FLAG =		1 << 5,
	ACTOR_BUTTERFLY_TYPE_FLAG =		1 << 6,
	ACTOR_REGION_TYPE_FLAG =		1 << 7,
	ACTOR_BULLET_TYPE_FLAG =		1 << 8,
	ACTOR_CHARACTER_TYPE_FLAG =		1 << 9,
	ACTOR_LAST_TYPE_FLAG =			ACTOR_CHARACTER_TYPE_FLAG
};

typedef uint16_t MixinFlagType;

enum NODISCARD MixinFlags : MixinFlagType
{
	MIXIN_ANIMATED_FLAG =			1 << 0,
	MIXIN_COLLIDABLE_FLAG =			1 << 1,
	MIXIN_DEBUGGABLE_FLAG =			1 << 2,
	MIXIN_GROUNDED_FLAG =			1 << 3
};

class ActorCreator
{
public:
	virtual Actor* createActor(const ActorClass& desc) = 0;
	NODISCARD virtual std::shared_ptr<Actor> createUnmanagedActor(const ActorClass& desc) = 0;
	virtual void reserveForActors(const size_t numActors) = 0;
};

class MovableObjectListenerImpl: public Ogre::MovableObject::Listener
{
public:
	void _addLightTypes(Ogre::MovableObject* obj);
	void _addLightTypes(Ogre::Renderable* renderable);

	// Ogre::MovableObject::Listener
	void objectDestroyed(Ogre::MovableObject*) override {}
	void objectAttached(Ogre::MovableObject*) override {}
	void objectDetached(Ogre::MovableObject*) override {}
	void objectMoved(Ogre::MovableObject*) override {}
	bool objectRendering(const Ogre::MovableObject*, const Ogre::Camera*) override 
	{
		return true; 
	}
	const Ogre::LightList* objectQueryLights(const Ogre::MovableObject* obj) override;

private:
	// fills in an array of light types, directional lights first with -1 
	// meaning no light
	template <typename Ty, size_t N>
	void fillLightArray(const Ogre::LightList& lightList, 
		std::array<Ty, N>& lightTypeArray)
	{
		lightTypeArray.fill(-1);

		Ogre::LightList::size_type index = 0;
		for (auto it = lightList.begin(); 
			it != lightList.end() && index < N;
			++index, ++it)
		{
			lightTypeArray[index] = static_cast<float>((*it)->getType());
		}
	}
};

/*
	This struct is used as a fill-in class and is passed to Actor's constructor,
	this allows actor's parameters to be set in any order (especially the 
	optional parameters), avoids changes to Actor's constructor and the ActorClass
	object can be passed to different interfaces for processing

	Same is done with DynamicClass and DynamicActor
*/
struct NODISCARD ActorClass
{
	friend Actor;

	/*
		The general syntax is: 
			actor's name, 
			actor's type,
			argument to use/create an entity,
			arguments to use/create a node:
				(scene node) child node or a parent node and a use_existing_node
					or a create_child_node tag
				(tag point) parent entity and bone name
			(optional) offsets
	*/

	// default constructor, use setters to fill in the class
	ActorClass();
	~ActorClass() = default;

	// constructor to create entity, use existing node
	DEPRECATED ActorClass(const Ogre::String& name, const Ogre::String& type, 
		const Ogre::String& meshFilename, Ogre::Node* node, use_existing_node_t);

	// constructor to create entity and create a child node
	DEPRECATED ActorClass(const Ogre::String& name, const Ogre::String& type, 
		const Ogre::String& meshFilename, Ogre::Node* parentNode, create_child_node_t,
		const Ogre::Vector3& translate = Ogre::Vector3::ZERO, 
		const Ogre::Quaternion& rotate = Ogre::Quaternion::IDENTITY,
		const Ogre::Vector3& scale = Ogre::Vector3::UNIT_SCALE);

	// constructor to create entity and create a tag point on a bone
	DEPRECATED ActorClass(const Ogre::String& name, const Ogre::String& type, 
		const Ogre::String& meshFilename, const Ogre::String& boneName,
		Ogre::Entity* parentEntity, const Ogre::Vector3& translate = Ogre::Vector3::ZERO, 
		const Ogre::Quaternion& rotate = Ogre::Quaternion::IDENTITY, 
		const Ogre::Vector3& scale = Ogre::Vector3::UNIT_SCALE);

	// constructor to use existing entity and existing node
	DEPRECATED ActorClass(const Ogre::String& name, const Ogre::String& type, 
		Ogre::Entity* entity, Ogre::Node* node, use_existing_node_t);

	// constructor to use existing entity, create a child node 
	DEPRECATED ActorClass(const Ogre::String& name, const Ogre::String& type, 
		Ogre::Entity* entity, Ogre::Node* parentNode, create_child_node_t,
		const Ogre::Vector3& translate = Ogre::Vector3::ZERO, 
		const Ogre::Quaternion& rotate = Ogre::Quaternion::IDENTITY,
		const Ogre::Vector3& scale = Ogre::Vector3::UNIT_SCALE);

	// constructor to use existing entity attached to an existing scene node 
	DEPRECATED ActorClass(const Ogre::String& name, const Ogre::String& type, 
		Ogre::SceneNode* sceneNode);

	// constructor to use existing entity and create a tag point on a bone
	DEPRECATED ActorClass(const Ogre::String& name, const Ogre::String& type, 
		Ogre::Entity* entity, const Ogre::String& boneName,
		Ogre::Entity* parentEntity, const Ogre::Vector3& translate = Ogre::Vector3::ZERO, 
		const Ogre::Quaternion& rotate = Ogre::Quaternion::IDENTITY, 
		const Ogre::Vector3& scale = Ogre::Vector3::UNIT_SCALE);
	
	ActorClass(const ActorClass& rhs) = delete;
	ActorClass& operator=(const ActorClass& rhs) = delete;
	ActorClass(ActorClass&& rhs) NOEXCEPT;
	ActorClass& operator=(ActorClass&& rhs) NOEXCEPT;

	// returns whether an actor can be created, otherwise the actor 
	// is empty (e.g. a region) 
	NODISCARD operator bool() const NOEXCEPT
	{
		// entity and node can be created
		return ((mMeshFilename != Ogre::StringUtil::BLANK || mEntity) && // entity
			mNode || (mBoneName != Ogre::StringUtil::BLANK && mParentEntity)); // node
	}

	void setName(const Ogre::String& name) NOEXCEPT { mName = name; }
	void setEntity(const Ogre::String& filename) 
	{ 
		assert(!mEntity && 
			"entity already provided"); 

		mMeshFilename = filename; 
	}

	void setEntity(Ogre::Entity* entity) 
	{ 
		assert(!mEntity && 
			"entity already provided"); 

		mEntity = entity; 
	}

	void setNode(Ogre::Node* node, create_child_node_t) 
	{ 
		setNode(node);
		mIsNodeParent = true; 
	}

	void setNode(Ogre::Node* node, use_existing_node_t) 
	{ 
		setNode(node);
		mIsNodeParent = false; 
	}

	// use the scene node given and extract the attached entity 
	// (first in the list if more than one)
	void setSceneNode(Ogre::SceneNode* sceneNode)
	{
		initFromSceneNode(sceneNode);
	}

	void setTagPoint(Ogre::Entity* parentEntity, const Ogre::String& boneName) 
	{ 
		assert((mIsNodeParent || !mNode) && // node is a parent (not owned) or node not set
			   !mParentEntity &&
			   "an usable node or parent entity already provided");

		mParentEntity = parentEntity;
		mBoneName = boneName; 
		mIsTagPoint = true; 
	}

	// adds an additional param to be passed to the constructor
	template <typename ValTy>
	void addParam(const Ogre::String& name, ValTy&& value, const ParameterType type)
	{ 
		if (!mAdditionalParams)
			mAdditionalParams = std::make_unique<StringInterface>();

		try {
			mAdditionalParams->addParameter(name, std::forward<ValTy>(value), type); 
		} 
		catch (const Ogre::Exception&) {
			OGRE_EXCEPT(Ogre::Exception::ERR_DUPLICATE_ITEM, "Actor \"" + mName + 
				"\" already has an additional parameter with name \"" + name + 
				"\".", "ActorClass::addParam");
		}
	}

	NODISCARD const Ogre::String& getParam(const Ogre::String& name) const 
	{ 
		if (mAdditionalParams)
		{
			try {
				return mAdditionalParams->getParameter(name); 
			} 
			catch (const Ogre::Exception&) {
				OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, "Actor \"" + mName + 
					"\" does not have an additional parameter with name \"" + name + 
					"\".", "ActorClass::getParam");
			}
		}

		OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, "Actor \"" + mName + 
			"\" does not have any additional parameters (requested \"" + name + 
			"\").", "ActorClass::getParam");
	}

	template <typename ValTy>
	NODISCARD const ValTy getParam(const Ogre::String& name) const 
	{ 
		if (mAdditionalParams)
		{
			try {
				return mAdditionalParams->getParameter<ValTy>(name); 
			} 
			catch (const Ogre::Exception&) {
				OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, "Actor \"" + mName + 
					"\" does not have an additional parameter with name \"" + name + 
					"\".", "ActorClass::getParam");
			}
		}

		OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, "Actor \"" + mName + 
			"\" does not have any additional parameters (requested \"" + name + 
			"\").", "ActorClass::getParam");
	}

	NODISCARD const bool hasParams() const NOEXCEPT
	{
		return mAdditionalParams.operator bool();
	}

	// relative to the parent
	void setTransforms(const Ogre::Vector3& offsetPosition, 
		const Ogre::Quaternion& offsetOrientation = Ogre::Quaternion::IDENTITY, 
		const Ogre::Vector3& offsetScale = Ogre::Vector3::UNIT_SCALE) NOEXCEPT
	{ 
		mNodeTranslate = offsetPosition; 
		mNodeRotate = offsetOrientation;
		mNodeScale = offsetScale;
	}
	// relative to the parent
	void setTranslate(const Ogre::Vector3& vector) NOEXCEPT
	{ 
		mNodeTranslate = vector; 
	}
	// relative to the parent
	void setRotate(const Ogre::Quaternion& quat) NOEXCEPT
	{
		mNodeRotate = quat; 
	}
	// relative to the parent
	void setScale(const Ogre::Vector3& scale) NOEXCEPT 
	{ 
		mNodeScale = scale;
	}
	void addChild(ActorClass&& desc) 
	{
		mChildren.push_back(std::move(desc));
	}
	// replaces the entire parameter list, discarding any existing parameters
	void setAdditionalParams(StringInterface& params, discard_parameters_t);
	// replaces each parameter one-by-one, overwriting if needed, leaving the 
	// argument list empty
	void setAdditionalParams(StringInterface&& params, replace_parameters_t);
	// replaces each parameter one-by-one, overwriting if needed, leaving the 
	// argument list intact
	void setAdditionalParams(const StringInterface& params, replace_parameters_t);
	void setType(const Ogre::String& type) NOEXCEPT
	{ 
		mType = type;
	}
	void setVisible(const bool visible) NOEXCEPT 
	{ 
		mIsVisible = visible;
	}
	void _reserve(const size_t numChildren) 
	{ 
		mChildren.reserve(numChildren); 
	}
	void _changeType(const Ogre::String& type) const NOEXCEPT;
	void _setTypeFlag(const ActorFlagType flag) const NOEXCEPT 
	{ 
		mTypeFlag = flag;
	}
	void _setCreator(ActorCreator* creator) const NOEXCEPT 
	{ 
		mCreator = creator;
	}

	NODISCARD const Ogre::String& getName() const NOEXCEPT { return mName; }
	NODISCARD const Ogre::String& getType() const NOEXCEPT { return mType; }
	NODISCARD const Ogre::String& getMeshFilename() const NOEXCEPT { return mMeshFilename; }
	NODISCARD const Ogre::Entity* getEntity() const NOEXCEPT { return mEntity; }
	NODISCARD const Ogre::Node* getNode() const NOEXCEPT { return mNode; }
	NODISCARD const Ogre::String& getBoneName() const NOEXCEPT { return mBoneName; }
	NODISCARD const Ogre::Entity* getParentEntity() const NOEXCEPT { return mParentEntity; }
	NODISCARD const bool getVisible() const NOEXCEPT { return mIsVisible; }
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT { return mTypeFlag; }

private:
	void initFromSceneNode(Ogre::SceneNode* sceneNode);
	inline void setNode(Ogre::Node* node)
	{
		assert((mIsNodeParent || !mNode) && // node is a parent (not owned) or node not set
			   !mParentEntity &&
			   "an usable node or parent entity already provided");

		mNode = node; 
		mIsTagPoint = false; 
	}

	std::unique_ptr<StringInterface> mAdditionalParams;
	std::vector<ActorClass> mChildren;
	Ogre::String mName;
	Ogre::String mMeshFilename;
	Ogre::Entity* mEntity;
	Ogre::Node* mNode;
	Ogre::String mBoneName;
	Ogre::Entity* mParentEntity;
	Ogre::Vector3 mNodeTranslate;
	Ogre::Quaternion mNodeRotate;
	Ogre::Vector3 mNodeScale;
	bool mIsNodeParent;
	bool mIsTagPoint;
	bool mIsVisible;

	mutable Ogre::String mType;
	mutable ActorFlagType mTypeFlag;
	mutable ActorCreator* mCreator;
};

// is the type an actor mixin or an actor type
template <typename Ty>
struct is_actor_type : 
	estd::bool_constant<std::is_class<Ty>::value && 
	std::is_base_of<Actor, Ty>::value> {};

// has the minimum requirements to appear on the screen: an entity attached to 
// a scene node or a tag point also an empty actor is possible (e.g. a region) 
// to make use of the actor's interface and mixins (e.g. for collision detection)
class Actor: public ActorCreator
{
public:
	friend std::ostream& operator<<(std::ostream& os, const Actor& actor) NOEXCEPT;
	friend void swap(Actor& lhs, Actor& rhs) NOEXCEPT;

	// virtual
	virtual void update(const Ogre::Real deltaSeconds);
	virtual void reset();
	virtual void show();
	virtual void hide(); 

	// the AABB for an entity is recalculated at each call
	NODISCARD virtual const Ogre::AxisAlignedBox& getBoundingBox() 
	{ 
		return mEntity->getWorldBoundingBox(true); 
	}

	// pure virtual
	// what type of Actor is it: Player, AI, Ball, Border etc?
	NODISCARD virtual const Ogre::String& getActorType() const NOEXCEPT = 0;
	// what group does the Actor belong to: static or dynamic?
	NODISCARD virtual const Ogre::String& getActorGroup() const NOEXCEPT = 0;

	Actor* createChild(const ActorClass& desc);
	void addChild(std::shared_ptr<Actor> actor);
	void removeChild(Actor* actor);
	void clearChildren() NOEXCEPT;
	NODISCARD bool hasChildren() NOEXCEPT;

	NODISCARD const ActorPtrList& getChildren() const NOEXCEPT { return mChildren; }
	NODISCARD const Ogre::String& getName() const NOEXCEPT { return mName; }
	NODISCARD Ogre::Node* getNode() const NOEXCEPT { return mNode; }
	NODISCARD Ogre::SceneNode* getSceneNode() const;
	NODISCARD Ogre::TagPoint* getTagPoint() const;
	NODISCARD Ogre::Entity* getEntity() const NOEXCEPT { return mEntity; }
	NODISCARD ActorCreator* getCreator() const NOEXCEPT { return mCreator; }
	NODISCARD Actor* getParent() const NOEXCEPT { return mParent; }
	NODISCARD const bool getVisible() const NOEXCEPT { return mIsVisible; }
	NODISCARD const bool getDestroyFlag() const NOEXCEPT { return mDestroy; }
	NODISCARD const Ogre::Vector3& getDefaultPosition() const NOEXCEPT 
	{ 
		return mNode->getInitialPosition();
	}
	NODISCARD const Ogre::Quaternion& getDefaultOrientation() const NOEXCEPT 
	{ 
		return mNode->getInitialOrientation(); 
	}
	NODISCARD const Ogre::Vector3& getDefaultScale() const NOEXCEPT 
	{ 
		return mNode->getInitialScale();
	}
	NODISCARD static MovableObjectListenerImpl _getMovableObjectListener() NOEXCEPT 
	{ 
		return mMovableObjectListenerImpl;
	}

	void setDestroyFlag(bool flag) NOEXCEPT { mDestroy = flag; }
	void setVisible(bool visible) { (visible) ? show() : hide(); }

	// applies a function for this actor and all its children
	template <typename Func>
	void for_all(Func function, unsigned int depth = 0) NOEXCEPT_COND(
		std::is_nothrow_invocable<Func, Actor* const>::value) // C++17 only
	{
		function(this, depth);

		if (this->hasChildren())
		{
			for (const auto& child : mChildren)
			{
				++depth;
				child->for_all(function, depth);
				--depth;
			}
		}
	}

	// finds an actor with the specified name between this actor and its children
	NODISCARD Actor* find(const Ogre::String& name);

	// finds an actor between this actor and its children 
	template <typename Pred>
	NODISCARD Actor* find_if(const Pred pred) NOEXCEPT_COND(
		std::is_nothrow_invocable<Pred, Actor* const>::value) // C++17 only
	{
		if (pred(this))
			return this;

		if (this->hasChildren())
		{
			for (const auto& child : mChildren)
			{
				Actor* result = child->find_if(pred);
				if (result)
					return result; 
			}
		}

		return nullptr;
	}


protected:
	Actor() = default;
	Actor(const ActorClass& desc);
	virtual ~Actor();

	Actor(const Actor& rhs, Ogre::Node* parentNode = nullptr);
	Actor& operator=(const Actor& rhs);
	Actor(Actor&& rhs) NOEXCEPT;
	Actor& operator=(Actor&& rhs);

	// works with tag points only:
	// attaches another movable object to the bone, does not replace mNode; 
	// see reattachToBone
	void attachToBone(Ogre::MovableObject* object, 
		const Ogre::Vector3 translate = Ogre::Vector3::ZERO, 
		const Ogre::Quaternion rotate = Ogre::Quaternion::IDENTITY);

	// works with tag points only:
	// reattaches the actor's entity to a different bone, either of the same 
	// entity or the one provided with as the second argument and replaces 
	// mNode to point to the new tag point; see attachToBone 
	void reattachToBone(const Ogre::String& boneName, 
		Ogre::Entity* entityWithBone = nullptr);

	// changes the entity, destroying the previous
	void changeEntity(const Ogre::String& meshFilename);

	NODISCARD Actor* getChild(const Ogre::String& nameOrType);

	NODISCARD BitSet<MixinFlagType>& getUpdateFlags() NOEXCEPT 
	{
		return mUpdateFlags;
	}
	NODISCARD const BitSet<MixinFlagType>& getUpdateFlags() const NOEXCEPT 
	{
		return mUpdateFlags;
	}

private:
	void initSceneNode(const ActorClass& desc);
	void setParent(Actor* actor) NOEXCEPT { mParent = actor; }

	// ActorCreator.h
	Actor* createActor(const ActorClass& desc) override;
	NODISCARD std::shared_ptr<Actor> createUnmanagedActor(const ActorClass& desc) override;
	void reserveForActors(const size_t numActors) override;

	ActorPtrList mChildren;
	Ogre::String mName;
	Ogre::Node* mNode;
	Ogre::Entity* mEntity; 
	BitSet<MixinFlagType> mUpdateFlags;
	ActorCreator* mCreator;
	Actor* mParent;
	bool mIsTagPoint;
	bool mIsVisible;
	bool mDestroy;

	static MovableObjectListenerImpl mMovableObjectListenerImpl;
};

/*	
	Non-owning factory interface definition: defers instantiation to subclasses 
	and allows Scene class to be extended to allow all clients to produce new 
	instances of this object, integrated with the standard processing

	Allows subclasses to decide on object types during run-time

	Maintains the open/close principle: software entities should be open for 
	extension, but closed for modification
*/
class NODISCARD ActorFactory
{
public:
	virtual ~ActorFactory();

	NODISCARD virtual std::shared_ptr<Actor> createInstance(const ActorClass& desc) = 0;

	NODISCARD virtual const Ogre::String& getType() const NOEXCEPT = 0;
	NODISCARD virtual const ActorFlagType getTypeFlag() const NOEXCEPT = 0;

protected:
	ActorFactory();
};

#include "HeaderSuffix.h"

#endif