// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef COLLISIONMANAGER_H
#define COLLISIONMANAGER_H

#include "HeaderPrefix.h"

class CollisionManager
{
public:
	class Listener
	{
	public:
		Listener() {}
		virtual ~Listener() {}

		virtual void collisionOccured(Collidable* one, Collidable* two) = 0; 
	};
	typedef std::set<Listener*> ListenerPtrList;

	void addListener(Listener* listener) { mListeners.insert(listener); }
	void removeListener(Listener* listener) NOEXCEPT { mListeners.erase(listener); }

	CollisionManager();
	~CollisionManager();

	/* 
	boost::variant did not work here, no way to return the active member, since 
		its type is not known can call intersects() when using a 
		boost::static_visitor, but cannot pass an argument to it (again, no way 
		to return from other Actor)
	Templating class Actor to pass Ogre::AxisAlignedBox or Ogre::Sphere, would 
		separate instances of Actor based on its bounding shape and would pollute
		the code, since bounding shape type does not define the type of Actor
	Ogre::AxisAlignedBox and Ogre::Sphere should derive from a pure virtual base
		class, since they have the same methods. If I were to modify the engine,
		all this would be avoided
	*/
	void handleCollisions();
	void handleOneCollision(Collidable* one);

	void registerCollidable(Collidable* collidable);
    void removeCollidable(Collidable* collidable);

	void setDebugOutput(bool flag) 
	{
		mDebugOutput = flag;
	}

	NODISCARD const CollidablePtrList& getCollidables() const 
	{ 
		return mCollidables; 
	}

	NODISCARD static CollisionManager* getSingletonPtr() NOEXCEPT;
	NODISCARD static CollisionManager& getSingleton();

private:
	typedef std::vector<std::pair<void*, bool>> PendingCollidableList;

	void registerCollisions(Collidable* one, Collidable* two);
	void processCollision(Collidable* one, Collidable* two);
	void updatePendingCollidables();

	void notifyCollisionOccured(Collidable* one, Collidable* two);

	PendingCollidableList mPendingCollidables;
	CollidablePtrList mCollidables;
	ListenerPtrList mListeners;
	bool mDebugOutput;

	static CollisionManager* msSingleton;

protected:
};

#include "HeaderSuffix.h"

#endif