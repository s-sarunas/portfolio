// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef MAPSELECTSTATE_H
#define MAPSELECTSTATE_H

#include "HeaderPrefix.h"

#include "StringInterface.h"

// headers defining base classes
#include "AppState.h"

class MapSelectState: public AppState
{
public:
	MapSelectState(AppState::Listener* listener);
	~MapSelectState();

private:
	StringInterface mCallbackParams;

protected:
	// SimpleMenu::Listener
	void itemSelected(const Dropdown* dropdown);
	void buttonHit(const Button* button) override;
};

#include "HeaderSuffix.h"

#endif