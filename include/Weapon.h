// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef WEAPON_H
#define WEAPON_H

#include "HeaderPrefix.h"

#include "Flash.h"

#define WEAPON_BONE_MUZZLE_NAME "muzzle"

// headers defining base classes
#include "StaticActor.h"

class Weapon: public StaticActor
{
public:
	Weapon(const ActorClass& desc, const Ogre::String& handleBoneName);
	~Weapon();

	Weapon(const Weapon& rhs) = delete;
	Weapon& operator=(const Weapon& rhs) = delete;
	Weapon(Weapon&& rhs);
	Weapon& operator=(Weapon&& rhs);

	// Actor.h
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override
	{
		return ACTOR_WEAPON_TYPE_NAME;
	}

	void attachToHandle();
	void attachToSheath();

	void getMuzzleTransforms(Ogre::Vector3& pos, Ogre::Quaternion& orient, 
		Ogre::Vector3& scale);
	NODISCARD Flash* getFlash() const NOEXCEPT { return mFlash; }

private:
	Ogre::Entity* mParentEntity;
	Ogre::SceneNode* mParentSceneNode;
	Ogre::Bone* mSheathBone;
	Ogre::Bone* mHandleBone;

	// raw pointer access to child actors
	Flash* mFlash;

protected:
};

class NODISCARD WeaponFactory: public ActorFactory
{
public:
	WeaponFactory() {}
	~WeaponFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{ 
		return ACTOR_WEAPON_TYPE_NAME;
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{
		return NULL; 
	}
};

#include "HeaderSuffix.h"

#endif
