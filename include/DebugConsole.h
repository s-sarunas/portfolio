// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DEBUG_CONSOLE_H
#define DEBUG_CONSOLE_H

#include "HeaderPrefix.h"

#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <fstream>

// headers defining base classes
#include "InputHandler.h"

#define DEBUG_CONSOLE_HISTORY_LIMIT 8

#define DEBUG_CONSOLE_TO_STRING_ENABLE(boolean) (boolean ? "enabled." : "disabled."); 
#define DEBUG_CONSOLE_TO_STRING_VISIBLE(boolean) (boolean ? "visible." : "hidden."); 
#define DEBUG_CONSOLE_NEW_COMMAND_STRING "> "
#define DEBUG_CONSOLE_CURSOR_STRING "_"
#define DEBUG_CONSOLE_HELP_STRING ", /? or ? for more help."

using std::cout;
using std::endl;

#define DEBUG_WRITE(stream) DebugConsole::getSingleton() << stream
#define DEBUG_WRITELN(stream) DebugConsole::getSingleton() << stream << '\n'
#define DEBUG_EXECUTE(cmd) DebugConsole::getSingleton().executeCommand(cmd)

class DebugConsole: public InputHandler
{
	typedef std::queue<Ogre::String> MessageQueue;
public:
    DebugConsole();
    ~DebugConsole();

	DebugConsole& operator<<(char c) NOEXCEPT; // wraps in a string
	DebugConsole& operator<<(const char* str) NOEXCEPT; // wraps in a string
	DebugConsole& operator<<(char* str) NOEXCEPT; // wraps in a string
	DebugConsole& operator<<(const Ogre::String& str) NOEXCEPT; // copies strings
	DebugConsole& operator<<(Ogre::String&& str) NOEXCEPT; // moves strings
	DebugConsole& operator<<(const std::vector<std::string>& vec) NOEXCEPT; // copies strings
	DebugConsole& operator<<(const std::ostringstream& oss) NOEXCEPT; // extracts a string
	DebugConsole& operator<<(const std::ostream& os) NOEXCEPT; // extracts a string

	// tries object's output operation overload
	// NOTE: can lead to bloating
	template <typename Ty>
	DebugConsole& operator<<(const Ty& obj) NOEXCEPT 
	{
		try {
			std::ostringstream oss;
			oss << obj; 

			*this << oss;
		}
		catch (...) {
			// do nothing, avoid exceptions in error logging and allow this 
			// in noexcept functions like the destructor
		}

		return *this;
	}

	bool executeCommand(const Ogre::String& cmd);
	void toggle() NOEXCEPT { setEnabled(!mActive); }

	void setEnabled(bool enabled) NOEXCEPT;

	NODISCARD const bool getEnabled() const NOEXCEPT { return mActive; }
	NODISCARD const MessageQueue& getIntermediateQueue() const NOEXCEPT
	{ 
		return mIntermediateQueue.get();
	};
	NODISCARD const MessageQueue& getProcessingQueue() const NOEXCEPT
	{
		return mProcessingQueue;
	}

	// InputHandler.h
	bool textInput(const SDL_TextInputEvent& arg) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
	bool keyReleased(const SDL_KeyboardEvent& arg) override;

    NODISCARD static DebugConsole* getSingletonPtr() NOEXCEPT;
    NODISCARD static DebugConsole& getSingleton();

private:
	typedef std::function<void(const Ogre::StringVector&)> CommandFunctionType;
	typedef std::map<Ogre::String, CommandFunctionType> OperandType;
	typedef std::map<Ogre::String, OperandType> OperatorType;

	typedef std::vector<SDL_Keycode> ButtonsList;

	void init();
	NODISCARD OperatorType initOperators() NOEXCEPT;
	void run();
	bool execute();
	bool execute(const Ogre::String& input);
	void handleMessages();
	template <typename MapType>
	void autoComplete(const MapType& dictionary, const Ogre::StringVector& inputVector);

	inline void flush(const Ogre::String& buffer);
	inline void output(const Ogre::String& message);

	template <typename Ty>
	void addToMessageQueue(Ty&& message) NOEXCEPT
	{		
		try {
			std::lock_guard<std::mutex> lock(mIntermediateQueue.mMutex);
			mIntermediateQueue.get().push(std::forward<Ty>(message));
		} 
		catch (...) {
			// do nothing, avoid exceptions in error logging and allow this 
			// in noexcept functions like the destructor
		}

		mMessageAvailable.notify_one();
	}

	inline void closeOutput() NOEXCEPT;
	inline void clearLine();
	inline void clearCursor() NOEXCEPT;
	inline void nextLine() NOEXCEPT;
	inline void newLine() NOEXCEPT;
	inline void handleAutocomplete(const Ogre::String& word);
	// move messages from intermediate queue to the processing queue
	// mIntermediateQueue must be locked before calling this
	inline bool transferMessageOwnership() NOEXCEPT; 
	NODISCARD inline Ogre::StringVector processInput(const Ogre::String& input);
	inline void printHelp();

	class NODISCARD Chord
	{
		typedef std::vector<SDL_Keycode> KeyList;
		typedef std::function<void()> VoidFunction;

	public:
		typedef std::initializer_list<SDL_Keycode> Keys;

		Chord(const char* name, const Keys keys, const VoidFunction function)
			: mName(name),
			  mKeys(keys),
			  mNumKeys(mKeys.size()),
			  mFunction(function)
		{
		}
		
		~Chord() = default;

		NODISCARD bool hit(const KeyList& buttons)
		{ 
			if (mNumKeys <= buttons.size())
			{
				for (unsigned int i = 0; i < mNumKeys; ++i)
				{ // if the keys don't match
					if (mKeys[i] != buttons[i])
						return false;
				}

				run(); // run the chord otherwise

				return true;
			}

			return false;
		}

		void run()
		{
			try {
				mFunction();
			} 
			catch (std::bad_function_call&) {
				OGRE_EXCEPT(Ogre::Exception::ERR_INTERNAL_ERROR, 
					"Bad function call associated with chord \"" + 
					mName + "\".", "Chord::run");
			}
		}

	private:
		const Ogre::String mName;
		const KeyList mKeys;
		const KeyList::size_type mNumKeys;
		const VoidFunction mFunction;
	};

	struct History
	{
		History() : mCurrentIndex(NULL) {}

		~History() = default;

		NODISCARD Ogre::String next() NOEXCEPT // down arrow
		{
			if (mCurrentIndex < mHistory.size() - 1)
				return mHistory[++mCurrentIndex];

			reset();
			return Ogre::StringUtil::BLANK;
		}

		NODISCARD const Ogre::String& previous() NOEXCEPT // up arrow
		{
			if (mCurrentIndex > NULL) 
				return mHistory[--mCurrentIndex];

			return mHistory[mCurrentIndex];
		}

		template <typename Ty>
		void add(Ty&& command)
		{
			if (!command.empty())
			{
				if (mHistory.size() == DEBUG_CONSOLE_HISTORY_LIMIT)
					mHistory.pop_front();

				mHistory.push_back(std::forward<Ty>(command));

				reset();
			}
		}

		void reset() NOEXCEPT 
		{
			mCurrentIndex = mHistory.size();
		}

	private:
		// deque does not invalidate iterators when deleting or 
		//		inserting at either end
		// more expensive access than a vector
		// expansion of a deque is cheaper than a vector
		// larger minimal memory cost than a vector
		// most importantly - does not have to move the memory around when 
		//		deleting or inserting from the beginning
		typedef std::deque<Ogre::String> StringList;

		StringList mHistory;
		StringList::size_type mCurrentIndex;
	};

	template <typename QueueTy>
	struct LockableQueue
	{
		using queue_type = QueueTy;

		std::mutex mMutex;

		NODISCARD QueueTy& get() NOEXCEPT { return mQueue; }
		NODISCARD const QueueTy& get() const NOEXCEPT { return mQueue; }

	private:
		QueueTy mQueue;
	};

	enum SPECIAL_STRINGS
	{
		SPECIAL_STR_COMMAND,
		SPECIAL_STR_CURSOR,
		SPECIAL_STR_HELP_SUFFIX,
		SPECIAL_STR_MAX
	};

	const OperatorType mOperators;
	std::ofstream mConsoleOutStream;
	std::unique_ptr<std::thread> mThread;
	std::condition_variable mMessageAvailable;
	LockableQueue<MessageQueue> mIntermediateQueue;
	MessageQueue mProcessingQueue;
	Ogre::String mRunningInput;
	Ogre::String mCommandInExecution;
	ButtonsList mButtonsHeld;
	// const char* allows compile-time optimisations
	const std::array<const char*, SPECIAL_STR_MAX> mSpecialStrings; 
	std::atomic<bool> mRunning;
	std::atomic<bool> mActive;
	bool mOutputClosed;

	std::vector<Chord> mChords;
	History mHistory;

    static DebugConsole* msSingleton;

protected:
};

#include "HeaderSuffix.h"

#endif