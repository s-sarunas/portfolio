// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef COLLIDABLE_H
#define COLLIDABLE_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "Actor.h"
#include "Mixin.h"

class Collidable: virtual public Actor, public Mixin
{
public:
	// pure virtual
	virtual void handleCollision(Collidable* collidable, bool isColliding) = 0;

	friend std::ostream& operator<<(std::ostream& os, const Collidable& collidable) NOEXCEPT;

	void addCollision(Collidable* collidable);
	void clearCollisions() NOEXCEPT;
	NODISCARD bool hasCollisions() const NOEXCEPT;
	NODISCARD bool hasCollided(const Ogre::String& nameOrType) const;
	NODISCARD bool hasCollided(const Collidable* collidable) const;
	NODISCARD bool isCollisionMasked(ActorFlagType typeFlag) const NOEXCEPT;
	void reset();

	// NOTE: ignores inactive collisions
	NODISCARD CollisionList& getCollisions() NOEXCEPT 
	{ 
		return mCollisionResults;
	}
	// NOTE: ignores inactive collisions
	NODISCARD const CollidablePtrList getCollisions(const Ogre::String& type);
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT 
	{ 
		return mTypeFlag;
	}
	NODISCARD BitMask<ActorFlagType>& getCollisionMask() NOEXCEPT
	{ 
		return mCollisionMask;
	}
	NODISCARD const bool getVisibleToListeners() const NOEXCEPT 
	{
		return mVisibleToListeners;
	}

	void setVisibleToListeners(bool visible) NOEXCEPT 
	{ 
		mVisibleToListeners = visible; 
	}

private:
	CollisionList mCollisionResults;
	// used integers instead of std::bitset to practice bit manipulation, 
	// which is still commonly used in many APIs
	ActorFlagType mTypeFlag;
	// by default everything is masked, unmask to receive collision callbacks
	BitMask<ActorFlagType> mCollisionMask; 
	bool mVisibleToListeners;

protected:
	Collidable(const ActorFlagType actorFlag);
	virtual ~Collidable();

	Collidable(const Collidable& rhs);
	Collidable& operator=(const Collidable& rhs);
	Collidable(Collidable&& rhs);
	Collidable& operator=(Collidable&& rhs) NOEXCEPT;

	// Mixin.h
	NODISCARD const MixinFlagType getMixinType() const NOEXCEPT 
	{ 
		return MIXIN_COLLIDABLE_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif
