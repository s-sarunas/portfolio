// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef MATH_H
#define MATH_H

#ifndef _DEBUG
#define BOOST_DISABLE_ASSERTS
#endif

#include <random>
#include <numeric>

#include "boost\multi_array.hpp"

#include "InAppDebug.h"

#define MATH_NUMERICAL_INTEGRATION_WARNING_LIMIT 200

/*
	Define MATH_DOUBLE_PRECISION as 1 before including this header to use 
	double precision instead of the default Ogre::Real, which depends on 
	the OGRE_DOUBLE_PRECISION macro
*/
#ifndef MATH_DOUBLE_PRECISION
#define MATH_DOUBLE_PRECISION 0
#endif

struct left_handed_t {};
// right-handed coordinate system is the default
static const left_handed_t left_handed; 

namespace Math
{
#if MATH_DOUBLE_PRECISION == 1
	typedef double RealType;
#else
	typedef Ogre::Real RealType;
#endif

#define MATH_EPSILON std::numeric_limits<RealType>::epsilon()

	class RandomNumberGenerator: public Ogre::Math::RandomValueProvider
	{
	public:
		typedef std::random_device SeederType;
		typedef std::mt19937 EngineType;
		typedef std::uniform_real_distribution<RealType> DistributionType;
		typedef std::common_type<
			std::random_device::result_type, 
			EngineType::result_type
		>::type SeedType;

		RandomNumberGenerator()
		  : mSeeder(), 
			mSeed(mSeeder.operator()()),
			mEngine(mSeed), // seeds
			mDistribution(DistributionType{0.0f, 1.0f})
		{
			Ogre::Math::SetRandomValueProvider(this);
		}

		~RandomNumberGenerator()
		{
			Ogre::Math::SetRandomValueProvider(nullptr);
		}

		// Ogre::Math::RandomValueProvider
		// returns a random value in the range [0,1] for further 
		// processing by Ogre
		NODISCARD Ogre::Real getRandomUnit() override
		{
			return mDistribution(mEngine);
		}

		SeedType reseed()
		{
			mSeed = mSeeder();
			mEngine.seed(mSeed);

			return mSeed;
		}

		void seed(SeedType s)
		{
			mSeed = s;
			mEngine.seed(s);
		}

		NODISCARD const SeederType& getSeeder() const NOEXCEPT 
		{ 
			return mSeeder;
		}
		NODISCARD SeedType getSeed() const NOEXCEPT 
		{ 
			return mSeed;
		}
		NODISCARD const EngineType& getEngine() const NOEXCEPT 
		{ 
			return mEngine;
		}
		NODISCARD const DistributionType& getDistribution() const NOEXCEPT 
		{ 
			return mDistribution; 
		}

	private:
		// used to seed the engine (performance degrades sharply 
		// once the entropy pool is exhausted)
		SeederType mSeeder; 
		SeedType mSeed;
		// generates the numbers
		EngineType mEngine; 
		// uses the numbers generated to produce a distribution
		DistributionType mDistribution; 

	protected:
	};


	// NOTE: compile-time-only version of this requires expression templates
	// NOTE: separate specializations instead of a common for-loop allows for 
	// a better interface
	template <size_t N, typename Ty>
	struct Polynomial;

	template <typename Ty>
	struct Polynomial<2, Ty>
	{
		Polynomial() : a(0.0f), b(0.0f), c(0.0f) {}
		Polynomial(Ty _a, Ty _b = 0.0f, Ty _c = 0.0f) 
			: a(_a), b(_b), c(_c) 
		{
		}

		NODISCARD const/*expr*/ Ty substitute(Ty _x) const NOEXCEPT 
		{ 
			return (a * std::pow(_x, 2)) + (b * _x) + c; 
		}

		NODISCARD const/*expr*/ size_t degree() const NOEXCEPT 
		{ 
			return 2; 
		} 

		Ty a, b, c;
	};

	template <typename Ty>
	struct Polynomial<3, Ty>
	{
		Polynomial() : a(0.0f), b(0.0f), c(0.0f), d(0.0f) {}
		Polynomial(Ty _a, Ty _b = 0.0f, Ty _c = 0.0f, Ty _d = 0.0f) 
			: a(_a), b(_b), c(_c), d(_d) 
		{
		}

		NODISCARD const/*expr*/ Ty substitute(Ty _x) const NOEXCEPT 
		{ 
			return (a * std::pow(_x, 3)) + (b * std::pow(_x, 2)) + (c * _x) + d; 
		}

		NODISCARD const/*expr*/ size_t degree() const NOEXCEPT 
		{
			return 3; 
		} 

		Ty a, b, c, d;
	};

	// this overload is the default in Ogre, the result is normalised
	NODISCARD static const Ogre::Vector3 calcNormal(const Ogre::Vector3& a, 
		const Ogre::Vector3& b, const Ogre::Vector3& c) NOEXCEPT
	{
		// Ogre is a right-handed system (counter-clockwise rotation)
		Ogre::Vector3 normal = (b - a).crossProduct(c - a);
		normal.normalise();

		return normal;
	}

	NODISCARD static const Ogre::Vector3 calcNormal(const Ogre::Vector3& a, 
		const Ogre::Vector3& b, const Ogre::Vector3& c, left_handed_t) NOEXCEPT
	{
		Ogre::Vector3 normal = (c - a).crossProduct(b - a);
		normal.normalise();

		return normal;
	}

	// two-dimensional matrix of any size: 
	// no move semantics are provided for boost::multi_array (outdated)
	// (std::unique_ptr<Ty[]> could be used to overcome that, but that would 
	// overcomplicate the interface - std::valarray does not have this problem)
	typedef boost::multi_array<RealType, 2> Matrix; 
	typedef boost::detail::multi_array::sub_array<Matrix::element, 1> MatrixRow;
	typedef boost::detail::multi_array::const_sub_array<Matrix::element, 1> ConstMatrixRow;
	typedef Matrix::index_range MatrixRange;
	typedef Matrix::array_view<2>::type MatrixView2D;
	typedef Matrix::array_view<1>::type MatrixView1D;
	typedef Matrix::const_array_view<2>::type ConstMatrixView2D;
	typedef Matrix::const_array_view<1>::type ConstMatrixView1D;

	// other related types
	typedef std::vector<RealType> RealList;

	static std::ostream& operator<<(std::ostream& os, const ConstMatrixRow& row) NOEXCEPT
	{
		for (const auto& entry : row)
			os << std::fixed << std::setprecision(2) << entry << '\t';

		return os;
	}

	static std::ostream& operator<<(std::ostream& os, const Matrix& matrix) NOEXCEPT
	{
		os << "Matrix shape: " << matrix.shape()[0] << 'x' <<
			matrix.shape()[1] << '\n';

		for (const auto& row : matrix)
			os << row << '\n';

		return os;
	}

	static void matrixSwapRows(Matrix& _A, const Matrix::size_type row1, 
		const Matrix::size_type row2)
	{
		/* 
			Since MatrixViews and MatrixRows are only references to elements,
			they cannot be swapped (e.g. with std::swap) without a temporary copy

			And neither MatrixViews nor MatrixRows hold elements on their own
		*/

		// number of elements in the row (columns)
		const Matrix::size_type m = _A[row1].num_elements(); 

		// equivalent to Matrix::element* tmp = new Matrix::element[m]; 
		// except with RAII
		std::unique_ptr<Matrix::element[]> tmp(new Matrix::element[m]);

		memcpy(tmp.get(), _A[row1].origin(), sizeof(Matrix::element) * m);	// tmp = row1
		_A[row1] = _A[row2];												// row1 = row2
		memcpy(_A[row2].origin(), tmp.get(), sizeof(Matrix::element) * m);	// row2 = tmp
	}

	/*
		Matrix _A must have the last columns reserved for the matrix row b to 
		avoid resizing via matrixAugmentColumn(), which will reallocate each 
		matrix element

		TODO: this should be generalized to set any given column or row
	*/
	template <typename bType>
	static void matrixSetLastColumn(Matrix& _A, const bType& _b)
	{
		// last index for columns of _A
		const Matrix::size_type m = _A.shape()[1] - 1; 

		// replace last column of _A with _b
		MatrixView1D lastColumn = _A[boost::indices[MatrixRange()][m]];

		assert(_A.shape()[0] == _b.size() && 
			"matrix A must have the same number of rows as augmentation b has "
			"elements");

		std::copy(_b.begin(), _b.end(), lastColumn.begin());
	}

	// TODO: there should also be a matrixAugmentRow
	template <typename bType>
	static void matrixAugmentColumn(Matrix& _A, const bType& _b)
	{
		// add a column
		_A.resize(boost::extents[_A.shape()[0]][_A.shape()[1] + 1]);

		DEBUG_OUTPUT(LVL_PERF, 
			"Augmenting a column to a matrix reallocates all of the elements of "
			"the matrix");

		matrixSetLastColumn(_A, _b);
	}

	template <typename xType>
	static void solveLinearEquations(Matrix& _Ab, xType& _x)
	{
		const Matrix::size_type n = _Ab.shape()[0]; // number of rows
		const Matrix::size_type m = _Ab.shape()[1]; // number of columns

		if ((n + 1) != m) // if the matrix does not have the correct shape
		{ // throw
			OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, 
				"Matrix does not have the correct shape",
				"Math::solveLinearEquations");
		}

		gaussianElimination(_Ab);
		backwardSubstitution(_Ab, _x);
	}

	/*
		Solves equations of the form Ax = b
		xType and bType can be a:
			boost::detail::multi_array::sub_array<> - a row of a matrix
			boost::multi_array<>::array_view<1> - any part of a matrix described 
				by a slice (in 1 dimension only)
			std::array<>
			boost::array<>
			or any other one-dimensional array object with iterator access and 
				size() member function
	*/
	template <typename bType, typename xType>
	static void solveLinearEquations(Matrix& _A, const bType& _b, xType& _x)
	{
		const Matrix::size_type n = _A.shape()[0]; // number of rows
		const Matrix::size_type m = _A.shape()[1]; // number of columns

		if (n == m) // if a square matrix
		{ // resize to accommodate _b
			matrixAugmentColumn(_A, _b); 
		}
		else if ((n + 1) == m) // if already has an extra column
		{ // set the extra column to _b
			matrixSetLastColumn(_A, _b); 
		}
		else // otherwise
		{ // throw
			OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, 
				"Matrix does not have the correct shape",
				"Math::solveLinearEquations");
		}

		gaussianElimination(_A);
		backwardSubstitution(_A, _x);
	}

	static void gaussianElimination(Matrix& _Ab)
	{
		const Matrix::size_type n = _Ab.shape()[0] - 1; // last index for rows of _Ab
		const Matrix::size_type m = _Ab.shape()[1] - 1; // last index for columns of _Ab

		Matrix::element max = 0.0f, nextValue = 0.0f, pivotInverse = 0.0f, 
			coefficient = 0.0f;
		size_t maxRowIndex = 0;
		// for each column (excluding the augmented part)
		for (Matrix::size_type j = 0; j <= m - 1; ++j) 
		{
			/*
				Partial pivoting
			*/
			MatrixView1D currentColumn = _Ab[boost::indices[MatrixRange()][j]];

			// find the largest absolute element in the column below the pivot
			max = abs(currentColumn[j]);
			maxRowIndex = j;
			for (Matrix::size_type i = j + 1; i <= n; ++i)
			{
				nextValue = abs(currentColumn[i]);
				if (nextValue > max)
				{
					max = nextValue;
					maxRowIndex = i;
				}
			}

			// if max is zero - throw
			if (max == 0.0f)
			{
				OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, 
				"There is no single solution for the linear system",
					"Math::gaussianElimination");
			}

			// if max is element is not in the row j, swap the rows
			if (maxRowIndex != j)
				matrixSwapRows(_Ab, maxRowIndex, j);

			/*
				Finding the row-reduced echelon form
			*/
			MatrixView1D currentRow = _Ab[boost::indices[j][MatrixRange()]];

			// set pivot element to 1 by multiplying the entire row by 1/pivot
			pivotInverse = (1 / _Ab[j][j]);
			for (auto& elem : currentRow)
				elem *= pivotInverse;

			// clear lower column entries
			for (Matrix::size_type r = j + 1; r <= n; ++r)
			{
				coefficient = _Ab[r][j];
				for (Matrix::size_type i = 0; i <= m; ++i)
					_Ab[r][i] -= currentRow[i] * coefficient;
			}
		}
	}

	template <typename xType>
	static void backwardSubstitution(const Matrix& _Ab, xType& _x)
	{
		const Matrix::size_type n = _Ab.shape()[0] - 1; // last index for rows of _Ab
		const Matrix::size_type m = _Ab.shape()[1] - 1; // last index for columns of _Ab

		// element-by-element copy, since the memory is not sequential (copying 
		// the last column of _Ab) and MatrixView1D _b is just a 
		// {start, end, stride} for the original matrix _Ab
		const ConstMatrixView1D _b = _Ab[boost::indices[MatrixRange()][m]];

		assert(_x.size() >= _b.num_elements() && 
			"not enough space in the result");

		std::copy(_b.begin(), _b.end(), _x.begin());

		// index here goes below 0
		for (std::make_signed<Matrix::size_type>::type i = n - 1; i >= 0; --i)
		{
			for (Matrix::size_type j = i + 1; j <= n; ++j)
				_x[i] -= _Ab[i][j] * _x[j];
		}
	}

	// for most cubics, anything less than 0.02f step creates more error along 
	// with loss of performance than accuracy
	template <typename Ty>
	NODISCARD static Ty numericalIntegration(const Cubic<Ty>& _f, Ty _a, Ty _b, 
		const Ty _step = 0.02f)
	{
		if (_a > _b)
		{ // swap the direction of integration
			std::swap(_a, _b);
		}

		if (((abs(_b) - abs(_a)) / _step) > MATH_NUMERICAL_INTEGRATION_WARNING_LIMIT) 
		{
			DEBUG_OUTPUT(LVL_PERF, "Costly numerical integration of more than " + 
				Ogre::StringConverter::toString(MATH_NUMERICAL_INTEGRATION_WARNING_LIMIT) + 
				" iterations");
		}

		Ty result = 0.0f, current = _f.substitute(_a), next = 0.0f;
		for (Ty c = (_a + _step); c < _b; c += _step)
		{
			next = _f.substitute(c);
			result += next - current;

			current = next;
		}

		return result;
	}

	NODISCARD static Ogre::Vector3 barycentricToCartesian(RealType b1, RealType b2, 
		RealType b3, const Ogre::Vector3& v1, const Ogre::Vector3& v2, 
		const Ogre::Vector3& v3) NOEXCEPT
	{
		return (v1*b1 + v2*b2 + v3*b3);
	}

	NODISCARD static const RealType rayTriangleIntersection(
		const Ogre::Vector3& rayOrigin, const Ogre::Vector3& rayDir, 
		const Ogre::Vector3& vert0, const Ogre::Vector3& vert1, 
		const Ogre::Vector3& vert2) NOEXCEPT
	{
		/*
			Ray:					R(t) = rayOrigin + t*rayDir
			Point on a triangle:	T(u, v) = (1 - u - v)*vert0 + u*vert1 + v*vert2

			Equation: rayOrigin + t*rayDir = (1 - u - v)*vert0 + u*vert1 + v*vert2
			
			1. Expand and rearrange the terms
			2. Premultiply both sides by the matrix inverse to get the solution
			3. Apply the Cramer's rule to solve linear equations: 
				x = det(Ai)/det(A) = 1/det(A) * det(Ai), where Ai is the matrix 
				formed by replacing the i-th column of A by the column vector 
				(in this case rayOrigin - vert0)
			4. Apply the scalar triple product
				(a cross b) dot c = det(a, b, c)
			5. Perform the cross and dot products, reusing common factors
		*/

		const Ogre::Vector3 edge1 = vert1 - vert0;
		const Ogre::Vector3 edge2 = vert2 - vert0;
		const Ogre::Vector3 pvec = rayDir.crossProduct(edge2);
		Ogre::Real u, v, t;

		const Ogre::Real det = edge1.dotProduct(pvec);

		// only perform the test on the front-facing triangles
		if (det < MATH_EPSILON)
			return NULL;

		// calculate u parameter and test bounds
		const Ogre::Vector3 tvec = rayOrigin - vert0;
		u = tvec.dotProduct(pvec);
		if (u < 0.0f || u > det)
			return NULL;

		// calculate v parameter and test bounds
		Ogre::Vector3 qvec = tvec.crossProduct(edge1);
		v = rayDir.dotProduct(qvec);
		if (v < 0.0f || u + v > det)
			return NULL;

		// ray intersects the triangle
		t = edge2.dotProduct(qvec);
		const Ogre::Real inv_det = 1.0f / det;

		// scale the parameters
		t *= inv_det;
		//u *= inv_det;
		//v *= inv_det;

		/*Ogre::Vector3 intersectPoint = 
			barycentricToCartesian(1-u-v, u, v, vert0, vert1, vert2);*/

		return t;
	}
};

#endif