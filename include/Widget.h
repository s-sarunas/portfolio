// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef WIDGET_H
#define WIDGET_H

#include "HeaderPrefix.h"

#include "SimpleMenu.h"

#define WIDGET_VOID_BORDER 4
// padding around the tray, half of this horizontally inbetween the widget stack
#define WIDGET_PADDING 10.0f 

#define WIDGET_BORDER_PANEL_TYPENAME "BorderPanel"

static const Ogre::String WIDGET_TYPE_BUTTON = "Button"; 
static const Ogre::String WIDGET_TYPE_DROPDOWN = "Dropdown"; 

// headers defining base classes
#include "InputHandler.h"

class Widget: public InputHandler
{
public:
	typedef size_t size_type;

	virtual ~Widget() {}

	virtual void reset() = 0;
	NODISCARD virtual const Ogre::String& getType() const NOEXCEPT = 0;

	NODISCARD const Ogre::Real getTop() const NOEXCEPT 
	{ 
		return mRootElement->getTop();
	}
	NODISCARD const Ogre::Real getBottom() const NOEXCEPT 
	{ 
		return getTop() + getHeight(); 
	}
	NODISCARD const Ogre::Real getLeft() const NOEXCEPT
	{
		return mRootElement->getLeft();
	}
	NODISCARD const Ogre::Real getRight() const NOEXCEPT 
	{ 
		return getLeft() + getWidth();
	}
	NODISCARD const Ogre::Real getWidth() const NOEXCEPT 
	{ 
		return mRootElement->getWidth();
	}
	NODISCARD const Ogre::Real getHeight() const NOEXCEPT 
	{ 
		return mRootElement->getHeight();
	}

	NODISCARD const Ogre::String& getRootName() const NOEXCEPT 
	{ 
		return mRootName; 
	}
	NODISCARD const Ogre::String& getName() const NOEXCEPT 
	{ 
		return mName;
	}
	NODISCARD Ogre::BorderPanelOverlayElement* getRootElement() const NOEXCEPT 
	{ 
		return mRootElement; 
	}
	NODISCARD Ogre::TextAreaOverlayElement* getRootCaption() const NOEXCEPT 
	{
		return mRootCaption;
	}
	NODISCARD SimpleMenu::Listener* getListener() const NOEXCEPT 
	{
		return mListener;
	}
	NODISCARD const size_type getIndex() const NOEXCEPT 
	{
		return mIndex;
	}

private:
	Ogre::String mName;
	Ogre::String mRootName;
	Ogre::BorderPanelOverlayElement* mRootElement;
	Ogre::TextAreaOverlayElement* mRootCaption;
	SimpleMenu::Listener* mListener;
	size_type mIndex;

protected:
	// NOTE: The widget's root container must be a BorderPanel, other container 
	// types are not supported
	Widget(const Ogre::String& name, const Ogre::String& menuName, 
		const Ogre::DisplayString& caption, const Ogre::String& templateName, 
		const Ogre::Real width, const Ogre::Real height, const Ogre::Real top, 
		const Ogre::String& captionName, const size_type index, 
		SimpleMenu::Listener* listener)
	  : mName(name),
		mRootName(menuName + '/' + caption),
		mRootElement(static_cast<Ogre::BorderPanelOverlayElement*>(
			Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate(
				templateName, 
				WIDGET_BORDER_PANEL_TYPENAME, 
				mRootName))),
		mRootCaption(static_cast<Ogre::TextAreaOverlayElement*>(
			mRootElement->getChild(mRootName + captionName))),
		mListener(listener),
		mIndex(index)
	{
		assert(mListener && 
			"a valid widget listener is required");

		mRootElement->setWidth(width);
		mRootElement->setHeight(height);
		mRootElement->setLeft(-(width / 2));
		mRootElement->setTop(top);
		mRootElement->setHorizontalAlignment(Ogre::GuiHorizontalAlignment::GHA_CENTER);

		mRootCaption->setCaption(caption);
	}

	// should be localized to mouse moved events only
	NODISCARD static bool isCursorOver(Ogre::OverlayElement* element, 
		const Sint32 x, const Sint32 y, 
		const Sint32 voidBorder = WIDGET_VOID_BORDER) NOEXCEPT
	{
		const Ogre::OverlayManager* overlayManager = 
			Ogre::OverlayManager::getSingletonPtr();

		const Ogre::Real l = element->_getDerivedLeft() * 
			overlayManager->getViewportWidth();
		const Ogre::Real t = element->_getDerivedTop() * 
			overlayManager->getViewportHeight();
		const Ogre::Real r = l + element->getWidth();
		const Ogre::Real b = t + element->getHeight();

		return (x >= l + voidBorder && x <= r - voidBorder &&
				y >= t + voidBorder && y <= b - voidBorder);
	}
};

#include "HeaderSuffix.h"

#endif