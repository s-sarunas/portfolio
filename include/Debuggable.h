// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DEBUGGABLE_H
#define DEBUGGABLE_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "Actor.h"
#include "Mixin.h"

class Debuggable: virtual public Actor, public Mixin
{
public:
	virtual void setDebuggableVisibility(bool flag);

private:
	bool mLoaded;

protected:
	Debuggable();
	virtual ~Debuggable();

	// virtual
	virtual void initDebuggable() {}
	virtual void showDebuggable() {}
	virtual void hideDebuggable() {}

	// Mixin.h
	NODISCARD const MixinFlagType getMixinType() const NOEXCEPT 
	{
		return MIXIN_DEBUGGABLE_FLAG;
	}
};

#include "HeaderSuffix.h"

#endif