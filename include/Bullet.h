// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef BULLET_H
#define BULLET_H

#include "HeaderPrefix.h"

#include "Ray.h"

// headers defining base classes
#include "DynamicActor.h"

#define BULLET_SPEED 45.0f
#define BULLET_SELF_DESTRUCT_LIMIT_SQUARED 2500

class Bullet: public DynamicActor
{
public:
	Bullet(const ActorClass& desc, const Ogre::Vector3& direction);
	~Bullet();

	// Actor.h
	void update(const Ogre::Real deltaSeconds) override;
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{
		return ACTOR_BULLET_TYPE_NAME;
	}

	// Collidable.h
	void handleCollision(Collidable* collidable, bool isColliding) override;

private:
	void checkBounds() NOEXCEPT;
};

class NODISCARD BulletFactory: public ActorFactory
{
public:
	BulletFactory() {}
	~BulletFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override
	{
		return ACTOR_BULLET_TYPE_NAME;
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return ACTOR_BULLET_TYPE_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif