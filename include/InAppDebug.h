// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef INAPPDEBUG_H
#define INAPPDEBUG_H

#include "HeaderPrefix.h"

#include "DebugConsole.h"
#include "DebugTable.h"
#include "DebugTimer.h"
#include "DebugCamera.h"

// headers defining base classes
#include "Actor.h"
#include "InputHandler.h"

extern std::string getMemoryUsage();
extern std::string formatMemory(size_t bytes);

enum MESSAGE_LEVEL
{
    LVL_GREEN,
	LVL_CONSOLE,
	LVL_PERF,
    LVL_YELLOW, // WARNING would clash with OGRE
    LVL_RED, // ERROR would clash with Ogre
	MESSAGE_LEVEL_MAX
};

#define DEBUG_OUTPUT_CONSOLE_ERROR_LEVEL MESSAGE_LEVEL::LVL_PERF
#define DEBUG_OUTPUT_TABLE_GAP 3

#define DEBUG_ROOT_NAME "DEBUG_ROOT"
#define DEBUG_OBJECT_MESH_FILENAME "axis.mesh"
#define DEBUG_BILLBOARD_MATERIAL_NAME "light_billboard"

// appends __FUNCTION__ at the end of the message, other candidates are:
// __FUNCDNAME__ which needs manual demangling, or
// __FUNCSIG__ which gets lengthy for types like std::string
#define DEBUG_OUTPUT(lvl, msg, ...) \
	InAppDebug::outputMessage(lvl, msg, __FUNCTION__, __VA_ARGS__)

class InAppDebug: public ActorCreator, public InputHandler
{
public:
	InAppDebug();
	~InAppDebug();

	void _init(const Ogre::RenderWindow* window); // delayed initialization
	void update(const Ogre::Real deltaSeconds);
	void clearTable();
	void clearActors();
	void cleanup();

	void flipObjectVisibility();
	void setObjectsVisible(bool visible);

	NODISCARD const bool getObjectsVisible() const NOEXCEPT { return mObjectsVisible; }
	NODISCARD Ogre::SceneNode* getRootSceneNode() const NOEXCEPT { return mRootSceneNode; }
	NODISCARD DebugConsole* getConsole() const NOEXCEPT { return mConsole.get(); }
	NODISCARD DebugTable* getTable() const NOEXCEPT { return mTable.get(); }
	NODISCARD DebugCamera& getCameraMan() NOEXCEPT { return mCamera; }

	static void outputMessage(MESSAGE_LEVEL level, const Ogre::String& message, 
		const Ogre::String& source, bool noDuplicates = false) NOEXCEPT;
	// outputs a table from a string stream of values delimited with \t or \n 
	// data is row-major
	static void outputTable(const Ogre::StringVector& headers, const std::stringstream& ss);
	static void outputSceneHierarchy();
	static void outputActors(const ActorCollectionMap& actorCollection);
	static void outputActors(const CollidablePtrList& collidableList);
	static void saveTextureToFile(const Ogre::TexturePtr texture, 
		const Ogre::String& filename);
	NODISCARD static int getNumResourcesWithStr(Ogre::ResourceManager* manager, 
		const Ogre::String& str);
	NODISCARD static const unsigned int calcNumNodes(Ogre::SceneNode* root) NOEXCEPT;

	Actor* addDebugObject(
		Ogre::SceneNode* parentNode, 
		const bool visible = true, 
		const Ogre::String& meshName = DEBUG_OBJECT_MESH_FILENAME,
		const Ogre::Vector3& translation = Ogre::Vector3::ZERO,
		const Ogre::Quaternion& orientation = Ogre::Quaternion::IDENTITY, 
		const Ogre::Vector3& scale = Ogre::Vector3::UNIT_SCALE
		) NOEXCEPT;
	Actor* addDebugObject(
		const Ogre::Vector3& position, 
		const bool visible = true, 
		const Ogre::String& meshName = DEBUG_OBJECT_MESH_FILENAME,
		const Ogre::Quaternion& orientation = Ogre::Quaternion::IDENTITY, 
		const Ogre::Vector3& scale = Ogre::Vector3::UNIT_SCALE
		) NOEXCEPT;
	Actor* addBillboard(
		Ogre::SceneNode* parentNode, 
		const Ogre::String& materialName = DEBUG_BILLBOARD_MATERIAL_NAME
		) NOEXCEPT;
	Actor* addArrow(Ogre::SceneNode* parentNode) NOEXCEPT;
	Actor* addArrow(const Ogre::Vector3& direction, 
		const Ogre::Vector3& position = Ogre::Vector3::ZERO
		) NOEXCEPT;

	// InputHandler.h
	bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
	bool textInput(const SDL_TextInputEvent& arg) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
    bool keyReleased(const SDL_KeyboardEvent& arg) override;

	NODISCARD static InAppDebug* getSingletonPtr() NOEXCEPT;
	NODISCARD static InAppDebug& getSingleton();

private:
	typedef std::vector<size_t> HashList;

	void updateBillboards();

	// ActorCreator.h
	Actor* createActor(const ActorClass& desc) override;
	NODISCARD std::shared_ptr<Actor> createUnmanagedActor(const ActorClass& desc) override;
	void reserveForActors(const size_t numActors) override;

	// std::array over Ogre::String[] = {...}
    static const std::array<Ogre::String, MESSAGE_LEVEL_MAX> msMessageLevels; 
	static const std::hash<Ogre::String> msStringHash;
	static HashList msMessageHashes;

	ActorPtrList mActors;
	Ogre::MeshPtr mBillboardMesh;
	Ogre::SceneNode* mRootSceneNode;
	bool mObjectsVisible;

	// custom interfaces

	// on the heap, due to circular dependencies
	std::unique_ptr<DebugConsole> mConsole;
	// on the heap, since Ogre::OverlayManager required for the DebugTable 
	// is not initialized on construction of this class
	std::unique_ptr<DebugTable> mTable;
	// on the stack, uses global camera accessors to get 
	// the camera for construction
	DebugCamera mCamera;

	static InAppDebug* msSingleton;

protected:
};

#include "HeaderSuffix.h"

#endif