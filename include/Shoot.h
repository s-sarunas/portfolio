// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef SHOOT_H
#define SHOOT_H

#include "HeaderPrefix.h"

#include "Ray.h"

// headers defining bases classes
#include "Ability.h"

#define SHOOT_BULLET_STUN_DURATION std::chrono::milliseconds{500}
#define SHOOT_BULLET_INITIAL_COUNT 15
#define SHOOT_FLASH_DURATION_MS 40
#define SHOOT_COOLDOWN_MS 150

#define SHOOT_BULLET_MESH_FILENAME "bullet.mesh"

#define SHOOT_BUTTON_KB_EQUIP SDLK_e
#define SHOOT_BUTTON_KB_SHOOT SDLK_r

#define SHOOT_BUTTON_JS_EQUIP SDL_CONTROLLER_BUTTON_X
#define SHOOT_BUTTON_JS_SHOOT SDL_CONTROLLER_BUTTON_Y

template <>
struct ability_traits<Shoot>
{
	// is only one instance of this ability allowed to run at one time
	typedef std::true_type is_unique; 
	static const/*expr*/ Ability::PhaseIndex num_phases = 4;
};

class Shoot: public Ability
{
public:
	Shoot(Paddle* user);
	~Shoot();

	// Ability.h
	bool process(const Ogre::Real deltaSeconds) override;
	NODISCARD const Ogre::String& getName() const NOEXCEPT override 
	{ 
		return msName;
	}

	// InputHandler.h
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
    bool keyReleased(const SDL_KeyboardEvent& arg) override;
	bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;

private:
	// Ability.h
	void _phaseChanged(const PhaseIndex newPhase) override;

	void startShooting();
	void shootBullet();

	static const Ogre::String msName;
	AnimationStateWrapper& mTopAnimation;
	Ogre::Timer mTimer;
	unsigned int mNumBulletsShot;
	bool mShooting;

	static Ogre::ulong msBulletCount;

protected:
};

#include "HeaderSuffix.h"

#endif