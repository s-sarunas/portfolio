// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef STRINGINTERFACE_H
#define STRINGINTERFACE_H

#include "HeaderPrefix.h"

// NOTE: not an exhaustive list of potential types
enum ParameterType
{
	// built-ins
    PARAMETER_TYPE_BOOL,
    PARAMETER_TYPE_REAL,
    PARAMETER_TYPE_INT,
    PARAMETER_TYPE_UNSIGNED_INT, // size_t
	// classes
    PARAMETER_TYPE_STRING,
    PARAMETER_TYPE_VECTOR3,
    PARAMETER_TYPE_COLOURVALUE
};

class StringInterface
{
private:
	struct NODISCARD Parameter
	{
	public:
		// must be default constructible when used as std::map::mapped_type,
		// with std::map::operator[]
		Parameter() = default;
		template <typename ValTy>
		Parameter(ValTy&& value, const ParameterType type) 
			: mValue(std::forward<ValTy>(value)), mType(type) {}
		~Parameter() = default;

		// have to provide move semantics as they cannot be defaulted
		// in this version of the compiler
		Parameter(Parameter&& rhs) NOEXCEPT
		  : mValue(std::move(rhs.mValue)),
			mType(rhs.mType) 
		{
		}

		Parameter& operator=(Parameter&& rhs) NOEXCEPT 
		{
			if (this != std::addressof(rhs))
			{
				mValue = std::move(rhs.mValue);
				mType = rhs.mType;
			}

			return *this;
		}

		NODISCARD friend bool operator==(const Parameter& lhs, 
			const Parameter& rhs) NOEXCEPT
		{
			return (lhs.mValue == rhs.mValue &&
					lhs.mType == rhs.mType);
		}

		NODISCARD friend bool operator!=(const Parameter& lhs,
			const Parameter& rhs) NOEXCEPT
		{
			return !(lhs == rhs);
		}

		// provide specializations for each parameter type, 
		// since Ogre::StringConverter::parse functions are not templated
		template <typename ValTy>
		NODISCARD const ValTy getValue() const;

		template <>
		NODISCARD const bool getValue<bool>() const 
		{ 
			checkType(PARAMETER_TYPE_BOOL);
			return Ogre::StringConverter::parseBool(mValue); 
		}

		template <>
		NODISCARD const Ogre::Real getValue<Ogre::Real>() const
		{ 
			checkType(PARAMETER_TYPE_REAL);
			return Ogre::StringConverter::parseReal(mValue); 
		}

		template <>
		NODISCARD const int getValue<int>() const
		{
			checkType(PARAMETER_TYPE_INT);
			return Ogre::StringConverter::parseInt(mValue);
		}

		template <>
		NODISCARD const unsigned int getValue<unsigned int>() const
		{ 
			checkType(PARAMETER_TYPE_UNSIGNED_INT);
			return Ogre::StringConverter::parseUnsignedInt(mValue);
		}

		template <>
		NODISCARD const Ogre::Vector3 getValue<Ogre::Vector3>() const
		{ 
			checkType(PARAMETER_TYPE_VECTOR3);
			return Ogre::StringConverter::parseVector3(mValue); 
		}

		template <>
		NODISCARD const Ogre::ColourValue getValue<Ogre::ColourValue>() const
		{ 
			checkType(PARAMETER_TYPE_COLOURVALUE);
			return Ogre::StringConverter::parseColourValue(mValue); 
		}

		// string is a special case - returned by reference
		NODISCARD const Ogre::String& getValue() const
		{ 
			checkType(PARAMETER_TYPE_STRING);
			return mValue; 
		}

		// not recommended
		NODISCARD const Ogre::String getUncheckedValue() const NOEXCEPT
		{
			return mValue;
		}

		NODISCARD const ParameterType getType() const NOEXCEPT 
		{ 
			return mType; 
		}

	private:
		// imitates type safety
		inline /*constexpr*/ void checkType(const ParameterType type) const 
		{
			assert(mType == type && 
				"parameter type mismatch");
		}

		Ogre::String mValue;
		ParameterType mType;
	};

	// do not declare members here, the access specifier blocks can be reordered

public:
	typedef std::map<Ogre::String, Parameter> DictionaryMap;

	StringInterface() {}
	virtual ~StringInterface() {}

	StringInterface(const StringInterface& rhs) = delete;
	StringInterface& operator=(const StringInterface& rhs) = delete;

	StringInterface(StringInterface&& rhs) NOEXCEPT 
		: mDictionary(std::move(rhs.mDictionary)) 
	{
	}

	StringInterface& operator=(StringInterface&& rhs) NOEXCEPT
	{
		if (this != std::addressof(rhs))
		{
			mDictionary = std::move(rhs.mDictionary);
		}

		return *this;
	}

	// NOTE: no way to properly merge two maps without using nodes (C++17 
	// feature) and map::insert does not support overwriting have to unwrap 
	// the map's pairs
	void merge(const StringInterface& params, bool overwrite = true)
	{
		for (const auto& param : params.mDictionary)
		{
			if (overwrite)
				setParam(param.first, param.second);
			else
				addParam(param.first, param.second);
		}
	}

	void merge(StringInterface&& params, bool overwrite = true)
	{
		for (auto& param : params.mDictionary)
		{
			if (overwrite)
				setParam(std::move(param.first), std::move(param.second));
			else
				addParam(std::move(param.first), std::move(param.second));
		}

		params.mDictionary.clear();
	}

	////////////////////////////////////////////////////////////////////////////
	// Add parameter
	// Throws if parameter with the name already exists
	////////////////////////////////////////////////////////////////////////////

	// is class
	template <typename ValTy>
	typename std::enable_if<
		!estd::passable_by_value<typename ValTy>::value
	>::type
	addParameter(const Ogre::String& name, 
		const ValTy& value, const ParameterType type)
	{
		addParam(name, 
			Ogre::StringConverter::toString(value), type);
	}

	template <typename ValTy>
	typename std::enable_if<
		!estd::passable_by_value<typename ValTy>::value
	>::type
	addParameter(Ogre::String&& name, 
		const ValTy& value, const ParameterType type)
	{
		addParam(std::move(name), 
			Ogre::StringConverter::toString(value), type);
	}

	// not class
	template <typename ValTy>
	typename std::enable_if<
		estd::passable_by_value<typename ValTy>::value
	>::type
	addParameter(const Ogre::String& name, 
		const ValTy value, const ParameterType type)
	{
		addParam(name, 
			Ogre::StringConverter::toString(value), type);
	}

	template <typename ValTy>
	typename std::enable_if<
		estd::passable_by_value<typename ValTy>::value
	>::type
	addParameter(Ogre::String&& name, 
		const ValTy value, const ParameterType type)
	{
		addParam(std::move(name), 
			Ogre::StringConverter::toString(value), type);
	}

	// not using templates with forwarding references here to preserve 
	// strong types
	void addParameter(const Ogre::String& name, 
		const Ogre::String& value, const ParameterType type)
	{
		addParam(name, value, type);
	}

	void addParameter(const Ogre::String& name, 
		Ogre::String&& value, const ParameterType type)
	{
		addParam(name, std::move(value), type);
	}

	void addParameter(Ogre::String&& name, 
		const Ogre::String& value, const ParameterType type)
	{
		addParam(std::move(name), value, type);
	}

	void addParameter(Ogre::String&& name, 
		Ogre::String&& value, const ParameterType type)
	{
		addParam(std::move(name), std::move(value), type);
	}

	// explicit char* overloads to avoid using the general template, 
	// since no conversion to string is needed
	void addParameter(const Ogre::String& name, 
		const char* value, const ParameterType type)
	{
		addParam(name, value, type);
	}

	void addParameter(const Ogre::String& name, 
		char* value, const ParameterType type)
	{
		addParam(name, value, type);
	}

	////////////////////////////////////////////////////////////////////////////
	// Set parameter
	// Replaces parameter value if it already exits
	////////////////////////////////////////////////////////////////////////////

	// overloads for pairs
	// NOTE: could use map nodes here with C++17
	void setParameter(const DictionaryMap::value_type& param)
	{
		mDictionary[param.first] = param.second;
	}

	void setParameter(DictionaryMap::value_type&& param)
	{
		mDictionary[std::move(param.first)] = std::move(param.second);
	}

	// is class
	template <typename ValTy>
	typename std::enable_if<
		!estd::passable_by_value<typename ValTy>::value, 
		std::pair<DictionaryMap::iterator, bool>
	>::type
	setParameter(const Ogre::String& name, 
		const ValTy& value, const ParameterType type)
	{
		return setParam(name, 
			Ogre::StringConverter::toString(value), type);
	}

	template <typename ValTy>
	typename std::enable_if<
		!estd::passable_by_value<typename ValTy>::value, 
		std::pair<DictionaryMap::iterator, bool>
	>::type
	setParameter(Ogre::String&& name, 
		const ValTy& value, const ParameterType type)
	{
		return setParam(std::move(name), 
			Ogre::StringConverter::toString(value), type);
	}

	// not class
	template <typename ValTy>
	typename std::enable_if<
		estd::passable_by_value<typename ValTy>::value, 
		std::pair<DictionaryMap::iterator, bool>
	>::type
	setParameter(const Ogre::String& name, 
		const ValTy value, const ParameterType type)
	{
		return setParam(name, 
			Ogre::StringConverter::toString(value), type);
	}

	template <typename ValTy>
	typename std::enable_if<
		estd::passable_by_value<typename ValTy>::value, 
		std::pair<DictionaryMap::iterator, bool>
	>::type
	setParameter(Ogre::String&& name, 
		const ValTy value, const ParameterType type)
	{
		return setParam(std::move(name), 
			Ogre::StringConverter::toString(value), type);
	}

	// not using templates with forwarding references here to preserve 
	// strong types
	std::pair<DictionaryMap::iterator, bool>
	setParameter(const Ogre::String& name, 
		const Ogre::String& value, const ParameterType type)
	{
		return setParam(name, value, type);
	}

	std::pair<DictionaryMap::iterator, bool>
	setParameter(const Ogre::String& name, 
		const Ogre::String&& value, const ParameterType type)
	{
		return setParam(name, std::move(value), type);
	}

	std::pair<DictionaryMap::iterator, bool>
	setParameter(const Ogre::String&& name, 
		const Ogre::String& value, const ParameterType type)
	{
		return setParam(std::move(name), value, type);
	}

	std::pair<DictionaryMap::iterator, bool>
	setParameter(const Ogre::String&& name, 
		const Ogre::String&& value, const ParameterType type)
	{
		return setParam(std::move(name), std::move(value), type);
	}

	// explicit char* overloads to avoid using the general template, 
	// since no conversion to string is needed
	std::pair<DictionaryMap::iterator, bool> 
	setParameter(const Ogre::String& name, 
		const char* value, const ParameterType type)
	{
		return setParam(name, value, type);
	}

	std::pair<DictionaryMap::iterator, bool> 
	setParameter(const Ogre::String& name, 
		char* value, const ParameterType type)
	{
		return setParam(name, value, type);
	}

	////////////////////////////////////////////////////////////////////////////
	// Get parameter
	////////////////////////////////////////////////////////////////////////////

	const Ogre::String& getParameter(const Ogre::String& name) const
	{
		return getParam(name).getValue();
	}

	template <typename ValTy>
	const ValTy getParameter(const Ogre::String& name) const
	{
		return getParam(name).getValue<ValTy>();
	}

	////////////////////////////////////////////////////////////////////////////
	// Helpers
	////////////////////////////////////////////////////////////////////////////

	template <typename ValTy>
	static typename std::enable_if<
		!estd::passable_by_value<typename ValTy>::value, 
		DictionaryMap::value_type
	>::type
	make_entry(const Ogre::String& name, 
		const ValTy& value, const ParameterType type)
	{
		return makeEntry(name, 
			Ogre::StringConverter::toString(value), type);
	}

	template <typename ValTy>
	static typename std::enable_if<
		!estd::passable_by_value<typename ValTy>::value, 
		DictionaryMap::value_type
	>::type
	make_entry(Ogre::String&& name, 
		const ValTy& value, const ParameterType type)
	{
		return makeEntry(std::move(name), 
			Ogre::StringConverter::toString(value), type);
	}

	// not class
	template <typename ValTy>
	static typename std::enable_if<
		estd::passable_by_value<typename ValTy>::value, 
		DictionaryMap::value_type
	>::type
	make_entry(const Ogre::String& name,
		const ValTy value, const ParameterType type)
	{
		return makeEntry(name,
			Ogre::StringConverter::toString(value), type);
	}

	template <typename ValTy>
	static typename std::enable_if<
		estd::passable_by_value<typename ValTy>::value, 
		DictionaryMap::value_type
	>::type
	make_entry(Ogre::String&& name,
		const ValTy value, const ParameterType type)
	{
		return makeEntry(std::move(name),
			Ogre::StringConverter::toString(value), type);
	}

	// not using templates with forwarding references here to preserve strong types
	static DictionaryMap::value_type
	make_entry(const Ogre::String& name,
		const Ogre::String& value, const ParameterType type)
	{
		return makeEntry(name, value, type);
	}

	static DictionaryMap::value_type
	make_entry(const Ogre::String& name, 
		const Ogre::String&& value, const ParameterType type)
	{
		return makeEntry(name, std::move(value), type);
	}

	static DictionaryMap::value_type
	make_entry(const Ogre::String&& name, 
		const Ogre::String& value, const ParameterType type)
	{
		return makeEntry(std::move(name), value, type);
	}

	static DictionaryMap::value_type
	make_entry(const Ogre::String&& name, 
		const Ogre::String&& value, const ParameterType type)
	{
		return makeEntry(std::move(name), std::move(value), type);
	}

	// explicit char* overloads to avoid using the general template, since no 
	// conversion to string is needed
	static DictionaryMap::value_type
	make_entry(const Ogre::String& name, 
		const char* value, const ParameterType type)
	{
		return makeEntry(name, value, type);
	}

	static DictionaryMap::value_type
	make_entry(const Ogre::String& name, 
		char* value, const ParameterType type)
	{
		return makeEntry(name, value, type);
	}

	const DictionaryMap& getDictionary() const { return mDictionary; }

private:
	const Parameter& getParam(const Ogre::String& name) const
	{
		const DictionaryMap::const_iterator it = mDictionary.find(name);
		if (it != mDictionary.end())
			return it->second;

		OGRE_EXCEPT(Ogre::Exception::ERR_ITEM_NOT_FOUND, 
			"Parameter with name \"" + name + 
			"\" does not exist in the dictionary", 
			"StringInterface::getParam");
	}

	// using forward references in private to avoid writing overloads
	// caller has to guarantee that proper types are passed as arguments
	template <typename NameTy, typename ValTy>
	void addParam(NameTy&& name, 
		ValTy&& value, const ParameterType type)
	{
		addParam(
			std::forward<NameTy>(name), 
			Parameter(std::forward<ValTy>(value), type));
	}

	template <typename NameTy, typename ParamTy>
	void addParam(NameTy&& name, ParamTy&& param)
	{
		auto result = mDictionary.emplace(
			std::forward<NameTy>(name), 
			std::forward<ParamTy>(param));

		if (!result.second)
		{
			OGRE_EXCEPT(Ogre::Exception::ERR_DUPLICATE_ITEM, 
				"Parameter with name \"" + name + 
				"\" already exists in the dictionary",
				"StringInterface::addParam");
		}
	}

	template <typename NameTy, typename ValTy>
	std::pair<DictionaryMap::iterator, bool> 
	setParam(NameTy&& name, 
		ValTy&& value, const ParameterType type)
	{
		return setParam(
			std::forward<NameTy>(name), 
			Parameter(std::forward<ValTy>(value), type));
	}

	template <typename NameTy, typename ParamTy>
	std::pair<DictionaryMap::iterator, bool> 
	setParam(NameTy&& name, ParamTy&& param)
	{
		return estd::insert_or_assign(mDictionary, 
			std::forward<NameTy>(name), 
			std::forward<ParamTy>(param));
	}

	template <typename NameTy, typename ValTy>
	static DictionaryMap::value_type
	makeEntry(NameTy&& name,
		ValTy&& value, const ParameterType type)
	{
		// construct a parameter and move it
		return DictionaryMap::value_type(
			std::forward<NameTy>(name),
			Parameter(std::forward<ValTy>(value), type)); 
	}

	DictionaryMap mDictionary; 
};

static std::ostream& operator<<(std::ostream& os, const StringInterface& strInterface) NOEXCEPT
{
	// NOTE: should use [first, second] syntax with pairs everywhere in C++17 
	for (const auto& pair : strInterface.getDictionary())
	{
		os << pair.first << '\t'
		   << pair.second.getUncheckedValue() << '\t';

		switch (pair.second.getType())
		{
		case PARAMETER_TYPE_BOOL:
			os << "bool";
			break;
		case PARAMETER_TYPE_REAL:
			os << "Ogre::Real";
			break;
		case PARAMETER_TYPE_INT:
			os << "int";
			break;
		case PARAMETER_TYPE_UNSIGNED_INT:
			os << "unsigned int";
			break;
		case PARAMETER_TYPE_STRING:
			os << "Ogre::String";
			break;
		case PARAMETER_TYPE_VECTOR3:
			os << "Ogre::Vector3";
			break;
		case PARAMETER_TYPE_COLOURVALUE:
			os << "Ogre::ColourValue";
			break;
		default:
			os << "ERROR";
			break;
		};

		os << "\n";
	}

	return os;
}

#include "HeaderSuffix.h"

#endif