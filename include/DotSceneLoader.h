// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DOT_SCENELOADER_H
#define DOT_SCENELOADER_H

#include "HeaderPrefix.h"

#include "rapidxml.hpp"

class DotSceneLoader
{
public:
    DotSceneLoader(ActorCreator* creator, const Ogre::String& sceneFilename, 
		Ogre::SceneNode* rootSceneNode, const Ogre::String& groupName, 
		bool useInAppDebug = false);
    ~DotSceneLoader();

    void parseDotScene(const Ogre::String& filename, const Ogre::String& groupName);
	// meshName and additionalParams allows the actor descriptions to 
	// be changed from the ones in the .scene file
	std::vector<Actor*> loadDelayedActors(
		const Ogre::String& oldType, 
		const Ogre::String& newType, 
		const Ogre::String& meshName = Ogre::StringUtil::BLANK, 
		StringInterface* additionalParams = nullptr);
	bool toggleStaticGeometryBoundingBoxes();
	void updateStaticGeometryLightList();

	NODISCARD Ogre::StaticGeometry* getStaticGeometry() const NOEXCEPT 
	{
		return mStaticGeometry; 
	}

private:
	typedef std::unordered_map<Ogre::String, std::vector<ActorClass>> 
		DelayedActorDescriptionMap;

	inline void delayActor(ActorClass&& desc);
	void reserveSpace(rapidxml::xml_node<>* XMLNode, 
		const Ogre::String& attributeName);

    void processScene(rapidxml::xml_node<>* XMLRoot);
    void processSettings(rapidxml::xml_node<>* XMLRoot);
    void processActors(rapidxml::xml_node<>* XMLNode);
    NODISCARD ActorClass processActor(rapidxml::xml_node<>* XMLNode);
	void processParams(rapidxml::xml_node<>* XMLNode, ActorClass& desc);
	void processChildren(rapidxml::xml_node<>* XMLNode, ActorClass& desc);
	void processLights(rapidxml::xml_node<>* XMLNode);
	void processLight(rapidxml::xml_node<>* XMLNode);
	void processParticles(rapidxml::xml_node<>* XMLNode);
	void processParticle(rapidxml::xml_node<>* XMLNode);
    void processCamera(rapidxml::xml_node<>* XMLNode);

	void processStaticGeometry(rapidxml::xml_node<>* XMLGeometryRoot);
	void processBatches(rapidxml::xml_node<>* XMLNode);
	void processBatch(rapidxml::xml_node<>* XMLNode);
	void processEntity(rapidxml::xml_node<>* XMLNode, Ogre::Entity* entity);

	void unloadScene();

    NODISCARD const Ogre::String getAttribute(rapidxml::xml_node<>* XMLNode, 
		const Ogre::String& name) const;
    NODISCARD const Ogre::Real getAttributeReal(rapidxml::xml_node<>* XMLNode, 
		const Ogre::String& name) const;
    NODISCARD const bool getAttributeBool(rapidxml::xml_node<>* XMLNode, 
		const Ogre::String& name) const;
	NODISCARD const unsigned int getAttributeUnsignedInt(rapidxml::xml_node<>* XMLNode, 
		const Ogre::String& name) const;

	// have to be returned by value
    NODISCARD Ogre::Vector3 parseVector3(rapidxml::xml_node<>* XMLNode) const;
    NODISCARD Ogre::Quaternion parseQuaternion(rapidxml::xml_node<>* XMLNode) const;
    NODISCARD Ogre::ColourValue parseColour(rapidxml::xml_node<>* XMLNode) const;
	NODISCARD Ogre::Vector4 parseAttenuation(rapidxml::xml_node<>* XMLNode) const;

	rapidxml::xml_node<>* mXMLRoot;
	rapidxml::xml_document<> mXMLDoc;

	std::vector<Ogre::Entity*> mBatchEntities;
	DelayedActorDescriptionMap mDelayedActorDescriptions;
    Ogre::SceneNode* mRootSceneNode;
	Ogre::String mFilename;
	ActorCreator* mCreator;
	bool mCreateDebugEntities;
	bool mBoundingBoxesVisible;

	Ogre::StaticGeometry* mStaticGeometry;
protected:
};

#include "HeaderSuffix.h"

#endif