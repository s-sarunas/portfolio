// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef REGION_H
#define REGION_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "StaticActor.h"
#include "Collidable.h"
#include "Debuggable.h"

#define REGION_DEBUG_MESH_NAME "region.mesh"

class Region: public StaticActor, public Collidable, public Debuggable
{
public:
	Region(const ActorClass& desc, const Ogre::Vector3& min, 
		const Ogre::Vector3& max);
	~Region();

	// Actor.h
	NODISCARD const Ogre::AxisAlignedBox& getBoundingBox() override 
	{
		return mAxisAlignedBox; 
	}
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{
		return ACTOR_REGION_TYPE_NAME;
	}

	// Collidable.h
	void handleCollision(Collidable* collidable, bool isColliding) override;

private:
	Ogre::AxisAlignedBox mAxisAlignedBox;

protected:
	// Debuggable.h
	void initDebuggable() override;
};

class NODISCARD RegionFactory: public ActorFactory
{
public:
	RegionFactory() {}
	~RegionFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override
	{ 
		return ACTOR_REGION_TYPE_NAME;
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return ACTOR_REGION_TYPE_FLAG;
	}
};

#include "HeaderSuffix.h"

#endif