// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef ABILITY_H
#define ABILITY_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "InputHandler.h"

#define ABILITY_PHASE_DISABLED NULL

template <typename Ty>
struct ability_traits {}; // primary traits template
 
class Ability: public InputHandler
{
public:
	typedef Ogre::ushort PhaseIndex;

	Ability(Paddle* user, PhaseIndex numPhases);
	virtual ~Ability();

	virtual bool process(const Ogre::Real deltaSeconds) = 0;
	NODISCARD virtual const Ogre::String& getName() const NOEXCEPT = 0;

	NODISCARD bool isActive() const NOEXCEPT 
	{ 
		return !(mCurrentPhase == ABILITY_PHASE_DISABLED);
	}

private:
	PhaseIndex mCurrentPhase;
	const PhaseIndex mMaxPhases;

	Paddle* mUserPaddle;

protected:
	// do not call directly
	virtual void _phaseChanged(const PhaseIndex newPhase) = 0; 
	void setPhase(const PhaseIndex phase);
	void nextPhase();
	void previousPhase();

	NODISCARD const PhaseIndex getCurrentPhase() const NOEXCEPT 
	{ 
		return mCurrentPhase;
	}
	NODISCARD const PhaseIndex getMaxPhases() const NOEXCEPT 
	{
		return mMaxPhases; 
	}
	NODISCARD Paddle* getUser() const
	{
		assert(mUserPaddle && 
			"user paddle is not valid");

		return mUserPaddle;
	}
};

#include "HeaderSuffix.h"

#endif