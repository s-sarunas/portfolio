// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef SCOREBOARD_H
#define SCOREBOARD_H

#include "HeaderPrefix.h"

// (top, left, bottom, right) in pixels
#define SCOREBOARD_DRAW_RECTANGLE_POSITION 1, 37, 57, 66 
#define SCOREBOARD_SPACE_WIDTH 16
#define SCOREBOARD_ROTATION_SPEED_SCALAR 500
#define SCOREBOARD_TEXT_COLOR Ogre::ColourValue::Blue
#define SCOREBOARD_GAP 0.2f 

#define SCOREBOARD_FONT_NAME "Font/Scoreboard"
#define SCOREBOARD_MESH_NAME "scoreboard.mesh"
#define SCOREBOARD_UNKNOWN_GLYPH_CHAR '?'

// headers defining base classes
#include "StaticActor.h"

class Scoreboard: public StaticActor, public Ogre::Camera::Listener
{
public:
	Scoreboard(const ActorClass& desc);
	~Scoreboard();

	// Actor.h
	void update(const Ogre::Real deltaSeconds) override;
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{ 
		return ACTOR_SCOREBOARD_TYPE_NAME;
	}

	void setText(const Ogre::String& str);

private:
	void updateText();
	void updateTransforms();
	inline void updateOrientation();

	static Ogre::Real msScreenOffsetX;
	NODISCARD static const Ogre::Real getNextScreenOffset() NOEXCEPT 
	{ 
		msScreenOffsetX += SCOREBOARD_GAP; 
		return msScreenOffsetX; 
	}

	static void reduceScreenOffset() NOEXCEPT
	{ 
		msScreenOffsetX -= SCOREBOARD_GAP; 
	}

	// middle of the screen is (0, 0), top left - (-1, -1), bottom right - (1, 1)
	Ogre::Vector2 mScreenPosition;
	Ogre::String mText;
	Ogre::Matrix4 mLastCameraTransforms;
	Ogre::TextureManager* mTextureManager;
	Ogre::FontPtr mFont;
	Ogre::TexturePtr mTextTexture;
	Ogre::TexturePtr mFontTexture;
	Ogre::HardwarePixelBufferSharedPtr mFontBuffer;
	Ogre::HardwarePixelBufferSharedPtr mDestBuffer;
	Ogre::Image::Box mTextDrawRectangle;
	Ogre::Degree mAngle;
	bool mRotating;
	bool mTextDirty;

protected:
	// Paddle.h
	void scored(const unsigned int newScore);

	// Ogre::Camera::Listener
    void cameraPreRenderScene(Ogre::Camera* cam) override;
    void cameraPostRenderScene(Ogre::Camera* cam) override;
    void cameraDestroyed(Ogre::Camera* cam) override;

};

class NODISCARD ScoreboardFactory: public ActorFactory
{
public:
	ScoreboardFactory() {}
	~ScoreboardFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{ 
		return ACTOR_SCOREBOARD_TYPE_NAME; 
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return NULL; 
	}
};

#include "HeaderSuffix.h"

#endif

