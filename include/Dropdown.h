// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DROPDOWN_H
#define DROPDOWN_H

#include "HeaderPrefix.h"

#define DROPDOWN_WIDTH 192.0f
#define DROPDOWN_HEIGHT 64.0f

#define DROPDOWN_TEMPLATE_NAME "Menus/Dropdown"
#define DROPDOWN_CAPTION_SUFFIX "/DropdownCaption"
#define DROPDOWN_SMALLBOX_SUFFIX "/DropdownSmallBox"
#define DROPDOWN_SMALLTEXTAREA_SUFFIX "/DropdownSmallBox/DropdownSmallText"
#define DROPDOWN_EXPANDEDBOX_SUFFIX "/DropdownExpandedBox"
#define DROPDOWN_ITEM_TEMPLATE_NAME "Menus/DropdownItem"
#define DROPDOWN_ITEM_TEMPLATE_TYPE_NAME "BorderPanel"
#define DROPDOWN_ITEM_TEMPLATE_INSTANCE_INFIX "/Item/"
#define DROPDOWN_ITEM_TEXTAREA_SUFFIX "/DropdownItemText"
#define DROPDOWN_MATERIAL_UP "Menus/MiniTextBox"
#define DROPDOWN_MATERIAL_DOWN "Menus/MiniTextBox/Over"

// headers defining base classes
#include "Widget.h"

class Dropdown: public Widget
{
	typedef std::vector<
		std::pair<
			Ogre::BorderPanelOverlayElement*, 
			Ogre::TextAreaOverlayElement*>
		> ItemList;
public:
	Dropdown(const Ogre::String& name, const Ogre::String& menuName, 
		const Ogre::DisplayString& caption, Ogre::OverlayContainer* tray,
		const Ogre::Real top, const std::initializer_list<Ogre::DisplayString> items, 
		const ItemList::size_type itemIndex, const Widget::size_type widgetIndex, 
		SimpleMenu::Listener* listener);
	~Dropdown();

	NODISCARD ItemList::size_type getValue() const;
	NODISCARD const Ogre::DisplayString& getValueCaption() const NOEXCEPT;

	// Widget.h
	void reset() override;
	NODISCARD const Ogre::String& getType() const NOEXCEPT override
	{ 
		return WIDGET_TYPE_DROPDOWN;
	}

	// InputHandler.h
    bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
	bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;

private:
	enum State
	{
		DROPDOWN_STATE_UP,
		DROPDOWN_STATE_OVER
	};

	void setItems(const std::initializer_list<Ogre::DisplayString> captions, 
		const Widget::size_type defaultIndex);
	void setState(Ogre::BorderPanelOverlayElement* element, const State state);
	NODISCARD inline const State getState(Ogre::BorderPanelOverlayElement* element) const NOEXCEPT;
	void selectActiveItem();
	void setActiveItem(const ItemList::value_type& newElement);

	Ogre::OverlayContainer* mContainer;
	Ogre::BorderPanelOverlayElement* mSmallBox;
	Ogre::TextAreaOverlayElement* mSmallTextArea;
	Ogre::BorderPanelOverlayElement* mExpandedBox;
	ItemList mItems;
	ItemList::value_type mActiveItem;
	State mState;

protected:
};

#include "HeaderSuffix.h"

#endif