// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef BUTTON_H
#define BUTTON_H

#include "HeaderPrefix.h"

#define BUTTON_WIDTH 128.0f
#define BUTTON_HEIGHT 32.0f

#define BUTTON_TEMPLATE_NAME "Menus/Button"
#define BUTTON_CAPTION_SUFFIX "/ButtonCaption"
#define BUTTON_MATERIAL_OVER_NAME "Menus/Button/Over"
#define BUTTON_MATERIAL_UP_NAME "Menus/Button/Up"
#define BUTTON_MATERIAL_DOWN_NAME "Menus/Button/Down"

// headers defining base classes
#include "Widget.h"

class Button: public Widget
{
public:
	Button(const Ogre::String& name, const Ogre::String& menuName, 
		const Ogre::DisplayString& caption, Ogre::OverlayContainer* tray, 
		const Ogre::Real top, const Widget::size_type widgetIndex, 
		SimpleMenu::Listener* listener);
	~Button();

	// Widget.h
	void reset() override { setState(BUTTON_STATE_UP); }
	NODISCARD const Ogre::String& getType() const NOEXCEPT override
	{ 
		return WIDGET_TYPE_BUTTON;
	}

	// InputHandler.h
    bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
	bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;

private:
	enum State 
	{
		BUTTON_STATE_UP,
		BUTTON_STATE_OVER,
		BUTTON_STATE_DOWN
	};

	void setState(const State state);

	State mState;

protected:
};

#include "HeaderSuffix.h"

#endif