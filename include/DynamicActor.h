// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DYNAMIC_ACTOR_H
#define DYNAMIC_ACTOR_H

#include "HeaderPrefix.h"

#include "Ray.h"
#include "GameMath.h"

// headers defining base classes
#include "Actor.h"
#include "Collidable.h"

/*
	This struct is used as a fill-in class and is passed to DynamicActor's 
	constructor, this allows actor's parameters to be set in any order (especially
	the optional parameters), avoids changes to DynamicActor's constructor and 
	the DynamicClass object can be passed to different interfaces for processing

	Same is done with ActorClass and Actor
*/

struct DynamicClass
{
	DynamicClass();
	DynamicClass(Ogre::Real movementSpeed, bool autoMove, 
		const Ogre::Vector3& localDirection = UNIT_VECTOR_FORWARD,
		Ogre::Real speedMultiplier = NULL, bool canMove = true);
	~DynamicClass();

	DynamicClass(const DynamicClass& rhs);
	DynamicClass& operator=(const DynamicClass&) = default;

	// no move operations necessary - no movable types

	Ogre::Vector3 mLocalDirection;
	Ogre::Real mMovementSpeed;
	Ogre::Real mSpeedEquationMultiplier;
	bool mAutoMove;
	bool mCanMove;
};

// the lower the coefficient - the smaller the increase in speed - the longer
// the game
#define DYNAMIC_ACTOR_SPEED_COEFFICIENT 0.005f

// used alongside Ogre::Vector3::positionEquals to indicate the closeness 
// required for two positions to be considered equal
#define DYNAMIC_ACTOR_POSITION_TOLERANCE 0.5f

enum StatusEffects
{
	STATUS_EFFECT_STUNNED,
	STATUS_EFFECT_CASTING,
};

enum TransformSpace
{
	TRANSFORM_SPACE_LOCAL,
	TRANSFORM_SPACE_WORLD
};

class DynamicActor: virtual public Actor, public Collidable
{
	friend CollisionManager;
public:
	class Listener
	{
	public:
		Listener() {}
		virtual ~Listener() {}

		virtual void actorMoved(DynamicActor* actor) = 0;
	};
	typedef std::set<Listener*> ListenerPtrList;

	void addListener(Listener* listener) { mListeners.insert(listener); }
	void removeListener(Listener* listener) NOEXCEPT { mListeners.erase(listener); }

	class StatusEffect
	{
	public:
		template <typename DurTy>
		StatusEffect(DurTy&& duration, StatusEffects status) :
			mTimer(std::forward<DurTy>(duration)), mStatus(status) {}
		~StatusEffect() {}

		inline void update(const Ogre::Real deltaSeconds) NOEXCEPT 
		{ 
			mTimer.update(deltaSeconds);
		}
		NODISCARD inline const bool ended() NOEXCEPT 
		{ 
			return mTimer.hasEnded(); 
		}

		NODISCARD const eOgre::CountdownTimer& getTimer() const NOEXCEPT
		{ 
			return mTimer;
		}
		NODISCARD const StatusEffects getStatus() const NOEXCEPT
		{ 
			return mStatus;
		}

	private:
		eOgre::CountdownTimer mTimer;
		StatusEffects mStatus;
	};

	typedef std::vector<StatusEffect> StatusEffectList;

	bool moveTo(const Ogre::Real deltaSeconds, const Ogre::Vector3& pos, 
		bool forceMove = false);
	void move(const Ogre::Real deltaSeconds, const Ogre::Vector3& direction, 
		const TransformSpace relativeTo = TRANSFORM_SPACE_LOCAL, 
		bool forceMove = false);
	void move(const Ogre::Real deltaSeconds, bool forceMove = false);
	void reflect(Collidable* collidable);
	void reflect(DynamicActor* dynamicActor,
		const Ogre::Degree& degreesPerUnit);
	void stepBack();
	void incrementSpeed() NOEXCEPT;

	template <typename DurTy>
	void applyStatusEffect(DurTy&& duration, StatusEffects status)
	{
		switch (status)
		{
		case STATUS_EFFECT_STUNNED:
		case STATUS_EFFECT_CASTING:
			stop();
			break;
		default:
			break;
		}

		mStatusEffects.emplace_back(std::forward<DurTy>(duration), status);
	}

	// virtual
	virtual void translate(const Ogre::Vector3& vector);
	virtual void setPosition(const Ogre::Vector3& vector);
	virtual void setOrientation(const Ogre::Quaternion& orientation);

	// Actor.h
	void update(const Ogre::Real deltaSeconds) override;
	void reset() override;
	NODISCARD const Ogre::String& getActorGroup() const NOEXCEPT final 
	{ 
		return ACTOR_DYNAMIC_GROUP_NAME; 
	}

	// Collidable.h
	void handleCollision(Collidable* collidable, bool isColliding) override;

	void setDirection(const Ogre::Vector3& direction, 
		const TransformSpace relativeTo = TRANSFORM_SPACE_LOCAL) NOEXCEPT;
	void setMovementSpeed(const Ogre::Real speed)
	{ 
		mMovementSpeed = speed; 
		mSpeedDirty = true; 
		speedChanged(); 
	}
	void setSpeedMultiplier(Ogre::Real multiplier) NOEXCEPT 
	{
		mSpeedEquationMultiplier = multiplier;
	}
	// set whether the actor should automatically move in the direction 
	// provided (useful for analogue input or actors that do not change direction
	void setAutoMove(bool autoMove) NOEXCEPT 
	{ 
		mAutoMove = autoMove;
	}

	void addToStep(const Ogre::Vector3& vector) NOEXCEPT 
	{ 
		mStep += vector; 
	}
	void addMovementSpeed(const Ogre::Real amount)
	{
		mMovementSpeed += amount;
		mSpeedDirty = true; 
		speedChanged();
	}
	NODISCARD const bool canMove() const NOEXCEPT 
	{
		return mCanMove;
	}
	void stop() NOEXCEPT 
	{ 
		mCanMove = false;
		mStep = Ogre::Vector3::ZERO;
	}
	void start() NOEXCEPT 
	{
		mCanMove = true; 
	}

	NODISCARD const Ogre::Vector3& getVelocity() const NOEXCEPT
	{
		return mVelocity;
	}
	// in world space
	NODISCARD const Ogre::Vector3& getWorldDirection() const NOEXCEPT 
	{
		return mWorldDirection;
	}
	NODISCARD const Ogre::Vector3 getForward() const NOEXCEPT
	{
		return getNode()->getOrientation() * UNIT_VECTOR_FORWARD;
	}
	NODISCARD const Ogre::Vector3 getRight() const NOEXCEPT 
	{ 
		return getNode()->getOrientation() * UNIT_VECTOR_RIGHT;
	}
	NODISCARD const Ogre::Vector3 getLeft() const NOEXCEPT 
	{ 
		return -getRight(); 
	}
	NODISCARD const Ogre::Vector3 getUp() const NOEXCEPT 
	{
		return getNode()->getOrientation() * UNIT_VECTOR_UP;
	}
	NODISCARD const Ogre::Vector3& getStep() const NOEXCEPT 
	{ 
		return mStep;
	}
	NODISCARD const StatusEffectList& getStatusEffects() const NOEXCEPT 
	{
		return mStatusEffects;
	}
	NODISCARD const Ogre::Real getMovementSpeed() const NOEXCEPT 
	{ 
		return mMovementSpeed;
	}
	NODISCARD const Ogre::Real getSpeedMultiplier() const NOEXCEPT 
	{ 
		return mSpeedEquationMultiplier;
	}
	NODISCARD const Ogre::Real getActualSpeed() NOEXCEPT 
	{ 
		recalculateSpeed(); 
		return mActualSpeed; 
	}
	NODISCARD const bool getAutoMove() const NOEXCEPT 
	{ 
		return mAutoMove;
	}

protected:
	DynamicActor(const ActorClass& desc, const DynamicClass& dyn);
	virtual ~DynamicActor(); 

	DynamicActor(const DynamicActor& rhs, Ogre::SceneNode* parentNode = nullptr);
	DynamicActor& operator=(const DynamicActor& rhs);
	DynamicActor(DynamicActor&& rhs);
	DynamicActor& operator=(DynamicActor&& rhs) NOEXCEPT;

	// virtual 
	virtual void speedChanged() {};

	// returns the scaled speed given the speed base for this actor, 
	// used for comparison purposes
	NODISCARD Ogre::Real recalculateSpeed(const Ogre::Real speedBase) NOEXCEPT;
	void resetParameters() NOEXCEPT;

	NODISCARD const DynamicClass& getDefaultValues() const NOEXCEPT 
	{ 
		return mDefaultValues; 
	}

private:
	DynamicActor& operator=(const DynamicClass& rhs) NOEXCEPT;

	void recalculateSpeed() NOEXCEPT;
	void recalculateVelocity() NOEXCEPT;
	void recalculateDirection() NOEXCEPT;
	void stepForward(const Ogre::Real deltaSeconds);
	void setSceneNodeDirection(const Ogre::Vector3& direction);
	void notifyActorMoved(DynamicActor* actor);

	Ogre::Vector3 mVelocity;
	Ogre::Vector3 mWorldDirection;
	/*
		Quadratic increase: 
			ax^2 + bx + c => 
			DYNAMIC_ACTOR_SPEED_COEFFICIENT * mSpeedEquationMultiplier^2 + mMovementSpeed
		Square root would be a much better function to use, 
		however: (sqrt >10 times slower than multiplication) * number of actors

		NOTE: This will only calculate the scale value, the base speed 
		must be added to the result, in other words this will only return 
		ax^2 + bx of the quadratic. This is because this way it works for both, 
		speed and velocity: 
			scaledVelocity = baseVelocity * (1.0f + scale value), 
			scaledSpeed = baseSpeed (+/-) scale value 
	*/
	Math::Quadratic<> mSpeedEquation;
	StatusEffectList mStatusEffects;
	Ogre::Vector3 mStep;
	Ogre::Real mMovementSpeed;
	Ogre::Real mSpeedEquationMultiplier;
	Ogre::Real mActualSpeed;
	bool mSpeedDirty;
	bool mVelocityDirty;
	bool mAutoMove;
	bool mCanMove;

	const DynamicClass mDefaultValues;

	ListenerPtrList mListeners;
};

#include "HeaderSuffix.h"

#endif