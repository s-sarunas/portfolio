// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef ANIMATIONSTATEWRAPPER_H
#define ANIMATIONSTATEWRAPPER_H

#include "HeaderPrefix.h"

#define ANIMATION_STATE_WRAPPER_SPEED_FADE_IN 2.0f

#define ANIMATION_STATE_WRAPPER_PREFIX_TOP "top"
#define ANIMATION_STATE_WRAPPER_PREFIX_BOT "bot"

enum AnimationSection
{
	ANIMATION_COMPLETE,
	ANIMATION_TOP,
	ANIMATION_BOTTOM
};

// extends Ogre::AnimationState to blend, inverse and provide helper functions 
class AnimationStateWrapper
{
public:
	AnimationStateWrapper(Ogre::Entity* parentEntity, AnimationSection animSection,
		const Ogre::String& startingAnimation = Ogre::StringUtil::BLANK,
		bool loop = true, bool inverse = false);
	~AnimationStateWrapper();

	AnimationStateWrapper(const AnimationStateWrapper& rhs) = delete;
	AnimationStateWrapper(const AnimationStateWrapper& rhs, 
		Ogre::Entity* parentEntity) = delete;
	AnimationStateWrapper& operator=(const AnimationStateWrapper& rhs) = delete;
	AnimationStateWrapper(AnimationStateWrapper&& rhs) NOEXCEPT;
	AnimationStateWrapper& operator=(AnimationStateWrapper&& rhs) NOEXCEPT;

	NODISCARD Ogre::AnimationState* get() NOEXCEPT { return mAnimationState; }
	NODISCARD const Ogre::AnimationState* get() const NOEXCEPT { return mAnimationState; }

	void update(Ogre::Real deltaSeconds) NOEXCEPT;
	// does not reset the blending parameters
	void reset() NOEXCEPT; 

	// swaps transforms of two bones - unused
	/*void inverseBoneTransforms(const Ogre::String& firstBoneName, 
		const Ogre::String& secondBoneName);*/

	void changeTo(const Ogre::String& name, bool loop = true,
		bool isInversed = false, bool blendIn = false);
	// NOTE: the animations should be symmetrical or the forward and 
	// backward animations should be separate
	NODISCARD bool hasHalfEnded() const NOEXCEPT;
	NODISCARD bool hasEnded() const NOEXCEPT;

	NODISCARD const bool getInversed() const NOEXCEPT { return mInverse; }
	NODISCARD const bool getBlendIn() const NOEXCEPT { return mBlend; }

private:
	NODISCARD Ogre::AnimationState* init(const Ogre::String& name, 
		bool loop, bool inverse, bool blendIn);

	// blends the animations together while they're both running; 
	// blend speed is based on a constant
	void progressBlending(const Ogre::Real deltaSeconds) NOEXCEPT;

	struct PreviousStateData
	{
		// default constructor
		PreviousStateData()
			: mState(nullptr),
			  mInversed(false),
			  mBlended(false) {}

		// constructor from an object of AnimationStateWrapper
		PreviousStateData(const AnimationStateWrapper& other)
			: mState(other.mAnimationState),
			  mInversed(other.mInverse),
			  mBlended(other.mBlend) {}

		~PreviousStateData() {}

		PreviousStateData(const PreviousStateData& rhs) = delete;
		PreviousStateData(const PreviousStateData& rhs, 
			Ogre::Entity* parentEntity) = delete;
		PreviousStateData& operator=(const PreviousStateData& rhs) = delete;
		
		PreviousStateData(PreviousStateData&& rhs) NOEXCEPT
		{
			*this = std::move(rhs);
		}

		PreviousStateData& operator=(PreviousStateData&& rhs) NOEXCEPT
		{
			if (this != std::addressof(rhs))
			{
				// call the base class move assignment operator 
				// ...

				// free existing resources of the source
				// ...

				// copy data from the source object
				mState = rhs.mState;
				mInversed = rhs.mInversed;
				mBlended = rhs.mBlended;

				// release pointer ownership from the source object so that 
				// the destructor does not free the memory multiple times
				// these below are not necessary since they're non-owning pointers
				rhs.mState = nullptr;
			}

			return *this;
		}

		NODISCARD Ogre::AnimationState* operator->() NOEXCEPT 
		{ 
			return mState;
		}
		NODISCARD const Ogre::AnimationState* operator->() const NOEXCEPT 
		{ 
			return mState; 
		}

		NODISCARD operator bool() const NOEXCEPT 
		{ 
			return (mState != nullptr); 
		}

		Ogre::AnimationState* mState;
		bool mInversed;
		bool mBlended;
	};
	PreviousStateData mPrevData;

	Ogre::Entity* mParentEntity;
	Ogre::AnimationState* mAnimationState;
	AnimationSection mSection;
	bool mInverse;
	bool mBlend;

protected:
};

#include "HeaderSuffix.h"

#endif