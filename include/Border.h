// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef BORDER_H
#define BORDER_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "StaticActor.h"
#include "Collidable.h"

#define BORDER_CLONER_DUPLICATES 1U

// using strings instead of enums, it simplifies argument passing and also 
// ideally the border type should be in the xml, hence as a string
static const Ogre::String BORDER_TYPE_NORMAL = "normal";
static const Ogre::String BORDER_TYPE_CLONER = "cloner";

class Border: public StaticActor, public Collidable
{
public:
	Border(const ActorClass& desc, const Ogre::String& type);
	~Border();

	// Actor.h
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{
		return ACTOR_BORDER_TYPE_NAME;
	}

	// Collidable.h
	void handleCollision(Collidable* collidable, bool isColliding) override;

	NODISCARD const Ogre::String& getBorderType() const NOEXCEPT { return mType; }

private:
	Ogre::String mType;

protected:
};

class NODISCARD BorderFactory: public ActorFactory
{
public:
	BorderFactory() {}
	~BorderFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{
		return ACTOR_BORDER_TYPE_NAME;
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return ACTOR_BORDER_TYPE_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif