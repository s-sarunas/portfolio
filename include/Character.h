// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef CHARACTER_H
#define CHARACTER_H

#include "HeaderPrefix.h"

#include "Weapon.h"

// headers defining base classes
#include "StaticActor.h"
#include "Animated.h"
#include "Collidable.h"

#define CHARACTER_MESH_FILENAME "fox.mesh"
#define CHARACTER_ANIMATION_BOT_STAND_NAME "botStand"
#define CHARACTER_ANIMATION_BOT_RUN_NAME "botRunForward"
#define CHARACTER_ANIMATION_TOP_STAND_NAME "topStand"
#define CHARACTER_ANIMATION_TOP_EQUIP_NAME "topEquip"
#define CHARACTER_ANIMATION_TOP_SHOOT_NAME "topShoot"
#define CHARACTER_BONE_HANDLE_NAME "handle"
#define CHARACTER_BONE_SHEATH_NAME "sheath"

class Character: public StaticActor, public Collidable, public Animated<2>
{
public:
	Character(const ActorClass& desc, const Ogre::String& weaponFilename);
	~Character();

	Character(const Character& rhs, Ogre::Node* parentNode = nullptr) = delete;
	Character& operator=(const Character&) = delete;
	Character(Character&& rhs);
	Character& operator=(Character&& rhs);

	// Actor.h
	void update(const Ogre::Real deltaSeconds) override;
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{ 
		return ACTOR_CHARACTER_TYPE_NAME;
	}

	// Collidable.h
	void handleCollision(Collidable* collidable, bool isColliding) override;

	NODISCARD Weapon* getWeapon() const NOEXCEPT { return mWeapon; }

private:
	NODISCARD const Ogre::Vector3 getBonePosition(const Ogre::String& boneName) const;

	// raw pointer access to child actors
	Weapon* mWeapon;

protected:
};

class NODISCARD CharacterFactory: public ActorFactory
{
public:
	CharacterFactory() {}
	~CharacterFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override
	{
		return ACTOR_CHARACTER_TYPE_NAME; 
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{
		return ACTOR_CHARACTER_TYPE_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif