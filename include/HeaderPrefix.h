// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#include "GamePrerequisites.h"

#pragma warning (push)

// 'class1' : inherits 'class2::member' via dominance
// Base class Actor is virtual, StaticActor and DynamicActor override its member 
// functions directly, making them the dominant classes in the hierarchy. 
// Siblings to StaticActor or DynamicActor such as mixins Collidable, Animated 
// etc. do not override Actor's members, therefore StaticActor or DynamicActor's
// overrides are chosen by dominance, instead of being ambiguous, which triggers
// this warning
// This is a desirable feature
#pragma warning (disable : 4250)