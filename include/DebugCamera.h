// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DEBUG_CAMERA_H
#define DEBUG_CAMERA_H

#include "HeaderPrefix.h"

#define DEBUG_CAMERA_SPEED_ACCELERATE 1500.0f 
#define DEBUG_CAMERA_SPEED_MAX_SQUARED 40000.0f
#define DEBUG_CAMERA_SPEED_ROTATION 0.15f
#define DEBUG_CAMERA_SPEED_DECELERATE_PERCENT 14.0f
#define DEBUG_CAMERA_SPEED_STOP_THRESHOLD_SQUARED 0.09f

// headers defining base classes
#include "InputHandler.h"

class DebugCamera: public InputHandler
{
public:
	DebugCamera();
	~DebugCamera();

	// delayed initialization
	void _init(); 
	void update(const Ogre::Real deltaSeconds) NOEXCEPT;
	void reset();

	void flipEnabled() NOEXCEPT { setEnabled(!mEnabled); }
	void setEnabled(bool enabled) NOEXCEPT;
	NODISCARD const bool getEnabled() const NOEXCEPT { return mEnabled; }

	// InputHandler.h
	bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
    bool keyReleased(const SDL_KeyboardEvent& arg) override;

private:
	enum Direction
	{
		DEBUG_CAMERA_FORWARD =		1 << 0,
		DEBUG_CAMERA_BACKWARD =		1 << 1,
		DEBUG_CAMERA_LEFT =			1 << 2,
		DEBUG_CAMERA_RIGHT =		1 << 3
	};

	void accelerate(const Ogre::Vector3& direction, 
		const Ogre::Real deltaSeconds) NOEXCEPT;
	void decelerate(const Ogre::Real deltaSeconds) NOEXCEPT;

	Ogre::Camera* mCamera;
	Ogre::Vector3 mDefaultPosition;
	Ogre::Vector3 mDefaultDirection;
	Ogre::Vector3 mVelocity;
	BitSet<int8_t> mDirectionFlags;
	bool mEnabled;

protected:
};

#include "HeaderSuffix.h"

#endif