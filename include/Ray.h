// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef RAY_H
#define RAY_H

#include "HeaderPrefix.h"

#include "Actor.h"

template <typename OriginType>
class Ray;

template <>
class Ray<Actor>
{
public:
	// adjustment vector is relative to the origin of the node
	Ray(const Actor* actor, 
		const Ogre::Vector3& direction = Ogre::Vector3::NEGATIVE_UNIT_Y);
	~Ray();

	Ray(const Ray& rhs) = delete;
	Ray(const Ray& rhs, Actor* newParentActor);
	Ray& operator=(const Ray& rhs) = delete;
	Ray(Ray&& rhs) NOEXCEPT;
	Ray& operator=(Ray&& rhs) NOEXCEPT;

	friend std::ostream& operator<<(std::ostream& os, const Ray<Actor>& r) NOEXCEPT;

	NODISCARD const Ogre::Real cast(const Ogre::Vector3& vert0, 
		const Ogre::Vector3& vert1, const Ogre::Vector3& vert2) const;
	NODISCARD const Ogre::Vector3 getPoint(Ogre::Real t) NOEXCEPT;

	NODISCARD const Ogre::String& getName() const NOEXCEPT
	{ 
		return mName; 
	}
	NODISCARD const Actor* getParentActor() const NOEXCEPT 
	{
		return mParentActor; 
	}
	NODISCARD const Ogre::Vector3 getOrigin() const NOEXCEPT 
	{ 
		return mParentActor->getNode()->getPosition() + mAdjustment;
	}
	NODISCARD const Ogre::Vector3& getDirection() const NOEXCEPT 
	{ 
		return mDirection; 
	}
	NODISCARD const Ogre::Vector3& getAdjustment() const NOEXCEPT 
	{ 
		return mAdjustment; 
	}

	void setDirection(const Ogre::Vector3& direction) NOEXCEPT 
	{ 
		mDirection = direction; 
	}

private:
	Ogre::String mName;
	const Actor* mParentActor;
	Ogre::Vector3 mDirection;
	const Ogre::Vector3 mAdjustment;

protected:
};

template <>
class Ray<Ogre::Node>
{
public:
	// adjustment vector is relative to the origin of the node
	Ray(const Ogre::Node* node, 
		const Ogre::Vector3& direction = Ogre::Vector3::NEGATIVE_UNIT_Y, 
		const Ogre::Vector3& adjustment = Ogre::Vector3::ZERO);
	~Ray();

	Ray(const Ray& rhs) = delete;
	Ray(const Ray& rhs, Ogre::Node* parentNode);
	Ray& operator=(const Ray& rhs) = delete;
	Ray(Ray&& rhs) NOEXCEPT;
	Ray& operator=(Ray&& rhs) NOEXCEPT;

	friend std::ostream& operator<<(std::ostream& os, const Ray<Ogre::Node>& r) NOEXCEPT;

	NODISCARD const Ogre::Real cast(const Ogre::Vector3& vert0, 
		const Ogre::Vector3& vert1, const Ogre::Vector3& vert2) const;
	NODISCARD const Ogre::Vector3 getPoint(Ogre::Real t) NOEXCEPT;

	NODISCARD const Ogre::String& getName() const NOEXCEPT 
	{ 
		return mName;
	}
	NODISCARD const Ogre::Node* getParent() const NOEXCEPT 
	{ 
		return mParent;
	}
	NODISCARD const Ogre::Vector3 getOrigin() const NOEXCEPT 
	{ 
		return mParent->getPosition() + mAdjustment; 
	}
	NODISCARD const Ogre::Vector3& getDirection() const NOEXCEPT 
	{
		return mDirection;
	}
	NODISCARD const Ogre::Vector3& getAdjustment() const NOEXCEPT 
	{ 
		return mAdjustment; 
	}

	void setDirection(const Ogre::Vector3& direction) NOEXCEPT 
	{ 
		mDirection = direction;
	}
	void setAdjustment(const Ogre::Vector3& adjustment) NOEXCEPT
	{ 
		mAdjustment = adjustment;
	}

private:
	Ogre::String mName;
	const Ogre::Node* mParent;
	Ogre::Vector3 mDirection;
	Ogre::Vector3 mAdjustment;

protected:
};

template <>
class Ray<Ogre::Vector3>
{
public:
	Ray(const Ogre::String& name, const Ogre::Vector3& origin, 
		const Ogre::Vector3& direction = Ogre::Vector3::NEGATIVE_UNIT_Y);
	~Ray();

	Ray(const Ray& rhs);
	Ray& operator=(const Ray& rhs);
	Ray(Ray&& rhs) NOEXCEPT;
	Ray& operator=(Ray&& rhs) NOEXCEPT;

	friend std::ostream& operator<<(std::ostream& os, const Ray<Ogre::Vector3>& r) NOEXCEPT;

	NODISCARD const Ogre::Real cast(const Ogre::Vector3& vert0, 
		const Ogre::Vector3& vert1, const Ogre::Vector3& vert2) const;
	NODISCARD const Ogre::Vector3 getPoint(Ogre::Real t) NOEXCEPT;

	NODISCARD const Ogre::String& getName() const NOEXCEPT 
	{ 
		return mName;
	}
	NODISCARD const Ogre::Vector3& getOrigin() const NOEXCEPT 
	{ 
		return mOrigin;
	}
	NODISCARD const Ogre::Vector3& getDirection() const NOEXCEPT 
	{
		return mDirection; 
	}

	void setOrigin(const Ogre::Vector3& origin) NOEXCEPT 
	{ 
		mOrigin = origin; 
	}
	void setDirection(const Ogre::Vector3& direction) NOEXCEPT 
	{
		mDirection = direction;
	}

private:
	Ogre::String mName;
	Ogre::Vector3 mOrigin;
	Ogre::Vector3 mDirection;

protected:
};

#include "HeaderSuffix.h"

#endif