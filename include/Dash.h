// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef DASH_H
#define DASH_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "Ability.h"
#include "CollisionManager.h"

#define DASH_DURATION_SECONDS 0.45f
#define DASH_SPEED_INITIAL_MIN 130.0f
#define DASH_SPEED_INITIAL_MAX 170.0f

template <>
struct ability_traits<Dash>
{
	// is only one instance of this ability allowed to run at one time
	typedef std::true_type is_unique; 
	static const/*expr*/ Ability::PhaseIndex num_phases = 3;
};

class Dash: public Ability, public CollisionManager::Listener
{
public:
	Dash(Paddle* user);
	~Dash();

	// Ability.h
	bool process(const Ogre::Real deltaSeconds) override;
	NODISCARD const Ogre::String& getName() const NOEXCEPT override 
	{ 
		return msName;
	}

private:
	// Ability.h
	void _phaseChanged(const PhaseIndex newPhase) override;

	NODISCARD Border* getBorder();
	void reverseDirection();

	static const Ogre::String msName;
	Ogre::Real mAcceleration;
	Ogre::Real mInitialMovementSpeed;
	Border* mInitialBorder;

protected:
	// CollisionManager::Listener
	void collisionOccured(Collidable* one, Collidable* two) override;
};

#include "HeaderSuffix.h"

#endif