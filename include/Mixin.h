// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef MIXIN_H
#define MIXIN_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "Actor.h"

class Mixin: virtual public Actor
{
public:
	void _needsUpdate() NOEXCEPT
	{
		getUpdateFlags().set(getMixinType());
	}

	NODISCARD const bool _isUpdatePending() const NOEXCEPT 
	{ 
		return getUpdateFlags().isSet(getMixinType());
	}

	void _updated() NOEXCEPT 
	{ 
		getUpdateFlags().clear(getMixinType());
	}

private:
protected:
	Mixin() {}
	virtual ~Mixin() {}

	NODISCARD virtual const MixinFlagType getMixinType() const NOEXCEPT = 0;
};

#include "HeaderSuffix.h"

#endif