// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef STATIC_ACTOR_H
#define STATIC_ACTOR_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "Actor.h"

class StaticActor: virtual public Actor
{
public:
	StaticActor(const ActorClass& desc);
	virtual ~StaticActor();

	StaticActor(const StaticActor&) = default;
	StaticActor& operator=(const StaticActor&) = default;
	StaticActor(StaticActor&& rhs) NOEXCEPT;
	StaticActor& operator=(StaticActor&& rhs);

	// Actor.h
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{
		return ACTOR_DEBUG_TYPE_NAME;
	}
	NODISCARD const Ogre::String& getActorGroup() const NOEXCEPT final 
	{
		return ACTOR_STATIC_GROUP_NAME;
	}

private:
protected:
};

class NODISCARD StaticFactory: public ActorFactory
{
public:
	StaticFactory() {}
	~StaticFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc) override;

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{ 
		return ACTOR_STATIC_GROUP_NAME;
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override
	{ 
		return ACTOR_STATIC_TYPE_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif