// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef ABILITIESMANAGER_H
#define ABILITIESMANAGER_H

#include "HeaderPrefix.h"

#include "Shoot.h"
#include "Dash.h"

// headers defining bases classes
#include "InputHandler.h"

class AbilitiesManager: public InputHandler
{
public:
	AbilitiesManager();
	~AbilitiesManager();

	void update(const Ogre::Real deltaSeconds);

	template <typename Ty, typename... TArgs>
	void useAbility(TArgs&&... args) // forwarding reference
	{
		static_assert(std::is_base_of<Ability, Ty>::value, 
			"an ability used does not derive from pure virtual class Ability");

		if (isReady<Ty>())
		{
			mActiveAbilities.push_back(
				std::make_shared<Ty>(std::forward<TArgs>(args)...));
		}
	}

	// check if the ability is already active or isn't unique
	template <typename K>
	NODISCARD bool isReady()
	{
		if (std::is_same<
				ability_traits<K>::is_unique, 
				std::false_type
			>::value || 
			mActiveAbilities.empty())
		{ // ability is not unique or no abilities active - ready
			return true; 
		}
		else 
		{ // ability is unique
			for (const auto& ability : mActiveAbilities)
			{
				if (std::dynamic_pointer_cast<K>(ability))
				{ // active ability found - not ready
					return false; 
				}
			}

			// no active ability found - ready
			return true; 
		}
	}

	template <typename Func>
	void for_each_active_ability(Func function) NOEXCEPT_COND(
		std::is_nothrow_invocable<Func, Ability*>::value) // C++17 only
	{
		if (!mActiveAbilities.empty())
		{
			for (auto& ability : mActiveAbilities)
			{
				// all abilities in the vector are valid since they are 
				// in a unique_ptr, no need to check
				function(ability.get());
			}
		}
	}

	// InputHandler.h
    bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool textInput(const SDL_TextInputEvent& arg) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
    bool keyReleased(const SDL_KeyboardEvent& arg) override;
    bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;

private:
	typedef std::vector<std::shared_ptr<Ability>> AbilityPtrList;
	AbilityPtrList mActiveAbilities;

protected:
};

#include "HeaderSuffix.h"

#endif