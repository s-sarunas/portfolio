// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef AI_H
#define AI_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "Paddle.h"

class AI: public Paddle
{
public: 
	enum Difficulty
	{
		DIFFICULTY_EASY,
		DIFFICULTY_MEDIUM,
		DIFFICULTY_HARD
	};

	AI(const ActorClass& desc);
	~AI();

	AI(const AI& rhs) = delete;
	AI& operator=(const AI&) = delete;
	AI(AI&& rhs);
	AI& operator=(AI&& rhs);

	void updateEndPoint(const Ball* currentBall) NOEXCEPT;
	NODISCARD const Difficulty getDifficulty() const NOEXCEPT 
	{ 
		return mSettings->getDifficulty();
	}

	// Paddle.h
	NODISCARD bool isAI() NOEXCEPT override { return true; }

	// Actor.h
	void update(const Ogre::Real deltaSeconds) override;
	void reset() override;
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{ 
		return ACTOR_AI_TYPE_NAME; 
	}

	// Collidable.h
	void handleCollision(Collidable* collidable, bool isColliding) override;

private:
	typedef eOgre::CountdownTimer::duration_type TimerDurationType;

	struct NODISCARD Settings
	{
		typedef std::pair<Ogre::Real, Ogre::Real> RealProperty;
		typedef std::pair<TimerDurationType, TimerDurationType> TimeProperty;

		Settings(const Difficulty difficulty, 
			const RealProperty& speed, 
			const RealProperty& idleSpeed, 
			const RealProperty& hitOffset, 
			const TimeProperty& reactionTime) 
		  : mDifficulty(difficulty), mSpeed(speed), mIdleSpeed(idleSpeed), 
			mHitOffset(hitOffset), mReactionTime(reactionTime) {};
		~Settings() = default;

		NODISCARD const Difficulty getDifficulty() const NOEXCEPT 
		{ 
			return mDifficulty;
		}
		NODISCARD const Ogre::Real getRandomSpeed() const NOEXCEPT
		{
			return getRandom(mSpeed);
		}
		NODISCARD const Ogre::Real getRandomIdleSpeed() const NOEXCEPT 
		{ 
			return getRandom(mIdleSpeed);
		}
		NODISCARD const Ogre::Real getRandomHitOffset() const NOEXCEPT 
		{ 
			return getRandom(mHitOffset); 
		}
		NODISCARD const TimerDurationType getRandomReactionTime() const NOEXCEPT 
		{ 
			return getRandom(mReactionTime); 
		}

	private:
		NODISCARD inline const Ogre::Real 
			getRandom(const RealProperty& prop) const NOEXCEPT
		{
			return RNG(prop.first, prop.second);
		}

		NODISCARD inline const TimerDurationType 
			getRandom(const TimeProperty& prop) const NOEXCEPT
		{
			// implicit conversion to float to properly interpolate between the
			// two extremes
			const Ogre::Real low = static_cast<Ogre::Real>(prop.first.count());
			const Ogre::Real high = static_cast<Ogre::Real>(prop.second.count());
			const Ogre::Real random = RNG(low, high);

			return TimerDurationType(static_cast<TimerDurationType::rep>(random));
		}

		const Difficulty mDifficulty;
		const RealProperty mSpeed;
		const RealProperty mIdleSpeed;
		const RealProperty mHitOffset;
		const TimeProperty mReactionTime;
	};

	template <typename Ty1, typename Ty2>
	static /*constexpr*/ std::pair<
		typename estd::decay_unwrap<Ty1>::type, 
		typename estd::decay_unwrap<Ty2>::type
	> 
	NODISCARD make_property(Ty1&& low, Ty2&& high)
	{
		return std::make_pair(
			std::forward<Ty1>(low), 
			std::forward<Ty2>(high));
	}

	static const Settings msEasySettings;
	static const Settings msMediumSettings;
	static const Settings msHardSettings;

	enum class Phase
	{
		MOVING_TO_HIT_BALL,
		WAITING_FOR_BALL,
		MOVING_TO_CENTRE,
		IDLE_MOVING_TO_RANDOM,
		IDLING
	};

	// moves the hitPoint slightly to the left or right (based on the 
	// difficulty and a random number generator)
	void adjustHitPoint() NOEXCEPT;
	void setPhase(Phase phase, bool delayReaction = false) NOEXCEPT;
	template <typename DurationType = TimerDurationType>
	void idleFor(const DurationType& duration);
	void updateIdleValues() NOEXCEPT;
	void updatePlayValues() NOEXCEPT;
	NODISCARD const Settings* getSettings(const Difficulty difficulty) NOEXCEPT;
	void changeDifficulty(const Difficulty difficulty) NOEXCEPT 
	{ 
		mSettings = getSettings(difficulty); 
	}

	Phase mCurrentPhase;
	const Settings* mSettings;
	eOgre::CountdownTimer mLogicTimer;
	Ogre::Vector3 mMoveToPosition;
	Ogre::Vector3 mHitPoint;
	const Ogre::Real mMaxLeft; // available space on the left of the paddle
	const Ogre::Real mMaxRight; // available space on the right of the paddle

protected:
	// CollisionManager::Listener
	void collisionOccured(Collidable* one, Collidable* two) override;

	// Config::Listener
	void propertyChanged(const Config::Property& prop) override;
};

class NODISCARD AIFactory: public ActorFactory
{
public:
	AIFactory() {}
	~AIFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc) override;

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{
		return ACTOR_AI_TYPE_NAME; 
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return ACTOR_AI_TYPE_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif
