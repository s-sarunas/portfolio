// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef CINEMATICSTATE_H
#define CINEMATICSTATE_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "AppState.h"

#define CINEMATIC_STATE_OVERLAY_NAME "Menus/Splash"
#define CINEMATIC_STATE_OVERLAY_MATERIAL_NAME "Menus/SplashLogo"

class CinematicState: public AppState
{
public:
	CinematicState(AppState::Listener* listener);
	~CinematicState();

	// AppState.h
	void process(const Ogre::Real deltaSeconds) override;
	void end(const AppResultType::value_type result = NULL) override;

	// InputHandler.h
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
    bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;

private:
	Ogre::Real increaseFadePercent(Ogre::Real amount) NOEXCEPT 
	{
		return mCurrentFadeAmount += amount;
	}

	Ogre::TextureUnitState* mTextureUnit;
	Ogre::Real mCurrentFadeAmount;

protected:
};

#include "HeaderSuffix.h"

#endif
