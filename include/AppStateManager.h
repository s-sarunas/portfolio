// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef APPSTATEMANAGER_H
#define APPSTATEMANAGER_H

#include "HeaderPrefix.h"

#include "AppNode.h"
#include "SimpleMenu.h"

#define CURSOR_OVERLAY_NAME "Menus/Cursor"
#define CURSOR_OVERLAY_ELEMENT_NAME "CursorImage"

#define APPSTATEMANAGER_BUTTON_KB_PAUSE SDLK_ESCAPE
#define APPSTATEMANAGER_BUTTON_JS_PAUSE SDL_CONTROLLER_BUTTON_START
#define APPSTATEMANAGER_BUTTON_JS_PAUSE_ALT SDL_CONTROLLER_BUTTON_BACK

// headers defining base classes
#include "InputHandler.h"

class AppStateManager: public InputHandler
{
public:
	AppStateManager(AppState::Listener* listener);
	~AppStateManager();

	bool update(const Ogre::Real deltaSeconds);
	void enterMenu();
	NODISCARD bool isActive() const NOEXCEPT;
	void _previousState();

	// InputHandler.h
    bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool textInput(const SDL_TextInputEvent& arg) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
    bool keyReleased(const SDL_KeyboardEvent& arg) override;
    bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;

	NODISCARD static AppStateManager* getSingletonPtr() NOEXCEPT;
	NODISCARD static AppStateManager& getSingleton();

private:
	void nextState(const AppResultType& result);

	void refreshCursor() NOEXCEPT;
	void showCursor();
	void hideCursor() NOEXCEPT;

	AppState::Listener* mListener;
	Ogre::Overlay* mCursor;
	Ogre::OverlayContainer* mCursorContainer;
	AppNodePtr mCurrentNode; 
	AppNodePtr mGameNode; // used to indicate that the game is running

	static AppStateManager* msSingleton;

protected:
};

#include "HeaderSuffix.h"

#endif