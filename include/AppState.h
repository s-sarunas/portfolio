// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef APPSTATE_H
#define APPSTATE_H

#include "HeaderPrefix.h"

#include "Config.h"

#include "boost/optional.hpp"

typedef boost::optional<size_t> AppResultType;

// using enums for basic callbacks to avoid strings
// other events are handled in AppStateManager itself
enum MenuEvent
{
	MENU_EVENT_STATE_PREVIOUS,
	MENU_EVENT_START_GAME,
	MENU_EVENT_RETURN_TO_MENU,
	MENU_EVENT_RETURN_TO_GAME,
	MENU_EVENT_QUIT
};

// headers defining base classes
#include "InputHandler.h"
#include "SimpleMenu.h"

class AppState: public InputHandler, public SimpleMenu::Listener
{
public:
	class Listener
	{
	public:
		virtual ~Listener() {}

		virtual void handleMenuEvents(const MenuEvent menuEvent, 
			const StringInterface* params) = 0;
		virtual void handleMenuEvents(Config::Property&& prop) = 0;
	};

	virtual ~AppState();

	virtual void process(const Ogre::Real deltaSeconds) {}
	NODISCARD const AppResultType& getResult() const NOEXCEPT { return mResult; }

	virtual void start();
	virtual void end(const AppResultType::value_type result);

	// InputHandler.h
    bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
	bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;

private:
	void show();
	void hide();

	Listener* mListener;
	std::unique_ptr<SimpleMenu> mSimpleMenu;
	Ogre::Overlay* mOverlay;
	AppResultType mResult;

protected:
	AppState(Listener* listener, const Ogre::String& menuName = Ogre::StringUtil::BLANK, 
		const Ogre::String& overlayName = Ogre::StringUtil::BLANK);

	void notifyListener(const MenuEvent menuEvent, const StringInterface* params = nullptr);
	void notifyListener(Config::Property&& prop);

	NODISCARD Ogre::Overlay* getOverlay() const NOEXCEPT { return mOverlay; }
	NODISCARD SimpleMenu* getSimpleMenu() const NOEXCEPT { return mSimpleMenu.get(); }
};

#include "HeaderSuffix.h"

#endif