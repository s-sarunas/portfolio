// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef GROUNDED_H
#define GROUNDED_H

#include "HeaderPrefix.h"

#include <functional>

#include "Ray.h"
#include "InAppDebug.h"

// headers defining base classes
#include "Actor.h"
#include "Mixin.h"

// One of the mixins that extends an Actor to allow for it to be moved vertically 
// down to the surface of the ground mesh to imitate gravity.

// Virtual inheritance is used throughout the Actor's hierarchy to allow mixins
// to be specified at the most-derived class, while allowing them to manipulate 
// the Actor's members and override its interface. 

// It's used only for learning purposes and should be avoided in such performance
// critical sections. All the handling (ctor, dtor, copy/move semantics) of a 
// virtual base falls on the most-derived class and carries speed and size 
// overhead.

class Grounded: virtual public Actor, public Mixin
{
public:
	static void groundActor(Actor* actor, bool rotate);

	template <typename RayOriginTy>
	static void groundActor(Actor* actor, const Ray<RayOriginTy>& ray, bool rotate)
	{
		if (rotate)
			actor->getSceneNode()->setFixedYawAxis(true, UNIT_VECTOR_UP);

		ground(actor, ray, rotate);

	}

	NODISCARD Ray<Actor>& getRay() NOEXCEPT { return mRay; } // non-const access

private:
	void init();

	template <typename RayOriginTy>
	static void ground(Actor* actor, const Ray<RayOriginTy>& ray, bool rotate)
	{
		Scene::getSingleton().for_each_actor<Terrain>(ACTOR_TERRAIN_TYPE_NAME, 
			[actor, &ray, rotate](Terrain* terrain) {
				Grounded::ground(actor, terrain, ray, rotate);
			}
		);
	}

	template <typename RayOriginTy>
	static void ground(Actor* actor, Terrain* terrain, const Ray<RayOriginTy>& ray, 
		bool rotate)
	{
		// find if the actor is in the AABB of the terrain
		if (terrain->getBoundingBox().contains(actor->getBoundingBox()))
		{
			// distance/face
			const std::pair<Ogre::Real, const Terrain::Face*> result = 
				terrain->cast(ray); 
			if (result.first != 0.0f)
			{
				Ogre::Vector3 groundingVector = result.first * ray.getDirection();
				Ogre::Node* node = actor->getNode();

				// if the actor is dynamic, add the grounding vector to the step
				DynamicActor* dynamic = dynamic_cast<DynamicActor*>(actor);
				if (dynamic)
					dynamic->addToStep(groundingVector);

				// translate the actor regardless
				node->translate(groundingVector);

				// rotation
				if (rotate)
				{
					// dot product returns the angle (the similarity in direction), 
					// cross product returns the rotation axis (a vector 
					// orthogonal to the two vectors)
					Ogre::Quaternion rotationBetween = 
						(node->getOrientation() * UNIT_VECTOR_UP).getRotationTo(
						result.second->normal);

					rotationBetween.normalise();
					node->setOrientation(rotationBetween * node->getOrientation());
				}
			}
		}
	}

	Ray<Actor> mRay;
	bool mRotate;

protected:
	Grounded(bool rotate);
	virtual ~Grounded();

	Grounded(const Grounded& rhs) = delete;
	Grounded(const Grounded& rhs, Actor* newActorParent);
	Grounded& operator=(const Grounded& rhs) = delete;
	Grounded(Grounded&& rhs) NOEXCEPT;
	Grounded& operator=(Grounded&& rhs) NOEXCEPT;

	void update(const Ogre::Real deltaSeconds);

	// Mixin.h
	NODISCARD const MixinFlagType getMixinType() const 
	{ 
		return MIXIN_GROUNDED_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif