// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef PAUSESTATE_H
#define PAUSESTATE_H

#include "HeaderPrefix.h"

// headers defining base classes
#include "AppState.h"

class PauseState: public AppState
{
public:
	PauseState(AppState::Listener* listener);
	~PauseState();

private:
protected:
	// SimpleMenu::Listener
	void buttonHit(const Button* button) override;
};

#include "HeaderSuffix.h"

#endif
