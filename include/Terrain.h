// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef TERRAIN_H
#define TERRAIN_H

#include "HeaderPrefix.h"

#include "GameMath.h"

// headers defining base classes
#include "StaticActor.h"
#include "Debuggable.h"

class Terrain: public StaticActor, public Debuggable
{
public:
	struct NODISCARD Face
	{
		Face(Ogre::Vector3 _v0, Ogre::Vector3 _v1, Ogre::Vector3 _v2) 
			: v0(_v0), v1(_v1), v2(_v2), normal(Math::calcNormal(_v0, _v1, _v2)) {}
		~Face() {}

		Ogre::Vector3 v0, v1, v2, normal;
	};

	Terrain(const ActorClass& desc, const Ogre::String& polysoupFilename);
	~Terrain();

	// Actor.h
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{
		return ACTOR_TERRAIN_TYPE_NAME; 
	}

	/*
		If the definition was in the translation unit, explicit instantiation 
		would have been necessary (in .cpp):
			template const Ogre::Real Terrain::cast(const Ray<Ogre::SceneNode>&) const;
			template const Ogre::Real Terrain::cast(const Ray<Ogre::Vector3>&) const;
	*/
	template <typename RayOriginType>
	NODISCARD const std::pair<Ogre::Real, const Face*> cast(const Ray<RayOriginType>& ray)
	{
		Ogre::Real distance = 0.0f;
		for (const auto& face : mFaces)
		{
			distance = ray.cast(face.v0, face.v1, face.v2);
		
			if (distance)
				return std::make_pair(distance, &face);	
		}

		return std::make_pair(0.0f, nullptr);
	}

private:
	Ogre::Entity* mPolysoup;
	std::vector<Face> mFaces;

protected:
	// Debuggable.h
	void initDebuggable() override;
	void showDebuggable() override;
	void hideDebuggable() override;
};

class NODISCARD TerrainFactory: public ActorFactory
{
public:
	TerrainFactory() {}
	~TerrainFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{ 
		return ACTOR_TERRAIN_TYPE_NAME;
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return NULL;
	}
};

#include "HeaderSuffix.h"

#endif