// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef CORE_H
#define CORE_H

#include "HeaderPrefix.h"

#include "InAppDebug.h"
#include "Config.h"

#include <thread>

// headers defining base classes
#include "OgreFrameListener.h"
#include "AppState.h"

extern void setSceneManager(Ogre::SceneManager* manager);
extern void setCamera(Ogre::Camera* camera);

#define CORE_PRIMARY_CAMERA_NAME "main_camera"
#define CORE_WINDOW_TITLE "OGRE Render Window"

#define CORE_LOG_FILENAME "3d_game.log"
#define CORE_CFG_FILENAME "3d_game.cfg"

#ifdef _DEBUG
#	define CORE_RESOURCES_FILENAME "resources_d.cfg"
#	define CORE_PLUGINS_FILENAME "plugins_d.cfg"
#else
#	define CORE_RESOURCES_FILENAME "resources.cfg"
#	define CORE_PLUGINS_FILENAME "plugins.cfg"
#endif

#define CORE_VIEWPORT_BACKGROUND_COLOR Ogre::ColourValue::Black
#define CORE_NEAR_CLIP_DISTANCE 0.1f
#define CORE_MAXDELTA_SECONDS 0.8f

class Core: public Ogre::FrameListener, public AppState::Listener
{
public:
	Core();
	~Core();

	void _go(std::vector<std::string> cmdArgs);
	void checkResourceLeaks();
	void resetDelta() NOEXCEPT;

	void _setDebugNumUpdates(const int numFrames) NOEXCEPT 
	{ 
		mNumUpdates = numFrames;
	}
	void setFpsLimit(const Ogre::Real newLimit) NOEXCEPT;

	NODISCARD AppStateManager* getAppStateManager() const NOEXCEPT
	{
		return mAppStateManager.get();
	}

	NODISCARD Ogre::RenderWindow* getWindow() const NOEXCEPT
	{
		return mWindow;
	}

	NODISCARD static Core* getSingletonPtr() NOEXCEPT;
	NODISCARD static Core& getSingleton();

private:
	void updateOnce(const Ogre::Real deltaSeconds);
	void createScene(const Ogre::String& sceneFilename) NOEXCEPT;
	void destroyScene();

	// setup functions
	bool setup();
	void createCamera();
	void chooseSceneManager();
	void createListeners();
	void createViewport();
	void initialiseSDL();
	void setupResources();
	void loadResources();
	bool configure();
	void processCmdArgs(const std::vector<std::string>& cmdArguments);
	
	// SDL functions
	void messagePump();
	void handleWindowEvents(const SDL_WindowEvent& evt);
	void handleInputEvents(const SDL_Event& evt);

	// Ogre essentials
	// Ogre::LogManager is first in the list, because it has to be destoyed last, 
	// after all log messages are recorded in Ogre::Root
	std::unique_ptr<Ogre::LogManager> mLogManager; 
	std::unique_ptr<Ogre::Root> mRoot;
	std::unique_ptr<Ogre::OverlaySystem> mOverlaySystem;
	Ogre::Timer mLoopTimer;
	Ogre::Viewport* mViewport;
	Ogre::RenderWindow* mWindow;

	// SDL essentials
	SDL_Window* mSDLWindow; // non-owning raw pointer
	JoystickList mJoysticks; // a vector of non-owning raw pointers

	// helper variables
	Ogre::Real mFpsLimit;
	Ogre::Real mFrameTargetMicroseconds;
	int mNumUpdates;
	bool mWindowHasFocus;
	bool mUnloadResources;
	bool mShowConfig;
	bool mUpdatePaused;
	bool mQuitPending;

	// custom interfaces
	InAppDebug mInAppDebug;

	// on the heap, since it gets explicitly constructed and destructed 
	// when the player enters and leaves the scene
	std::unique_ptr<Scene> mScene;

	// on the heap, since it has to be created after the window is initialised
	std::unique_ptr<AppStateManager> mAppStateManager; 

	static Core* msSingleton;

protected:
	// Ogre::FrameListener - Ogre internal
	bool frameRenderingQueued(const Ogre::FrameEvent& evt) override;
	bool frameStarted(const Ogre::FrameEvent& evt) override;
	bool frameEnded(const Ogre::FrameEvent& evt) override;

	// AppState::Listener
	void handleMenuEvents(const MenuEvent menuEvent, const StringInterface* params) override;
	void handleMenuEvents(Config::Property&& prop) override;
};

#include "HeaderSuffix.h"

#endif