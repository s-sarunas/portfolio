// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef PADDLE_H
#define PADDLE_H

#include "HeaderPrefix.h"

#include "Character.h"
#include "AbilitiesManager.h"

// headers defining base classes
#include "InputHandler.h"
#include "DynamicActor.h"
#include "Animated.h"
#include "CollisionManager.h"
#include "Grounded.h"
#include "Debuggable.h"
#include "Config.h"

#define PADDLE_SPEED_STARTING 65.0f
#define PADDLE_SPEED_INCREASE_MULTIPLIER 0.25f
#define PADDLE_MOUSE_SENSITIVITY 150.0f
#define PADDLE_MOUSE_RESOLUTION 512.0f
// left-right axis movement of the left joystick
#define PADDLE_JS_MOVEMENT_AXIS 0

#define PADDLE_ANIMATION_JUMP_NAME "jumpFromWall"
#define PADDLE_BUTTON_KB_LEFT SDLK_j
#define PADDLE_BUTTON_KB_RIGHT SDLK_k
#define PADDLE_BUTTON_KB_ABILITY_DASH SDLK_q
#define PADDLE_BUTTON_KB_ABILITY_SHOOT SDLK_e

#define PADDLE_BUTTON_JS_ABILITY_DASH SDL_CONTROLLER_BUTTON_A
#define PADDLE_BUTTON_JS_ABILITY_SHOOT SDL_CONTROLLER_BUTTON_X

class Paddle: public DynamicActor, public Grounded, public Animated<1>, 
	public Debuggable, public InputHandler, public CollisionManager::Listener, 
	public Config::Listener
{
public:
	typedef int score_type;

	Paddle(const ActorClass& desc);
	virtual ~Paddle(); // can be a standalone or a base class for AI

	Paddle(const Paddle& rhs) = delete;
	Paddle& operator=(const Paddle&) = delete;
	Paddle(Paddle&& rhs);
	Paddle& operator=(Paddle&& rhs);

	// Actor.h
	void update(const Ogre::Real deltaSeconds) override;
	void reset() override;
	NODISCARD const Ogre::String& getActorType() const NOEXCEPT override 
	{ 
		return ACTOR_PLAYER_TYPE_NAME;
	}

	// Collidable.h
	void handleCollision(Collidable* collidable, bool isColliding) override;

	// InputHandler.h
    bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool textInput(const SDL_TextInputEvent& arg) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
    bool keyReleased(const SDL_KeyboardEvent& arg) override;
    bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;

	NODISCARD virtual bool isAI() NOEXCEPT { return false; }

	NODISCARD const Ogre::Real getLength() const NOEXCEPT { return mLength; }
	NODISCARD const score_type getScore() const NOEXCEPT { return mScore; }
	NODISCARD Character* getCharacter() const NOEXCEPT { return mCharacter; }
	NODISCARD Region* getRegion() const NOEXCEPT { return mRegion; }
	NODISCARD Scoreboard* getScoreboard() const NOEXCEPT { return mScoreboard; }

	void setScore(score_type score);
	void setJoystick(SDL_Joystick* joystick) NOEXCEPT { mJoystick = joystick; }
	void increaseScore(score_type points = 1) { setScore(mScore + points); }
	void decreaseScore(score_type points = 1) { setScore(mScore - points); }

private:
	typedef uint8_t MovementFlag;
	enum MovementFlags : MovementFlag
	{
		PADDLE_MOVE_LEFT =		1 << 0,
		PADDLE_MOVE_RIGHT =		1 << 1,
		PADDLE_MOVE_FORWARD =	1 << 2,
		PADDLE_MOVE_BACKWARD =	1 << 3,
	};

	// DynamicActor.h
	void translate(const Ogre::Vector3& vector) override;

	std::vector<Ogre::Vector2> mMouseMotion;
	SDL_Joystick* mJoystick;
	BitSet<MovementFlag> mMoveFlags;
	score_type mScore;
	Ogre::Real mLength;

	// raw pointer access to child actors
	Character* mCharacter;
	Region* mRegion;
	Scoreboard* mScoreboard;

	// interfaces
	AbilitiesManager mAbilitiesManager;

protected:
	NODISCARD AbilitiesManager& getAbilitiesManager() NOEXCEPT 
	{
		return mAbilitiesManager;
	}

	// CollisionManager::Listener
	void collisionOccured(Collidable* one, Collidable* two) override;

	// Config::Listener
	void propertyChanged(const Config::Property& prop) override;

	// Debuggable.h
	void setDebuggableVisibility(bool flag) override;
};

class NODISCARD PaddleFactory: public ActorFactory
{
public:
	PaddleFactory() {}
	~PaddleFactory() {}

	NODISCARD std::shared_ptr<Actor> createInstance(const ActorClass& desc);

	NODISCARD const Ogre::String& getType() const NOEXCEPT override 
	{
		return ACTOR_PLAYER_TYPE_NAME; 
	}
	NODISCARD const ActorFlagType getTypeFlag() const NOEXCEPT override 
	{ 
		return ACTOR_PLAYER_TYPE_FLAG; 
	}
};

#include "HeaderSuffix.h"

#endif