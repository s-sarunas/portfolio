// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef SCENE_H
#define SCENE_H

#include "HeaderPrefix.h"

#include "InAppDebug.h"
#include "DotSceneLoader.h"
#include "Scoreboard.h"
#include "Border.h"
#include "GameMath.h"

#include <type_traits>

extern void setGameMode(const GameModes mode);

// headers defining base classes
#include "InputHandler.h"
#include "CollisionManager.h"

#define SCENE_CHALLENGE_BORDER_MESH_FILENAME "border_wall.mesh"

#define SCENE_LIGHTS_MAX 4
#define SCENE_UNORDERED_MAP_MAX_LOAD_FACTOR 0.7f

class Scene: public ActorCreator, public InputHandler, 
	public CollisionManager::Listener
{
public:
	Scene(const Ogre::String& sceneFilename, const GameModes gameMode, 
		const JoystickList& joystickList, bool unloadResources);
	~Scene();

	void update(const Ogre::Real deltaSeconds);
	void reset();

	void addActor(std::shared_ptr<Actor> actor);
	// use Actor::setDestroyFlag(true) to destroy the actor at the next update cycle
	void destroyActor(Actor* actor);
	void destroyActorType(const Ogre::String& type);

	NODISCARD const ActorCollectionMap& getActorCollection() const NOEXCEPT 
	{ 
		return mActorCollection;
	}
	NODISCARD const ActorPtrList* getActorList(const Ogre::String& type) const;
	NODISCARD ActorPtrList* getActorList(const Ogre::String& type);
	NODISCARD const ActorPtrList::size_type getNumActors(const Ogre::String& type) const;

	NODISCARD Actor* getActor(const Ogre::String& name, const Ogre::String& type) const;
	// consider using the overload with actor type as second argument for 
	// better performance
	NODISCARD Actor* getActor(const Ogre::String& name) const;

	template <typename Pred>
	NODISCARD Actor* getActorIf(const Pred pred) const NOEXCEPT_COND(
		std::is_nothrow_invocable<Pred, const Actor*>::value) // C++17 only
	{
		for (const auto& actorList : mActorCollection)
		{
			for (const auto& actor : actorList.second)
			{
				Actor* actr = actor.get();
				if (pred(const_cast<const Actor*>(actr)))
				{
					return actr;
				}
			}
		}

		return nullptr;
	}

	NODISCARD Paddle* getPlayer() const { assert(mPlayer); return mPlayer; }

	// create functions for independent, scene-managed actors
	Butterfly* createButterfly(ActorClass& desc, 
		const Ogre::String& pathFilename, const Ogre::String& animationName);
	Paddle* createPaddle(ActorClass& desc, 
		bool isAIControlled, const Ogre::String& characterMeshFilename);
	Character* createCharacter(ActorClass& desc, const Ogre::String& weaponFilename);
	Ball* createBall(ActorClass& desc);
	Border* createBorder(ActorClass& desc, const Ogre::String& type);
	Region* createRegion(ActorClass& desc);
	Bullet* createBullet(ActorClass& desc, const Ogre::Vector3& direction);
	Terrain* createTerrain(ActorClass& desc, const Ogre::String& polysoupFilename);
	Scoreboard* createScoreboard(ActorClass& desc);

	void addActorFactory(std::unique_ptr<ActorFactory>&& factory, bool overwrite = false);
	void removeActorFactory(ActorFactory* factory);
	NODISCARD const bool hasActorFactory(ActorFactory* factory);
	NODISCARD ActorFactory* getActorFactory(const Ogre::String& type);

	NODISCARD ActorFlagType allocateNextActorTypeFlag() NOEXCEPT;

	void assignInput(SDL_Joystick* joystick) NOEXCEPT;
	void unassignInput(SDL_Joystick* joystick) NOEXCEPT;

	NODISCARD const Ogre::SceneNode* getRootSceneNode() const NOEXCEPT 
	{
		return mRootSceneNode;
	}
	NODISCARD DotSceneLoader* getLoader() NOEXCEPT 
	{
		return mLoader.get(); 
	}
	NODISCARD CollisionManager& getCollisionManager() NOEXCEPT 
	{
		return mCollisionManager; 
	}

	// InputHandler.h
    bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool textInput(const SDL_TextInputEvent& arg) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
    bool keyReleased(const SDL_KeyboardEvent& arg) override;
    bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;

	/*
		Helper functions
	*/

	// unsafe version - do not erase actors inside the function
	// used for functions returning void
	// does not cascade to the actors' children (handle it in the parent)
	template <typename Func>
	typename std::enable_if<
		std::is_same<typename std::result_of<Func(Actor*)>::type, void>::value
	>::type
	for_each_actor(Func function) NOEXCEPT_COND(
		std::is_nothrow_invocable<Func, Actor*>::value) // C++17 only
	{
		for (const auto& actorList : mActorCollection)
		{
			for (const auto& actor : actorList.second)
			{
				function(actor.get());
			}
		}
	}

	// safe version - return false if the actor was destroyed, true otherwise
	// used for functions returning bool
	// does not cascade to the actors' children (handle it in the parent)
	template <typename Func>
	typename std::enable_if<
		std::is_same<typename std::result_of<Func(Actor*)>::type, bool>::value
	>::type
	for_each_actor(Func function) 
	{
		for (const auto& actorList : mActorCollection)
		{
			for (ActorPtrList::size_type i = 0; i < actorList.second.size();)
			{
				Actor* actor = actorList.second[i].get();
				if (function(actor)) 
					++i; // advance only if the actor was not destroyed
			}
		}
	}

	// unsafe version - do not erase actors inside the function
	// loops through a specific type of actor, downcasting the pointer to the 
	// type provided does not cascade to the actors' children (handle it 
	// in the parent)
	template <typename Type, typename Func>
	void for_each_actor(const Ogre::String& type, Func function)
	{
		static_assert(is_actor_type<Type>::value, 
			"invalid actor type supplied");

		ActorPtrList* actorList = getActorList(type);
		if (actorList)
		{
			for (const auto& actor : (*actorList))
			{
				Type* typeActor = dynamic_cast<Type*>(actor.get());
				if (typeActor)
				{
					function(typeActor);
				}
				else
				{
					OGRE_EXCEPT(Ogre::Exception::ERR_INVALID_STATE, "Actor \"" + 
						actor->getName() + "\" is not of type \"" + 
						typeid(Type).name() + "\".", "Scene::for_each_actor");
				}
			}
		}
		else
		{
			DEBUG_OUTPUT(LVL_YELLOW, "No actor list exists for type \"" + type +
				'\"', true);
		}
	}

	// unsafe version - do not erase actors inside the function
	// loops through all the scene's actors, downcasting the pointer to the type provided
	// does not cascade to the actors' children (handle it in the parent)
	template <typename Type, typename Func>
	void for_each_actor(Func function) NOEXCEPT_COND(
		std::is_nothrow_invocable<Func, Type*>::value) // C++17 only
	{
		static_assert(is_actor_type<Type>::value,
			"invalid actor type supplied");

		for (const auto& actorList : mActorCollection)
		{
			for (const auto& actor : actorList.second)
			{
				Type* typeActor = dynamic_cast<Type*>(actor.get());
				if (typeActor)
					function(typeActor);
			}
		}
	}

	NODISCARD static Scene* getSingletonPtr() NOEXCEPT;
	NODISCARD static Scene& getSingleton();

private:
	typedef std::unordered_map<Ogre::String, std::unique_ptr<ActorFactory>> 
		ActorFactoryMap;

	NODISCARD const Ogre::String parseFilename(const Ogre::String& filename);
	void unloadAllUsedResources();

	// custom interfaces

	// on the heap, since the loader uses the scene's methods to create actors
	std::unique_ptr<DotSceneLoader> mLoader;
	CollisionManager mCollisionManager;

	ActorCollectionMap mActorCollection;
	ActorFactoryMap mActorFactoryMap;
	Paddle* mPlayer; // only a single human player is supported
	/* 
		Create an instance of the RNG, binds to Ogre on construction and
		guarantees the point of definition (as opposed to a static in the header)
	*/
	Math::RandomNumberGenerator mRandomNumberGenerator;
    Ogre::SceneNode* mRootSceneNode;
	ActorFlagType mNextActorTypeFlag;
	bool mUnload;

	static Scene* msSingleton;

protected:
	// CollisionManager::Listener
	void collisionOccured(Collidable* one, Collidable* two) override;

	// ActorCreator.h
	Actor* createActor(const ActorClass& desc) override;
	NODISCARD std::shared_ptr<Actor> createUnmanagedActor(const ActorClass& desc) override;
	void reserveForActors(const size_t numActors) override;
};

#include "HeaderSuffix.h"

#endif