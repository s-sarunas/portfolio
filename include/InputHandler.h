// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

class InputHandler
{
public:
	InputHandler() {}
	virtual ~InputHandler() {}

	// bool return type indicates whether the event should be propagated forward

	// mouse events
    virtual bool mouseMoved(const SDL_MouseMotionEvent& arg) { return true; }
    virtual bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) { return true; }
    virtual bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) { return true; }

	// keyboard events
    virtual bool textInput(const SDL_TextInputEvent& arg) { return true; }
    virtual bool keyPressed(const SDL_KeyboardEvent& arg) { return true; }
    virtual bool keyReleased(const SDL_KeyboardEvent& arg) { return true; }

	// joystick events
    virtual bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) { return true; }
    virtual bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) { return true; }
    virtual bool joyAxisMoved(const SDL_JoyAxisEvent& arg, int axis) { return true; }
    virtual bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) { return true; }
};

#endif
