// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef APPNODE_H
#define APPNODE_H

#include "HeaderPrefix.h"

#include "AppState.h"

typedef std::shared_ptr<AppNode> AppNodePtr;

// headers defining base classes
#include "InputHandler.h"

/*
	A tree structure with dynamically allocated branches. Used by AppStateManager
	to navigate between states, e.g. MenuState, PauseState etc. that manage 
	instances of SimpleMenus and provide callbacks
*/
class AppNode: public InputHandler
{
	typedef std::shared_ptr<AppState> AppStatePtr;
public:
	AppNode() : mIsPopup(false) {}
	AppNode(AppStatePtr appState, bool isPopup = false) 
		: mState(std::move(appState)), mIsPopup(isPopup) {}
	virtual ~AppNode() {}

	void update(const Ogre::Real deltaSeconds) 
	{
		mState->process(deltaSeconds);
	}
	NODISCARD const AppResultType& getResult() const NOEXCEPT
	{ 
		return mState->getResult(); 
	}

	// assumes that the state completed successfully with a result
	NODISCARD AppNodePtr next(const AppResultType& result, const AppNodePtr& prev)
	{
		const AppResultType::value_type index = result.get();
		assert(index < mNext.size() && 
			"not enough next states supplied");

		AppNodePtr n = mNext[index];

		// reassign popup's states
		if (n->mIsPopup)
		{
			n->mPrev = prev;
			n->mNext = {prev};
		}

		n->start();

		return n;
	}

	// assumes that the state was interrupted in favour of another state
	NODISCARD AppNodePtr previous()
	{
		end();
		mPrev->start();

		return mPrev;
	}

	NODISCARD bool hasNext() const NOEXCEPT 
	{ 
		return !mNext.empty(); 
	}
	NODISCARD bool hasPrevious() const NOEXCEPT 
	{
		return mPrev.operator bool();
	}

	void setNext(const std::initializer_list<AppNodePtr> next) NOEXCEPT
	{ 
		mNext = next; 
	}
	void setNext(const AppNodePtr& next)
	{
		assert(mNext.empty() && 
			"next nodes already set");
		assert(!mIsPopup && 
			"popups cannot have predefined states");

		mNext.push_back(next);
	}

	void setPrevious(const AppNodePtr& prev) 
	{ 
		assert(!mPrev && 
			"previous node already set");
		assert(!mIsPopup && 
			"popups cannot have predefined states");

		mPrev = prev; 
	}

	// InputHandler.h
	bool mouseMoved(const SDL_MouseMotionEvent& arg) override 
	{ 
		mState->mouseMoved(arg); 
		return true; 
	}

    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override 
	{ 
		mState->mousePressed(arg, id); 
		return true;
	}

    bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override 
	{ 
		mState->mouseReleased(arg, id); 
		return true; 
	}

    bool keyPressed(const SDL_KeyboardEvent& arg) override 
	{ 
		mState->keyPressed(arg); 
		return true;
	}

	bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override 
	{
		mState->joyButtonPressed(evt, button); 
		return true;
	}

    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override 
	{
		mState->joyButtonReleased(evt, button);
		return true;
	}

    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override 
	{ 
		mState->joyPovMoved(arg, index);
		return true;
	}

private:
	void start()
	{ 
		if (mState) 
			mState->start(); 
	}
	void end() 
	{ 
		if (mState)
			mState->end(NULL);
	}

	AppStatePtr mState;

	std::vector<AppNodePtr> mNext;
	AppNodePtr mPrev;
	const bool mIsPopup;

protected:
};

#include "HeaderSuffix.h"

#endif