// This source file is part of the portfolio at http://www.sarunas.me.uk/
// Copyright (c) 2009-2019, Sarunas Staranevicius
// All rights reserved.

#ifndef SIMPLEMENU_H
#define SIMPLEMENU_H

#include "HeaderPrefix.h"

#define SIMPLEMENU_TRAY_SUFFIX "/ButtonTray"
#define SIMPLEMENU_TEMPLATE_NAME "Menus/Tray"
#define SIMPLEMENU_TYPE_NAME "BorderPanel"
#define SIMPLEMENU_INSTANCE_SUFFIX "/ButtonContainer"

// headers defining base classes
#include "InputHandler.h"

class SimpleMenu: public InputHandler
{
public:
	class Listener
	{
	public:
		virtual ~Listener() {}

		virtual void itemSelected(const Dropdown* dropdown) {}
		virtual void buttonHit(const Button* button) {}
	};

	void setListener(Listener* listener) NOEXCEPT { mListener = listener; }

	SimpleMenu(const Ogre::String& name);
	~SimpleMenu();

	void createButtons(const std::initializer_list<Ogre::String> names, 
		const std::initializer_list<Ogre::DisplayString> captions);
	void createButton(const Ogre::String& name, const Ogre::DisplayString& caption);
	void createDropdown(const Ogre::String& name, const Ogre::DisplayString& caption, 
		const std::initializer_list<Ogre::DisplayString> items, const size_t itemIndex);

	void reset();

	void show() { mTray->show(); reset(); }
	void hide() NOEXCEPT { mTray->hide(); }

	NODISCARD Widget* getActiveWidget() const NOEXCEPT { return mActiveWidget; }

	// InputHandler.h
    bool mouseMoved(const SDL_MouseMotionEvent& arg) override;
    bool mousePressed(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool mouseReleased(const SDL_MouseButtonEvent& arg, Ogre::uint8 id) override;
    bool keyPressed(const SDL_KeyboardEvent& arg) override;
	bool joyButtonPressed(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyButtonReleased(const SDL_JoyButtonEvent& evt, int button) override;
    bool joyPovMoved(const SDL_JoyHatEvent& arg, int index) override;
	
private:
	typedef std::vector<std::unique_ptr<Widget>> WidgetList;

	void resizeTray();
	NODISCARD inline const Ogre::Real getNextTop() const;

	Listener* mListener;
	WidgetList mWidgets;
	Widget* mActiveWidget;
	Ogre::String mName;
	Ogre::OverlayManager* mOverlayManager;
	Ogre::Overlay* mTray;
	Ogre::OverlayContainer* mTrayContainer;
	bool mJoystickActive;
	bool mNeedsResize;

protected:
};

#include "HeaderSuffix.h"

#endif